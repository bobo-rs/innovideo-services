package middleware

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/framework/service"
	"gitee.com/bobo-rs/innovideo-services/library/exception"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/net/ghttp"
)

// 销毁登录缓存用户信息-用于修改用户资料之后，更新缓存信息
func (m *sMiddleware) RemoveLoginCache(r *ghttp.Request) {
	r.Middleware.Next()
	if r.GetError() != nil {
		return
	}

	// 操作成功-销毁登陆用户信息（修改用户信息、头像、性别、生日等）
	item := service.BizCtx().GetUser(r.Context())
	if item != nil {
		_ = service.User().RemoveLoginCache(r.Context(), item.Uid, item.AccountToken)
	}
}

// RemoveAdminAuthTokenCache 操作管理员状态，移除管理员缓存Token
func (m *sMiddleware) RemoveAdminAuthTokenCache(r *ghttp.Request) {
	// 中间件执行
	r.Middleware.Next()

	// 操作失败则返回
	if r.GetError() != nil {
		return
	}

	// 获取管理员ID
	var adminId uint
	switch r.Method {
	case `GET`:
		adminId = r.Get(`id`).Uint()
	default:
		adminId = r.GetForm(`id`).Uint()
	}

	// 管理员ID
	if adminId == 0 {
		return
	}

	// 移除缓存
	err := m.removeAdminAuthTokenHandler(r.Context(), adminId)
	if err != nil {
		g.Log().Debug(r.Context(), `移除管理员登录授权缓存`, err)
	}
}

// RefreshSysMap 刷新系统字典值中间件
func (m *sMiddleware) RefreshSysMap(r *ghttp.Request) {
	// 后置执行
	r.Middleware.Next()
	if r.GetError() != nil {
		return
	}

	// 刷新MAP[字典值：添加、编辑、删除、更新]
	_, _ = service.SysMap().GetMapAll(r.Context(), true)
}

// removeAdminAuthTokenHandler 移除管理员授权登录Token处理
func (m *sMiddleware) removeAdminAuthTokenHandler(ctx context.Context, adminId ...uint) error {

	// 获取用户ID
	uid, err := service.Rbac().GetUidByAid(ctx, adminId...)
	if err != nil {
		return err
	}

	// 获取管理员Token
	tokenData, err := service.User().GetLoginUserTokenByUid(ctx, uid...)
	if err != nil {
		return err
	}

	// 是否为空
	if len(tokenData) == 0 {
		return exception.New(`当前操作管理员皆已下线`)
	}
	// 移除登录token
	for _, item := range tokenData {
		_ = service.User().RemoveLoginCache(ctx, item.UserId, item.Token)
	}
	return nil
}
