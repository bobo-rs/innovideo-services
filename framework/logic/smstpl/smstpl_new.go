package smstpl

import "gitee.com/bobo-rs/innovideo-services/framework/service"

type sSmsTpl struct {
}

func init() {
	service.RegisterSmsTpl(New())
}

func New() *sSmsTpl {
	return &sSmsTpl{}
}
