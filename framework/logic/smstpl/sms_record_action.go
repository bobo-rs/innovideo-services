package smstpl

import (
	"context"
	"fmt"
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/framework/model/entity"
	"github.com/gogf/gf/v2/errors/gerror"
)

// SaveRecord 保存短信记录
func (t *sSmsTpl) SaveRecord(ctx context.Context, records ...entity.SmsRecord) error {
	if len(records) == 0 {
		return gerror.New(`缺少短信记录`)
	}
	_, err := dao.SmsRecord.Ctx(ctx).Save(records)
	return err
}

// GetRecordList 获取短信发送记录
func (t *sSmsTpl) GetRecordList(ctx context.Context, in model.SmsRecordListInput) (out *model.SmsRecordListOutput, err error) {
	out = &model.SmsRecordListOutput{}
	// 获取数据
	err = t.RecordModel().ListAndTotal(ctx, model.CommonListAndTotalInput{
		Where:                t.RecordWhere(in.SmsRecordWhereItem),
		CommonPaginationItem: in.CommonPaginationItem,
		Sort:                 fmt.Sprintf(`%s desc`, dao.SmsRecord.Columns().Id),
	}, &out.Rows, &out.Total)
	if err != nil {
		return nil, err
	}
	// 迭代处理数据
	for key, row := range out.Rows {
		t.FormatRecord(row)
		out.Rows[key] = row
	}
	return
}
