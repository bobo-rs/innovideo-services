package article

import (
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/logic/base"
)

// ArticleModel 文章Model
func (a *sArticle) ArticleModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.Article.Table(),
	}
}

// CategoryModel 文章分类Model
func (a *sArticle) CategoryModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.ArticleCategory.Table(),
	}
}
