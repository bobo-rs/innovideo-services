package article

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/library/exception"
	"github.com/gogf/gf/v2/frame/g"
)

// ProcessDetailById 通过文章ID获取文章详情
func (a *sArticle) ProcessDetailById(ctx context.Context, id uint, pointer interface{}) error {
	if id == 0 {
		return exception.New(`缺少文章ID`)
	}
	return a.ArticleModel().Scan(ctx, g.Map{
		dao.Article.Columns().Id: id,
	}, pointer)
}

// CheckExistsById 通过文章ID检测文章是否存在
func (a *sArticle) CheckExistsById(ctx context.Context, id uint) bool {
	b, _ := a.ArticleModel().Exists(ctx, g.Map{
		dao.Article.Columns().Id: id,
	})
	return b
}
