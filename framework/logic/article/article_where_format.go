package article

import (
	"gitee.com/bobo-rs/creative-framework/pkg/utils"
	"gitee.com/bobo-rs/innovideo-services/consts"
	"gitee.com/bobo-rs/innovideo-services/enums"
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
)

// ArticleWhere 处理并获取文章条件
func (a *sArticle) ArticleWhere(in model.ArticleWhereItem) map[string]interface{} {
	where := make(map[string]interface{})
	// 文章名
	if in.Name != "" {
		where[dao.Article.Columns().Name+consts.OrmWhereLike] = utils.OrmWhereLike(in.Name)
	}
	// 状态
	if enums.ArticleStatus(in.Status).Fmt() != "" {
		where[dao.Article.Columns().Status] = in.Status
	}
	// 是否显示
	if enums.ArticleShow(in.IsShow).Fmt() != "" {
		where[dao.Article.Columns().IsShow] = in.IsShow
	}
	// 类目
	if in.AssocAlias != "" {
		where[dao.Article.Columns().AssocAlias] = in.AssocAlias
	}
	return where
}

// FormatArticle 格式化文章详情状态
func (a *sArticle) FormatArticle(detail *model.ArticleDetailFmtItem) {
	detail.FmtStatus = enums.ArticleStatus(detail.Status).Fmt()
	detail.FmtIsShow = enums.ArticleShow(detail.IsShow).Fmt()
}
