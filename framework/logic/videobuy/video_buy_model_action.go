package videobuy

import (
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/logic/base"
)

// BuyModel 视频购买Model
func (b *sVideoBuy) BuyModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.VideoBuy.Table(),
	}
}

// BillModel 视频购买账单明细Model
func (b *sVideoBuy) BillModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.VideoBuyBill.Table(),
	}
}

// EpisodesModel 视频购买续集Model
func (b *sVideoBuy) EpisodesModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.VideoBuyEpisodes.Table(),
	}
}
