package videobuy

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/library/tools"
	"github.com/gogf/gf/v2/frame/g"
)

// VideoBuyWhere 视频购买查询条件
func (b *sVideoBuy) VideoBuyWhere(ctx context.Context, item model.VideoBuyWhereItem) map[string]interface{} {
	var (
		columns = dao.VideoBuy.Columns()
		where   = g.Map{}
	)

	// 用户ID
	if item.Uid > 0 {
		where[columns.Uid] = item.Uid
	}

	// 视频ID
	if item.VideoId > 0 {
		where[columns.VideoId] = item.VideoId
	}

	// 状态
	if item.Status.Is() {
		where[columns.Status] = item.Status
	}

	// 编号
	if item.BuyNo != "" {
		if buyNo := tools.ExtractValidCodes(item.BuyNo); len(buyNo) > 0 {
			where[columns.BuyNo] = buyNo
		} else {
			where[columns.BuyNo] = ``
		}
	}

	if item.VideoName != "" {
		where[columns.VideoName] = item.VideoName
	}
	return where
}
