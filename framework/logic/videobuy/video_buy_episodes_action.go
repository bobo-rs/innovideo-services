package videobuy

import (
	"context"
	"gitee.com/bobo-rs/creative-framework/pkg/utils"
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/framework/model/entity"
	"gitee.com/bobo-rs/innovideo-services/framework/service"
	"gitee.com/bobo-rs/innovideo-services/library/exception"
	"github.com/gogf/gf/v2/frame/g"
)

// BuyEpisodesAuthResult 授权用户购买视频续集结果
func (b *sVideoBuy) BuyEpisodesAuthResult(ctx context.Context, videoId uint) map[uint]bool {
	var (
		list      []model.VideoBuyEpisodesAuthItem
		buyResult = make(map[uint]bool)
	)

	// 获取已购买集数
	err := b.EpisodesModel().Scan(ctx, g.Map{
		dao.VideoBuyEpisodes.Columns().VideoId: videoId,
		dao.VideoBuyEpisodes.Columns().Uid:     service.BizCtx().GetUid(ctx),
	}, &list)
	if err != nil {
		return nil
	}

	// 组装购买视频续集结果
	for _, item := range list {
		buyResult[item.EpisodesId] = true
	}
	return buyResult
}

// GetBuyEpisodesList 获取购买剧集列表
func (b *sVideoBuy) GetBuyEpisodesList(ctx context.Context, where model.VideoBuyEpisodesWhereItem) (rows []model.VideoBuyEpisodesItem, err error) {
	// 获取数据
	err = dao.VideoBuyEpisodes.Ctx(ctx).
		OmitEmpty().
		Where(where).
		OrderAsc(dao.VideoBuyEpisodes.Columns().EpisodesNum).
		Scan(&rows)
	return
}

// GetBuyEpisodes 生成视频购买剧集数据
func (b *sVideoBuy) GenBuyEpisodes(ctx context.Context, in model.GenBuyEpisodesInput) error {
	// 生成数据
	_, err := dao.VideoBuyEpisodes.Ctx(ctx).Save(b.processGenBuyEpisodes(in))
	if err != nil {
		return exception.Newf(`生成购买剧集失败 %s`, err.Error())
	}
	return nil
}

// GetBuyEpisodesIdSet 获取已购买的剧集ID集合
func (b *sVideoBuy) GetBuyEpisodesIdSet(ctx context.Context, videoId, uid uint) ([]uint, error) {
	// 获取购买剧集列表
	list, err := b.GetBuyEpisodesList(ctx, model.VideoBuyEpisodesWhereItem{
		VideoId: videoId,
		Uid:     uid,
	})
	if err != nil {
		return nil, err
	}
	// 整理购买视频剧集ID集合
	return utils.NewArray(list).Units(dao.VideoBuyEpisodes.Columns().EpisodesId), nil
}

// IsPurchased 检验用户是否已购买过视频剧集
func (b *sVideoBuy) IsPurchased(ctx context.Context, uid, videoId uint) bool {
	r, _ := b.EpisodesModel().Exists(ctx, g.Map{
		dao.VideoBuyEpisodes.Columns().Uid:     uid,
		dao.VideoBuyEpisodes.Columns().VideoId: videoId,
	})
	return r
}

// DeletePurchasedEpisodes 删除购买过的视频剧集（购买视频退款）
func (b *sVideoBuy) DeletePurchasedEpisodes(ctx context.Context, billId uint, episodesId []uint) error {
	// 删除购买过的剧集
	return b.EpisodesModel().Delete(ctx, g.Map{
		dao.VideoBuyEpisodes.Columns().BillId:     billId,
		dao.VideoBuyEpisodes.Columns().EpisodesId: episodesId,
	})
}

// processGenBuyEpisodes 处理购买续集生成数据
func (b *sVideoBuy) processGenBuyEpisodes(in model.GenBuyEpisodesInput) []entity.VideoBuyEpisodes {
	var (
		adds []entity.VideoBuyEpisodes
	)
	// 迭代生成购买剧集
	for _, episode := range in.Episodes {
		adds = append(adds, entity.VideoBuyEpisodes{
			VideoId:      in.VideoId,
			BuyId:        in.BuyId,
			BillId:       in.BillId,
			Uid:          in.Uid,
			EpisodesName: episode.EpisodesName,
			EpisodesNum:  episode.EpisodesNum,
			EpisodesId:   episode.Id,
		})
	}
	return adds
}
