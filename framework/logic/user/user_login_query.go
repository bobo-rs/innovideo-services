package user

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/enums"
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/library/exception"
	"github.com/gogf/gf/v2/frame/g"
)

// ProcessLoginDetailByToken 通过Token获取当前登录详情
func (u *sUser) ProcessLoginDetailByToken(ctx context.Context, token string, detail interface{}) error {
	return u.LoginModel().Scan(ctx, g.Map{
		dao.UserLogin.Columns().Token: token,
	}, detail)
}

// GetLoginUserTokenByUid 通过用户ID获取用户登陆列表
func (u *sUser) GetLoginUserTokenByUid(ctx context.Context, uid ...uint) ([]model.LoginUserIdAndTokenItem, error) {
	if len(uid) == 0 || (len(uid) > 0 && uid[0] == 0) {
		return nil, exception.New(`缺少用户ID`)
	}
	var rows []model.LoginUserIdAndTokenItem
	err := u.LoginModel().Scan(ctx, g.Map{
		dao.UserLogin.Columns().UserId: uid,
		dao.UserLogin.Columns().Status: enums.LoginOauthStatusOk,
	}, &rows)
	if err != nil {
		return nil, exception.New(`暂无数据`)
	}

	return rows, nil
}
