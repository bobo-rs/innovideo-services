package user

import (
	"context"
	"fmt"
	"gitee.com/bobo-rs/creative-framework/pkg/encrypt"
	"gitee.com/bobo-rs/creative-framework/pkg/utils"
	"gitee.com/bobo-rs/innovideo-services/enums"
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/framework/model/entity"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/net/ghttp"
	"github.com/gogf/gf/v2/os/gtime"
)

// SaveUserDevice 添加或编辑医生设备信息
func (u *sUser) SaveUserDevice(ctx context.Context, device entity.UserDevice) error {
	if device.Id == 0 {
		// 通过设备HashID获取设备是否已存在
		device.Id, _ = u.ProcessDeviceIdByHash(ctx, device.DeviceHash)
	}
	// 保存用户设备信息
	_, err := dao.UserDevice.Ctx(ctx).OmitEmpty().Save(device)
	if err != nil {
		return gerror.Wrapf(err, `保存用户设备信息失败%s`, err.Error())
	}
	return nil
}

// ProcessLoginUserDevice 通过用户ID并处理用户设备信息
func (u *sUser) ProcessLoginUserDevice(ctx context.Context, uid uint) (afterDevice *model.UserDeviceLoginAfterItem, err error) {
	afterDevice = &model.UserDeviceLoginAfterItem{}
	// 获取设备信息
	afterDevice.UserDevice, err = u.GenUserDeviceParam(ctx, uid)
	if err != nil {
		return nil, err
	}
	// 获取设备详情
	detail := model.UserDeviceBasicItem{}
	if err = u.ProcessDeviceDetailByHash(ctx, afterDevice.UserDevice.DeviceHash, &detail); err == nil {
		// 设备已存在
		afterDevice.UserDevice.Id = detail.Id
		afterDevice.UserDevice.IsDisabled = detail.IsDisabled
	} else {
		// 新设备
		afterDevice.IsNewDevice = true
	}
	// 设备是否禁用
	isDisabled := enums.UserDeviceDisabled(afterDevice.UserDevice.IsDisabled)
	if isDisabled != enums.UserDeviceDisabledOk {
		return nil, gerror.Newf(`当前设备已被禁用，请使用常用设备解禁`)
	}

	// 添加或更新设备信息
	if err = u.SaveUserDevice(ctx, *afterDevice.UserDevice); err != nil {
		return nil, err
	}
	return afterDevice, nil
}

// DeviceUserAgent 通过UserAgent获取设备消息
func (u *sUser) DeviceUserAgent(ctx context.Context) (item *model.UserDeviceUserAgentItem, err error) {
	item = &model.UserDeviceUserAgentItem{
		UserAgent: ghttp.RequestFromCtx(ctx).UserAgent(),
	}
	// 设备类型
	item.DeviceName = utils.ParseUserAgentDevice(item.UserAgent)
	item.DeviceType = utils.DeviceFrom(item.UserAgent)
	// MAC地址
	item.Mac, err = utils.MacString()
	if err != nil {
		return nil, err
	}
	return
}

// DeviceHashId 用户ID生成并获取设备HashID
func (u *sUser) DeviceHashId(uid uint, item *model.UserDeviceUserAgentItem) string {
	// 生成HashID
	return encrypt.NewM().MustEncryptString(fmt.Sprintf(
		`%d-%s-%s-%s`, uid, item.DeviceName, item.DeviceType, item.Mac,
	))
}

// GenUserDeviceParam 生成用户设备参数
func (u *sUser) GenUserDeviceParam(ctx context.Context, uid uint) (*entity.UserDevice, error) {
	// 获取设备信息
	item, err := u.DeviceUserAgent(ctx)
	if err != nil {
		return nil, err
	}

	// 初始化
	device := &entity.UserDevice{
		UserId:      uid,
		DeviceName:  item.DeviceName,
		DeviceType:  item.DeviceType,
		Mac:         item.Mac,
		LastLoginAt: gtime.Now(),
	}

	// 生成设备HashID
	device.DeviceHash = u.DeviceHashId(uid, item)
	return device, nil
}
