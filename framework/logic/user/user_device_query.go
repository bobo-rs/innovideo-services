package user

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/frame/g"
)

// ProcessDeviceDetailByHash 通过HashId获取登录设备详情
func (u *sUser) ProcessDeviceDetailByHash(ctx context.Context, hashId string, pointer interface{}) error {
	if len(hashId) == 0 {
		return gerror.New(`缺少设备Hash值`)
	}
	return u.DeviceModel().Scan(ctx, g.Map{
		dao.UserDevice.Columns().DeviceHash: hashId,
	}, pointer)
}

// ProcessDeviceIdByHash 通过HashID获取设备ID
func (u *sUser) ProcessDeviceIdByHash(ctx context.Context, hashId string) (uint, error) {
	item := model.UserDeviceBasicItem{}
	err := u.ProcessDeviceDetailByHash(ctx, hashId, &item)
	if err != nil {
		return 0, err
	}
	return item.Id, nil
}
