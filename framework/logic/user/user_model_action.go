package user

import (
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/logic/base"
)

// UserModel 实例用户基础Model
func (u *sUser) UserModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.User.Table(),
	}
}

// DetailModel 实例用户详情Model
func (u *sUser) DetailModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.UserDetail.Table(),
	}
}

// CredentialModel 实例用户证件Model
func (u *sUser) CredentialModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.UserCredential.Table(),
	}
}

// DeviceModel 实例用户设备Model
func (u *sUser) DeviceModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.UserDevice.Table(),
	}
}

// FundsModel 实例拥护资金Model
func (u *sUser) FundsModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.UserFunds.Table(),
	}
}

// LoginModel 实例用户登录Model
func (u *sUser) LoginModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.UserLogin.Table(),
	}
}

// AdminModel 实例用户管理员Model
func (u *sUser) AdminModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.UserAdmin.Table(),
	}
}
