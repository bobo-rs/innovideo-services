package user

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/frame/g"
)

// ProcessUserDetailByUid 通过用户ID获取用户详情
func (u *sUser) ProcessUserDetailByUid(ctx context.Context, uid uint, pointer interface{}) error {
	if uid == 0 {
		return gerror.New(`用户ID不能为空`)
	}
	return u.DetailModel().Scan(ctx, g.Map{
		dao.UserDetail.Columns().UserId: uid,
	}, pointer)
}
