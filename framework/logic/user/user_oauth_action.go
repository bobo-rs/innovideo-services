package user

import (
	"context"
	"fmt"
	jwtHandler "gitee.com/bobo-rs/creative-framework/pkg/jwt"
	"gitee.com/bobo-rs/creative-framework/pkg/utils"
	"gitee.com/bobo-rs/innovideo-services/enums"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/framework/service"
	"gitee.com/bobo-rs/innovideo-services/library/exception"
	"gitee.com/bobo-rs/innovideo-services/library/models"
	"gitee.com/bobo-rs/innovideo-services/library/services/jwt"
	"gitee.com/bobo-rs/innovideo-services/library/services/login"
	"gitee.com/bobo-rs/innovideo-services/library/tools"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/util/grand"
	"github.com/gogf/gf/v2/util/guid"
)

// Login 账户密码或手机号快速认证登录
func (u *sUser) Login(ctx context.Context, in model.UserOauthLoginInput) (*model.UserOauthLoginOutput, error) {
	var (
		loginOauth  = login.New()
		safeAccount = in.Account // 安全账户名
		beforeParam = models.LoginValidateBeforeItem{
			Account:  in.Account,
			Sign:     in.Sign,
			IsRemove: true,
			Pwd:      in.Pwd,
			Mobile:   in.Mobile,
			Email:    in.Email,
			Oauth:    in.Oauth,
		}
	)
	// 登录前置认证服务
	err := loginOauth.ValidateBefore(ctx, beforeParam)
	if err != nil {
		return nil, exception.NewCode(enums.ErrorLoginErrLock, err.Error())
	}

	// 后置处理
	defer func() {
		if err != nil {
			// 记录错误次数
			_ = loginOauth.IncrAccountLockNum(ctx, safeAccount)
		} else {
			// 登录成功-移除锁定
			_ = loginOauth.RemoveAccountLock(ctx, safeAccount)
		}
	}()

	// 解析用户登录认证参数
	parse, err := loginOauth.ParseLoginParam(ctx, beforeParam)
	if err != nil {
		return nil, err
	}
	// 重置安全账户-手机号或账户
	safeAccount = parse.Account

	// 登录处理
	detail, err := u.processLoginOauth(ctx, in, parse)
	if err != nil {
		return nil, err
	}

	// 后置用户信息验证
	afterDetail, err := u.UserOauthValidateAfter(ctx, model.UserOauthAfterItem{
		Detail: detail,
		Other:  in.UserOauthOtherParam,
	})
	if err != nil {
		return nil, err
	}

	// 后置处理登录成功信息，例如：记录登录日志、次数、设备信息
	oauthAfter, err := u.AfterUserOauth(ctx, afterDetail)
	if err != nil {
		return nil, err
	}
	return &model.UserOauthLoginOutput{
		UserOauthLoginItem: *oauthAfter,
	}, nil
}

// AfterUserOauth 后置操作-登录认证处理
func (u *sUser) AfterUserOauth(ctx context.Context, afterDetail *model.UserOauthAfterValidateItem) (*model.UserOauthLoginItem, error) {
	item := &model.UserOauthLoginItem{
		Status: afterDetail.Status,
	}
	tokenInfo, err := jwt.New(ctx).CreateToken(jwtHandler.AccountDetail{
		Uid:      afterDetail.User.Id,
		Account:  afterDetail.Account,
		Nickname: afterDetail.Nickname,
	})
	if err != nil {
		return nil, gerror.Wrapf(err, `登录失败%s`, err.Error())
	}
	// Token信息
	item.TokenItem = tokenInfo

	// 记录登录设备
	err = u.ProcessUserLoginAfter(ctx, model.UserLoginAfterItem{
		UserOauthAfterValidateItem: *afterDetail,
		UserOauthLoginItem: model.UserOauthLoginItem{
			TokenItem: tokenInfo,
			Status:    afterDetail.Status,
		},
	})
	if err != nil {
		return nil, err
	}
	return item, nil
}

// UserOauthValidateAfter 用户登录认证后置规则验证
func (u *sUser) UserOauthValidateAfter(ctx context.Context, after model.UserOauthAfterItem) (*model.UserOauthAfterValidateItem, error) {
	if status := enums.UserStatus(after.Detail.User.Status); status != enums.UserStatusOk {
		return nil, fmt.Errorf(`登录失败，账户%s`, status.Fmt())
	}
	// 读取设备信息
	device, err := u.ProcessLoginUserDevice(ctx, after.Detail.Id)
	if err != nil {
		return nil, err
	}
	// 是否新设备，用户状态是正常且是新设备，才重置
	if after.Detail.Status == enums.UserLoginStatusOk && device.IsNewDevice == true {
		after.Detail.Status = enums.UserLoginStatusND
	}

	// 处理其他规则
	if err = u.processUserOauthOther(ctx, after); err != nil {
		return nil, err
	}

	return &model.UserOauthAfterValidateItem{
		UserOauthLoginDetailItem: *after.Detail,
		UserDeviceLoginAfterItem: *device,
	}, nil
}

// CreateOauthUser 用户登录创建用户消息
func (u *sUser) CreateOauthUser(ctx context.Context, item *model.UserSaveUserDetailItem) error {
	// 处理新用户消息
	u.processNewUserDetail(ctx, item)
	uid, err := u.SaveUser(ctx, *item)
	if err != nil {
		return err
	}
	item.Id = uid
	return nil
}

// processUserOauthOther 处理用户认证其他规则
func (u *sUser) processUserOauthOther(ctx context.Context, after model.UserOauthAfterItem) error {
	if after.Other == nil {
		return nil
	}
	// 管理员邀请注册用户
	if len(after.Other.InviteToken) > 0 {
		err := service.Rbac().AddInviteAdminUser(ctx, model.UserAdminInviteAddItem{
			Uid:      after.Detail.Id,
			Nickname: after.Detail.Nickname,
			Token:    after.Other.InviteToken,
		})
		if err != nil {
			return err
		}
	}
	return nil
}

// processNewUserDetail 处理并创建新用户消息
func (u *sUser) processNewUserDetail(ctx context.Context, item *model.UserSaveUserDetailItem) {
	// 自动生成账户
	item.Account = tools.GenAccount()
	// 注册账户IP
	item.RegIp = utils.GetIp(ctx)
	item.LoginIp = item.RegIp
	item.PwdSalt = grand.S(6)
	// 用户唯一值
	rawUniqueId := item.Account
	if item.Mobile != `` {
		rawUniqueId += item.Mobile
	}
	if item.Email != `` {
		rawUniqueId += item.Email
	}
	item.UniqueId = guid.S([]byte(rawUniqueId))
}

// processLoginOauth 认证登录-账户、手机号等登录处理
func (u *sUser) processLoginOauth(ctx context.Context, in model.UserOauthLoginInput, parse *models.LoginParseParamItem) (detail *model.UserOauthLoginDetailItem, err error) {
	// 按场景登录
	switch in.Oauth {
	case enums.UserOauthAccount:
		// 账户登录
		detail, err = u.OauthAccountLogin(ctx, model.UserOauthAccountLoginItem{
			Account: in.Account,
			Pwd:     parse.Pwd,
		})
	case enums.UserOauthSmsCaptcha:
		// 短信验证码登录
		detail, err = u.OauthSmsCaptchaLogin(ctx, model.UserOauthSmsLoginItem{
			OauthCaptchaCode: model.OauthCaptchaCode{
				Code: in.Code,
			},
			UserOauthSendSmsCaptchaItem: model.UserOauthSendSmsCaptchaItem{
				Mobile: parse.Mobile,
			},
		})
	case enums.UserOauthEmailCaptcha:
		// 邮箱验证码登录
		detail, err = u.OauthEmailCaptchaLogin(ctx, model.UserOauthEmailCaptchaLogin{
			OauthCaptchaCode: model.OauthCaptchaCode{
				Code: in.Code,
			},
			OauthEmail: model.OauthEmail{
				Email: parse.Email,
			},
		})
	default:
		err = gerror.New(`登录场景不存在`)
	}
	return
}
