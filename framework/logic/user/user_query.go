package user

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/framework/model/entity"
	"gitee.com/bobo-rs/innovideo-services/library/exception"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/frame/g"
)

// QueryDetailByMobile 通过手机号获取用户详情
func (u *sUser) QueryDetailByMobile(ctx context.Context, mobile string, pointer interface{}) error {
	if len(mobile) == 0 {
		return gerror.New(`缺少手机号`)
	}
	return u.UserModel().Scan(ctx, g.Map{
		dao.User.Columns().Mobile: mobile,
	}, pointer)
}

// QueryDetailByUid 通过用户ID获取用户详情
func (u *sUser) QueryDetailByUid(ctx context.Context, uid uint, pointer interface{}) error {
	if uid == 0 {
		return gerror.New(`用户ID不能为空`)
	}
	return u.UserModel().Scan(ctx, g.Map{
		dao.User.Columns().Id: uid,
	}, pointer)
}

// QueryDetailByAccount 通过用户账户获取用户详情
func (u *sUser) QueryDetailByAccount(ctx context.Context, account string, pointer interface{}) error {
	if len(account) == 0 {
		return gerror.New(`缺少账户`)
	}
	return u.UserModel().Scan(ctx, g.Map{
		dao.User.Columns().Account: account,
	}, pointer)
}

// QueryDetailByEmail 通过邮箱获取用户详情
func (u *sUser) QueryDetailByEmail(ctx context.Context, email string, pointer interface{}) error {
	if len(email) == 0 {
		return exception.New(`缺少邮件`)
	}
	return u.UserModel().Scan(ctx, g.Map{
		dao.User.Columns().Email: email,
	}, pointer)
}

// CheckUserExistsByMobile 通过手机号-检测用户是否存在
func (u *sUser) CheckUserExistsByMobile(ctx context.Context, mobile string) bool {
	b, _ := u.UserModel().Exists(ctx, g.Map{
		dao.User.Columns().Mobile: mobile,
	})
	return b
}

// CheckUserExistsByAccount 通过账号-检查用户是否存在
func (u *sUser) CheckUserExistsByAccount(ctx context.Context, account string) bool {
	b, _ := u.UserModel().Exists(ctx, g.Map{
		dao.User.Columns().Account: account,
	})
	return b
}

// CheckUserExistsByUid 通过用户ID检测用户是否存在
func (u *sUser) CheckUserExistsByUid(ctx context.Context, uid uint) bool {
	b, _ := u.UserModel().Exists(ctx, g.Map{
		dao.User.Columns().Id: uid,
	})
	return b
}

// ProcessAccountNameByUid 通过用户ID获取账户名
func (u *sUser) ProcessAccountNameByUid(ctx context.Context, uid uint) (*model.UserAccountNameItem, error) {
	var detail model.UserAccountNameItem
	err := u.QueryDetailByUid(ctx, uid, &detail)
	return &detail, err
}

// ProcessUserByUid 通过用户ID获取用户信息
func (u *sUser) ProcessUserByUid(ctx context.Context, uid uint) (*entity.User, error) {
	detail := &entity.User{}
	err := u.QueryDetailByUid(ctx, uid, detail)
	if err != nil {
		return nil, exception.New(`用户信息不存在`)
	}
	return detail, nil
}
