package logic

import (
	_ "gitee.com/bobo-rs/innovideo-services/framework/logic/article"
	_ "gitee.com/bobo-rs/innovideo-services/framework/logic/attachment"
	_ "gitee.com/bobo-rs/innovideo-services/framework/logic/bizctx"
	_ "gitee.com/bobo-rs/innovideo-services/framework/logic/common"
	_ "gitee.com/bobo-rs/innovideo-services/framework/logic/config"
	_ "gitee.com/bobo-rs/innovideo-services/framework/logic/email"
	_ "gitee.com/bobo-rs/innovideo-services/framework/logic/middleware"
	_ "gitee.com/bobo-rs/innovideo-services/framework/logic/pay"
	_ "gitee.com/bobo-rs/innovideo-services/framework/logic/pgcoin"
	_ "gitee.com/bobo-rs/innovideo-services/framework/logic/rbac"
	_ "gitee.com/bobo-rs/innovideo-services/framework/logic/smstpl"
	_ "gitee.com/bobo-rs/innovideo-services/framework/logic/syslog"
	_ "gitee.com/bobo-rs/innovideo-services/framework/logic/sysmap"
	_ "gitee.com/bobo-rs/innovideo-services/framework/logic/upload"
	_ "gitee.com/bobo-rs/innovideo-services/framework/logic/user"
	_ "gitee.com/bobo-rs/innovideo-services/framework/logic/video"
	_ "gitee.com/bobo-rs/innovideo-services/framework/logic/videobuy"
	_ "gitee.com/bobo-rs/innovideo-services/framework/logic/videocate"
	_ "gitee.com/bobo-rs/innovideo-services/framework/logic/videoplay"
)
