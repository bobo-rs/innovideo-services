package rbac

import (
	"context"
	"gitee.com/bobo-rs/creative-framework/pkg/utils"
	"gitee.com/bobo-rs/innovideo-services/enums"
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/library/exception"
	"github.com/gogf/gf/v2/frame/g"
)

// CheckInviteExistsByToken 通过Token检测邀请信息是否存在
func (r *sRbac) CheckInviteExistsByToken(ctx context.Context, token string) bool {
	b, _ := r.InviteModel().Exists(ctx, g.Map{
		dao.AdminInvite.Columns().Token: token,
	})
	return b
}

// CheckInviteValidByToken 通过邀请Token检测邀请信息是否有效
func (r *sRbac) CheckInviteValidByToken(ctx context.Context, token string) error {
	// 获取邀请信息
	item := model.AdminInviteValidItem{}
	err := r.InviteModel().Scan(ctx, g.Map{
		dao.AdminInvite.Columns().Token: token,
	}, &item)
	if err != nil {
		return exception.New(`邀请信息不存在`)
	}

	// 验证有效性
	if err = r.checkInviteValid(item); err != nil {
		return err
	}
	return nil
}

// GetInviteValidDetailById 通过邀请ID获取邀请有效详情数据
func (r *sRbac) GetInviteValidDetailById(ctx context.Context, id uint) (detail *model.AdminInviteValidItem, err error) {
	detail = &model.AdminInviteValidItem{}
	// 获取数据
	err = r.InviteModel().DetailPri(ctx, id, &detail)
	return
}

// GetInviteUserByToken 通过邀请ID获取用户权限信息
func (r *sRbac) GetInviteUserByToken(ctx context.Context, token string) (detail *model.AdminInviteUserValidItem, err error) {
	detail = &model.AdminInviteUserValidItem{}

	// 获取数据
	err = r.InviteModel().Scan(ctx, g.Map{
		dao.AdminInvite.Columns().Token: token,
	}, &detail)
	if err != nil {
		return nil, exception.New(`邀请信息不存在`)
	}

	// 验证有效性
	if err = r.checkInviteValid(detail.AdminInviteValidItem); err != nil {
		return nil, err
	}
	return
}

// checkInviteValid 验证邀请信息有效性
func (r *sRbac) checkInviteValid(item model.AdminInviteValidItem) error {

	// 状态是否邀请中
	if status := enums.AdminInviteStatus(item.Status); status != enums.AdminInviteStatusZero {
		return exception.New(status.Fmt())
	}

	// 是否过期
	if err := utils.IsExpired(item.Expired); err != nil {
		return exception.New(`邀请已过期`)
	}
	return nil
}
