package rbac

import (
	"gitee.com/bobo-rs/creative-framework/pkg/utils"
	"gitee.com/bobo-rs/innovideo-services/enums"
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
)

// RolesWhere 角色条件
func (r *sRbac) RolesWhere(in model.RolesListInput) map[string]interface{} {
	var (
		where   = make(map[string]interface{})
		columns = dao.Roles.Columns()
	)

	// 角色名
	if len(in.Name) > 0 {
		where[columns.Name] = in.Name
	}
	// 状态
	if in.Status.Fmt() != "" {
		where[columns.RoleStatus] = in.Status
	}
	return where
}

// UserAdminWhere 用户管理员搜索条件
func (r *sRbac) UserAdminWhere(in model.UserAdminWhereItem) map[string]interface{} {
	var (
		where   = make(map[string]interface{})
		columns = dao.UserAdmin.Columns()
	)

	// 管理员名
	if len(in.ManageName) > 0 {
		where[columns.ManageName+` like ?`] = `%` + in.ManageName + `%`
	}

	// 管理员编号
	if len(in.ManageNo) > 0 {
		where[columns.ManageNo] = utils.SplitNonCharWords(in.ManageNo)
	}

	// 状态
	if enums.AdminManageStatus(in.ManageStatus).Fmt() != "" {
		where[columns.ManageStatus] = in.ManageStatus
	}

	// 是否超管
	if enums.AdminIsSuperManage(in.IsSuperManage).Fmt() != "" {
		where[columns.IsSuperManage] = in.IsSuperManage
	}
	return where
}

// FormatUserAdmin 格式化用户管理员
func (r *sRbac) FormatUserAdmin(detail *model.UserAdminGetDetailItem) {
	detail.FmtManageStatus = enums.AdminManageStatus(detail.ManageStatus).Fmt()
	detail.FmtIsSuperManage = enums.AdminIsSuperManage(detail.IsSuperManage).Fmt()
}
