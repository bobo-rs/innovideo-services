package rbac

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"github.com/gogf/gf/v2/frame/g"
)

// CheckRolesExistsById 通过角色ID检测角色是否存在
func (r *sRbac) CheckRolesExistsById(ctx context.Context, id uint) bool {
	if id == 0 {
		return false
	}
	b, _ := r.RolesModel().Exists(ctx, g.Map{
		dao.Roles.Columns().Id: id,
	})
	return b
}

// RolesTotalById 通过角色ID获取角色总数
func (r *sRbac) RolesTotalById(ctx context.Context, id ...uint) int {
	if len(id) == 0 {
		return 0
	}
	total, _ := r.RolesModel().Total(ctx, g.Map{
		dao.Roles.Columns().Id: id,
	})
	return total
}
