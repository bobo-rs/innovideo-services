package rbac

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/library/exception"
	"github.com/gogf/gf/v2/frame/g"
)

// CheckAdminDepartByAid 通过管理员ID检测管理员是否关联部门
func (r *sRbac) CheckAdminDepartByAid(ctx context.Context, adminId uint) bool {
	if adminId == 0 {
		return false
	}
	// 是否存在
	b, _ := r.AdminDepartmentModel().Exists(ctx, g.Map{
		dao.AdminDepartment.Columns().AdminId: adminId,
	})
	return b
}

// CheckAdminDepartByDepartId 通过部门ID检测部门是否关联管理员
func (r *sRbac) CheckAdminDepartByDepartId(ctx context.Context, departId uint) bool {
	if departId == 0 {
		return false
	}
	// 是否存在
	b, _ := r.AdminDepartmentModel().Exists(ctx, g.Map{
		dao.AdminDepartment.Columns().DepartId: departId,
	})
	return b
}

// ProcessAdminIdAndDepartIdList 获取管理员ID关联部门ID列表
func (r *sRbac) ProcessAdminIdAndDepartIdList(ctx context.Context, where interface{}) (rows []model.AdminIdAndDepartIdItem, err error) {
	rows = []model.AdminIdAndDepartIdItem{}
	err = r.AdminDepartmentModel().Scan(ctx, where, &rows)
	if err != nil {
		return nil, exception.New(`数据不存在`)
	}
	return
}
