package rbac

import "gitee.com/bobo-rs/innovideo-services/framework/service"

func init() {
	service.RegisterRbac(New())
}
