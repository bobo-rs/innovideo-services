package pgcoin

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/library/tools"
	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
	"time"
)

// PGCoinBillWhere 盘古币账单条件
func (c *sPgCoin) PGCoinBillWhere(in model.PGCoinBillWhere) map[string]interface{} {
	var (
		where   = g.Map{}
		columns = dao.PanguCoinBill.Columns()
	)

	// 用户ID
	if in.Uid > 0 {
		where[columns.Uid] = in.Uid
	}

	// 编号
	if in.BillNo != `` {
		if nos := tools.ExtractValidCodes(in.BillNo); len(nos) > 0 {
			where[columns.BillNo] = nos
		} else {
			where[columns.BillNo] = ``
		}
	}

	// 状态
	if in.Status.Is() {
		where[columns.Status] = in.Status
	}

	// 类型
	if in.Type.Is() {
		where[columns.BillType] = in.Type
	}
	return where
}

// TransactionWhereModel 实例盘古币流水查询条件Model
func (c *sPgCoin) TransactionWhereModel(ctx context.Context, in model.PGCoinTransactionWhere) *gdb.Model {
	var (
		orm     = dao.PanguCoinTransaction.Ctx(ctx)
		columns = dao.PanguCoinTransaction.Columns()
	)
	// 用户ID
	if in.Uid > 0 {
		orm = orm.Where(columns.Uid, in.Uid)
	}

	// 流水号
	if in.TransactionNo != `` {
		orm = orm.Where(columns.TransactionNo, tools.ExtractValidCodes(in.TransactionNo))
	}

	// 类型
	if in.Type.Is() {
		orm = orm.Where(columns.Type, in.Type)
	}

	// 搜索时间
	if dateArr := tools.FmtSearchDate(in.SearchAt); dateArr != nil {
		orm = orm.WhereBetween(columns.CreateAt, dateArr[0], dateArr[1])
	} else if in.IsDefaultAt {
		// 启用时间：默认一年内
		orm = orm.WhereGTE(columns.CreateAt, time.Now().AddDate(-1, 0, 0).Format(time.DateOnly))
	}
	return orm
}

// OrderWhereModel 盘古币订单查询Model
func (c *sPgCoin) OrderWhereModel(ctx context.Context, in model.PGCoinOrderWhere) *gdb.Model {
	var (
		orm     = dao.PanguCoinOrder.Ctx(ctx)
		columns = dao.PanguCoinOrder.Columns()
	)
	// 充值订单ID
	if in.Id > 0 {
		orm = orm.Where(columns.Id, in.Id)
	}
	// 用户ID
	if in.Uid > 0 {
		orm = orm.Where(columns.Uid, in.Uid)
	}

	// 状态
	if in.Status.Is() {
		orm = orm.Where(columns.Status, in.Status)
	}

	// 支付状态
	if in.PayStatus.Is() {
		orm = orm.Where(columns.PayStatus, in.PayStatus)
	}

	// 订单编号
	if in.OrderNo != `` {
		orm = orm.Where(columns.OrderNo, tools.ExtractValidCodes(in.OrderNo))
	}

	// 支付编号
	if in.PayNo != `` {
		orm = orm.Where(columns.PayNo, tools.ExtractValidCodes(in.PayNo))
	}

	// 商户交易号
	if in.TradeNo != `` {
		orm = orm.Where(columns.TradeNo, tools.ExtractValidCodes(in.TradeNo))
	}

	// 退款编号
	if in.RefundNo != `` {
		orm = orm.Where(columns.RefundNo, tools.ExtractValidCodes(in.RefundNo))
	}

	// 支付方式
	if in.PayCode != `` {
		orm = orm.Where(columns.PayCode, in.PayCode)
	}

	// 支付时间
	if payAt := tools.FmtSearchDate(in.PayAt); len(payAt) > 0 {
		orm = orm.WhereBetween(columns.PayAt, payAt[0], payAt[1])
	}

	// 退款时间
	if refundAt := tools.FmtSearchDate(in.RefundAt); len(refundAt) > 0 {
		orm = orm.WhereBetween(columns.RefundAt, refundAt[0], refundAt[1])
	}

	// 订单时间
	if createAt := tools.FmtSearchDate(in.SearchAt); len(createAt) > 0 {
		orm = orm.WhereBetween(columns.CreateAt, createAt[0], createAt[1])
	} else if in.IsDefaultAt {
		orm = orm.WhereGTE(columns.CreateAt, time.Now().AddDate(0, -1, 0).Format(time.DateOnly))
	}

	return orm
}
