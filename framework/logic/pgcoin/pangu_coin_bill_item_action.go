package pgcoin

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/enums"
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/model/entity"
	"gitee.com/bobo-rs/innovideo-services/library/exception"
)

// GenPGCoinBillItem 生成盘古币账单流水明细
func (c *sPgCoin) GenPGCoinBillItem(ctx context.Context, en ...entity.PanguCoinBillItem) error {
	if len(en) == 0 {
		return exception.New(`缺少盘古币明细数据`)
	}
	_, err := dao.PanguCoinBillItem.Ctx(ctx).Save(en)
	if err != nil {
		return exception.New(`生成盘古币账单明细失败`)
	}
	return nil
}

// GetBillItemList 获取账单明细列表
func (c *sPgCoin) GetBillItemList(ctx context.Context, where interface{}) (list []entity.PanguCoinBillItem, err error) {
	// 获取账单列表
	err = dao.PanguCoinBillItem.Ctx(ctx).
		Where(where).
		OrderDesc(dao.PanguCoinBillItem.Columns().Id).
		Scan(&list)
	return
}

// processGenPGCoinBillItemParam 处理并生成盘古币账单流水参数
func (c *sPgCoin) processGenPGCoinBillItemParam(item *entity.PanguCoinBillItem, coin uint, tranType enums.PGCoinTransactionType) {
	item.PanguCoin = int(coin)
	// 支出转换（负数）
	if tranType == enums.PGCoinTransactionTypeConsume {
		item.PanguCoin = -item.PanguCoin
	}
	item.Type = uint(tranType)
}
