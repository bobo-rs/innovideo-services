package pgcoin

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/enums"
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/framework/service"
	"gitee.com/bobo-rs/innovideo-services/library/exception"
	"github.com/gogf/gf/v2/frame/g"
	"time"
)

// GetOrderList 获取盘古币充值订单列表
func (c *sPgCoin) GetOrderList(ctx context.Context, in model.PGCoinOrderListInput) (out *model.PGCoinOrderListOutput, err error) {
	out = &model.PGCoinOrderListOutput{}
	// 获取数据
	err = c.OrderWhereModel(ctx, in.PGCoinOrderWhere).
		Page(in.Page, in.Size).
		OrderDesc(dao.PanguCoinOrder.Columns().Id).
		ScanAndCount(&out.Rows, &out.Total, false)
	return
}

// GetUserOrderList 获取用户充值订单列表
func (c *sPgCoin) GetUserOrderList(ctx context.Context, in model.PGCoinUserOrderListInput) (out *model.PGCoinOrderListOutput, err error) {
	// 初始化
	where := model.PGCoinOrderWhere{
		Uid:       service.BizCtx().GetUid(ctx),
		Status:    enums.PGCoinOrderStatus(-1),
		PayStatus: enums.PGCoinOrderPay(-1),
	}
	// 按TAB类型处理搜索条件
	switch in.TABType {
	case enums.PGCoinUserTABPending:
		// 待支付
		where.Status = enums.PGCoinOrderStatusPending
		where.PayStatus = enums.PGCoinOrderPayPending
	case enums.PGCoinUserTABOK:
		// 已完成
		where.Status = enums.PGCoinOrderStatusSuccess
		where.PayStatus = enums.PGCoinOrderPayPaid
	}
	return c.GetOrderList(ctx, model.PGCoinOrderListInput{
		CommonPaginationItem: in.CommonPaginationItem,
		PGCoinOrderWhere:     where,
	})
}

// GetOrderDetail 获取盘古币充值订单详情
func (c *sPgCoin) GetOrderDetail(ctx context.Context, id uint, formSide enums.FormSide) (detail *model.PGCoinOrderDetailItem, err error) {
	detail = &model.PGCoinOrderDetailItem{}
	// 获取详情
	orm := dao.PanguCoinOrder.Ctx(ctx).Where(dao.PanguCoinOrder.Columns().Id, id)
	if formSide == enums.FormSideBus {
		orm = orm.Where(dao.PanguCoinOrder.Columns().Uid, service.BizCtx().GetUid(ctx))
	}
	err = orm.Scan(&detail)
	return
}

// CancelOrder 取消充值订单
func (c *sPgCoin) CancelOrder(ctx context.Context, id uint, formSide enums.FormSide) error {
	if id == 0 {
		return exception.New(`缺少充值订单ID`)
	}
	where := g.Map{
		dao.PanguCoinOrder.Columns().Id:        id,
		dao.PanguCoinOrder.Columns().Status:    enums.PGCoinOrderStatusPending,
		dao.PanguCoinOrder.Columns().PayStatus: enums.PGCoinOrderPayPending,
	}
	if formSide == enums.FormSideBus {
		where[dao.PanguCoinOrder.Columns().Uid] = service.BizCtx().GetUid(ctx)
	}

	// 更新数据
	return c.OrderModel().Update(ctx, where, g.Map{
		dao.PanguCoinOrder.Columns().Status: enums.PGCoinOrderStatusCancel,
	})
}

// TodayUserRechargeCount  统计用户今日充值次数
func (c *sPgCoin) TodayUserRechargeCount(ctx context.Context, countType bool) int {
	var (
		columns = dao.PanguCoinOrder.Columns()
		orm     = dao.PanguCoinOrder.Ctx(ctx).
			Where(columns.Uid, service.BizCtx().GetUid(ctx)).
			WhereGT(columns.CreateAt, time.Now().Format(time.DateOnly))
		payStatus = enums.PGCoinOrderPayPending // 默认未支付
		err       error
	)
	defer func() {
		if err != nil {
			g.Log().Debug(ctx, `统计今日用户充值总数`, err)
		}
	}()
	// 统计已支付
	if countType {
		payStatus = enums.PGCoinOrderPayPaid
	}
	total, err := orm.Where(columns.PayStatus, payStatus).Count()
	if err != nil {
		return 0
	}
	return total
}
