package pgcoin

import (
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/logic/base"
)

// OrderModel 盘古币充值订单Model
func (c *sPgCoin) OrderModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.PanguCoinOrder.Table(),
	}
}

// BillModel 盘古币账单Model
func (c *sPgCoin) BillModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.PanguCoinBill.Table(),
	}
}

// TransactionModel 盘古币流水Model
func (c *sPgCoin) TransactionModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.PanguCoinTransaction.Table(),
	}
}

// BillItemModel 盘古币账单明细Model
func (c *sPgCoin) BillItemModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.PanguCoinBillItem.Table(),
	}
}
