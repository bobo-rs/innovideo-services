package pgcoin

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/framework/service"
	"strings"
	"time"
)

type sPgCoin struct {
}

func New() *sPgCoin {
	return &sPgCoin{}
}

// Config 读取盘古币配置
func (c *sPgCoin) Config(ctx context.Context) (config *model.PGCConfigItem, err error) {
	// 获取配置组
	rawGroup, err := service.Config().GetGroupVar(ctx, `pangu_coin`)
	if err != nil {
		return nil, err
	}

	// 转换为MAP属性
	group := rawGroup.MapStrVar()
	config = &model.PGCConfigItem{
		PGCRate:          group[`pangu_coin_rate`].Uint(),
		PGCExpired:       group[`pangu_coin_expired`].Int(),
		PGCExpiredUnit:   group[`pangu_coin_expired_unit`].String(),
		PGCSymbol:        group[`pangu_coin_symbol`].String(),
		Currency:         group[`coin_currency`].String(),
		PGCRechargeLimit: group[`pangu_coin_recharge_limit`].Uint(),
		PGCRechargeMin:   group[`pangu_coin_recharge_min`].Uint(),
		ReturnUrl:        group[`pangu_coin_return_url`].String(),
		CancelUrl:        group[`pangu_coin_cancel_url`].String(),
	}

	// 叠加效期时间
	t := time.Now()
	switch strings.ToLower(config.PGCExpiredUnit) {
	case `y`:
		// 按年
		t = t.AddDate(config.PGCExpired, 0, 0)
	case `m`:
		// 按月
		t = t.AddDate(0, config.PGCExpired, 0)
	default:
		// 按日
		t = t.AddDate(0, 0, config.PGCExpired)
	}

	// 组装过期时间数据
	config.PGCExpiredTime = t.Unix()
	config.PGCExpiredAt = t.Format(time.DateTime)

	return
}
