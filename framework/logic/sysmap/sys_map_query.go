package sysmap

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/library/exception"
	"github.com/gogf/gf/v2/frame/g"
)

// GetDetailById 通过字典ID获取字典详情
func (m *sSysMap) GetDetailById(ctx context.Context, id uint, pointer interface{}) error {
	if id == 0 {
		return exception.New(`字典ID为空`)
	}
	return m.MapModel().Scan(ctx, g.Map{
		dao.SysMap.Columns().Id: id,
	}, pointer)
}
