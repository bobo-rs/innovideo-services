package sysmap

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/library/exception"
	"github.com/gogf/gf/v2/frame/g"
)

// GetValueDetailById 通过字典值ID获取字典值详情
func (m *sSysMap) GetValueDetailById(ctx context.Context, id uint, pointer interface{}) error {
	if id == 0 {
		return exception.New(`缺少字典值ID`)
	}
	return m.ValueModel().Scan(ctx, g.Map{
		dao.SysMapValue.Columns().Id: id,
	}, pointer)
}
