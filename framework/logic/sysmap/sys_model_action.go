package sysmap

import (
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/logic/base"
)

// MapModel 字典Model
func (m *sSysMap) MapModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.SysMap.Table(),
	}
}

// ValueModel 字典值Model
func (m *sSysMap) ValueModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.SysMapValue.Table(),
	}
}
