package config

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/framework/service"
	"github.com/gogf/gf/v2/errors/gerror"
)

type sConfig struct {
}

func init() {
	service.RegisterConfig(New())
}

func New() *sConfig {
	return &sConfig{}
}

// List 获取配置列表
func (c *sConfig) List(ctx context.Context, in model.ConfigListInput) (rows []model.ConfigDetailItem, err error) {
	if in.GroupId == 0 && len(in.GroupName) == 0 {
		return nil, gerror.New(`配置组ID和配置组名二选一`)
	}
	rows = []model.ConfigDetailItem{}
	// 获取配置列表
	err = dao.Config.Ctx(ctx).
		OmitEmpty().
		Where(in).
		Scan(&rows)
	if err != nil {
		return nil, gerror.Wrapf(err, `获取配置失败%s`, err.Error())
	}
	return rows, nil
}
