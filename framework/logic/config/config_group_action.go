package config

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/model/entity"
	"github.com/gogf/gf/v2/errors/gerror"
)

// SaveConfigGroup 保存配置组
func (c *sConfig) SaveConfigGroup(ctx context.Context, params ...entity.ConfigGroup) error {
	if len(params) == 0 {
		return gerror.New(`缺少配置组`)
	}
	_, err := dao.ConfigGroup.Ctx(ctx).Save(params)
	if err != nil {
		return gerror.Wrapf(err, `保存配置组失败%s`, err.Error())
	}
	return nil
}
