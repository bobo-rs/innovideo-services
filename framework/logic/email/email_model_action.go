package email

import (
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/logic/base"
)

// TemplateModel 邮件模板Model
func (e *sEmail) TemplateModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.EmailTemplate.Table(),
	}
}
