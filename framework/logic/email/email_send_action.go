package email

import (
	"context"
	"gitee.com/bobo-rs/creative-framework/pkg/utils"
	"gitee.com/bobo-rs/innovideo-services/library/exception"
	"gitee.com/bobo-rs/innovideo-services/library/services/email"
	"strconv"
)

// SendCaptcha 发送邮件验证码
func (e *sEmail) SendCaptcha(ctx context.Context, emailAddr, varName string) error {
	// 获取邮件模板
	detail, err := e.GetTemplateByVarName(ctx, varName)
	if err != nil {
		return exception.New(`邮件模板不存在`)
	}
	// 验证码
	code := strconv.Itoa(utils.RandInt(6))
	// 发送验证码
	return email.New().SendCaptcha(ctx, emailAddr, detail.Subject, utils.SmsReplaceVal(detail.Content, code), code)
}

// SendMessage 发送邮件消息
func (e *sEmail) SendMessage(ctx context.Context, varName string, addrs, tempParamsSet []string) error {
	// 获取邮件模板
	detail, err := e.GetTemplateByVarName(ctx, varName)
	if err != nil {
		return exception.New(`邮件模板不存在`)
	}
	return e.SendMail(ctx, addrs, detail.Subject, utils.SmsReplaceVal(detail.Content, tempParamsSet...))
}

// SendMail 发送邮件
func (e *sEmail) SendMail(ctx context.Context, addrs []string, subject, content string) error {
	// 发送邮件
	service, err := email.New().MailService(ctx)
	if err != nil {
		return err
	}
	return service.SendMail(subject, content, addrs...)
}
