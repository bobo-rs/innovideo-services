package common

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/maps"
	"github.com/gogf/gf/v2/errors/gerror"
)

type (
	CommonStringInterfaceMap map[string]interface{}
)

func New() *sCommon {
	return &sCommon{}
}

// CommonMaps 公共map列表
func (c *sCommon) CommonMaps(ctx context.Context, mapKey ...string) (map[string]interface{}, error) {
	// map列表
	mapList := CommonStringInterfaceMap{
		`attachment`: CommonStringInterfaceMap{},
		`common`: CommonStringInterfaceMap{
			`channel_id`: maps.ChannelId,
			`vip_type`:   maps.VipType,
			`language`:   maps.Language,
		},
		`user`: CommonStringInterfaceMap{
			`status`:                 maps.UserStatus,
			`credential_type`:        maps.UserCredentialType,
			`credential_expire_type`: maps.UserCredentialExpireType,
			`device_disabled`:        maps.UserDeviceDisabled,
			`login_status`:           maps.UserLoginStatus,
			`login_is_use`:           maps.UserLoginIsUse,
			`login_is_new_device`:    maps.UserLoginIsNewDevice,
		},
		`sms_template`: CommonStringInterfaceMap{
			`status`:    maps.SmsTemplateStatus,
			`sms_type`:  maps.SmsTemplateSmsType,
			`var_alias`: maps.SmsTemplateVarAlias,
		},
	}
	// 是否指定MAP
	if len(mapKey) > 0 && len(mapKey[0]) > 0 {
		if _, ok := mapList[mapKey[0]]; !ok {
			return nil, gerror.New(`Map标签不存在`)
		}
		return mapList[mapKey[0]].(CommonStringInterfaceMap), nil
	}
	// 返回所有
	return mapList, nil
}
