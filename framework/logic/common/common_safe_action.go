package common

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/enums"
	"gitee.com/bobo-rs/innovideo-services/library/models"
	"gitee.com/bobo-rs/innovideo-services/library/services/safety"
)

// GetSign 获取安全规则签名
func (c *sCommon) GetSign(ctx context.Context, tag enums.SafeTags) (*models.SafeSignItem, error) {
	return safety.New().GetSign(ctx, tag)
}

// Verify 验证安全规则签名属性
func (c *sCommon) Verify(ctx context.Context, sign string, tag enums.SafeTags, isRemove ...bool) (*models.SignItem, error) {
	return safety.New().Verify(ctx, sign, tag, isRemove...)
}

// EncryptExampleValue 安全签名-加密示例值（例如：登录密码、手机号加密等）
func (c *sCommon) EncryptExampleValue(ctx context.Context, text, secret string) (string, error) {
	return safety.New().AESHandler(secret).EncryptString([]byte(text))
}
