package attachment

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/library/exception"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/frame/g"
)

// ProcessAttachmentListAndTotal 处理并获取附件列表和总数
func (a *sAttachment) ProcessAttachmentListAndTotal(ctx context.Context, in model.CommonListAndTotalInput, list interface{}, total *int) error {
	if in.Where == nil {
		return gerror.New(`缺少搜索条件`)
	}
	return dao.Attachment.Ctx(ctx).
		Where(in.Where).
		Page(in.Page, in.Size).
		ScanAndCount(list, total, true)
}

// ProcessAttachmentDetailByHasher 处理并通过Hash值获取附件详情
func (a *sAttachment) ProcessAttachmentDetailByHasher(ctx context.Context, hasher string, detail interface{}) error {
	return a.ProcessAttachmentDetail(
		ctx, g.Map{dao.Attachment.Columns().Hasher: hasher}, detail,
	)
}

// ProcessAttachmentDetailByAttachId 处理并通过附件ID值获取附件详情
func (a *sAttachment) ProcessAttachmentDetailByAttachId(ctx context.Context, attachId string, detail interface{}) error {
	return a.ProcessAttachmentDetail(
		ctx, g.Map{dao.Attachment.Columns().AttachId: attachId}, detail,
	)
}

// ProcessAttachmentDetail 处理并通过值获取附件详情
func (a *sAttachment) ProcessAttachmentDetail(ctx context.Context, where interface{}, detail interface{}) error {
	if where == nil {
		return gerror.New(`缺少搜索条件`)
	}
	return dao.Attachment.Ctx(ctx).Where(where).Scan(detail)
}

// CheckAttachmentExistsByHasher 通过hasher检测附件是否存在
func (a *sAttachment) CheckAttachmentExistsByHasher(ctx context.Context, hasher string) bool {
	res, err := a.CheckAttachmentExists(
		ctx,
		g.Map{dao.Attachment.Columns().Hasher: hasher},
	)
	if err != nil {
		return false
	}
	return res
}

// CheckAttachmentExistsByAttachId 通过附件ID检测附件是否存在
func (a *sAttachment) CheckAttachmentExistsByAttachId(ctx context.Context, attachId string) bool {
	res, err := a.CheckAttachmentExists(
		ctx,
		g.Map{dao.Attachment.Columns().AttachId: attachId},
	)
	if err != nil {
		return false
	}
	return res
}

// CheckAttachmentExists 检测附件是否存在
func (a *sAttachment) CheckAttachmentExists(ctx context.Context, where interface{}) (bool, error) {
	if where == nil {
		return false, gerror.New(`缺少搜索条件`)
	}
	total, err := dao.Attachment.Ctx(ctx).
		Where(where).
		Count()
	if err != nil {
		return false, err
	}
	return total > 0, nil
}

// ProcessListAndTotal 获取文件列表和数量
func (a *sAttachment) ProcessListAndTotal(ctx context.Context, in model.CommonListAndTotalInput, list interface{}, total *int) error {
	if in.Where == nil || g.IsNil(in.Where) {
		return gerror.New(`缺少请求参数`)
	}
	orm := dao.Attachment.Ctx(ctx).Where(in.Where)
	if len(in.Sort) > 0 {
		orm = orm.Order(in.Sort)
	}
	return orm.Page(in.Page, in.Size).ScanAndCount(list, total, true)
}

// ProcessScan 扫描文件数据
func (a *sAttachment) ProcessScan(ctx context.Context, where, scan interface{}, sort ...string) error {
	if where == nil || g.IsNil(where) {
		return gerror.New(`缺少请求参数`)
	}
	orm := dao.Attachment.Ctx(ctx).Where(where)
	if len(sort) > 0 {
		orm = orm.Order(sort)
	}
	return orm.Scan(scan)
}

// ProcessDetailById 通过文件ID获取文件详情
func (a *sAttachment) ProcessDetailById(ctx context.Context, id uint, detail interface{}) error {
	if id == 0 {
		return gerror.New(`缺少文件ID`)
	}
	return a.ProcessScan(
		ctx, g.Map{dao.Attachment.Columns().Id: id}, detail,
	)
}

// ProcessDetailByAttachId 通过附件ID获取文件详情
func (a *sAttachment) ProcessDetailByAttachId(ctx context.Context, attachId string, detail interface{}) error {
	return a.ProcessScan(
		ctx, g.Map{dao.Attachment.Columns().AttachId: attachId}, detail,
	)
}

// ProcessTotal 获取附件总数
func (a *sAttachment) ProcessTotal(ctx context.Context, where interface{}) (int, error) {
	if where == nil || g.IsNil(where) {
		return 0, gerror.New(`缺少请求参数`)
	}
	return dao.Attachment.Ctx(ctx).Where(where).Count()
}

// CheckExists 检查附件是否存在
func (a *sAttachment) CheckExists(ctx context.Context, where interface{}) bool {
	total, err := a.ProcessTotal(ctx, where)
	if err != nil {
		return false
	}
	return total > 0
}

// CheckExistsById 通过文件ID检查附件是否存在
func (a *sAttachment) CheckExistsById(ctx context.Context, id uint) bool {
	return a.CheckExists(ctx, g.Map{dao.Attachment.Columns().Id: id})
}

// CheckExistsByAttachId 通过附件ID检查附件是否存在
func (a *sAttachment) CheckExistsByAttachId(ctx context.Context, attachId string) bool {
	return a.CheckExists(ctx, g.Map{dao.Attachment.Columns().AttachId: attachId})
}

// ProcessAttachUrlByAttachId 通过附件ID获取附件地址
func (a *sAttachment) ProcessAttachUrlByAttachId(ctx context.Context, attachId string) (string, error) {
	var detail model.AttachmentUrlItem
	err := a.ProcessDetailByAttachId(ctx, attachId, &detail)
	if err != nil {
		return "", err
	}
	return detail.AttUrl, nil
}

// MustAttachUrlByAttachId 通过附件ID获取附件URL地址
func (a *sAttachment) MustAttachUrlByAttachId(ctx context.Context, attachId string) string {
	attrUrl, _ := a.ProcessAttachUrlByAttachId(ctx, attachId)
	return attrUrl
}

// AttachUrlAttrMapByAttachId 通过附件ID获取附件属性MAP
func (a *sAttachment) AttachUrlAttrMapByAttachId(ctx context.Context, attachId ...string) (map[string]*model.AttachmentUrlAttrItem, error) {
	if len(attachId) == 0 {
		return nil, exception.New(`附件ID为空`)
	}
	var (
		list    []model.AttachmentUrlAttrItem
		attrMap = make(map[string]*model.AttachmentUrlAttrItem)
	)
	// 获取附件列表
	if err := a.AttachmentModel().Scan(ctx, g.Map{
		dao.Attachment.Columns().AttachId: attachId,
	}, &list); err != nil {
		return nil, exception.New(`附件属性不存在`)
	}
	// 迭代附件字典
	for _, item := range list {
		attrMap[item.AttachId] = &item
	}
	return attrMap, nil
}

// MustAttachUrlAttrMapByAttachId 通过附件ID获取附件地址属性MAP
func (a *sAttachment) MustAttachUrlAttrMapByAttachId(ctx context.Context, attachId ...string) map[string]*model.AttachmentUrlAttrItem {
	attrMap, _ := a.AttachUrlAttrMapByAttachId(ctx, attachId...)
	return attrMap
}
