package attachment

import (
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/logic/base"
)

// AttachmentModel 附件Model
func (a *sAttachment) AttachmentModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.Attachment.Table(),
	}
}
