package video

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/model/entity"
	"gitee.com/bobo-rs/innovideo-services/library/exception"
	"github.com/gogf/gf/v2/frame/g"
)

// SaveVideoPersonal 保存视频相关人员资料
func (v *sVideo) SaveVideoPersonal(ctx context.Context, en entity.VideoPersonal) error {
	_, err := dao.VideoPersonal.Ctx(ctx).
		OmitEmpty().
		Save(en)
	if err != nil {
		return exception.New(`保存失败`)
	}
	return nil
}

// deletePersonalByVideoId 通过视频ID删除视频相关人员
func (v *sVideo) deletePersonalByVideoId(ctx context.Context, videoId uint) error {
	where := g.Map{
		dao.VideoPersonal.Columns().VideoId: videoId,
	}
	if b, _ := v.PersonalModel().Exists(ctx, where); !b {
		return nil
	}
	return v.PersonalModel().Delete(ctx, where)
}
