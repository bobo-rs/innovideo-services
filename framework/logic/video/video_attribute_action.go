package video

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/model/entity"
	"gitee.com/bobo-rs/innovideo-services/library/exception"
)

// SaveAttribute 添加|编辑视频属性
func (v *sVideo) SaveAttribute(ctx context.Context, item entity.VideoAttribute) error {
	if item.VideoId == 0 {
		return exception.New(`保存视频属性，视频ID不能为空`)
	}
	// 保存数据
	_, err := dao.VideoAttribute.Ctx(ctx).
		OmitEmpty().
		Save(item)
	if err != nil {
		return exception.New(`保存视频属性失败，请重试`)
	}
	return nil
}
