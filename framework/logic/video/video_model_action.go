package video

import (
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/logic/base"
)

// VideoModel 视频Model
func (v *sVideo) VideoModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.Video.Table(),
	}
}

// AttributeModel 视频属性Model
func (v *sVideo) AttributeModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.VideoAttribute.Table(),
	}
}

// EpisodesModel 视频续集Model
func (v *sVideo) EpisodesModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.VideoEpisodes.Table(),
	}
}

// LabelModel 视频标签Model
func (v *sVideo) LabelModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.VideoLabel.Table(),
	}
}

// LogModel 视频操作日志Model
func (v *sVideo) LogModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.VideoLog.Table(),
	}
}

// PersonalModel 视频相关人员Model
func (v *sVideo) PersonalModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.VideoPersonal.Table(),
	}
}

// ResolutionModel 视频续集清晰度Model
func (v *sVideo) ResolutionModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.VideoResolution.Table(),
	}
}

// UserActionsModel 视频用户操作Model
func (v *sVideo) UserActionsModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.VideoUserActions.Table(),
	}
}
