package video

import (
	"context"
	"fmt"
	"gitee.com/bobo-rs/creative-framework/pkg/algorithm/uniquecode"
	"gitee.com/bobo-rs/creative-framework/pkg/utils"
	"gitee.com/bobo-rs/innovideo-services/consts"
	"gitee.com/bobo-rs/innovideo-services/enums"
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"github.com/gogf/gf/v2/frame/g"
)

// VideoWhere 视频搜索条件
func (v *sVideo) VideoWhere(ctx context.Context, item model.VideoWhereItem) map[string]interface{} {
	var (
		ids     []uint
		where   = g.Map{}
		columns = dao.Video.Columns()
	)

	// 视频编号
	if uniquecode.CheckUniqueCode(item.VideoNo, true) {
		where[columns.VideoNo] = item.VideoNo
	}

	// 视频关键词
	if item.Keywords != "" {
		kwd := utils.OrmWhereLike(item.Keywords)
		where[columns.Keywords+consts.OrmWhereLike+` Or `+columns.VideoName+consts.OrmWhereLike] = []string{
			kwd, kwd,
		}
	}

	// 视频类目
	if item.CateId1 > 0 {
		where[columns.CateId1] = item.CateId1
	}

	// 视频二级类目
	if item.CateId2 > 0 {
		where[columns.CateId2] = item.CateId2
	}

	// 地区
	if item.Region != "" {
		where[columns.Region] = item.Region
	}

	// 年份
	if item.YearDate != "" {
		where[columns.YearDate] = item.YearDate
	}

	// 是否收费
	if t := enums.VideoFeeType(item.FeeType); t.Is() {
		where[columns.FeeType] = item.FeeType
	}

	// 是否新剧
	if n := enums.VideoNew(item.IsNew); n.Is() {
		where[columns.IsNew] = item.IsNew
	}

	// 是否推荐
	if c := enums.VideoRecommend(item.IsRecommend); c.Is() {
		where[columns.IsRecommend] = c
	}

	// 是否精品剧
	if b := enums.VideoBeat(item.IsBest); b.Is() {
		where[columns.IsBest] = b
	}

	// 是否置顶
	if t := enums.VideoTop(item.IsTop); t.Is() {
		where[columns.IsTop] = t
	}

	// 是否热播
	if h := enums.VideoHot(item.IsHot); h.Is() {
		where[columns.IsHot] = h
	}

	// 状态
	if s := enums.VideoStatus(item.Status); s.Is() {
		where[columns.Status] = s
	}

	// 是否发布
	if r := enums.VideoRelease(item.IsRelease); r.Is() {
		where[columns.IsRelease] = r
	}

	// 视频标签
	if len(item.Label) > 0 {
		if videoId, _ := v.GetVideoIdByValue(ctx, item.Label...); len(videoId) > 0 {
			ids = append(ids, videoId...)
		}
	}

	// 视频ID
	if len(ids) > 0 {
		where[columns.Id] = ids
	}
	return where
}

// VideoSort 视频列表排序
func (v *sVideo) VideoSort(by uint16) string {
	defaultSort := fmt.Sprintf(
		`%s, %s, %s, %s desc`,
		dao.Video.Columns().IsTop,
		dao.Video.Columns().IsRecommend,
		dao.Video.Columns().IsHot,
		dao.Video.Columns().Id,
	)
	// 排序类型
	switch by {
	case 1:
		// 按播放量
		defaultSort = fmt.Sprintf(`%s, %s`, dao.Video.Columns().PlayNum, defaultSort)
	case 2:
		// 按浏览量
		defaultSort = fmt.Sprintf(`%s, %s`, dao.Video.Columns().Browse, defaultSort)
	}
	return defaultSort
}
