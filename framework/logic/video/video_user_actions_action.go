package video

import (
	"context"
	"fmt"
	"gitee.com/bobo-rs/creative-framework/pkg/utils"
	"gitee.com/bobo-rs/innovideo-services/enums"
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/framework/model/entity"
	"gitee.com/bobo-rs/innovideo-services/framework/service"
	"gitee.com/bobo-rs/innovideo-services/library/exception"
	"github.com/gogf/gf/v2/frame/g"
)

// 用户视频关联操作（收藏、点赞、踩一下）
func (v *sVideo) VideoAssocAction(ctx context.Context, videoId uint, t enums.VideoUserActionType) error {
	var (
		uid   = service.BizCtx().GetUid(ctx)
		where = g.Map{
			dao.VideoUserActions.Columns().VideoId: videoId,
			dao.VideoUserActions.Columns().UserId:  uid,
			dao.VideoUserActions.Columns().Type:    t,
		}
	)
	// 是否存在，存在则删除
	if b, _ := v.UserActionsModel().Exists(ctx, where); b {
		// 解除关联
		_ = v.UserActionsModel().Delete(ctx, where)
		return nil
	}

	// 关联数据
	err := v.UserActionsModel().Save(ctx, entity.VideoUserActions{
		VideoId: videoId,
		UserId:  uid,
		Type:    string(t),
		Ip:      utils.GetIp(ctx),
	})
	if err != nil {
		return exception.New(`操作失败`)
	}
	return nil
}

// GetUserVideoActions 获取用户视频操作属性（收藏、点赞、踩）
func (v *sVideo) GetUserVideoActions(ctx context.Context, videoId uint) (attr model.VideoUserActionsAttrItem) {
	attr = model.VideoUserActionsAttrItem{}
	// 获取用户收藏、点赞、踩数据
	var list []model.VideoUserActionsBasicItem
	err := v.UserActionsModel().Scan(ctx, g.Map{
		dao.VideoUserActions.Columns().VideoId: videoId,
		dao.VideoUserActions.Columns().UserId:  service.BizCtx().GetUid(ctx),
	}, &list)
	if err != nil || len(list) == 0 {
		return attr
	}

	// 迭代处理视频操作
	for _, item := range list {
		switch enums.VideoUserActionType(item.Type) {
		case enums.VideoUserActionTypeCollect:
			attr.IsCollect = true // 收藏
		case enums.VideoUserActionTypeLike:
			attr.IsLike = true // 点赞
		case enums.VideoUserActionTypeTread:
			attr.IsTread = true // 踩
		}
	}
	return attr
}

// GetUserActionVideoList 获取用户视频操作列表数据
func (v *sVideo) GetUserActionVideoList(ctx context.Context, t enums.VideoUserActionType, page, size int) (out *model.VideoUserListOutput, err error) {
	out = &model.VideoUserListOutput{}
	// 获取视频数据
	err = dao.VideoUserActions.Ctx(ctx).
		FieldsPrefix(dao.Video.Table(), model.VideoUserListItem{}).
		LeftJoin(dao.Video.Table(), fmt.Sprintf(
			`%s.%s = %s.%s`,
			dao.Video.Table(), dao.Video.Columns().Id,
			dao.VideoUserActions.Table(), dao.VideoUserActions.Columns().VideoId,
		)).
		WherePrefix(dao.VideoUserActions.Table(), g.Map{
			dao.VideoUserActions.Columns().UserId: service.BizCtx().GetUid(ctx),
			dao.VideoUserActions.Columns().Type:   t,
		}).
		Page(page, size).
		Order(fmt.Sprintf(`%s.%s desc`, dao.VideoUserActions.Table(), dao.VideoUserActions.Columns().CreateAt)).
		ScanAndCount(&out.Rows, &out.Total, false)
	return
}

// GetUserVideoIdByType 通过收点踩类型获取视频ID
func (v *sVideo) GetUserVideoIdByType(ctx context.Context, t enums.VideoUserActionType) []uint {
	// 获取用户操作视频ID
	var list []model.VideoUserActionsBasicItem
	err := v.UserActionsModel().Scan(ctx, g.Map{
		dao.VideoUserActions.Columns().UserId: service.BizCtx().GetUid(ctx),
		dao.VideoUserActions.Columns().Type:   t,
	}, &list)
	if err != nil {
		return nil
	}

	// 转换视频ID
	return utils.NewArray(list).Units(dao.VideoUserActions.Columns().VideoId)
}
