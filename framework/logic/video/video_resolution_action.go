package video

import (
	"context"
	"gitee.com/bobo-rs/creative-framework/pkg/utils"
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/framework/model/entity"
	"gitee.com/bobo-rs/innovideo-services/framework/service"
	"gitee.com/bobo-rs/innovideo-services/library/exception"
	"github.com/gogf/gf/v2/frame/g"
)

// SaveVideoResolution 保存视频清晰度数据
func (v *sVideo) SaveVideoResolution(ctx context.Context, episodesId uint, params ...*model.VideoResolutionSaveItem) error {
	if len(params) == 0 || (len(params) > 0 && params[0] == nil) {
		return exception.New(`缺少视频清晰度数据`)
	}
	// 获取附件ID
	attachIds := utils.NewArray(params).Strings(dao.VideoResolution.Columns().AttachId)
	if len(attachIds) == 0 {
		return exception.New(`视频地址附件ID为空`)
	}

	var (
		adds    []entity.VideoResolution
		attrMap = service.Attachment().MustAttachUrlAttrMapByAttachId(ctx, attachIds...) // 获取附件URL属性
	)

	// 迭代处理数据
	for _, item := range params {
		add := entity.VideoResolution{
			Id:         item.Id,
			Resolution: item.Resolution,
			AttachId:   item.AttachId,
			EpisodesId: episodesId,
		}
		// 属性数据
		if attr := attrMap[item.AttachId]; attr != nil {
			add.VideoUrl = attr.AttUrl
			add.Size = uint64(attr.AttSize)
			add.Duration = attr.Duration
			add.DurationString = attr.DurationString
			add.MimeType = attr.AttType
		}
		// 组装数据
		adds = append(adds, add)
	}

	// 保存数据
	if err := v.ResolutionModel().Save(ctx, adds); err != nil {
		return exception.New(`视频清晰度保存失败`)
	}
	return nil
}

// GetResolutionMapByEpisodesId 通过视频续集ID获取续集清晰度列表
func (v *sVideo) GetResolutionMapByEpisodesId(ctx context.Context, episodesId ...uint) (map[uint][]model.VideoResolutionListItem, error) {
	if len(episodesId) == 0 {
		return nil, exception.New(`视频续集ID为空`)
	}
	var (
		resoles    []model.VideoResolutionListItem
		resolesMap = make(map[uint][]model.VideoResolutionListItem)
	)
	err := v.ResolutionModel().Scan(ctx, g.Map{
		dao.VideoResolution.Columns().EpisodesId: episodesId,
	}, &resoles)
	if err != nil {
		return nil, exception.New(`视频续集清晰度资料为空`)
	}

	// 组装视频续集清晰度
	for _, item := range resoles {
		rs, ok := resolesMap[item.EpisodesId]
		if !ok {
			// 初始化
			rs = make([]model.VideoResolutionListItem, 0)
		}
		resolesMap[item.EpisodesId] = append(rs, item)
	}

	// 视频清晰度MAP
	return resolesMap, nil
}

// GetEpisodesResolutionDetail 获取视频续集详情及视频清晰度
func (v *sVideo) GetEpisodesResolutionDetail(ctx context.Context, episodesId uint, resolution string) (detail *model.EpisodesResolutionDetailItem, err error) {
	detail = &model.EpisodesResolutionDetailItem{}
	// 获取视频集数详情
	err = v.EpisodesModel().DetailPri(ctx, episodesId, &detail.VideoEpisodesBasicItem)
	if err != nil {
		return nil, exception.New(`当前视频集不存在`)
	}

	// 获取视频清晰度
	err = dao.VideoResolution.Ctx(ctx).
		Where(dao.VideoResolution.Columns().EpisodesId, episodesId).
		Where(dao.VideoResolution.Columns().Resolution, resolution).
		Scan(&detail.VideoResolutionAttrItem)
	if err != nil {
		return nil, exception.New(`当前视频集资料不存在`)
	}
	return
}
