package pay

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/framework/service"
)

type sPay struct {
}

func New() *sPay {
	return &sPay{}
}

// Config 获取支付配置
func (p *sPay) Config(ctx context.Context) (*model.PayConfig, error) {
	rawConfig, err := service.Config().GetGroupVar(ctx, `pay`)
	if err != nil {
		return nil, err
	}
	config := rawConfig.MapStrVar()
	// 返回数据
	return &model.PayConfig{
		Currency:  config[`pay_currency`].String(),
		ReturnUrl: config[`pay_return_url`].String(),
		CancelUrl: config[`pay_cancel_url`].String(),
		NotifyUrl: config[`pay_notify_url`].String(),
	}, nil
}
