package pay

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/enums"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/framework/service"
	"gitee.com/bobo-rs/innovideo-services/library/exception"
)

// beforePaymentEvent 获取前置支付信息
func (p *sPay) beforePaymentEvent(ctx context.Context, item model.PaymentItem) (res *model.PaymentResultData, err error) {
	res = &model.PaymentResultData{}
	// 按事件场景获取支付信息
	switch item.Event {
	case enums.PayEventCoins:
		res, err = service.PgCoin().GetPayPGCoinDetail(ctx, model.PGCoinPayDetailInput{
			OrderNo: item.No,
		})
	default:
		return nil, exception.Newf(`对应的支付[%s]场景不存在`, string(item.Event))
	}
	return
}

// afterPaymentEvent 后置支付事件处理
func (p *sPay) afterPaymentEvent(ctx context.Context, item model.PaymentItem, response interface{}) (err error) {
	// 按照事件场景同步后置数据
	if item.PayType == enums.PayTypePayPal {
		resp := response.(map[string]interface{})
		switch item.Event {
		case enums.PayEventCoins:
			// 更新盘古币
			err = service.PgCoin().CallbackUpdateCoinsPayMethod(ctx, model.CallbackUpdateCoinsPayMethodInput{
				PayNo:   resp[`pay_no`].(string),
				OrderNo: item.No,
				PayType: item.PayType,
			})
		default:
		}
	}
	return err
}
