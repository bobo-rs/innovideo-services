package upload

import (
	"context"
	"fmt"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"github.com/gogf/gf/v2/encoding/gjson"
	"github.com/gogf/gf/v2/net/ghttp"
	"github.com/gogf/gf/v2/util/gconv"
	"testing"
)

var (
	Ctx = context.Background()
)

func TestSUpload_FileUpload(t *testing.T) {
	param := `{"file":{"Filename":"1710813117418.jpg","Header":{"Content-Disposition":["form-data; name=\"file\"; filename=\"1710813117418.jpg\""],"Content-Type":["image/jpeg"]},"Size":109123},"name":"","random_name":true}`
	rawJson, _ := gjson.DecodeToJson(param)

	fileData := rawJson.Get("file")
	var file *ghttp.UploadFile
	if err := gconv.Struct(fileData, &file); err != nil {
		fmt.Println(err, rawJson, file)
		return
	}

	req := model.UploadFileInput{
		UploadFileItem: model.UploadFileItem{
			File: file,
		},
	}
	fmt.Println(New().FileUpload(Ctx, req))
}
