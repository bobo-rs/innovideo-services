package upload

import (
	"context"
	"fmt"
	"gitee.com/bobo-rs/creative-framework/pkg/sredis"
	"gitee.com/bobo-rs/innovideo-services/consts"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/library/exception"
	"gitee.com/bobo-rs/innovideo-services/library/tools"
	"time"
)

// SetUploadBlockIsCompleted 设置上传文件块的完成信息
func (u *sUpload) SetUploadBlockIsCompleted(ctx context.Context, hash string, item model.UploadBlockFile) error {
	// 缓存已上传完成内容
	return sredis.NewCache().Set(ctx, tools.CacheKey(consts.UploadBlockIsCompleted, hash), item, time.Hour*24)
}

// GetUploadBlockIsCompleted 获取上传文件块的完成信息
func (u *sUpload) GetUploadBlockIsCompleted(ctx context.Context, hash string) (*model.UploadBlockFile, error) {
	// 获取数据
	r, err := sredis.NewCache().Get(ctx, tools.CacheKey(consts.UploadBlockIsCompleted, hash))
	if err != nil {
		return nil, exception.Newf(`获取上传文件块失败%s`, err.Error())
	}
	// 检测内容是否存在
	if r.IsEmpty() {
		return nil, exception.New(`未检测到已上传文件块或还在上传中`)
	}
	var item *model.UploadBlockFile
	if err = r.Struct(&item); err != nil {
		return nil, err
	}
	return item, nil
}

// GetUploadBlockMergeCompleted 获取文件合并成功信息
func (u *sUpload) GetUploadBlockMergeCompleted(ctx context.Context, hash string) (*model.UploadBlockGenMerge, error) {
	// 获取数据
	r, err := sredis.NewCache().Get(ctx, tools.CacheKey(consts.UploadBlockIsCompleted, hash))
	if err != nil {
		return nil, exception.Newf(`获取上传文件块失败%s`, err.Error())
	}
	item := &model.UploadBlockGenMerge{}
	if err = r.Struct(&item); err != nil {
		return nil, err
	}
	return item, nil
}

// SetUploadBlockMergeCompleted 设置上传文合并完成信息
func (u *sUpload) SetUploadBlockMergeCompleted(ctx context.Context, hash string, item *model.UploadBlockGenMerge) error {
	return sredis.NewCache().Set(ctx, tools.CacheKey(consts.UploadBlockMergeCompleted, hash), item, time.Hour*24)
}

// SetUploadBlockFileIndex 设置上传文件块索引
func (u *sUpload) SetUploadBlockFileIndex(ctx context.Context, item model.UploadBlockItem) ([]model.UploadBlockItem, error) {
	key := tools.CacheKey(consts.UploadBlockMainIndexFile, item.Hash)
	// 获取历史上传数据
	blocks, err := u.GetUploadBlockFileIndex(ctx, item.Hash)
	if err != nil {
		return nil, err
	}
	// 设置并缓存内容
	blocks = append(blocks, item)
	if err = sredis.NewCache().Set(ctx, key, blocks, time.Hour*24); err != nil {
		return nil, fmt.Errorf(`创建上传内容索引失败%w`, err)
	}
	return blocks, nil
}

// GetUploadBlockFileIndex 获取上传历史文件块索引
func (u *sUpload) GetUploadBlockFileIndex(ctx context.Context, hash string) ([]model.UploadBlockItem, error) {
	var (
		key = tools.CacheKey(consts.UploadBlockMainIndexFile, hash)
	)
	r, err := sredis.NewCache().Get(ctx, key)
	if err != nil {
		return nil, err
	}
	// 获取数据
	blocks := make([]model.UploadBlockItem, 0)
	if r.IsEmpty() {
		return blocks, nil
	}
	// 转换内容
	if err = r.Structs(&blocks); err != nil {
		return nil, err
	}
	return blocks, nil
}

// RemoveUploadBlockFileCache 移除上传文件索引及其他缓存
func (u *sUpload) RemoveUploadBlockFileCache(ctx context.Context, hash string) error {
	return sredis.NewCache().Removes(ctx, []interface{}{
		tools.CacheKey(consts.UploadBlockMainIndexFile, hash),
		tools.CacheKey(consts.UploadBlockIsCompleted, hash),
		tools.CacheKey(consts.UploadBlockMergeCompleted, hash),
	})
}
