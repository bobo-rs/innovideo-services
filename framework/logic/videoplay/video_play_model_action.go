package videoplay

import (
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/logic/base"
)

// PlayModel 视频播放Model
func (p *sVideoPlay) PlayModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.VideoPlay.Table(),
	}
}

// SequelModel 视频播放续集Model
func (p *sVideoPlay) SequelModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.VideoPlaySequel.Table(),
	}
}
