package videocate

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"github.com/gogf/gf/v2/frame/g"
)

// CheckCategoryChildExistsById 检测子级类目是否存在
func (c *sVideoCate) CheckCategoryChildExistsById(ctx context.Context, pid uint) bool {
	b, _ := c.CategoryModel().Exists(ctx, g.Map{
		dao.VideoCategory.Columns().Pid: pid,
	})
	return b
}
