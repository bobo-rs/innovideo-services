package videocate

import (
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/logic/base"
)

// CategoryModel 视频分类Model
func (c *sVideoCate) CategoryModel() *base.TblBaseService {
	return &base.TblBaseService{
		Table: dao.VideoCategory.Table(),
	}
}
