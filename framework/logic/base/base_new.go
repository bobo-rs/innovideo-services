package base

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
)

type (
	// tblBaseModel 数据表继承基础查询Model接口
	tblBaseModel interface {
		ListAndTotal(ctx context.Context, in model.CommonListAndTotalInput, pointer interface{}, total *int) error
		Scan(ctx context.Context, where, pointer interface{}) error
		ScanOmitEmpty(ctx context.Context, where, pointer interface{}) error
		Total(ctx context.Context, where interface{}) (int, error)
		TotalPri(ctx context.Context, id ...uint) (int, error)
		Exists(ctx context.Context, where interface{}) (bool, error)
		ExistsPri(ctx context.Context, id uint) (bool, error)
		Update(ctx context.Context, where, data interface{}) error
		UpdatePri(ctx context.Context, id uint, column string, value interface{}) error
		Delete(ctx context.Context, where interface{}) error
		DeletePri(ctx context.Context, id uint) error
		Save(ctx context.Context, data interface{}) error
		DetailPri(ctx context.Context, id uint, pointer interface{}) error
	}

	// TblBaseService 数据表基础查询操作
	TblBaseService struct {
		tblBaseModel
		Table string
	}
)
