package validate

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/framework/service"
	"gitee.com/bobo-rs/innovideo-services/library/exception"
	"github.com/gogf/gf/v2/util/gvalid"
)

// RuleRegisterEmail 注册邮件规则
func RuleRegisterEmail() {
	gvalid.RegisterRuleByMap(map[string]gvalid.RuleFunc{
		`email-tpl-exists-id`: RuleEmailTplExistsById,
	})
}

// RuleEmailTplExistsById 验证邮件模板是否存在
func RuleEmailTplExistsById(ctx context.Context, in gvalid.RuleFuncInput) error {
	if in.Value.IsEmpty() {
		return nil
	}

	// 验证模板是否存在
	if b, _ := service.Email().TemplateModel().ExistsPri(ctx, in.Value.Uint()); !b {
		return exception.New(`邮件模板不存在`)
	}
	return nil
}
