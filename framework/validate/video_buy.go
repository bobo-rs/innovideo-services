package validate

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/framework/service"
	"gitee.com/bobo-rs/innovideo-services/library/exception"
	"github.com/gogf/gf/v2/util/gvalid"
)

// RuleIsBuyEpisodesIdSet 验证视频剧集是否已购买
func RuleIsBuyEpisodesIdSet(ctx context.Context, in gvalid.RuleFuncInput) error {
	if len(in.Value.Uints()) == 0 {
		return nil
	}
	// 检测剧集ID是否已购买
	episodes, err := service.VideoBuy().GetBuyEpisodesList(ctx, model.VideoBuyEpisodesWhereItem{
		Uid:        service.BizCtx().GetUid(ctx),
		EpisodesId: in.Value.Uints(),
	})
	if err != nil {
		return err
	}
	// 是否已购买
	if len(episodes) > 0 {
		return exception.Newf(`请勿重复购买下列剧集：%s`, service.VideoBuy().FmtBuyEpisodeContent(episodes))
	}
	return nil
}

// RuleBuyExistsById 验证购买信息是否存在
func RuleBuyExistsById(ctx context.Context, in gvalid.RuleFuncInput) error {
	if in.Value.Uint() == 0 {
		return nil
	}
	// 购买信息是否存在
	if b, _ := service.VideoBuy().BuyModel().ExistsPri(ctx, in.Value.Uint()); !b {
		return exception.New(`视频购买信息不存在`)
	}
	return nil
}

// RuleVideoBillExistsById 视频购买账单是否存在
func RuleVideoBillExistsById(ctx context.Context, in gvalid.RuleFuncInput) error {
	if in.Value.Uint() == 0 {
		return nil
	}
	// 购买信息是否存在
	if b, _ := service.VideoBuy().BillModel().ExistsPri(ctx, in.Value.Uint()); !b {
		return exception.New(`视频购买账单信息不存在`)
	}
	return nil
}
