package validate

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/service"
	"gitee.com/bobo-rs/innovideo-services/library/exception"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/util/gvalid"
)

// RuleRegisterSysMap 注册系统字典规则
func RuleRegisterSysMap() {
	gvalid.RegisterRuleByMap(map[string]gvalid.RuleFunc{
		`sys-map-exists-id`:       RuleSysMapExistsById,
		`sys-map-value-exists-id`: RuleSysMapValueExistsById,
		`sys-map-exists-name`:     RuleSysMapExistsByName,
	})
}

// RuleSysMapExistsById 通过字典ID检测字典值是否存在
func RuleSysMapExistsById(ctx context.Context, in gvalid.RuleFuncInput) error {
	if in.Value.Uint() == 0 {
		return nil
	}
	// 是否存在
	if b, _ := service.SysMap().MapModel().ExistsPri(ctx, in.Value.Uint()); !b {
		return exception.New(`字典名不存在`)
	}
	return nil
}

// RuleSysMapExistsByName 通过字典名检测字典是否存在
func RuleSysMapExistsByName(ctx context.Context, in gvalid.RuleFuncInput) error {
	if in.Value.IsEmpty() {
		return nil
	}
	// 是否存在
	if b, _ := service.SysMap().MapModel().Exists(ctx, g.Map{
		dao.SysMap.Columns().Name: in.Value.String(),
	}); !b {
		return exception.New(`字典名不存在`)
	}
	return nil
}

// RuleSysMapValueExistsById 通过字典值ID检测字典值是否存在
func RuleSysMapValueExistsById(ctx context.Context, in gvalid.RuleFuncInput) error {
	if in.Value.Uint() == 0 {
		return nil
	}
	// 字典值是否存在
	if b, _ := service.SysMap().ValueModel().ExistsPri(ctx, in.Value.Uint()); !b {
		return exception.New(`字典值不存在`)
	}
	return nil
}
