package validate

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/enums"
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/framework/service"
	"gitee.com/bobo-rs/innovideo-services/library/exception"
	"github.com/gogf/gf/v2/frame/g"
)

// CheckVideoBillRefund 验证视频退款账单退款信息
func CheckVideoBillRefund(ctx context.Context, in model.RefundVideoBillInput) (detail *model.VideoBillRefundItem, err error) {
	detail = &model.VideoBillRefundItem{}
	// 获取账单详情
	err = service.VideoBuy().BillModel().DetailPri(ctx, in.BillId, &detail)
	if err != nil {
		return nil, err
	}

	// 是否全额退款
	if detail.Status == int(enums.VideoBuyBillStatusRefund) {
		return nil, exception.New(`该账单已全额退款`)
	}

	// 退款盘古币不能大于账单盘古币
	if in.RefundCoins > (detail.BuyPanguCoin + detail.RefundPanguCoin) {
		return nil, exception.New(`退款盘古币不能大于账单盘古币`)
	}

	// 获取退款剧集
	if total, _ := service.VideoBuy().EpisodesModel().Total(ctx, g.Map{
		dao.VideoBuyEpisodes.Columns().BillId:     in.BillId,
		dao.VideoBuyEpisodes.Columns().EpisodesId: in.EpisodesIdSet,
	}); len(in.EpisodesIdSet) == 0 || total != len(in.EpisodesIdSet) {
		return nil, exception.New(`请勾选的退款剧集或剧集不存在`)
	}
	return
}
