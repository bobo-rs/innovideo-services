package validate

import (
	"fmt"
	"gitee.com/bobo-rs/innovideo-services/enums"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/library/exception"
)

// CheckGenCoinsBillParam 验证生成盘古币账单参数
func CheckGenCoinsBillParam(in model.PGCoinRechargeBillInput) error {
	if !in.Type.Is() {
		return exception.New(`盘古币充值类型不存在`)
	}
	// 订单充值
	if in.Type == enums.PGCoinBillTypeRecharge && in.OrderId == 0 {
		return exception.New(`充值订单ID为空`)
	}

	// 账户ID
	if in.Uid == 0 {
		return exception.New(`充值账户为空`)
	}
	return nil
}

// CheckUserAvailableCoins 消费时验证账户可用盘古币
func CheckUserAvailableCoins(in model.CheckAvailableCoins) error {
	// 核验账单和账户可用量
	if in.BillAvailableCoins != in.FundsCoins {
		return exception.Newf(`账户异常，盘古币账单可用数量%d和账户可用数量%d不匹配，请核验`, in.BillAvailableCoins, in.FundsCoins)
	}
	// 消费盘古币是否超过可以用盘古币
	if in.ConsumedCoins > in.FundsCoins {
		return exception.New(fmt.Sprintf(`当前可用盘古币%d`, in.FundsCoins))
	}
	return nil
}
