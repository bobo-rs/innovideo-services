package validate

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/framework/dao"
	"gitee.com/bobo-rs/innovideo-services/framework/service"
	"gitee.com/bobo-rs/innovideo-services/library/exception"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/util/gvalid"
)

// RuleVideoCategoryExistsById 检测视频类目是否存在
func RuleVideoCategoryExistsById(ctx context.Context, in gvalid.RuleFuncInput) error {
	if in.Value.Uint() == 0 {
		return nil
	}

	// 是否存在
	if b, _ := service.VideoCate().CategoryModel().ExistsPri(ctx, in.Value.Uint()); !b {
		return exception.New(`视频类目不存在`)
	}
	return nil
}

// RuleVideoCategoryDelete 检测视频类目删除规则
func RuleVideoCategoryDelete(ctx context.Context, in gvalid.RuleFuncInput) error {
	if in.Value.Uint() == 0 {
		return nil
	}
	// 类目是否存在
	if b, _ := service.VideoCate().CategoryModel().ExistsPri(ctx, in.Value.Uint()); !b {
		return exception.New(`类目不存在`)
	}
	// 子级类目
	if service.VideoCate().CheckCategoryChildExistsById(ctx, in.Value.Uint()) {
		return exception.New(`存在子级视频类目不允许删除`)
	}
	return nil
}

// RuleIsTopVideoCategory 验证类目是否符合推荐要求
func RuleIsTopVideoCategory(ctx context.Context, in gvalid.RuleFuncInput) error {
	if in.Value.Uint() == 0 {
		return nil
	}
	// 验证是否顶级大类
	if b, _ := service.VideoCate().CategoryModel().Exists(ctx, g.Map{
		dao.VideoCategory.Columns().Pid: 0,
		dao.VideoCategory.Columns().Id:  in.Value.Uint(),
	}); !b {
		return exception.New(`非一级大类或类目不存在不允许操作`)
	}
	return nil
}
