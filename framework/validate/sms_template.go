package validate

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/framework/service"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/util/gvalid"
)

// RuleRegisterSmsTemplate 注册短信模板
func RuleRegisterSmsTemplate() {
	gvalid.RegisterRuleByMap(map[string]gvalid.RuleFunc{
		`sms-tmpl-exists-id`: RuleSmsTemplateExistsById,
	})
}

// RuleSmsTemplateExistsById 通过短信模板ID检测模板是否存在
func RuleSmsTemplateExistsById(ctx context.Context, in gvalid.RuleFuncInput) error {
	// ID为0，无需验证，强制验证需结合：required
	if in.Value.Uint() == 0 {
		return nil
	}
	// 是否存在
	if by, err := service.SmsTpl().CheckTemplateExistsById(ctx, in.Value.Uint()); err != nil || !by {
		return gerror.New(`短信模板不存在`)
	}
	return nil
}
