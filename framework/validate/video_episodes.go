package validate

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/framework/service"
	"gitee.com/bobo-rs/innovideo-services/library/exception"
	"github.com/gogf/gf/v2/util/gvalid"
)

// RuleEpisodesExistsById 通过剧集ID检测视频剧集是否存在
func RuleEpisodesExistsById(ctx context.Context, in gvalid.RuleFuncInput) error {
	if in.Value.Uint() == 0 {
		return nil
	}
	// 视频剧集
	if b, _ := service.Video().EpisodesModel().ExistsPri(ctx, in.Value.Uint()); !b {
		return exception.New(`视频剧集不存在`)
	}
	return nil
}

// RuleResolutionExistsById 通过清晰度ID检测视频清晰度是否存在
func RuleResolutionExistsById(ctx context.Context, in gvalid.RuleFuncInput) error {
	if in.Value.Uint() == 0 {
		return nil
	}

	// 视频清晰度
	if b, _ := service.Video().ResolutionModel().ExistsPri(ctx, in.Value.Uint()); !b {
		return exception.New(`清晰度数据不存在`)
	}
	return nil
}
