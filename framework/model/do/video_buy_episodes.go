// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// VideoBuyEpisodes is the golang structure of table video_buy_episodes for DAO operations like Where/Data.
type VideoBuyEpisodes struct {
	g.Meta       `orm:"table:video_buy_episodes, do:true"`
	Id           interface{} // 视频购买单集ID
	BillId       interface{} // 视频购买账单ID
	BuyId        interface{} // 视频购买ID
	Uid          interface{} // 用户ID
	VideoId      interface{} // 视频ID
	EpisodesId   interface{} // 单集ID
	EpisodesName interface{} // 单集名称
	EpisodesNum  interface{} // 续集集数
	CreateAt     *gtime.Time // 创建时间
}
