// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// UserAdmin is the golang structure of table user_admin for DAO operations like Where/Data.
type UserAdmin struct {
	g.Meta        `orm:"table:user_admin, do:true"`
	Id            interface{} // 管理员ID
	UserId        interface{} // 用户ID
	ManageName    interface{} // 管理名称
	ManageNo      interface{} // 管理员编号
	IsSuperManage interface{} // 是否超管：0普管，1超管
	ManageStatus  interface{} // 管理员状态：0正常，1冻结，2离职，3违规，4注销
	LogoffAt      *gtime.Time // 注销时间
	UpdateAt      *gtime.Time // 更新时间
	CreateAt      *gtime.Time // 穿件时间
}
