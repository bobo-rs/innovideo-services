// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// AdminDepartment is the golang structure of table admin_department for DAO operations like Where/Data.
type AdminDepartment struct {
	g.Meta   `orm:"table:admin_department, do:true"`
	Id       interface{} // 管理员部门ID
	AdminId  interface{} // 管理员ID，非用户ID
	DepartId interface{} // 部门ID
	UpdateAt *gtime.Time // 更新时间
	CreateAt *gtime.Time // 创建时间
}
