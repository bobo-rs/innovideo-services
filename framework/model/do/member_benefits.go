// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// MemberBenefits is the golang structure of table member_benefits for DAO operations like Where/Data.
type MemberBenefits struct {
	g.Meta   `orm:"table:member_benefits, do:true"`
	Id       interface{} // 会员可享服务ID
	Title    interface{} // 服务名
	Content  interface{} // 内容
	Alias    interface{} // 服务别名，例如：video视频服务
	CreateAt *gtime.Time // 创建时间
}
