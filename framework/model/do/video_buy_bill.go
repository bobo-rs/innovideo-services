// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// VideoBuyBill is the golang structure of table video_buy_bill for DAO operations like Where/Data.
type VideoBuyBill struct {
	g.Meta          `orm:"table:video_buy_bill, do:true"`
	Id              interface{} // 视频购买账单ID
	BuyId           interface{} // 购买ID
	Uid             interface{} // 用户ID
	BuyPanguCoin    interface{} // 支付盘古币
	RefundPanguCoin interface{} // 退款盘古币
	BuyEpisodesNum  interface{} // 购买集数
	Content         interface{} // 购买内容，例如[视频名+集数，集数，集数。。。]
	Remark          interface{} // 退款备注
	Status          interface{} // 购买状态：0已购买，1已退款,2部分退款
	Type            interface{} // 购买类型：0单集购买，1全集购买，2余集买断
	RefundAt        *gtime.Time // 退款时间
	UpdateAt        *gtime.Time // 更新时间
	CreateAt        *gtime.Time // 创建时间
}
