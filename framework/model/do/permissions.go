// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// Permissions is the golang structure of table permissions for DAO operations like Where/Data.
type Permissions struct {
	g.Meta        `orm:"table:permissions, do:true"`
	Id            interface{} // 权限ID
	Name          interface{} // 权限名
	Description   interface{} // 权限详情
	MenuType      interface{} // 权限类型：0主菜单，1子菜单，2页面操作，3数据授权
	Icon          interface{} // Icon图
	Path          interface{} // 页面地址
	ApiPath       interface{} // 接口地址
	Pid           interface{} // 父级权限ID
	IsShow        interface{} // 菜单是否显示：0显示，1隐藏
	IsDisabled    interface{} // 是否禁用：0正常，1禁用
	PermCode      interface{} // 操作权限码
	DisableColumn interface{} // 禁用字段（3.数据授权类型）
	Sort          interface{} // 排序值0-255
	UpdateAt      *gtime.Time // 更新时间
	CreateAt      *gtime.Time // 创建时间
}
