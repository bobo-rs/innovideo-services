// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// MemberTiers is the golang structure of table member_tiers for DAO operations like Where/Data.
type MemberTiers struct {
	g.Meta      `orm:"table:member_tiers, do:true"`
	Id          interface{} // 会员身份ID
	Name        interface{} // 会员身份名
	Description interface{} // 会员服务描述
	Tiers       interface{} // 会员等级：0普通会员，1初级会员，2中级会员，3高级会员，4级顶级会员，5级超级会员，6至臻级会员
	MarketPrice interface{} // 原价
	Price       interface{} // 销售价
	ServiceDate interface{} // 服务时间期限
	ServiceUnit interface{} // 服务时间单位：d天，m月,y年
	Status      interface{} // 会员状态：0启用，1禁用
	SaleNum     interface{} // 销量
	ImageUrl    interface{} // 会员推广图
	BgColor     interface{} // 背景色
	Creator     interface{} // 创建人名
	CreatorId   interface{} // 创建人ID
	UpdateAt    *gtime.Time // 更新时间
	CreateAt    *gtime.Time // 创建时间
}
