// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// UserDevice is the golang structure of table user_device for DAO operations like Where/Data.
type UserDevice struct {
	g.Meta      `orm:"table:user_device, do:true"`
	Id          interface{} // 用户设备ID
	UserId      interface{} // 用户ID
	DeviceName  interface{} // 设备名称
	DeviceType  interface{} // 设备类型：iOS，安卓，H5，PC
	Mac         interface{} // MAC地址
	DeviceHash  interface{} // 设备Hash值[用户ID+设备名+类型+MAC]
	LastLoginAt *gtime.Time // 最后登录时间
	IsDisabled  interface{} // 是否禁用：0正常，1禁用
	UpdateAt    *gtime.Time // 更新时间
	CreateAt    *gtime.Time // 创建时间
}
