// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// VideoPersonal is the golang structure of table video_personal for DAO operations like Where/Data.
type VideoPersonal struct {
	g.Meta   `orm:"table:video_personal, do:true"`
	Id       interface{} // 视频相关人员ID
	VideoId  interface{} // 视频ID
	ActorsId interface{} // 演员ID
	Name     interface{} // 参演名
	Type     interface{} // 角色类型：0导演，1副导演，2领衔主演，3主演，4客串嘉宾，5其他
	Portray  interface{} // 饰演角色
	IsShow   interface{} // 是否展示：0展示，1隐藏
	UpdateAt *gtime.Time // 更新时间
	CreateAt *gtime.Time // 创建时间
}
