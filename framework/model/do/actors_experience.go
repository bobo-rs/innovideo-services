// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// ActorsExperience is the golang structure of table actors_experience for DAO operations like Where/Data.
type ActorsExperience struct {
	g.Meta   `orm:"table:actors_experience, do:true"`
	Id       interface{} // 个人经历ID
	ActorsId interface{} // 演员ID
	Content  interface{} // 经历内容
	ExpDate  *gtime.Time // 相关经历时间
	UpdateAt *gtime.Time // 更新时间
	CreateAt *gtime.Time // 创建时间
}
