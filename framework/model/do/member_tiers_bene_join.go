// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// MemberTiersBeneJoin is the golang structure of table member_tiers_bene_join for DAO operations like Where/Data.
type MemberTiersBeneJoin struct {
	g.Meta        `orm:"table:member_tiers_bene_join, do:true"`
	Id            interface{} // 会员关联ID
	BenefitsAlias interface{} // 会员可享服务别名
	MbTiersId     interface{} // 会员身份ID
	CreateAt      *gtime.Time // 创建时间
}
