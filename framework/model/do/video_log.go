// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// VideoLog is the golang structure of table video_log for DAO operations like Where/Data.
type VideoLog struct {
	g.Meta     `orm:"table:video_log, do:true"`
	Id         interface{} // 视频日志ID
	VideoId    interface{} // 视频ID
	Content    interface{} // 操作内容
	ManageId   interface{} // 管理员ID
	UserId     interface{} // 用户ID
	ManageName interface{} // 操作管理员名
	Url        interface{} // 操作URL
	CreateAt   *gtime.Time // 创建时间
}
