// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// SysMap is the golang structure of table sys_map for DAO operations like Where/Data.
type SysMap struct {
	g.Meta    `orm:"table:sys_map, do:true"`
	Id        interface{} // 系统字典ID
	Name      interface{} // 字典名，例如：sys_user_sex
	Explain   interface{} // 字典名解析值，例如：用户性别
	IsDisable interface{} // 是否禁用：0正常，1禁用
	UpdateAt  *gtime.Time // 更新时间
	CreateAt  *gtime.Time // 创建时间
}
