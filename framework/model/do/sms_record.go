// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// SmsRecord is the golang structure of table sms_record for DAO operations like Where/Data.
type SmsRecord struct {
	g.Meta     `orm:"table:sms_record, do:true"`
	Id         interface{} // 短信发送记录编号
	Mobile     interface{} // 接受短信的手机号
	Content    interface{} // 短信内容
	Code       interface{} // 验证码
	Ip         interface{} // 添加记录ip
	TemplateId interface{} // 短信模板ID
	SmsType    interface{} // 短信类型：0验证码，1消息通知，2营销短信
	SerialId   interface{} // 发送记录id
	Results    interface{} // 发送结果
	UpdateAt   *gtime.Time // 更新时间
	CreateAt   *gtime.Time // 创建时间
}
