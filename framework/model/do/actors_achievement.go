// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// ActorsAchievement is the golang structure of table actors_achievement for DAO operations like Where/Data.
type ActorsAchievement struct {
	g.Meta      `orm:"table:actors_achievement, do:true"`
	Id          interface{} // 演员个人成就ID
	ActorsId    interface{} // 演员ID
	AchTitle    interface{} // 成就Title
	AchieveDate *gtime.Time // 成就时间
	UpdateAt    *gtime.Time // 更新时间
	CreateAt    *gtime.Time // 创建时间
}
