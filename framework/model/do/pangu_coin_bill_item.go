// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// PanguCoinBillItem is the golang structure of table pangu_coin_bill_item for DAO operations like Where/Data.
type PanguCoinBillItem struct {
	g.Meta        `orm:"table:pangu_coin_bill_item, do:true"`
	Id            interface{} // 账单明细ID
	BillId        interface{} // 账单ID
	TransactionNo interface{} // 流水单号
	Uid           interface{} // 用户ID
	PanguCoin     interface{} // 消费盘古币
	Type          interface{} // 账单流水类型：0收入，1支出
	UpdateAt      *gtime.Time // 更新时间
	CreateAt      *gtime.Time // 创建时间
}
