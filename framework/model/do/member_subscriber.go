// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// MemberSubscriber is the golang structure of table member_subscriber for DAO operations like Where/Data.
type MemberSubscriber struct {
	g.Meta       `orm:"table:member_subscriber, do:true"`
	Id           interface{} // 会员订阅ID
	SubscriberNo interface{} // 订阅编号
	Uid          interface{} // 用户ID
	PayStatus    interface{} // 支付状态：0待支付，1已支付
	Status       interface{} // 订阅状态：0待确认，1服务中，2服务完成，3取消服务，4已退款
	PayCode      interface{} // 支付方式别名
	PayName      interface{} // 支付方式名
	DayFee       interface{} // 折扣每日单价=支付总额/订阅天数
	SubTotalNum  interface{} // 订阅天数
	PayFee       interface{} // 支付金额
	RefundFee    interface{} // 退款金额
	MbId         interface{} // 会员身份ID
	MbName       interface{} // 会员身份名
	SubStartDate *gtime.Time // 会员生效时间
	SubEndDate   *gtime.Time // 会员过期时间
	PayAt        *gtime.Time // 会员支付时间
	TradeNo      interface{} // 商户交易支付号
	PayNo        interface{} // 第三方支付交易号
	RefundNo     interface{} // 第三方退款交易号
	RefundAt     *gtime.Time // 退款时间
	Remark       interface{} // 备注
	UpdateAt     *gtime.Time // 更新时间
	CreateAt     *gtime.Time // 创建时间
}
