// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// VideoPlaySequel is the golang structure of table video_play_sequel for DAO operations like Where/Data.
type VideoPlaySequel struct {
	g.Meta        `orm:"table:video_play_sequel, do:true"`
	Id            interface{} // 播放明细ID
	PlayId        interface{} // 播放ID
	VideoSequelId interface{} // 视频续集ID
	CurrPlayRate  interface{} // 当前播放进度
	CurrPlayTime  interface{} // 当前播放时间
	PlaySequel    interface{} // 播放续集
	Resolution    interface{} // 当前播放精度：480P，720P，1080P等
	Ip            interface{} // 当前ip
	UpdateAt      *gtime.Time // 更新时间
	CreateAt      *gtime.Time // 创建时间
}
