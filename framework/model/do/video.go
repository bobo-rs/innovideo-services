// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// Video is the golang structure of table video for DAO operations like Where/Data.
type Video struct {
	g.Meta      `orm:"table:video, do:true"`
	Id          interface{} // 视频ID
	VideoNo     interface{} // 视频编号
	VideoName   interface{} // 视频名
	Keywords    interface{} // 关键词
	CateId1     interface{} // 一级类目
	CateId2     interface{} // 二级类目
	Region      interface{} // 地区
	YearDate    interface{} // 上映年份
	ReleaseDate *gtime.Time // 上映时间
	IsRecommend interface{} // 是否推荐：0否，1是
	IsBest      interface{} // 是否精品：0否，1是
	IsTop       interface{} // 是否置顶：0否，1是
	IsNew       interface{} // 是否新剧：0否，1是
	IsHot       interface{} // 是否热播：0否，1是
	FeeType     interface{} // 收费类型：0免费，1VIP付费
	ImageUrl    interface{} // 海报
	Browse      interface{} // 浏览量
	PlayNum     interface{} // 播放量
	Collect     interface{} // 收藏量
	ShareNum    interface{} // 分享量
	Score       interface{} // 评分，最高分10分
	Status      interface{} // 视频状态：0待审核，1审核通过，2驳回申请，3违规下架, 4视频删除
	IsRelease   interface{} // 是否发布：0未发布，1已发布
	UpdateAt    *gtime.Time // 更新时间
	CreateAt    *gtime.Time // 创建时间
}
