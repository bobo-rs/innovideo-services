// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// VideoLabel is the golang structure of table video_label for DAO operations like Where/Data.
type VideoLabel struct {
	g.Meta   `orm:"table:video_label, do:true"`
	Id       interface{} // 视频标签ID
	VideoId  interface{} // 视频ID
	Value    interface{} // 标签值
	CreateAt *gtime.Time // 创建时间
}
