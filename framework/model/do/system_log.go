// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// SystemLog is the golang structure of table system_log for DAO operations like Where/Data.
type SystemLog struct {
	g.Meta       `orm:"table:system_log, do:true"`
	Id           interface{} // 日志id
	ModuleName   interface{} // 操作模块名
	Content      interface{} // 操作详情
	TagName      interface{} // TAG栏目名，例如：系统管理
	Ip           interface{} // 操作IP地址
	OperateType  interface{} // 日志操作类型：0操作日志，1登录操作，2系统日志
	ApiUrl       interface{} // api URL
	UserId       interface{} // 用户ID
	ManageId     interface{} // 账户id
	ManageName   interface{} // 账户名
	MerchantId   interface{} // 商户ID
	ResponseText interface{} // 资源响应
	ParamText    interface{} // 请求资源
	UpdateAt     *gtime.Time // 更新时间
	CreateAt     *gtime.Time // 创建时间
}
