// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// SysMapValue is the golang structure of table sys_map_value for DAO operations like Where/Data.
type SysMapValue struct {
	g.Meta   `orm:"table:sys_map_value, do:true"`
	Id       interface{} // 字典值ID
	Value    interface{} // 字典值
	Explain  interface{} // 字典值解析
	Name     interface{} // 字典名-sys_map[name]
	UpdateAt *gtime.Time // 更新时间
	CreateAt *gtime.Time // 创建时间
}
