// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// UserCredential is the golang structure of table user_credential for DAO operations like Where/Data.
type UserCredential struct {
	g.Meta          `orm:"table:user_credential, do:true"`
	Id              interface{} // 用户证件ID
	UserId          interface{} // 用户ID
	IdCard          interface{} // 证件编号
	JustUrl         interface{} // 证件正面URL地址
	OppositeUrl     interface{} // 证件反面URL地址
	Type            interface{} // 证件类型：0身份证
	ExpireType      interface{} // 证件效期类型：0有效期，1长期
	ExpireStartDate *gtime.Time // 证件注册时间
	ExpireEndDate   *gtime.Time // 证件到期时间
	UpdateAt        *gtime.Time // 更新时间
	CreateAt        *gtime.Time // 创建时间
}
