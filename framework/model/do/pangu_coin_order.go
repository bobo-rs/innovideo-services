// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// PanguCoinOrder is the golang structure of table pangu_coin_order for DAO operations like Where/Data.
type PanguCoinOrder struct {
	g.Meta          `orm:"table:pangu_coin_order, do:true"`
	Id              interface{} // 盘古币订单ID
	Uid             interface{} // 用户ID
	OrderNo         interface{} // 订单号
	TradeNo         interface{} // 商户交易号
	PayNo           interface{} // 第三方支付交易号
	RefundNo        interface{} // 退款交易号
	PayCode         interface{} // 支付别名
	PayName         interface{} // 支付名
	PayFee          interface{} // 支付金额
	RefundFee       interface{} // 退款金额
	PanguCoin       interface{} // 兑换盘古币数量
	RefundPanguCoin interface{} // 退回盘古币数量
	Currency        interface{} // 当前支付货币，默认人民币CNY
	PayAt           *gtime.Time // 支付时间
	RefundAt        *gtime.Time // 退款时间
	PayStatus       interface{} // 支付状态：0待支付，1已支付
	Status          interface{} // 状态：0待确认，1已完成，2已取消，3已退款
	Remark          interface{} // 备注
	Content         interface{} // 描述内容
	UpdateAt        *gtime.Time // 更新时间
	CreateAt        *gtime.Time // 创建时间
}
