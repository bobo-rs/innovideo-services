// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// ArticleCategory is the golang structure of table article_category for DAO operations like Where/Data.
type ArticleCategory struct {
	g.Meta      `orm:"table:article_category, do:true"`
	Id          interface{} // 文章类目ID
	Name        interface{} // 文章类目名
	Description interface{} // 详情
	Pid         interface{} // 父级ID
	Status      interface{} // 状态：0正常，1禁用
	IsShow      interface{} // 是否显示：0显示，1隐藏
	UpdateAt    *gtime.Time // 更新时间
	CreateAt    *gtime.Time // 创建时间
}
