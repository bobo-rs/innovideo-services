// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// VideoResolution is the golang structure of table video_resolution for DAO operations like Where/Data.
type VideoResolution struct {
	g.Meta         `orm:"table:video_resolution, do:true"`
	Id             interface{} // 视频清晰度ID
	EpisodesId     interface{} // 视频续集ID
	Resolution     interface{} // 清晰度：480P，720P，1080P等
	AttachId       interface{} // 附件ID
	VideoUrl       interface{} // 视频地址
	Size           interface{} // 视频大小（B）
	Duration       interface{} // 时长（S）
	DurationString interface{} // 格式化时长
	MimeType       interface{} // 视频类型
	UpdateAt       *gtime.Time // 更新时间
	CreateAt       *gtime.Time // 创建时间
}
