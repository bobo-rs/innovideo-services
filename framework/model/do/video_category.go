// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// VideoCategory is the golang structure of table video_category for DAO operations like Where/Data.
type VideoCategory struct {
	g.Meta      `orm:"table:video_category, do:true"`
	Id          interface{} // 视频分类ID
	Name        interface{} // 分类名
	TinyName    interface{} // 短标题
	Pid         interface{} // 父级ID
	Sort        interface{} // 排序：升序排序0-99999
	IsShow      interface{} // 是否显示
	IsRecommend interface{} // 是否推荐首页：0否，1是
	UpdateAt    *gtime.Time // 更新时间
	CreateAt    *gtime.Time // 创建时间
}
