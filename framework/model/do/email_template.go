// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// EmailTemplate is the golang structure of table email_template for DAO operations like Where/Data.
type EmailTemplate struct {
	g.Meta   `orm:"table:email_template, do:true"`
	Id       interface{} // 邮件模板ID
	Name     interface{} // 模板名
	VarName  interface{} // 模板变量名，例如：login_captcha[登录验证码]
	Subject  interface{} // 主题
	Content  interface{} // 邮件内容，变量{1}{2}等大括号包含数字
	UpdateAt *gtime.Time // 更新时间
	CreateAt *gtime.Time // 创建时间
}
