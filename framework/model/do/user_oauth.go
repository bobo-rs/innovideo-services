// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// UserOauth is the golang structure of table user_oauth for DAO operations like Where/Data.
type UserOauth struct {
	g.Meta   `orm:"table:user_oauth, do:true"`
	Id       interface{} // 用户授权ID
	Uid      interface{} // 用户ID
	Openid   interface{} // 授权openid
	Unionid  interface{} // 微信unionid
	Oauth    interface{} // 授权渠道，如：weixin
	CreateAt *gtime.Time // 创建时间
}
