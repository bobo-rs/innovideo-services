// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// PermissonsAssoc is the golang structure of table permissons_assoc for DAO operations like Where/Data.
type PermissonsAssoc struct {
	g.Meta        `orm:"table:permissons_assoc, do:true"`
	Id            interface{} // 角色权限关联ID
	AssocId       interface{} // 角色ID
	PermissionsId interface{} // 权限ID
	AssocType     interface{} // 关联类型：0角色关联，1账户关联
	CreateAt      *gtime.Time // 创建时间
}
