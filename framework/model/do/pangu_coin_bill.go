// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// PanguCoinBill is the golang structure of table pangu_coin_bill for DAO operations like Where/Data.
type PanguCoinBill struct {
	g.Meta             `orm:"table:pangu_coin_bill, do:true"`
	Id                 interface{} // 盘古币账单
	Uid                interface{} // 用户ID
	OrderId            interface{} // 盘古币充值订单ID
	BillNo             interface{} // 账单编号
	TotalPanguCoin     interface{} // 初始盘古币-留存记录不变
	AvailablePanguCoin interface{} // 可用盘古币
	ExpiredPanguCoin   interface{} // 过期盘古币
	ExpiredDate        *gtime.Time // 过期时间
	Status             interface{} // 盘古币账单状态：0正常，1交易完成，2部分过期，3全额过期，4部分退款，5全额退款
	BillType           interface{} // 账单类型：0充值兑换，1活动赠送，2平台赠送，3新用户赠送, 4售后退回
	Remark             interface{} // 备注
	UpdateAt           *gtime.Time // 更新时间
	CreateAt           *gtime.Time // 创建时间
}
