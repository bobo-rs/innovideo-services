// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// ActorsWorks is the golang structure of table actors_works for DAO operations like Where/Data.
type ActorsWorks struct {
	g.Meta         `orm:"table:actors_works, do:true"`
	Id             interface{} // 演员作品ID
	ActorsId       interface{} // 演员ID
	WorksName      interface{} // 作品名
	Type           interface{} // 类型：0主演，1领衔主演，2客串嘉宾，3群众
	IsMasterpieces interface{} // 是否代表作：0否，1是
	Portray        interface{} // 饰演角色
	UpdateAt       *gtime.Time // 更新时间
	CreateAt       *gtime.Time // 创建时间
}
