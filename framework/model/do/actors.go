// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// Actors is the golang structure of table actors for DAO operations like Where/Data.
type Actors struct {
	g.Meta        `orm:"table:actors, do:true"`
	Id            interface{} // 演员ID
	Name          interface{} // 姓名
	StageName     interface{} // 艺名
	FormerName    interface{} // 曾用名
	Country       interface{} // 国籍
	NativePlace   interface{} // 祖籍
	PlaceBirth    interface{} // 出生地
	BloodType     interface{} // 血型
	Constellation interface{} // 星座
	Zodiac        interface{} // 生肖
	Birthday      *gtime.Time // 出生日期
	Sex           interface{} // 性别：0男，1女
	Height        interface{} // 身高（CM）
	Weight        interface{} // 体重（KG）
	DebutAt       *gtime.Time // 出道时间
	Synopsis      interface{} // 简介
	ImageUrl      interface{} // 头像
	WorksNum      interface{} // 作品数
	RwNum         interface{} // 代表作品数
	Nation        interface{} // 民族
	GradInst      interface{} // 毕业院校
	GradDate      *gtime.Time // 毕业时间
	UpdateAt      *gtime.Time // 更新时间
	CreateAt      *gtime.Time // 创建时间
}
