// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// PanguCoinTransaction is the golang structure of table pangu_coin_transaction for DAO operations like Where/Data.
type PanguCoinTransaction struct {
	g.Meta          `orm:"table:pangu_coin_transaction, do:true"`
	Id              interface{} // 盘古币流水ID
	Uid             interface{} // 用户ID
	TransactionNo   interface{} // 流水号
	BeforePanguCoin interface{} // 变动前盘古币
	PanguCoin       interface{} // 变动盘古币
	AfterPanguCoin  interface{} // 变动后盘古币（当前数量）
	Type            interface{} // 流水类型：0收入，1支出
	Remark          interface{} // 备注
	CreateAt        *gtime.Time // 流水记录时间
}
