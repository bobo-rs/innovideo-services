// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// AdminInvite is the golang structure of table admin_invite for DAO operations like Where/Data.
type AdminInvite struct {
	g.Meta        `orm:"table:admin_invite, do:true"`
	Id            interface{} // 邀请ID
	Token         interface{} // 邀请Token
	AdminId       interface{} // 邀请管理员ID
	ManageName    interface{} // 邀请管理员名
	Expired       *gtime.Time // 过期时间
	Status        interface{} // 状态：0邀请中，1已邀请，2已撤销
	Uid           interface{} // 被邀请人用户ID
	RefreshNum    interface{} // 刷新次数
	DepartId      interface{} // 部门ID
	IsSuperManage interface{} // 是否超管：0普管，1超管
	UpdateAt      *gtime.Time // 更新时间
	CreateAt      *gtime.Time // 创建时间
}
