// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// Attachment is the golang structure of table attachment for DAO operations like Where/Data.
type Attachment struct {
	g.Meta         `orm:"table:attachment, do:true"`
	Id             interface{} // 附件ID
	CatId          interface{} // 附件分类
	Name           interface{} // 附件名
	RealName       interface{} // 附件原名
	AttSize        interface{} // 附件尺寸大小
	AttPath        interface{} // 附件路径
	AttUrl         interface{} // 附件地址
	AttType        interface{} // 附件类型：image/png
	Position       interface{} // 排序位置：从小到大
	ChannelId      interface{} // 渠道ID
	StorageType    interface{} // 存储类型：local本地，qiniu七牛等
	Hasher         interface{} // Hash值
	AttachId       interface{} // 附件ID
	Width          interface{} // 宽度
	Height         interface{} // 高度
	Duration       interface{} // 视频时长（S）
	DurationString interface{} // 格式化视频时长
	UpdateAt       *gtime.Time // 更新时间
	CreateAt       *gtime.Time // 创建时间
}
