// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// VideoBuy is the golang structure of table video_buy for DAO operations like Where/Data.
type VideoBuy struct {
	g.Meta            `orm:"table:video_buy, do:true"`
	Id                interface{} // 视频购买ID
	Uid               interface{} // 用户ID
	VideoId           interface{} // 视频ID
	BuyNo             interface{} // 视频购买编号
	VideoName         interface{} // 视频名称
	CompletePanguCoin interface{} // 全集盘古币数量
	AddPanguCoin      interface{} // 已累计消费盘古币数量
	Status            interface{} // 状态：0购买中，1已全剧购买
	UpdateAt          *gtime.Time // 更新时间
	CreateAt          *gtime.Time // 创建时间
}
