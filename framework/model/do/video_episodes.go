// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// VideoEpisodes is the golang structure of table video_episodes for DAO operations like Where/Data.
type VideoEpisodes struct {
	g.Meta        `orm:"table:video_episodes, do:true"`
	Id            interface{} // 视频集数ID
	VideoId       interface{} // 视频ID
	AttachId      interface{} // 附件ID
	ImageUrl      interface{} // 当前集数海报
	EpisodesName  interface{} // 续集标题
	Synopsis      interface{} // 简介
	EpisodesNum   interface{} // 集数
	ResolutionSet interface{} // 清晰度集合：480P，720P，1080P等
	PlayNum       interface{} // 播放次数
	UpdateAt      *gtime.Time // 更新时间
	CreateAt      *gtime.Time // 创建时间
}
