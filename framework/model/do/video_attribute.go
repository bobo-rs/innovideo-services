// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// VideoAttribute is the golang structure of table video_attribute for DAO operations like Where/Data.
type VideoAttribute struct {
	g.Meta            `orm:"table:video_attribute, do:true"`
	VideoId           interface{} // 视频ID
	Synopsis          interface{} // 简介概要
	EpisodesNum       interface{} // 总集数
	UpStatus          interface{} // 视频更新状态：0更新中，1已完结
	Description       interface{} // 视频宣传详情
	ShareTitle        interface{} // 分享视频名
	ShareContent      interface{} // 分享视频内容
	IsSeries          interface{} // 是否连续剧：0单剧，1连续剧
	FreePlayNum       interface{} // VIP视频免费播放集数
	TrialDuration     interface{} // 试看时长
	Lang              interface{} // 语言类型：中英双文
	CompletePanguCoin interface{} // 全集购买盘古币
	SinglePanguCoin   interface{} // 单集购买币
	AttachId          interface{} // 附件ID
	UpdateAt          *gtime.Time // 更新时间
}
