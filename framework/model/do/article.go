// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// Article is the golang structure of table article for DAO operations like Where/Data.
type Article struct {
	g.Meta      `orm:"table:article, do:true"`
	Id          interface{} // 文章ID
	Name        interface{} // 文章名
	AbbrevName  interface{} // 文章缩写名
	Description interface{} // 文章摘要
	Cid         interface{} // 文章分类ID
	Keywords    interface{} // 关键词
	ImgUrl      interface{} // 图片地址
	VideoUrl    interface{} // 视频地址
	Content     interface{} // 文章内容
	Status      interface{} // 状态：0正常，1禁用
	IsShow      interface{} // 是否显示：0正常，1隐藏
	Sort        interface{} // 排序值：0-99999升序
	AssocAlias  interface{} // 关联别名，例如：注册协议、网服协议等
	UpdateAt    *gtime.Time // 更新时间
	CreateAt    *gtime.Time // 创建时间
}
