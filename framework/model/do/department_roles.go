// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// DepartmentRoles is the golang structure of table department_roles for DAO operations like Where/Data.
type DepartmentRoles struct {
	g.Meta   `orm:"table:department_roles, do:true"`
	Id       interface{} // 部门角色关联ID
	RoleId   interface{} // 角色ID
	DepartId interface{} // 部门ID
	CreateAt *gtime.Time // 创建时间
}
