// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// VideoPlay is the golang structure of table video_play for DAO operations like Where/Data.
type VideoPlay struct {
	g.Meta         `orm:"table:video_play, do:true"`
	Id             interface{} // 播放ID
	VideoId        interface{} // 视频ID
	UserId         interface{} // 用户ID
	CurrPlayRate   interface{} // 当前播放进度，最大100，最小0
	CurrPlayTime   interface{} // 当前播放时间
	CurrPlaySequel interface{} // 当前播放续集
	PlayTime       interface{} // 播放时间
	Resolution     interface{} // 当前播放精度：480P，720P，1080P等
	Ip             interface{} // 当前播放IP
	UpdateAt       *gtime.Time // 更新时间
	CreateAt       *gtime.Time // 创建时间
}
