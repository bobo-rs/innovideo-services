// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// UserFunds is the golang structure of table user_funds for DAO operations like Where/Data.
type UserFunds struct {
	g.Meta                `orm:"table:user_funds, do:true"`
	UserId                interface{} // 用户ID
	RechargeTotalFee      interface{} // 充值总金额
	AvailableFee          interface{} // 可用金额
	FreezeFee             interface{} // 冻结金额
	PanguCoinTotal        interface{} // 兑换盘古币总数
	AvailablePanguCoin    interface{} // 可用盘古币数
	FreezePanguCoin       interface{} // 冻结盘古币数
	ConsumeTotalPanguCoin interface{} // 消费总盘古币数
	ConsumeTotalFee       interface{} // 消费总金额
	UpdateAt              *gtime.Time // 更新时间
}
