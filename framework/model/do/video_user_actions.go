// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// VideoUserActions is the golang structure of table video_user_actions for DAO operations like Where/Data.
type VideoUserActions struct {
	g.Meta   `orm:"table:video_user_actions, do:true"`
	Id       interface{} // 视频收藏ID
	VideoId  interface{} // 视频ID
	UserId   interface{} // 用户ID
	Ip       interface{} // 当前ip
	Type     interface{} // 操作类型：C收藏，L点赞，T踩
	CreateAt *gtime.Time // 创建时间
}
