package model

import (
	"gitee.com/bobo-rs/innovideo-services/enums"
	"gitee.com/bobo-rs/innovideo-services/framework/model/entity"
)

type (
	// UserAvailablePGCoinListOutput 获取用户可用盘古币账单列表输出结果
	UserAvailablePGCoinListOutput struct {
		TotalPGCoin uint                      `json:"total_pg_coin" dc:"可用盘古币总数"`
		Rows        []UserAvailablePGCoinItem `json:"rows" dc:"数据列表"`
	}

	// PGCoinBillListInput 获取盘古币账单列表请求参数
	PGCoinBillListInput struct {
		CommonPaginationItem
		PGCoinBillWhere
	}

	// PGCoinBillListOutput 获取盘古币账单列表响应结果
	PGCoinBillListOutput struct {
		CommonResponseItem
		Rows []PGCoinBillItem `json:"rows" dc:"数据列表"`
	}

	// PGCoinBillDetailOutput 获取盘古币账单详情响应结果
	PGCoinBillDetailOutput struct {
		PGCoinBillDetailItem
		Items []entity.PanguCoinBillItem `json:"items" dc:"账单明细"`
	}

	// UpdateBillExpiredInput 更新盘古币账单效期请求参数
	UpdateBillExpiredInput struct {
		Id        uint   `json:"id" dc:"账单ID"`
		ExpiredAt string `json:"expired_at" dc:"账单到期时间"`
		Remark    string `json:"remark" dc:"备注"`
	}
)

type (
	// UserAvailablePGCoinItem 用户可用盘古币账单明细属性
	UserAvailablePGCoinItem struct {
		Id                 uint `json:"id" dc:"账单ID"`
		AvailablePanguCoin uint `json:"available_pangu_coin" dc:"可用盘古币数量"`
	}

	// PGCoinBillWhere 盘古币账单条件属性
	PGCoinBillWhere struct {
		Uid    uint                   `json:"uid" dc:"用户ID"`
		BillNo string                 `json:"bill_no" dc:"账单编号"`
		Status enums.PGCoinBillStatus `json:"status" dc:"账单状态"`
		Type   enums.PGCoinBillType   `json:"type" dc:"账单类型"`
	}

	// PGCoinBillDetailItem 盘古币账单详情属性
	PGCoinBillDetailItem struct {
		PGCoinBillItem
		Uid    uint   `json:"uid" dc:"用户ID"`
		Remark string `json:"remark" dc:"备注"`
	}

	// PGCoinBillItem 盘古币账单属性
	PGCoinBillItem struct {
		Id                 uint                   `json:"id" dc:"账单ID"`
		OrderId            uint                   `json:"order_id" dc:"订单ID"`
		BillNo             string                 `json:"bill_no" dc:"账单编号"`
		TotalPanguCoin     uint                   `json:"total_pangu_coin" dc:"原始兑换盘古币"`
		AvailablePanguCoin uint                   `json:"available_pangu_coin" dc:"可用盘古币"`
		ExpiredPanguCoin   uint                   `json:"expired_pangu_coin" dc:"过期盘古币"`
		ExpiredDate        string                 `json:"expired_date" dc:"到期时间"`
		Status             enums.PGCoinBillStatus `json:"status" dc:"账单状态"`
		BillType           enums.PGCoinBillType   `json:"bill_type" dc:"账单类型"`
		CreateAt           string                 `json:"create_at" dc:"创建时间"`
	}
)
