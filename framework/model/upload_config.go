package model

type (
	// UploadConfigItem 上传配置属性
	UploadConfigItem struct {
		StorageType string                 `json:"storage_type" dc:"存储类型"`
		Config      map[string]interface{} `json:"config" dc:"配置内容"`
	}

	// UploadConfigLocalItem 本地上传配置
	UploadConfigLocalItem struct {
		StorageType string `json:"storage_type" dc:"存储类型"`
		Root        string `json:"root" dc:"上传根目录"`
		Path        string `json:"url" dc:"上传附件地址"`
		FileExt     string `json:"file_ext" dc:"文件上传控制"`
	}
)
