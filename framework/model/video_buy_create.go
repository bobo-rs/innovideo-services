package model

import "gitee.com/bobo-rs/innovideo-services/enums"

type (
	// CreateVideoPurchaseInput 创建购买视频请求参数
	CreateVideoPurchaseInput struct {
		VideoId       uint               `json:"video_id" dc:"视频ID"`
		BuyType       enums.VideoBuyType `json:"buy_type" dc:"购买类型"`
		EpisodesIdSet []uint             `json:"episodes_id_set" dc:"剧集ID集合：单集购买必传"`
	}

	// GenVideoSaleBillItem 生成视频购买账单数据
	GenVideoSaleBillItem struct {
		BuyType       enums.VideoBuyType
		AuthDetail    VideoDetailAuthItem
		Uid           uint
		ConsumedCoins uint
		Episodes      []VideoEpisodesBuyItem
		BuyStatus     enums.VideoBuyStatus
	}
)
