package model

type (

	// VideoLabelItem 是否标签ID
	VideoLabelItem struct {
		VideoId uint   `json:"video_id" dc:"视频ID"`
		Value   string `json:"value" dc:"标签值"`
	}
)
