package model

type (
	// AttributeAuthItem 视频属性授权属性
	AttributeAuthItem struct {
		EpisodesNum       uint `json:"episodes_num" dc:"总集数"`
		UpEpisodesNum     int  `json:"up_episodes_num" dc:"已上传集数"`
		IsSeries          uint `json:"is_series" dc:"是否连续剧：0单剧，1连续剧"`
		FreePlayNum       uint `json:"free_play_num" dc:"免费播放集数（连续剧）"`
		TrialDuration     uint `json:"trial_duration" dc:"试看时长（单剧）"`
		CompletePanguCoin uint `json:"complete_pangu_coin" dc:"全集购买盘古币"`
		SinglePanguCoin   uint `json:"single_pangu_coin" dc:"单集购买盘古币"`
	}
)
