package model

type (
	// UserAvailableFundsDetailItem 用户可用资金详情
	UserAvailableFundsDetailItem struct {
		AvailableFee       float64 `json:"available_fee" dc:"可用资金余额"`
		AvailablePanguCoin uint    `json:"available_pangu_coin" dc:"可用盘古币数量"`
	}
)
