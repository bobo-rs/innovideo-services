package model

import "gitee.com/bobo-rs/innovideo-services/framework/model/entity"

type (
	// SmsTemplateListInput 获取短信模板列表请求参数
	SmsTemplateListInput struct {
		CommonPaginationItem
		SmsTemplateWhereItem
	}

	// SmsTemplateListOutput 获取短信模板列表输出结果
	SmsTemplateListOutput struct {
		CommonResponseItem
		Rows []SmsTemplateItem `json:"rows" dc:"短信数据列表"`
	}

	// SmsTemplateDetailOutput 获取短信模板详情输出结果
	SmsTemplateDetailOutput struct {
		SmsTemplateItem
	}
)

type (

	// CurrTemplateItem 获取当前模板内容和ID
	CurrTemplateItem struct {
		Name       string `json:"name" dc:"模板名称"`
		Content    string `json:"content" dc:"模板内容"`
		TemplateId string `json:"template_id" dc:"外部平台模板ID"`
	}

	// SmsTemplateWhereItem 短信模板搜索条件
	SmsTemplateWhereItem struct {
		Name    string `json:"name" dc:"模板名"`
		Alias   string `json:"alias" dc:"模板变量别名"`
		Status  int    `json:"status" dc:"模板状态：0关闭，1启用"`
		SmsType int    `json:"sms_type" dc:"短信类型：0验证码，1消息通知，2营销短信"`
	}

	// SmsTemplateItem 短信模板属性
	SmsTemplateItem struct {
		entity.SmsTemplate
		TmplOut    SmsOutItem `json:"tmpl_out" dc:"外部短信平台属性"`
		FmtAlias   string     `json:"fmt_alias" dc:"模板别名"`
		FmtStatus  string     `json:"fmt_status" dc:"状态"`
		FmtSmsType string     `json:"fmt_sms_type" dc:"短信类型"`
	}
)
