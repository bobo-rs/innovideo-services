package model

import "gitee.com/bobo-rs/innovideo-services/enums"

type (
	// PGCoinOrderListInput 获取盘古币订单列表
	PGCoinOrderListInput struct {
		CommonPaginationItem
		PGCoinOrderWhere
	}

	// PGCoinUserOrderListInput 获取用户盘古币订单列表
	PGCoinUserOrderListInput struct {
		CommonPaginationItem
		TABType enums.PGCoinUserTAB `json:"tab_type" dc:"TAB列表类型"`
	}

	// PGCoinOrderListOutput 获取盘古币订单列表
	PGCoinOrderListOutput struct {
		CommonResponseItem
		Rows []PGCoinOrderListItem `json:"rows" dc:"数据列表"`
	}
)

type (
	// PGCoinOrderWhere 盘古币订单搜索条件
	PGCoinOrderWhere struct {
		Id        uint                    `json:"id" dc:"充值订单ID"`
		Uid       uint                    `json:"uid" dc:"用户ID"`
		OrderNo   string                  `json:"order_no" dc:"订单号"`
		PayNo     string                  `json:"pay_no" dc:"第三方支付交易号"`
		TradeNo   string                  `json:"trade_no" dc:"商户交易号"`
		RefundNo  string                  `json:"refund_no" dc:"第三方退款交易号"`
		PayCode   string                  `json:"pay_code" dc:"支付方式"`
		PayAt     string                  `json:"pay_at" dc:"支付时间"`
		RefundAt  string                  `json:"refund_at" dc:"退款时间"`
		Status    enums.PGCoinOrderStatus `json:"status" dc:"订单状态"`
		PayStatus enums.PGCoinOrderPay    `json:"pay_status" dc:"支付状态"`
		CommonSearchDateItem
	}

	// PGCoinOrderListItem 盘古币订单列表属性
	PGCoinOrderListItem struct {
		PGCoinOrderBasicItem
		PGCoinOrderNoSetItem
		PGCoinOrderFeeItem
		PGCoinOrderAtItem
	}

	// PGCoinOrderDetailItem 盘古币订单详情
	PGCoinOrderDetailItem struct {
		PGCoinOrderBasicItem
		PGCoinOrderNoSetItem
		PGCoinOrderFeeItem
		PGCoinOrderAtItem
		PGCoinOrderContentItem
		PGCoinOrderStatusItem
	}

	// PGCoinOrderBasicItem 盘古币订单基础信息
	PGCoinOrderBasicItem struct {
		Id      uint   `json:"id" dc:"盘古币订单ID"`
		Uid     uint   `json:"uid" dc:"用户ID"`
		PayCode string `json:"pay_code" dc:"支付方式"`
		PayName string `json:"pay_name" dc:"支付名称"`
	}

	// PGCoinOrderNoSetItem 盘古币订单编号集合
	PGCoinOrderNoSetItem struct {
		OrderNo  string `json:"order_no" dc:"订单号"`
		PayNo    string `json:"pay_no" dc:"第三方支付交易号"`
		TradeNo  string `json:"trade_no" dc:"商户交易号"`
		RefundNo string `json:"refund_no" dc:"第三方退款交易号"`
	}

	// PGCoinOrderFeeItem 盘古币订单金额
	PGCoinOrderFeeItem struct {
		PayFee          float64 `json:"pay_fee" dc:"支付金额"`
		RefundFee       float64 `json:"refund_fee" dc:"退款金额"`
		PanguCoin       uint    `json:"pangu_coin" dc:"兑换盘古币数量"`
		RefundPanguCoin uint    `json:"refund_pangu_coin" dc:"退款盘古币数量"`
		Currency        string  `json:"currency" dc:"货币符号"`
	}

	// PGCoinOrderAtItem 盘古币订单时间
	PGCoinOrderAtItem struct {
		PayAt    string `json:"pay_at" dc:"支付时间"`
		RefundAt string `json:"refund_at" dc:"退款时间"`
		CreateAt string `json:"create_at" dc:"创建时间"`
	}

	// PGCoinOrderContentItem PGCoinOrderContentItem 盘古币订单内容
	PGCoinOrderContentItem struct {
		Content string `json:"content" dc:"内容"`
		Remark  string `json:"remark" dc:"备注"`
	}

	// PGCoinOrderStatusItem 盘古币订单状态
	PGCoinOrderStatusItem struct {
		Status    enums.PGCoinOrderStatus `json:"status" dc:"订单状态"`
		PayStatus enums.PGCoinOrderPay    `json:"pay_status" dc:"支付状态"`
	}
)
