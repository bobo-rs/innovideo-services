package model

import (
	"gitee.com/bobo-rs/creative-framework/pkg/jwt"
	"gitee.com/bobo-rs/innovideo-services/enums"
	"gitee.com/bobo-rs/innovideo-services/framework/model/entity"
)

type (
	// UserOauthLoginInput 用户认证登录授权请求参数
	UserOauthLoginInput struct {
		UserOauthAccountLoginItem
		UserOauthSendSmsCaptchaItem
		OauthEmail
		OauthCaptchaCode
		UserOauthLoginSignItem
		*UserOauthOtherParam
		Oauth enums.UserOauth `json:"oauth" dc:"登录授权类型"`
	}

	// UserOauthLoginOutput 用户认证登录授权响应参数
	UserOauthLoginOutput struct {
		UserOauthLoginItem
	}

	// UserLoginSendSmsCaptchaInput 用户授权登录发送短信验证码
	UserLoginSendSmsCaptchaInput struct {
		UserOauthSendSmsCaptchaItem
		UserOauthLoginSignItem
	}

	// LoginSendEmailCaptchaInput 用户授权登录邮箱验证码
	LoginSendEmailCaptchaInput struct {
		OauthEmail
		UserOauthLoginSignItem
	}
)

type (
	// UserOauthAccountLoginItem 用户认证账户密码登录属性
	UserOauthAccountLoginItem struct {
		Account string `json:"account" dc:"登录账户"`
		Pwd     string `json:"pwd" dc:"登录密码"`
	}

	// UserOauthSmsLoginItem 用户认证短信验证码登录
	UserOauthSmsLoginItem struct {
		UserOauthSendSmsCaptchaItem
		OauthCaptchaCode
	}

	// UserOauthEmailCaptchaLogin 邮箱验证码登录
	UserOauthEmailCaptchaLogin struct {
		OauthEmail
		OauthCaptchaCode
	}

	// OauthCaptchaCode 登录认证验证码
	OauthCaptchaCode struct {
		Code string `json:"code" dc:"邮箱验证码"`
	}

	// UserOauthSendSmsCaptchaItem 用户认证登录发送短信验证码
	UserOauthSendSmsCaptchaItem struct {
		Mobile string `json:"mobile" dc:"手机号-加密后"`
	}

	// OauthEmail 授权认证邮箱
	OauthEmail struct {
		Email string `json:"email" dc:"邮箱-加密后"`
	}

	// UserOauthLoginSignItem 用户认证登录签名属性
	UserOauthLoginSignItem struct {
		Sign string `json:"sign" dc:"登录签名"`
	}

	// UserOauthLoginItem 用户认证登录授权属性
	UserOauthLoginItem struct {
		*jwt.TokenItem
		Status enums.UserLoginStatus `json:"status" dc:"登录状态：0正常，1待绑定账户，2待绑定手机号，3新设备"`
	}

	// UserOauthLoginDetailItem 用户认证授权登录基础详情属性
	UserOauthLoginDetailItem struct {
		*entity.User
		Status enums.UserLoginStatus `json:"status" dc:"登录状态：0正常，1待设置密码，2待绑定手机号，3新设备"`
	}

	// UserOauthAfterValidateItem 用户认证后置验证登录信息
	UserOauthAfterValidateItem struct {
		UserOauthLoginDetailItem
		UserDeviceLoginAfterItem
	}

	// UserOauthAfterItem 用户认证后置数据属性
	UserOauthAfterItem struct {
		Detail *UserOauthLoginDetailItem `json:"detail" dc:"用户认证授权登录基础详情属性"`
		Other  *UserOauthOtherParam      `json:"other" dc:"用户认证其他参数"`
	}

	// UserOauthOtherParam 用户认证其他参数
	UserOauthOtherParam struct {
		InviteToken string `json:"invite_token" dc:"邀请用户Token，例如管理员邀请注册等"`
	}
)
