package model

import (
	"gitee.com/bobo-rs/innovideo-services/enums"
	"gitee.com/bobo-rs/innovideo-services/framework/model/entity"
)

type (
	// VideoBuyBillListWhere 视频购买账单列表搜索条件
	VideoBuyBillListWhere struct {
		BuyId uint `json:"buy_id" dc:"视频购买ID"`
		Uid   uint `json:"uid" dc:"用户ID"`
	}

	// RefundVideoBillInput 视频退款请求参数
	RefundVideoBillInput struct {
		BillId        uint   `json:"bill_id" dc:"购买账单ID"`
		RefundCoins   uint   `json:"refund_coins" dc:"退款盘古币"`
		EpisodesIdSet []uint `json:"episodes_id_set" dc:"剧集ID集合"`
	}

	// VideoBillDetailOutput 获取视频账单详情输出结果
	VideoBillDetailOutput struct {
		VideoBuyBillDetailItem
		EpisodesList []*entity.VideoBuyEpisodes `json:"episodes_list" dc:"购买剧集列表"`
	}
)

type (

	// VideoBuyBillListItem 视频列表账单列表属性
	VideoBuyBillListItem struct {
		VideoBuyBillItem
		VideoBuyBillCoinItem
		VideoBuyBillAttrItem
		CreateAt string `json:"create_at" dc:"创建时间"`
	}

	// VideoBuyBillDetailItem 视频账单详情熟悉
	VideoBuyBillDetailItem struct {
		VideoBuyBillItem
		VideoBuyBillCoinItem
		VideoBuyBillAttrItem
		CreateAt string `json:"create_at" dc:"创建时间"`
	}

	// PrepareRefundBillItem 整理退款的账单信息
	PrepareRefundBillItem struct {
		BillDetail      *VideoBillRefundItem
		RefundCoins     uint
		EpisodesIdSet   []uint
		Status          enums.VideoBuyBillStatus
		EpisodesContent string
	}

	// VideoBillRefundItem 视频账单退款详情
	VideoBillRefundItem struct {
		VideoBuyBillItem
		VideoBuyBillCoinItem
		Remark string `json:"remark" dc:"备注"`
	}

	// VideoBuyBillItem 视频购买账单属性
	VideoBuyBillItem struct {
		Id     uint `json:"id" dc:"账单ID"`
		BuyId  uint `json:"buy_id" dc:"视频购买ID"`
		Uid    uint `json:"uid" dc:"用户ID"`
		Status int  `json:"status" dc:"状态：0已购买，1已退款, 2部分退款"`
		Type   int  `json:"type" dc:"类型：0单集购买，1全集购买，2余集买断"`
	}

	// VideoBuyBillCoinItem 视频购买账单盘古币属性
	VideoBuyBillCoinItem struct {
		BuyEpisodesNum  uint `json:"buy_episodes_num" dc:"本次购买剧集数"`
		BuyPanguCoin    uint `json:"buy_pangu_coin" dc:"支付盘古币"`
		RefundPanguCoin uint `json:"refund_pangu_coin" dc:"退款盘古币"`
	}

	// VideoBuyBillAttrItem 视频购买账单属性
	VideoBuyBillAttrItem struct {
		Content  string `json:"content" dc:"购买集数内容"`
		RefundAt string `json:"refund_at" dc:"退款时间"`
		Remark   string `json:"remark" dc:"备注"`
	}
)
