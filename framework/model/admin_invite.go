package model

import "gitee.com/bobo-rs/innovideo-services/enums"

type (
	// AdminInviteGetListInput 管理员邀请获取列表请求参数
	AdminInviteGetListInput struct {
		CommonPaginationItem
		Status enums.AdminInviteStatus `json:"status" dc:"状态"`
	}

	// AdminInviteGetListOutput 管理员邀请获取列表响应输出
	AdminInviteGetListOutput struct {
		CommonResponseItem
		Rows []AdminInviteListItem `json:"rows" dc:"数据列表"`
	}

	// AdminInviteAddInput 添加管理员邀请请求参数
	AdminInviteAddInput struct {
		IsSuperManage enums.AdminIsSuperManage `json:"is_super_manage" dc:"是否超管"`
		DepartId      []uint                   `json:"depart_id" dc:"部门ID"`
	}

	// AdminInviteAddOutput 添加管理员邀请输出结果
	AdminInviteAddOutput struct {
		Token string `json:"token" dc:"授权Token"`
	}
)

type (
	// AdminInviteListItem 管理员邀请列表属性
	AdminInviteListItem struct {
		AdminInviteItem
		AdminInviteManageItem
		AdminInviteValidItem
		CreateAt string `json:"create_at" dc:"创建时间"`
	}

	// AdminInviteDetailItem 管理员邀请详情属性
	AdminInviteDetailItem struct {
		AdminInviteItem
		AdminInviteManageItem
		AdminInviteValidItem
		AdminInviteUserItem
	}

	// AdminInviteUserValidItem 管理员邀请用户有效属性
	AdminInviteUserValidItem struct {
		AdminInviteValidItem
		AdminInviteUserItem
	}

	// AdminInviteValidItem 管理员邀请效期属性
	AdminInviteValidItem struct {
		Expired string `json:"expired" dc:"到期时间"`
		Status  uint   `json:"status" dc:"邀请状态"`
	}

	// AdminInviteItem 管理员邀请熟悉
	AdminInviteItem struct {
		Id    uint   `json:"id" dc:"邀请ID"`
		Token string `json:"token" dc:"邀请Token"`
	}

	// AdminInviteUserItem 管理员邀请用户信息
	AdminInviteUserItem struct {
		Uid           uint   `json:"uid" dc:"用户ID"`
		DepartId      string `json:"depart_id" dc:"部门ID"`
		IsSuperManage uint   `json:"is_super_manage" dc:"是否超管"`
	}

	// AdminInviteManageItem 管理员邀请管理属性
	AdminInviteManageItem struct {
		AdminId    uint   `json:"admin_id" dc:"管理员ID"`
		ManageName string `json:"manage_name" dc:"管理员"`
		RefreshNum uint   `json:"refresh_num" dc:"刷新时间"`
	}
)
