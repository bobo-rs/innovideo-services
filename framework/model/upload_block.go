package model

type (
	// UploadBlockInput 文件分块上传
	UploadBlockInput struct {
		Index    int    `json:"index" dc:"索引"`
		Block    []byte `json:"block" dc:"内容块"`
		IsLast   bool   `json:"is_last" dc:"是否最后一块：false否，true是"`
		FileHash string `json:"file_hash" dc:"文件Hash值"`
		UploadBlockFile
	}

	// UploadBlockOutput 文件分块上传
	UploadBlockOutput struct {
		Url string `json:"url" dc:"文件地址"`
	}

	// MergeUploadBlockOutput 合并完成后文件输出
	MergeUploadBlockOutput struct {
		Url string `json:"url" dc:"文件地址"`
	}

	// UploadBlockSpaceOutput 获取上传文件空间输出
	UploadBlockSpaceOutput struct {
		Hash string `json:"hash" dc:"文件HASH值"`
	}
)

type (
	// UploadBlockFile 上传的文件块信息
	UploadBlockFile struct {
		Filename string `json:"filename" dc:"文件名"`
		MimeType string `json:"mime_type" dc:"文件类型"`
		Size     int64  `json:"size" dc:"尺寸大小"`
	}

	// UploadBlockItem 上传文件块属性
	UploadBlockItem struct {
		Index     int    // 块索引
		Hash      string // 文件HASH值
		BlockHash string // 块HASH值
		Size      int    // 块大小
	}

	// UploadBlockMerge 文件块合并
	UploadBlockMerge struct {
		Filename string
		MimeType string
		FileHash string
		Blocks   []UploadBlockItem
	}

	// UploadBlockGenMerge 上传文件块生成合并文件
	UploadBlockGenMerge struct {
		Url      string // 上传文件地址
		Hashed   string // 文件内容Hash值
		AttachId string // 附件ID
	}
)
