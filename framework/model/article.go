package model

type (
	// ArticleGetListInput 获取文章列表请求参数
	ArticleGetListInput struct {
		CommonPaginationItem
		ArticleWhereItem
	}

	// ArticleGetListOutput 获取文章列表响应结果
	ArticleGetListOutput struct {
		CommonResponseItem
		Rows []ArticleListItem `json:"rows" dc:"数据列表"`
	}
)

type (
	// ArticleListItem 获取文章列表属性
	ArticleListItem struct {
		ArticleBasicItem
		ArticleDetailFmtItem
		CreateAt string `json:"create_at" dc:"创建时间"`
	}

	// ArticleDetailItem 文章详情属性
	ArticleDetailItem struct {
		ArticleBasicItem
		ArticleAttrItem
		ArticleUrlItem
		ArticleContentItem
		ArticleDetailFmtItem
	}

	// ArticleWhereItem 文章查询条件属性
	ArticleWhereItem struct {
		Name       string `json:"name" dc:"文章名"`
		Cid        int    `json:"cid" dc:"类目ID"`
		AssocAlias string `json:"assoc_alias" dc:"关联别名"`
		Status     int    `json:"status" dc:"文章状态"`
		IsShow     int    `json:"is_show" dc:"是否显示"`
	}

	// ArticleDetailFmtItem 文章详情格式化
	ArticleDetailFmtItem struct {
		ArticleStatusShowItem
		ArticleFmtItem
	}

	// ArticleBasicItem 文章基础属性
	ArticleBasicItem struct {
		Id         uint   `json:"id" dc:"文章ID"`
		Name       string `json:"name" dc:"文章名"`
		AbbrevName string `json:"abbrev_name" dc:"文章名缩写"`
		Cid        uint   `json:"cid" dc:"分类ID"`
		AssocAlias string `json:"assoc_alias" dc:"关联别名，例如注册协议"`
	}

	// ArticleAttrItem 文章属性
	ArticleAttrItem struct {
		Description string `json:"description" dc:"文章摘要"`
		Keywords    string `json:"keywords" dc:"关键词"`
	}

	// ArticleStatusShowItem 文章状态属性
	ArticleStatusShowItem struct {
		Status uint `json:"status" dc:"文章状态"`
		IsShow uint `json:"is_show" dc:"是否显示"`
		Sort   uint `json:"sort" dc:"排序"`
	}

	// ArticleContentItem 文章内容属性
	ArticleContentItem struct {
		Content string `json:"content" dc:"文章内容"`
	}

	// ArticleUrlItem 文章视频图片URL属性
	ArticleUrlItem struct {
		ImgUrl   string `json:"img_url" dc:"图片URL"`
		VideoUrl string `json:"video_url" dc:"视频URL"`
	}

	// ArticleFmtItem 格式化文章状态属性
	ArticleFmtItem struct {
		FmtStatus string `json:"fmt_status" dc:"格式化状态"`
		FmtIsShow string `json:"fmt_is_show" dc:"格式化显示"`
	}
)
