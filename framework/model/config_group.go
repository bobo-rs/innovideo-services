package model

type (

	// ConfigGroupCategoryItem 分类配置组属性
	ConfigGroupCategoryItem struct {
		Id          uint   `json:"id" dc:"配置组ID"`
		Name        string `json:"name" dc:"配置组名"`
		GroupName   string `json:"group_name" dc:"配置组关联名"`
		Description string `json:"description" dc:"简介详情"`
		Pid         uint   `json:"pid" dc:"父级ID"`
	}

	// ConfigGroupTreeListInput 获取配置组列表请求参数
	ConfigGroupTreeListInput struct {
		Name string `json:"name" dc:"配置名"`
	}

	// ConfigGroupTreeItem 配置组详情属性
	ConfigGroupTreeItem struct {
		ConfigGroupCategoryItem
		Children []ConfigGroupTreeItem `json:"children" dc:"子级配置"`
	}
)
