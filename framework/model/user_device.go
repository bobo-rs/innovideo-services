package model

import "gitee.com/bobo-rs/innovideo-services/framework/model/entity"

type (

	// UserDeviceUserAgentItem 通过用户请求UserAgent获取设备信息
	UserDeviceUserAgentItem struct {
		UserAgent  string `json:"user_agent" dc:"请求头"`
		DeviceType string `json:"device_type" dc:"设备类型"`
		DeviceName string `json:"device_name" dc:"设备名称"`
		Mac        string `json:"mac" dc:"MAC地址"`
	}

	// UserDeviceBasicItem 获取用户设备Hash和设备ID
	UserDeviceBasicItem struct {
		Id         uint   `json:"id" dc:"设备ID"`
		UserId     uint   `json:"user_id" dc:"用户ID"`
		IsDisabled uint   `json:"is_disabled" dc:"设备禁用"`
		DeviceHash string `json:"device_hash" dc:"设备Hash值"`
	}

	// UserDeviceLoginAfterItem 用户登录认证-用户设备后置处理
	UserDeviceLoginAfterItem struct {
		*entity.UserDevice
		IsNewDevice bool `json:"is_new" dc:"是否新设备：false否，true新设备"`
	}
)
