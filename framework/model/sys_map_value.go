package model

type (
	// SysMapValueListItem 字典值列表属性
	SysMapValueListItem struct {
		SysMapValueBasicItem
		SysMapValueAttrItem
		CreateAt string `json:"create_at" dc:"创建时间"`
	}

	// SysMapValueDetailItem 字典值详情属性
	SysMapValueDetailItem struct {
		SysMapValueBasicItem
		SysMapValueAttrItem
	}

	// SysMapValueBasicItem 字典值基础属性
	SysMapValueBasicItem struct {
		Id uint `json:"id" dc:"字典值ID"`
		SysMapValueAttrItem
	}

	// SysMapValueAttrItem 字典值属性
	SysMapValueAttrItem struct {
		Value   string `json:"value" dc:"字典值"`
		Name    string `json:"name" dc:"字典名"`
		Explain string `json:"explain" dc:"字典值解析"`
	}
)
