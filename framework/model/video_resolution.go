package model

type (
	// VideoResolutionSaveItem 视频清晰度添加|编辑属性
	VideoResolutionSaveItem struct {
		VideoResolutionBasicItem
	}
)

type (
	// VideoResolutionListItem 视频清晰度列表属性
	VideoResolutionListItem struct {
		VideoResolutionBasicItem
		VideoResolutionAttrItem
	}

	// EpisodesResolutionDetailItem 获取视频续集清晰度详情
	EpisodesResolutionDetailItem struct {
		VideoResolutionAttrItem
		VideoEpisodesBasicItem
	}

	// VideoResolutionBasicItem 视频清晰度基础属性
	VideoResolutionBasicItem struct {
		Id         uint   `json:"id" dc:"清晰度ID"`
		EpisodesId uint   `json:"episodes_id" dc:"视频续集ID"`
		Resolution string `json:"resolution" dc:"视频清晰度"`
		AttachId   string `json:"attach_id" dc:"附件ID"`
	}

	// VideoResolutionAttrItem 视频清晰度属性
	VideoResolutionAttrItem struct {
		VideoUrl       string `json:"video_url" dc:"视频地址"`
		MimeType       string `json:"mime_type" dc:"视频类型"`
		Size           uint   `json:"size" dc:"视频大小"`
		Duration       uint   `json:"duration" dc:"时长（S）"`
		DurationString string `json:"duration_string" dc:"格式化时长"`
	}
)
