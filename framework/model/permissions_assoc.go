package model

type (
	// PermAssocPermId 权限关联-权限ID
	PermAssocPermId struct {
		PermissionsId uint `json:"permissions_id" dc:"权限ID"`
	}
)
