package model

import (
	"gitee.com/bobo-rs/innovideo-services/enums"
	"gitee.com/bobo-rs/innovideo-services/framework/model/entity"
)

type (
	// PGCoinTransactionListInput 获取盘古币流水列表
	PGCoinTransactionListInput struct {
		CommonPaginationItem
		PGCoinTransactionWhere
	}

	// PGCoinTransactionListOutput 获取盘古币流水列表
	PGCoinTransactionListOutput struct {
		CommonResponseItem
		Rows []entity.PanguCoinTransaction `json:"rows" dc:"数据列表"`
	}
)

type (
	// PGCoinTransactionWhere 盘古币流水查询条件
	PGCoinTransactionWhere struct {
		Uid           uint                        `json:"uid" dc:"用户ID"`
		TransactionNo string                      `json:"transaction_no" dc:"流水号"`
		Type          enums.PGCoinTransactionType `json:"type" dc:"类型"`
		CommonSearchDateItem
	}
)
