package model

import "gitee.com/bobo-rs/innovideo-services/framework/model/entity"

type (
	// ConfigListInput 获取配置列表请求参数
	ConfigListInput struct {
		GroupId   uint   `json:"group_id" dc:"配置组ID"`
		GroupName string `json:"group_name" dc:"配置组名"`
	}

	// ConfigGroupItem 配置组名数据
	ConfigGroupItem struct {
		GroupName string `json:"group_name" dc:"配置组名"`
		GroupId   uint   `json:"group_id" dc:"配置组ID"`
		ConfigNameValueItem
	}

	// ConfigIdByNameAndGroupItem 根据配置名和配置组获取配置ID属性
	ConfigIdByNameAndGroupItem struct {
		ConfigGroupItem
		Id uint `json:"id" dc:"配置ID"`
	}

	// ConfigNameValueItem 配置名和配置值
	ConfigNameValueItem struct {
		Name         string `json:"name" dc:"配置名"`
		Value        string `json:"value" dc:"配置值"`
		DefaultValue string `json:"default_value" dc:"默认配置值"`
	}

	// ConfigSaveHandlerItem 业务配置保存处理属性
	ConfigSaveHandlerItem struct {
		SaveAll []entity.Config // 配置列表
		Keys    []string        // 缓存KEY
	}

	// ConfigDetailItem 配置详情
	ConfigDetailItem struct {
		entity.Config
	}
)
