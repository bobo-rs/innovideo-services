package model

import "gitee.com/bobo-rs/innovideo-services/framework/model/entity"

type (
	// UserSaveUserDetailItem 保存用户信息属性，添加或编辑
	UserSaveUserDetailItem struct {
		*entity.User
		entity.UserDetail
		entity.UserFunds
	}

	// UserDetailItem 用户详情属性
	UserDetailItem struct {
		Uid uint `json:"uid" dc:"用户ID"`
		UserAccountNameItem
		Detail UserBasicDetailItem `json:"detail" dc:"用户详情"`
	}

	// UserAccountNameItem 账户名属性
	UserAccountNameItem struct {
		Account  string `json:"account" dc:"账号"`
		Nickname string `json:"nickname" dc:"昵称"`
	}
)
