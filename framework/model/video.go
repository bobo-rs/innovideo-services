package model

import (
	"gitee.com/bobo-rs/innovideo-services/enums"
	"gitee.com/bobo-rs/innovideo-services/framework/model/entity"
)

type (
	// VideoSaveItem 添加编辑保存视频基础信息
	VideoSaveItem struct {
		VideoBasicItem
		Values []string `json:"values" dc:"标签值"`
		entity.VideoAttribute
	}

	// VideoListInput 获取视频列表请求参数
	VideoListInput struct {
		CommonPaginationItem
		VideoWhereItem
		By uint16 `json:"by" dc:"排序类型：0默认置顶>推荐>热播>ID，1按播放量>默认排序，2按评分>默认排序"`
	}

	// VideoListOutput 获取视频列表输出结果
	VideoListOutput struct {
		CommonResponseItem
		Rows []VideoListItem `json:"rows" dc:"数据列表"`
	}

	// VideoUserListOutput 用户视频列表输出结果
	VideoUserListOutput struct {
		CommonResponseItem
		Rows []VideoUserListItem `json:"rows" dc:"数据列表"`
	}

	// VideoDetailInput 获取视频详情请求参数
	VideoDetailInput struct {
		VideoId  uint           `json:"video_id" dc:"视频ID"`
		FormSide enums.FormSide `json:"form_side" dc:"平台端"`
	}

	// VideoPlayEpisodesOutput 用户业务端-视频播放续集详情输出结果
	VideoPlayEpisodesOutput struct {
		*VideoDetailAuthItem
		BuyDetail    *VideoBuyAuthItem       `json:"buy_detail" dc:"视频购买信息"`
		PlayDetail   VideoPlayAuthItem       `json:"play_detail" dc:"视频播放信息"`
		EpisodesList []VideoEpisodesListItem `json:"episodes_list" dc:"视频续集列表"`
	}

	// VideoPlayUrlInput 获取视频播放地址
	VideoPlayUrlInput struct {
		VideoNo    string `json:"video_no" dc:"视频编号"`
		EpisodesId uint   `json:"episodes_id" dc:"剧集ID"`
	}

	// VideoPlayUrlOutput 视频播放地址输出结果
	VideoPlayUrlOutput struct {
		Shuffle        string                    `json:"shuffle" dc:"随机数"`
		ResolutionList []VideoResolutionListItem `json:"resolution_list" dc:"清晰度列表"`
	}
)

type (
	// VideoListItem 视频列表属性
	VideoListItem struct {
		Id        uint   `json:"id" dc:"视频ID"`
		VideoNo   string `json:"video_no" dc:"视频编号"`
		VideoName string `json:"video_name" dc:"视频名"`
		VideoCateAttrItem
		Region      string `json:"region" dc:"上映地区"`
		YearDate    string `json:"year_date" dc:"上映年份"`
		ReleaseDate string `json:"release_date" dc:"上映时间"`
		FeeType     int    `json:"fee_type" dc:"收费类型：0免费，1VIP付费"`
		ImageUrl    string `json:"image_url" dc:"视频海报"`
		VideoActionAttrItem
		VideoNumAttrItem
		VideoStatusAttrItem
	}

	// VideoUserListItem 用户视频列表（播放、收藏、点赞）
	VideoUserListItem struct {
		Id        uint   `json:"id" dc:"视频ID"`
		VideoName string `json:"video_name" dc:"视频名"`
		FeeType   int    `json:"fee_type" dc:"收费类型：0免费，1VIP付费"`
		ImageUrl  string `json:"image_url" dc:"视频海报"`
		VideoNumAttrItem
		VideoStatusAttrItem
	}

	// VideoPlayListItem 用户视频播放视频列表属性
	VideoPlayListItem struct {
		VideoName string `json:"video_name" dc:"视频名"`
		FeeType   int    `json:"fee_type" dc:"收费类型：0免费，1VIP付费"`
		ImageUrl  string `json:"image_url" dc:"视频海报"`
		VideoNumAttrItem
		VideoStatusAttrItem
	}

	// VideoDetailItem 视频详情属性
	VideoDetailItem struct {
		VideoBasicItem
		Attribute    entity.VideoAttribute    `json:"attribute" dc:"视频属性"`
		Values       []VideoLabelItem         `json:"values" dc:"视频标签值"`
		PersonalList []VideoPersonalBasicItem `json:"personal_list" dc:"视频相关参演人员列表"`
	}

	// VideoDetailAuthItem 获取视频详情授权属性
	VideoDetailAuthItem struct {
		VideoAuthAttrItem
		AttributeAuthItem
	}

	// VideoAuthAttrItem 视频基础信息验证属性
	VideoAuthAttrItem struct {
		Id        uint   `json:"id" dc:"视频ID"`
		VideoNo   string `json:"video_no" dc:"视频编号"`
		VideoName string `json:"video_name" dc:"视频名"`
		FeeType   int    `json:"fee_type" dc:"收费类型：0免费，1VIP付费"`
	}

	// VideoBasicItem 视频基础信息
	VideoBasicItem struct {
		Id          uint   `json:"id" dc:"视频ID"`
		VideoNo     string `json:"video_no" dc:"视频编号"`
		VideoName   string `json:"video_name" dc:"视频名"`
		Keywords    string `json:"keywords" dc:"关键词"`
		CateId1     uint   `json:"cate_id1" dc:"一级分类"`
		CateId2     uint   `json:"cate_id2" dc:"二级分类"`
		Region      string `json:"region" dc:"上映地区"`
		YearDate    string `json:"year_date" dc:"上映年份"`
		ReleaseDate string `json:"release_date" dc:"上映时间"`
		FeeType     int    `json:"fee_type" dc:"收费类型：0免费，1VIP付费"`
		ImageUrl    string `json:"image_url" dc:"视频海报"`
	}

	// VideoWhereItem 视频搜索条件
	VideoWhereItem struct {
		VideoNo     string   `json:"video_no" dc:"视频编号"`
		Keywords    string   `json:"keywords" dc:"关键词"`
		CateId1     uint     `json:"cate_id1" dc:"一级分类"`
		CateId2     uint     `json:"cate_id2" dc:"二级分类"`
		Region      string   `json:"region" dc:"上映地区"`
		YearDate    string   `json:"year_date" dc:"上映年份"`
		FeeType     int      `json:"fee_type" dc:"收费类型：0免费，1VIP付费"`
		Label       []string `json:"label" dc:"标签值"`
		IsBest      int      `json:"is_best" dc:"是否精品剧"`
		IsTop       int      `json:"is_top" dc:"是否置顶"`
		IsHot       int      `json:"is_hot" dc:"是否热播"`
		IsNew       int      `json:"is_new" dc:"是否新片"`
		IsRecommend int      `json:"is_recommend" dc:"是否推荐"`
		Status      int      `json:"status" dc:"状态"`
		IsRelease   int      `json:"is_release" dc:"是否发布"`
		CommonFormSideItem
	}

	// VideoCateAttrItem 视频分类属性
	VideoCateAttrItem struct {
		CateId1 uint `json:"cate_id1" dc:"一级分类"`
		CateId2 uint `json:"cate_id2" dc:"二级分类"`
	}

	// VideoActionAttrItem 视频操作属性
	VideoActionAttrItem struct {
		IsBest      uint `json:"is_best" dc:"是否精品剧:0否，1是"`
		IsTop       uint `json:"is_top" dc:"是否置顶:0否，1是"`
		IsHot       uint `json:"is_hot" dc:"是否热播:0否，1是"`
		IsNew       uint `json:"is_new" dc:"是否新片:0否，1是"`
		IsRecommend uint `json:"is_recommend" dc:"是否推荐:0否，1是"`
		FeeType     uint `json:"fee_type" dc:"收费类型：0免费，1VIP付费"`
	}

	// VideoNumAttrItem 视频量属性
	VideoNumAttrItem struct {
		Browse   uint    `json:"browse" dc:"浏览量"`
		PlayNum  uint    `json:"play_num" dc:"播放量"`
		Collect  uint    `json:"collect" dc:"收藏量"`
		ShareNum uint    `json:"share_num" dc:"分享数量"`
		Score    float64 `json:"score" dc:"评分"`
	}

	// VideoStatusAttrItem 视频状态属性
	VideoStatusAttrItem struct {
		Status    uint `json:"status" dc:"视频状态：0待审核，1审核通过，2驳回申请，3违规下架"`
		IsRelease uint `json:"is_release" dc:"是否发布：0未发布，1已发布"`
	}
)
