package model

import "gitee.com/bobo-rs/innovideo-services/enums"

type (
	// SystemLogGetListInput 获取系统列表请求参数
	SystemLogGetListInput struct {
		CommonPaginationItem
		SystemLogWhereItem
	}

	// SystemLogGetListOutput 获取日志列表响应结果
	SystemLogGetListOutput struct {
		CommonResponseItem
		Rows []SystemLogListItem `json:"rows" dc:"数据列表"`
	}

	// SystemLogGetDetailOutput 系统日志获取详情响应结果
	SystemLogGetDetailOutput struct {
		*SystemLogDetailItem
	}
)

type (
	// RecordLogItem 记录日志属性
	RecordLogItem struct {
		Content      string // 日志内容也是日志名
		TagName      string // TAG栏目名-例如：系统管理
		ApiUrl       string // 接口地址
		ResponseText string // 响应结果
	}

	// SystemLogWhereItem 系统日志搜索条件属性
	SystemLogWhereItem struct {
		ModuleName  string               `json:"module_name" dc:"模块名"`
		Content     string               `json:"content" dc:"模块内容"`
		ManageName  string               `json:"manage_name" dc:"管理员名"`
		OperateType enums.LogOperateType `json:"operate_type" dc:"日志类型"`
		CreateAt    []string             `json:"create_at" dc:"创建时间"`
	}

	// SystemLogListItem 系统日志列表属性
	SystemLogListItem struct {
		SystemLogItem
		SystemLogFmtItem
	}

	// SystemLogDetailItem 系统日志详情属性
	SystemLogDetailItem struct {
		SystemLogItem
		SystemLogAttrItem
		SystemLogFmtItem
	}

	// SystemLogItem 系统日志列表属性
	SystemLogItem struct {
		Id          uint   `json:"id" dc:"日志ID"`
		ModuleName  string `json:"module_name" dc:"模块名"`
		Content     string `json:"content" dc:"日志描述"`
		Ip          string `json:"ip" dc:"IP"`
		OperateType int    `json:"operate_type" dc:"日志类型"`
		UserId      uint   `json:"user_id" dc:"用户ID"`
		ApiUrl      string `json:"api_url" dc:"接口地址"`
		ManageName  string `json:"manage_name" dc:"管理员名"`
		CreateAt    string `json:"create_at" dc:"创建时间"`
	}

	// SystemLogAttrItem 系统日志属性
	SystemLogAttrItem struct {
		ManageId     uint   `json:"manage_id" dc:"管理员ID"`
		MerchantId   uint   `json:"merchant_id" dc:"商户ID"`
		ResponseText string `json:"response_text" dc:"响应结果"`
		ParamText    string `json:"param_text" dc:"请求参数"`
	}

	// SystemLogFmtItem 格式化系统日志属性
	SystemLogFmtItem struct {
		FmtOperateType string `json:"fmt_operate_type" dc:"格式化日志类型"`
	}
)
