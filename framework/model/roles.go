package model

import "gitee.com/bobo-rs/innovideo-services/enums"

type (
	// RolesListInput 获取角色列表
	RolesListInput struct {
		CommonPaginationItem
		Name   string           `json:"name" dc:"角色名"`
		Status enums.RoleStatus `json:"status" dc:"角色状态"`
	}

	// RolesListOutput 获取角色列表-响应结果
	RolesListOutput struct {
		CommonResponseItem
		Rows []RolesItem `json:"rows" dc:"角色列表"`
	}
)

type (
	// RolesDetailItem 角色详情属性
	RolesDetailItem struct {
		RolesItem
		PermId []uint `json:"perm_id" dc:"权限ID集合"`
	}

	// RolesItem 角色属性
	RolesItem struct {
		Id          uint   `json:"id" dc:"角色ID"`
		Name        string `json:"name" dc:"角色名称"`
		Description string `json:"description" dc:"角色详情"`
		RoleStatus  uint   `json:"role_status" dc:"角色状态"`
		Sort        uint   `json:"sort" dc:"排序"`
		CreateAt    string `json:"create_at" dc:"创建时间"`
	}

	// RoleIdAndNameItem 角色ID和名称属性
	RoleIdAndNameItem struct {
		Id   uint   `json:"id" dc:"角色ID"`
		Name string `json:"name" dc:"角色名称"`
	}
)
