package model

import "gitee.com/bobo-rs/innovideo-services/enums"

type (
	// PGCoinRechargeInput 盘古币充值记录
	PGCoinRechargeInput struct {
		RechargePGC uint   `json:"recharge_pgc" dc:"充值盘古币数量"`
		PayType     string `json:"pay_type" dc:"支付方式"`
	}

	// PGCoinRechargeOutput 盘古币充值响应参数
	PGCoinRechargeOutput struct {
		OrderNo string  `json:"order_no" dc:"订单号"`
		PayFee  float64 `json:"pay_fee" dc:"充值金额"`
	}

	// PGCoinPayDetailInput 获取盘古币支付信息请求参数
	PGCoinPayDetailInput struct {
		OrderNo string `json:"order_no" dc:"订单编号"`
	}

	// PGCoinPayDetailOutput 获取盘古币支付信息响应结果
	PGCoinPayDetailOutput struct {
		PayDetail interface{} `json:"pay_detail" dc:"获取支付信息，如支付链接"`
	}

	// PGCoinPayNotifyInput 支付充值盘古币回调请求参数
	PGCoinPayNotifyInput struct {
		TradeNo string        `json:"trade_no" dc:"订单商户支付编号"`
		PayNo   string        `json:"pay_no" dc:"第三方支付单号"`
		PayType enums.PayType `json:"pay_type" dc:"支付方式"`
	}

	// PGCoinRechargeBillInput 生成盘古币充值账单请求参数
	PGCoinRechargeBillInput struct {
		OrderId           uint                 `json:"order_id" dc:"订单ID"`
		Uid               uint                 `json:"uid" dc:"用户ID"`
		RechargePanguCoin uint                 `json:"recharge_pangu_coin" dc:"充值盘古币数量"`
		Type              enums.PGCoinBillType `json:"type" dc:"充值类型"`
		Remark            string               `json:"remark" dc:"备注"`
	}

	// PGCoinConsumeInput 盘古币消费请求参数
	PGCoinConsumeInput struct {
		ConsumePGCoin uint `json:"consume_pg_coin" dc:"消费盘古币数量"`
		Uid           uint `json:"uid" dc:"用户ID"`
	}

	// BillItemAndUpdateBillInput 生成盘古币账单明细和更新账单
	BillItemAndUpdateBillInput struct {
		PGCoinConsumeInput                           // 盘古币消费请求参数
		BillList           []UserAvailablePGCoinItem // 盘古币账单列表
		TransactionNo      string                    // 盘古币流水号
	}

	// CallbackUpdateCoinsPayMethodInput 回调更新盘古币支付方式请求参数
	CallbackUpdateCoinsPayMethodInput struct {
		OrderNo string
		PayType enums.PayType
		PayNo   string
	}

	// CheckAvailableCoins 验证用户可用盘古币
	CheckAvailableCoins struct {
		ConsumedCoins      uint // 消费盘古币
		BillAvailableCoins uint // 账单可用盘古币
		FundsCoins         uint // 用户资金可用盘古币
	}

	// ConfirmCoinsPaymentInput 确认盘古币订单支付完成（PayPal必须流程，否则支付无法完成）
	ConfirmCoinsPaymentInput struct {
		PayType enums.PayType
		PayNo   string
		OrderNo string
	}
)

type (
	// PGCConfigItem 盘古币配置
	PGCConfigItem struct {
		PGCRate          uint   `json:"pgc_rate" dc:"兑换比例，如：1:100"`
		Currency         string `json:"currency" dc:"兑换货币符号"`
		PGCExpired       int    `json:"pgc_expired" dc:"盘古币效期时间"`
		PGCExpiredUnit   string `json:"pgc_expired_unit" dc:"盘古币效期单位"`
		PGCSymbol        string `json:"pgc_symbol" dc:"盘古币符号PGC"`
		PGCExpiredTime   int64  `json:"pgc_expired_time" dc:"过期时间（S）"`
		PGCExpiredAt     string `json:"pgc_expired_at" dc:"过期时间（date）"`
		PGCRechargeLimit uint   `json:"pgc_recharge_limit" dc:"充值限制"`
		PGCRechargeMin   uint   `json:"pgc_recharge_min" dc:"盘古币最少充值量"`
		ReturnUrl        string `json:"return_url" dc:"支付成功返回URL"`
		CancelUrl        string `json:"cancel_url" dc:"支付取消返回URL"`
	}

	// PGCoinPayDetailItem 盘古币支付详情属性
	PGCoinPayDetailItem struct {
		Id        uint                    `json:"id" dc:"订单ID"`
		Uid       uint                    `json:"uid" dc:"用户ID"`
		PanguCoin uint                    `json:"pangu_coin" dc:"盘古币数量"`
		PayFee    float64                 `json:"pay_fee" dc:"支付金额"`
		Status    enums.PGCoinOrderStatus `json:"status" dc:"订单状态"`
		Currency  string                  `json:"currency" dc:"支付货币"`
		PayStatus enums.PGCoinOrderPay    `json:"pay_status" dc:"支付状态"`
	}

	// PGCoinsConfirmPayDetailItem 盘古币确认支付详情
	PGCoinsConfirmPayDetailItem struct {
		PGCoinPayDetailItem
		OrderNo string `json:"order_no" dc:"订单编号"`
		PayNo   string `json:"pay_no" dc:"支付编号"`
		TradeNo string `json:"trade_no" dc:"商户交易号"`
		PayAt   string `json:"pay_at" dc:"支付时间"`
	}
)
