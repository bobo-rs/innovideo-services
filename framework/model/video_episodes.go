package model

import (
	"gitee.com/bobo-rs/innovideo-services/enums"
	"gitee.com/bobo-rs/innovideo-services/framework/model/entity"
)

type (
	// VideoEpisodesSaveItem 添加|编辑视频续集列表属性
	VideoEpisodesSaveItem struct {
		entity.VideoEpisodes
		ResolutionList []*VideoResolutionSaveItem `json:"resolution_list" dc:"视频清晰度列表"`
	}

	// VideoEpisodesListInput 获取视频续集列表请求参数
	VideoEpisodesListInput struct {
		VideoId  uint           `json:"video_id" dc:"视频ID"`
		FormSide enums.FormSide `json:"form_side" dc:"平台端"`
	}

	// VideoEpisodesListOutput 视频续集列表输出结果
	VideoEpisodesListOutput struct {
		CommonResponseItem
		Rows []VideoEpisodesListItem `json:"rows" dc:"视频续集列表"`
	}
)

type (
	// VideoEpisodesListItem 视频续集列表属性
	VideoEpisodesListItem struct {
		VideoEpisodesBasicItem
		VideoEpisodesExtAttrItem
		ResolutionList []VideoResolutionListItem `json:"resolution_list" dc:"清晰度列表"`
	}

	// VideoEpisodesDetailItem 视频续集详情属性
	VideoEpisodesDetailItem struct {
		VideoEpisodesBasicItem
		VideoEpisodesAttrItem
		ResolutionList []VideoResolutionListItem `json:"resolution_list" dc:"清晰度列表"`
	}

	// VideoEpisodesBasicItem 视频续集基础属性
	VideoEpisodesBasicItem struct {
		Id           uint   `json:"id" dc:"续集ID"`
		VideoId      uint   `json:"video_id" dc:"视频ID"`
		AttachId     string `json:"attach_id" dc:"附件ID"`
		ImageUrl     string `json:"image_url" dc:"续集推广图"`
		EpisodesName string `json:"episodes_name" dc:"续集名"`
		Synopsis     string `json:"synopsis" dc:"续集简介"`
		EpisodesNum  uint   `json:"episodes_num" dc:"当前集数编号"`
	}

	// VideoEpisodesBuyItem 视频剧集购买属性
	VideoEpisodesBuyItem struct {
		Id           uint   `json:"id" dc:"续集ID"`
		EpisodesName string `json:"episodes_name" dc:"续集名"`
		EpisodesNum  uint   `json:"episodes_num" dc:"当前集数编号"`
	}

	// VideoEpisodesId 获取视频续集ID
	VideoEpisodesId struct {
		Id uint `json:"id" dc:"续集ID"`
	}

	// VideoEpisodesAttrItem 视频续集属性
	VideoEpisodesAttrItem struct {
		ResolutionSet string `json:"resolution_set" dc:"清晰度集合"`
		PlayNum       uint   `json:"play_num" dc:"集数播放数量"`
		//FmtResolutionSet []string `json:"fmt_resolution_set" dc:"格式化清晰度列表"`
	}

	// VideoEpisodesExtAttrItem 视频续集额外属性
	VideoEpisodesExtAttrItem struct {
		IsPlay    uint `json:"is_play" dc:"是否已播放：0未播放，1已播放或播放中"`
		IsBuy     uint `json:"is_buy" dc:"是否购买：0否，1已购买"`
		IsDisable bool `json:"is_disable" dc:"是否禁止播放：false允许，true禁止"`
	}
)
