package model

import "gitee.com/bobo-rs/innovideo-services/enums"

type (
	// PaymentInput 盘古币支付请求
	PaymentInput struct {
		PaymentItem
	}

	// ConfirmPaymentInput 确认支付请求参数
	ConfirmPaymentInput struct {
		PayNo string `json:"pay_no" dc:"第三方支付订单号"`
		PaymentItem
	}

	// PayNotifyInput 支付回调请求参数
	PayNotifyInput struct {
		Event   enums.PayEvent `json:"event" dc:"支付事件：盘古币充值，订单支付"`
		PayType enums.PayType  `json:"pay_type" dc:"支付方式"`
	}
)

type (
	// PaymentItem 支付信息
	PaymentItem struct {
		No      string         `json:"no" dc:"订单号"`
		Event   enums.PayEvent `json:"event" dc:"支付事件：盘古币充值，订单支付"`
		PayType enums.PayType  `json:"pay_type" dc:"支付方式"`
	}

	// PaymentResultData 支付结果数据
	PaymentResultData struct {
		Amount   float64                `json:"amount" dc:"支付金额"`
		Currency string                 `json:"currency" dc:"货币型号"`
		Content  string                 `json:"content" dc:"支付内容"`
		Other    map[string]interface{} `json:"other" dc:"其他参数"`
	}

	// PayConfig 支付配置
	PayConfig struct {
		Currency  string `json:"currency" dc:"交易货币"`
		ReturnUrl string `json:"return_url" dc:"支付成功跳转地址"`
		CancelUrl string `json:"cancel_url" dc:"取消支付挑战URL"`
		NotifyUrl string `json:"notify_url" dc:"支付回调地址"`
	}
)
