package model

type (
	// EmailTemplateListInput 邮件模板列表请求参数
	EmailTemplateListInput struct {
		CommonPaginationItem
		Name    string `json:"name" dc:"模板名"`
		VarName string `json:"var_name" dc:"模板变量名"`
	}

	// EmailTemplateListOutput 邮件模板列表输出
	EmailTemplateListOutput struct {
		CommonResponseItem
		Rows []EmailTemplateListItem `json:"rows" dc:"数据列表"`
	}
)

type (
	// EmailTemplateListItem 邮件模板列表信息
	EmailTemplateListItem struct {
		EmailTemplateItem
		CreateAt string `json:"create_at" dc:"创建时间"`
	}

	// EmailTemplateItem 邮件模板信息
	EmailTemplateItem struct {
		Id      uint   `json:"id" dc:"模板ID"`
		Name    string `json:"name" dc:"模板名"`
		VarName string `json:"var_name" dc:"模板变量名"`
		Subject string `json:"subject" dc:"邮件主题"`
		Content string `json:"content" dc:"邮件内容"`
	}
)
