package model

type (
	// VideoPersonalBasicItem 视频相关参演人员基础属性
	VideoPersonalBasicItem struct {
		Id       uint   `json:"id" dc:"视频相关人员ID"`
		VideoId  uint   `json:"video_id" dc:"视频ID"`
		ActorsId uint   `json:"actors_id" dc:"演员ID"`
		Name     string `json:"name" dc:"参演名"`
		Type     uint   `json:"type" dc:"角色类型：0导演，1副导演，2领衔主演，3主演，4客串嘉宾，5其他"`
		Portray  string `json:"portray" dc:"饰演角色"`
		IsShow   uint   `json:"is_show" dc:"是否展示：0展示，1隐藏"`
	}
)
