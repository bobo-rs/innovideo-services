// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// VideoEpisodes is the golang structure for table video_episodes.
type VideoEpisodes struct {
	Id            uint        `json:"id"             description:"视频集数ID"`                 // 视频集数ID
	VideoId       uint        `json:"video_id"       description:"视频ID"`                   // 视频ID
	AttachId      string      `json:"attach_id"      description:"附件ID"`                   // 附件ID
	ImageUrl      string      `json:"image_url"      description:"当前集数海报"`                 // 当前集数海报
	EpisodesName  string      `json:"episodes_name"  description:"续集标题"`                   // 续集标题
	Synopsis      string      `json:"synopsis"       description:"简介"`                     // 简介
	EpisodesNum   uint        `json:"episodes_num"   description:"集数"`                     // 集数
	ResolutionSet string      `json:"resolution_set" description:"清晰度集合：480P，720P，1080P等"` // 清晰度集合：480P，720P，1080P等
	PlayNum       uint        `json:"play_num"       description:"播放次数"`                   // 播放次数
	UpdateAt      *gtime.Time `json:"update_at"      description:"更新时间"`                   // 更新时间
	CreateAt      *gtime.Time `json:"create_at"      description:"创建时间"`                   // 创建时间
}
