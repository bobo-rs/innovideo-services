// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// AdminInvite is the golang structure for table admin_invite.
type AdminInvite struct {
	Id            uint        `json:"id"              description:"邀请ID"`              // 邀请ID
	Token         string      `json:"token"           description:"邀请Token"`           // 邀请Token
	AdminId       uint        `json:"admin_id"        description:"邀请管理员ID"`           // 邀请管理员ID
	ManageName    string      `json:"manage_name"     description:"邀请管理员名"`            // 邀请管理员名
	Expired       *gtime.Time `json:"expired"         description:"过期时间"`              // 过期时间
	Status        uint        `json:"status"          description:"状态：0邀请中，1已邀请，2已撤销"` // 状态：0邀请中，1已邀请，2已撤销
	Uid           uint        `json:"uid"             description:"被邀请人用户ID"`          // 被邀请人用户ID
	RefreshNum    uint        `json:"refresh_num"     description:"刷新次数"`              // 刷新次数
	DepartId      string      `json:"depart_id"       description:"部门ID"`              // 部门ID
	IsSuperManage uint        `json:"is_super_manage" description:"是否超管：0普管，1超管"`      // 是否超管：0普管，1超管
	UpdateAt      *gtime.Time `json:"update_at"       description:"更新时间"`              // 更新时间
	CreateAt      *gtime.Time `json:"create_at"       description:"创建时间"`              // 创建时间
}
