// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// UserDevice is the golang structure for table user_device.
type UserDevice struct {
	Id          uint        `json:"id"            description:"用户设备ID"`                   // 用户设备ID
	UserId      uint        `json:"user_id"       description:"用户ID"`                     // 用户ID
	DeviceName  string      `json:"device_name"   description:"设备名称"`                     // 设备名称
	DeviceType  string      `json:"device_type"   description:"设备类型：iOS，安卓，H5，PC"`        // 设备类型：iOS，安卓，H5，PC
	Mac         string      `json:"mac"           description:"MAC地址"`                    // MAC地址
	DeviceHash  string      `json:"device_hash"   description:"设备Hash值[用户ID+设备名+类型+MAC]"` // 设备Hash值[用户ID+设备名+类型+MAC]
	LastLoginAt *gtime.Time `json:"last_login_at" description:"最后登录时间"`                   // 最后登录时间
	IsDisabled  uint        `json:"is_disabled"   description:"是否禁用：0正常，1禁用"`             // 是否禁用：0正常，1禁用
	UpdateAt    *gtime.Time `json:"update_at"     description:"更新时间"`                     // 更新时间
	CreateAt    *gtime.Time `json:"create_at"     description:"创建时间"`                     // 创建时间
}
