// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// Department is the golang structure for table department.
type Department struct {
	Id         uint        `json:"id"          description:"部门ID"`           // 部门ID
	DepartName string      `json:"depart_name" description:"全称，例如：财务部"`      // 全称，例如：财务部
	AbbrevName string      `json:"abbrev_name" description:"简称，例如：财务"`       // 简称，例如：财务
	DepartNo   string      `json:"depart_no"   description:"部门编号"`           // 部门编号
	Pid        uint        `json:"pid"         description:"父级ID"`           // 父级ID
	Status     uint        `json:"status"      description:"部门状态：0正常，1禁用"`   // 部门状态：0正常，1禁用
	Sort       uint        `json:"sort"        description:"排序：0-255，默认255"` // 排序：0-255，默认255
	UpdateAt   *gtime.Time `json:"update_at"   description:"更新时间"`           // 更新时间
	CreateAt   *gtime.Time `json:"create_at"   description:"创建时间"`           // 创建时间
}
