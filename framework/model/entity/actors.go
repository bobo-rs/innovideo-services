// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// Actors is the golang structure for table actors.
type Actors struct {
	Id            uint        `json:"id"            description:"演员ID"`     // 演员ID
	Name          string      `json:"name"          description:"姓名"`       // 姓名
	StageName     string      `json:"stage_name"    description:"艺名"`       // 艺名
	FormerName    string      `json:"former_name"   description:"曾用名"`      // 曾用名
	Country       string      `json:"country"       description:"国籍"`       // 国籍
	NativePlace   string      `json:"native_place"  description:"祖籍"`       // 祖籍
	PlaceBirth    string      `json:"place_birth"   description:"出生地"`      // 出生地
	BloodType     string      `json:"blood_type"    description:"血型"`       // 血型
	Constellation string      `json:"constellation" description:"星座"`       // 星座
	Zodiac        string      `json:"zodiac"        description:"生肖"`       // 生肖
	Birthday      *gtime.Time `json:"birthday"      description:"出生日期"`     // 出生日期
	Sex           uint        `json:"sex"           description:"性别：0男，1女"` // 性别：0男，1女
	Height        uint        `json:"height"        description:"身高（CM）"`   // 身高（CM）
	Weight        float64     `json:"weight"        description:"体重（KG）"`   // 体重（KG）
	DebutAt       *gtime.Time `json:"debut_at"      description:"出道时间"`     // 出道时间
	Synopsis      string      `json:"synopsis"      description:"简介"`       // 简介
	ImageUrl      string      `json:"image_url"     description:"头像"`       // 头像
	WorksNum      uint        `json:"works_num"     description:"作品数"`      // 作品数
	RwNum         uint        `json:"rw_num"        description:"代表作品数"`    // 代表作品数
	Nation        string      `json:"nation"        description:"民族"`       // 民族
	GradInst      string      `json:"grad_inst"     description:"毕业院校"`     // 毕业院校
	GradDate      *gtime.Time `json:"grad_date"     description:"毕业时间"`     // 毕业时间
	UpdateAt      *gtime.Time `json:"update_at"     description:"更新时间"`     // 更新时间
	CreateAt      *gtime.Time `json:"create_at"     description:"创建时间"`     // 创建时间
}
