// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// ActorsWorks is the golang structure for table actors_works.
type ActorsWorks struct {
	Id             uint        `json:"id"              description:"演员作品ID"`                 // 演员作品ID
	ActorsId       uint        `json:"actors_id"       description:"演员ID"`                   // 演员ID
	WorksName      string      `json:"works_name"      description:"作品名"`                    // 作品名
	Type           int         `json:"type"            description:"类型：0主演，1领衔主演，2客串嘉宾，3群众"` // 类型：0主演，1领衔主演，2客串嘉宾，3群众
	IsMasterpieces uint        `json:"is_masterpieces" description:"是否代表作：0否，1是"`            // 是否代表作：0否，1是
	Portray        string      `json:"portray"         description:"饰演角色"`                   // 饰演角色
	UpdateAt       *gtime.Time `json:"update_at"       description:"更新时间"`                   // 更新时间
	CreateAt       *gtime.Time `json:"create_at"       description:"创建时间"`                   // 创建时间
}
