// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// ArticleCategory is the golang structure for table article_category.
type ArticleCategory struct {
	Id          uint        `json:"id"          description:"文章类目ID"`       // 文章类目ID
	Name        string      `json:"name"        description:"文章类目名"`        // 文章类目名
	Description string      `json:"description" description:"详情"`           // 详情
	Pid         uint        `json:"pid"         description:"父级ID"`         // 父级ID
	Status      uint        `json:"status"      description:"状态：0正常，1禁用"`   // 状态：0正常，1禁用
	IsShow      uint        `json:"is_show"     description:"是否显示：0显示，1隐藏"` // 是否显示：0显示，1隐藏
	UpdateAt    *gtime.Time `json:"update_at"   description:"更新时间"`         // 更新时间
	CreateAt    *gtime.Time `json:"create_at"   description:"创建时间"`         // 创建时间
}
