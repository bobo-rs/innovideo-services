// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// PanguCoinTransaction is the golang structure for table pangu_coin_transaction.
type PanguCoinTransaction struct {
	Id              uint        `json:"id"                description:"盘古币流水ID"`      // 盘古币流水ID
	Uid             uint        `json:"uid"               description:"用户ID"`         // 用户ID
	TransactionNo   string      `json:"transaction_no"    description:"流水号"`          // 流水号
	BeforePanguCoin uint        `json:"before_pangu_coin" description:"变动前盘古币"`       // 变动前盘古币
	PanguCoin       int         `json:"pangu_coin"        description:"变动盘古币"`        // 变动盘古币
	AfterPanguCoin  int         `json:"after_pangu_coin"  description:"变动后盘古币（当前数量）"` // 变动后盘古币（当前数量）
	Type            uint        `json:"type"              description:"流水类型：0收入，1支出"` // 流水类型：0收入，1支出
	Remark          string      `json:"remark"            description:"备注"`           // 备注
	CreateAt        *gtime.Time `json:"create_at"         description:"流水记录时间"`       // 流水记录时间
}
