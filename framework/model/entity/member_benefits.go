// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// MemberBenefits is the golang structure for table member_benefits.
type MemberBenefits struct {
	Id       uint        `json:"id"        description:"会员可享服务ID"`          // 会员可享服务ID
	Title    string      `json:"title"     description:"服务名"`               // 服务名
	Content  string      `json:"content"   description:"内容"`                // 内容
	Alias    string      `json:"alias"     description:"服务别名，例如：video视频服务"` // 服务别名，例如：video视频服务
	CreateAt *gtime.Time `json:"create_at" description:"创建时间"`              // 创建时间
}
