// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// SysMap is the golang structure for table sys_map.
type SysMap struct {
	Id        uint        `json:"id"         description:"系统字典ID"`              // 系统字典ID
	Name      string      `json:"name"       description:"字典名，例如：sys_user_sex"` // 字典名，例如：sys_user_sex
	Explain   string      `json:"explain"    description:"字典名解析值，例如：用户性别"`      // 字典名解析值，例如：用户性别
	IsDisable uint        `json:"is_disable" description:"是否禁用：0正常，1禁用"`        // 是否禁用：0正常，1禁用
	UpdateAt  *gtime.Time `json:"update_at"  description:"更新时间"`                // 更新时间
	CreateAt  *gtime.Time `json:"create_at"  description:"创建时间"`                // 创建时间
}
