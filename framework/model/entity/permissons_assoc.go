// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// PermissonsAssoc is the golang structure for table permissons_assoc.
type PermissonsAssoc struct {
	Id            uint        `json:"id"             description:"角色权限关联ID"`         // 角色权限关联ID
	AssocId       uint        `json:"assoc_id"       description:"角色ID"`             // 角色ID
	PermissionsId uint        `json:"permissions_id" description:"权限ID"`             // 权限ID
	AssocType     uint        `json:"assoc_type"     description:"关联类型：0角色关联，1账户关联"` // 关联类型：0角色关联，1账户关联
	CreateAt      *gtime.Time `json:"create_at"      description:"创建时间"`             // 创建时间
}
