// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// MemberTiersBeneJoin is the golang structure for table member_tiers_bene_join.
type MemberTiersBeneJoin struct {
	Id            uint        `json:"id"             description:"会员关联ID"`   // 会员关联ID
	BenefitsAlias string      `json:"benefits_alias" description:"会员可享服务别名"` // 会员可享服务别名
	MbTiersId     uint        `json:"mb_tiers_id"    description:"会员身份ID"`   // 会员身份ID
	CreateAt      *gtime.Time `json:"create_at"      description:"创建时间"`     // 创建时间
}
