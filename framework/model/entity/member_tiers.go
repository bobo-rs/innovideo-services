// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// MemberTiers is the golang structure for table member_tiers.
type MemberTiers struct {
	Id          uint        `json:"id"           description:"会员身份ID"`                                            // 会员身份ID
	Name        string      `json:"name"         description:"会员身份名"`                                             // 会员身份名
	Description string      `json:"description"  description:"会员服务描述"`                                            // 会员服务描述
	Tiers       uint        `json:"tiers"        description:"会员等级：0普通会员，1初级会员，2中级会员，3高级会员，4级顶级会员，5级超级会员，6至臻级会员"` // 会员等级：0普通会员，1初级会员，2中级会员，3高级会员，4级顶级会员，5级超级会员，6至臻级会员
	MarketPrice float64     `json:"market_price" description:"原价"`                                                // 原价
	Price       float64     `json:"price"        description:"销售价"`                                               // 销售价
	ServiceDate uint        `json:"service_date" description:"服务时间期限"`                                            // 服务时间期限
	ServiceUnit string      `json:"service_unit" description:"服务时间单位：d天，m月,y年"`                                   // 服务时间单位：d天，m月,y年
	Status      uint        `json:"status"       description:"会员状态：0启用，1禁用"`                                      // 会员状态：0启用，1禁用
	SaleNum     uint        `json:"sale_num"     description:"销量"`                                                // 销量
	ImageUrl    string      `json:"image_url"    description:"会员推广图"`                                             // 会员推广图
	BgColor     string      `json:"bg_color"     description:"背景色"`                                               // 背景色
	Creator     string      `json:"creator"      description:"创建人名"`                                              // 创建人名
	CreatorId   uint        `json:"creator_id"   description:"创建人ID"`                                             // 创建人ID
	UpdateAt    *gtime.Time `json:"update_at"    description:"更新时间"`                                              // 更新时间
	CreateAt    *gtime.Time `json:"create_at"    description:"创建时间"`                                              // 创建时间
}
