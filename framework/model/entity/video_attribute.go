// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// VideoAttribute is the golang structure for table video_attribute.
type VideoAttribute struct {
	VideoId           uint        `json:"video_id"            description:"视频ID"`             // 视频ID
	Synopsis          string      `json:"synopsis"            description:"简介概要"`             // 简介概要
	EpisodesNum       uint        `json:"episodes_num"        description:"总集数"`              // 总集数
	UpStatus          uint        `json:"up_status"           description:"视频更新状态：0更新中，1已完结"` // 视频更新状态：0更新中，1已完结
	Description       string      `json:"description"         description:"视频宣传详情"`           // 视频宣传详情
	ShareTitle        string      `json:"share_title"         description:"分享视频名"`            // 分享视频名
	ShareContent      string      `json:"share_content"       description:"分享视频内容"`           // 分享视频内容
	IsSeries          uint        `json:"is_series"           description:"是否连续剧：0单剧，1连续剧"`   // 是否连续剧：0单剧，1连续剧
	FreePlayNum       uint        `json:"free_play_num"       description:"VIP视频免费播放集数"`      // VIP视频免费播放集数
	TrialDuration     uint        `json:"trial_duration"      description:"试看时长"`             // 试看时长
	Lang              string      `json:"lang"                description:"语言类型：中英双文"`        // 语言类型：中英双文
	CompletePanguCoin uint        `json:"complete_pangu_coin" description:"全集购买盘古币"`          // 全集购买盘古币
	SinglePanguCoin   uint        `json:"single_pangu_coin"   description:"单集购买币"`            // 单集购买币
	AttachId          string      `json:"attach_id"           description:"附件ID"`             // 附件ID
	UpdateAt          *gtime.Time `json:"update_at"           description:"更新时间"`             // 更新时间
}
