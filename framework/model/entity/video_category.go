// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// VideoCategory is the golang structure for table video_category.
type VideoCategory struct {
	Id          uint        `json:"id"           description:"视频分类ID"`         // 视频分类ID
	Name        string      `json:"name"         description:"分类名"`            // 分类名
	TinyName    string      `json:"tiny_name"    description:"短标题"`            // 短标题
	Pid         uint        `json:"pid"          description:"父级ID"`           // 父级ID
	Sort        uint        `json:"sort"         description:"排序：升序排序0-99999"` // 排序：升序排序0-99999
	IsShow      uint        `json:"is_show"      description:"是否显示"`           // 是否显示
	IsRecommend uint        `json:"is_recommend" description:"是否推荐首页：0否，1是"`   // 是否推荐首页：0否，1是
	UpdateAt    *gtime.Time `json:"update_at"    description:"更新时间"`           // 更新时间
	CreateAt    *gtime.Time `json:"create_at"    description:"创建时间"`           // 创建时间
}
