// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// Attachment is the golang structure for table attachment.
type Attachment struct {
	Id             uint        `json:"id"              description:"附件ID"`                  // 附件ID
	CatId          uint        `json:"cat_id"          description:"附件分类"`                  // 附件分类
	Name           string      `json:"name"            description:"附件名"`                   // 附件名
	RealName       string      `json:"real_name"       description:"附件原名"`                  // 附件原名
	AttSize        int         `json:"att_size"        description:"附件尺寸大小"`                // 附件尺寸大小
	AttPath        string      `json:"att_path"        description:"附件路径"`                  // 附件路径
	AttUrl         string      `json:"att_url"         description:"附件地址"`                  // 附件地址
	AttType        string      `json:"att_type"        description:"附件类型：image/png"`        // 附件类型：image/png
	Position       uint        `json:"position"        description:"排序位置：从小到大"`             // 排序位置：从小到大
	ChannelId      uint        `json:"channel_id"      description:"渠道ID"`                  // 渠道ID
	StorageType    string      `json:"storage_type"    description:"存储类型：local本地，qiniu七牛等"` // 存储类型：local本地，qiniu七牛等
	Hasher         string      `json:"hasher"          description:"Hash值"`                 // Hash值
	AttachId       string      `json:"attach_id"       description:"附件ID"`                  // 附件ID
	Width          uint        `json:"width"           description:"宽度"`                    // 宽度
	Height         uint        `json:"height"          description:"高度"`                    // 高度
	Duration       uint        `json:"duration"        description:"视频时长（S）"`               // 视频时长（S）
	DurationString string      `json:"duration_string" description:"格式化视频时长"`               // 格式化视频时长
	UpdateAt       *gtime.Time `json:"update_at"       description:"更新时间"`                  // 更新时间
	CreateAt       *gtime.Time `json:"create_at"       description:"创建时间"`                  // 创建时间
}
