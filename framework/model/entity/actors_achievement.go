// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// ActorsAchievement is the golang structure for table actors_achievement.
type ActorsAchievement struct {
	Id          uint        `json:"id"           description:"演员个人成就ID"` // 演员个人成就ID
	ActorsId    uint        `json:"actors_id"    description:"演员ID"`     // 演员ID
	AchTitle    string      `json:"ach_title"    description:"成就Title"`  // 成就Title
	AchieveDate *gtime.Time `json:"achieve_date" description:"成就时间"`     // 成就时间
	UpdateAt    *gtime.Time `json:"update_at"    description:"更新时间"`     // 更新时间
	CreateAt    *gtime.Time `json:"create_at"    description:"创建时间"`     // 创建时间
}
