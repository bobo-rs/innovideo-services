// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// ActorsExperience is the golang structure for table actors_experience.
type ActorsExperience struct {
	Id       uint        `json:"id"        description:"个人经历ID"` // 个人经历ID
	ActorsId uint        `json:"actors_id" description:"演员ID"`   // 演员ID
	Content  string      `json:"content"   description:"经历内容"`   // 经历内容
	ExpDate  *gtime.Time `json:"exp_date"  description:"相关经历时间"` // 相关经历时间
	UpdateAt *gtime.Time `json:"update_at" description:"更新时间"`   // 更新时间
	CreateAt *gtime.Time `json:"create_at" description:"创建时间"`   // 创建时间
}
