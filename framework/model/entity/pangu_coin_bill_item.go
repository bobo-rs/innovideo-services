// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// PanguCoinBillItem is the golang structure for table pangu_coin_bill_item.
type PanguCoinBillItem struct {
	Id            uint        `json:"id"             description:"账单明细ID"`         // 账单明细ID
	BillId        uint        `json:"bill_id"        description:"账单ID"`           // 账单ID
	TransactionNo string      `json:"transaction_no" description:"流水单号"`           // 流水单号
	Uid           uint        `json:"uid"            description:"用户ID"`           // 用户ID
	PanguCoin     int         `json:"pangu_coin"     description:"消费盘古币"`          // 消费盘古币
	Type          uint        `json:"type"           description:"账单流水类型：0收入，1支出"` // 账单流水类型：0收入，1支出
	UpdateAt      *gtime.Time `json:"update_at"      description:"更新时间"`           // 更新时间
	CreateAt      *gtime.Time `json:"create_at"      description:"创建时间"`           // 创建时间
}
