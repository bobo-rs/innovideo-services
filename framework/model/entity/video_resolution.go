// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// VideoResolution is the golang structure for table video_resolution.
type VideoResolution struct {
	Id             uint        `json:"id"              description:"视频清晰度ID"`              // 视频清晰度ID
	EpisodesId     uint        `json:"episodes_id"     description:"视频续集ID"`               // 视频续集ID
	Resolution     string      `json:"resolution"      description:"清晰度：480P，720P，1080P等"` // 清晰度：480P，720P，1080P等
	AttachId       string      `json:"attach_id"       description:"附件ID"`                 // 附件ID
	VideoUrl       string      `json:"video_url"       description:"视频地址"`                 // 视频地址
	Size           uint64      `json:"size"            description:"视频大小（B）"`              // 视频大小（B）
	Duration       uint        `json:"duration"        description:"时长（S）"`                // 时长（S）
	DurationString string      `json:"duration_string" description:"格式化时长"`                // 格式化时长
	MimeType       string      `json:"mime_type"       description:"视频类型"`                 // 视频类型
	UpdateAt       *gtime.Time `json:"update_at"       description:"更新时间"`                 // 更新时间
	CreateAt       *gtime.Time `json:"create_at"       description:"创建时间"`                 // 创建时间
}
