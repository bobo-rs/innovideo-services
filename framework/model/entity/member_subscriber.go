// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// MemberSubscriber is the golang structure for table member_subscriber.
type MemberSubscriber struct {
	Id           uint        `json:"id"             description:"会员订阅ID"`                          // 会员订阅ID
	SubscriberNo string      `json:"subscriber_no"  description:"订阅编号"`                            // 订阅编号
	Uid          uint        `json:"uid"            description:"用户ID"`                            // 用户ID
	PayStatus    uint        `json:"pay_status"     description:"支付状态：0待支付，1已支付"`                  // 支付状态：0待支付，1已支付
	Status       uint        `json:"status"         description:"订阅状态：0待确认，1服务中，2服务完成，3取消服务，4已退款"` // 订阅状态：0待确认，1服务中，2服务完成，3取消服务，4已退款
	PayCode      string      `json:"pay_code"       description:"支付方式别名"`                          // 支付方式别名
	PayName      string      `json:"pay_name"       description:"支付方式名"`                           // 支付方式名
	DayFee       float64     `json:"day_fee"        description:"折扣每日单价=支付总额/订阅天数"`                // 折扣每日单价=支付总额/订阅天数
	SubTotalNum  uint        `json:"sub_total_num"  description:"订阅天数"`                            // 订阅天数
	PayFee       float64     `json:"pay_fee"        description:"支付金额"`                            // 支付金额
	RefundFee    float64     `json:"refund_fee"     description:"退款金额"`                            // 退款金额
	MbId         uint        `json:"mb_id"          description:"会员身份ID"`                          // 会员身份ID
	MbName       string      `json:"mb_name"        description:"会员身份名"`                           // 会员身份名
	SubStartDate *gtime.Time `json:"sub_start_date" description:"会员生效时间"`                          // 会员生效时间
	SubEndDate   *gtime.Time `json:"sub_end_date"   description:"会员过期时间"`                          // 会员过期时间
	PayAt        *gtime.Time `json:"pay_at"         description:"会员支付时间"`                          // 会员支付时间
	TradeNo      string      `json:"trade_no"       description:"商户交易支付号"`                         // 商户交易支付号
	PayNo        string      `json:"pay_no"         description:"第三方支付交易号"`                        // 第三方支付交易号
	RefundNo     string      `json:"refund_no"      description:"第三方退款交易号"`                        // 第三方退款交易号
	RefundAt     *gtime.Time `json:"refund_at"      description:"退款时间"`                            // 退款时间
	Remark       string      `json:"remark"         description:"备注"`                              // 备注
	UpdateAt     *gtime.Time `json:"update_at"      description:"更新时间"`                            // 更新时间
	CreateAt     *gtime.Time `json:"create_at"      description:"创建时间"`                            // 创建时间
}
