// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// UserFunds is the golang structure for table user_funds.
type UserFunds struct {
	UserId                uint        `json:"user_id"                  description:"用户ID"`    // 用户ID
	RechargeTotalFee      float64     `json:"recharge_total_fee"       description:"充值总金额"`   // 充值总金额
	AvailableFee          float64     `json:"available_fee"            description:"可用金额"`    // 可用金额
	FreezeFee             float64     `json:"freeze_fee"               description:"冻结金额"`    // 冻结金额
	PanguCoinTotal        uint        `json:"pangu_coin_total"         description:"兑换盘古币总数"` // 兑换盘古币总数
	AvailablePanguCoin    uint        `json:"available_pangu_coin"     description:"可用盘古币数"`  // 可用盘古币数
	FreezePanguCoin       uint        `json:"freeze_pangu_coin"        description:"冻结盘古币数"`  // 冻结盘古币数
	ConsumeTotalPanguCoin float64     `json:"consume_total_pangu_coin" description:"消费总盘古币数"` // 消费总盘古币数
	ConsumeTotalFee       float64     `json:"consume_total_fee"        description:"消费总金额"`   // 消费总金额
	UpdateAt              *gtime.Time `json:"update_at"                description:"更新时间"`    // 更新时间
}
