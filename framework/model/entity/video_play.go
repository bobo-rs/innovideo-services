// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// VideoPlay is the golang structure for table video_play.
type VideoPlay struct {
	Id             uint        `json:"id"               description:"播放ID"`                    // 播放ID
	VideoId        uint        `json:"video_id"         description:"视频ID"`                    // 视频ID
	UserId         uint        `json:"user_id"          description:"用户ID"`                    // 用户ID
	CurrPlayRate   uint        `json:"curr_play_rate"   description:"当前播放进度，最大100，最小0"`        // 当前播放进度，最大100，最小0
	CurrPlayTime   uint        `json:"curr_play_time"   description:"当前播放时间"`                  // 当前播放时间
	CurrPlaySequel uint        `json:"curr_play_sequel" description:"当前播放续集"`                  // 当前播放续集
	PlayTime       uint64      `json:"play_time"        description:"播放时间"`                    // 播放时间
	Resolution     string      `json:"resolution"       description:"当前播放精度：480P，720P，1080P等"` // 当前播放精度：480P，720P，1080P等
	Ip             string      `json:"ip"               description:"当前播放IP"`                  // 当前播放IP
	UpdateAt       *gtime.Time `json:"update_at"        description:"更新时间"`                    // 更新时间
	CreateAt       *gtime.Time `json:"create_at"        description:"创建时间"`                    // 创建时间
}
