// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// VideoBuy is the golang structure for table video_buy.
type VideoBuy struct {
	Id                uint        `json:"id"                  description:"视频购买ID"`         // 视频购买ID
	Uid               uint        `json:"uid"                 description:"用户ID"`           // 用户ID
	VideoId           uint        `json:"video_id"            description:"视频ID"`           // 视频ID
	BuyNo             string      `json:"buy_no"              description:"视频购买编号"`         // 视频购买编号
	VideoName         string      `json:"video_name"          description:"视频名称"`           // 视频名称
	CompletePanguCoin uint        `json:"complete_pangu_coin" description:"全集盘古币数量"`        // 全集盘古币数量
	AddPanguCoin      uint        `json:"add_pangu_coin"      description:"已累计消费盘古币数量"`     // 已累计消费盘古币数量
	Status            uint        `json:"status"              description:"状态：0购买中，1已全剧购买"` // 状态：0购买中，1已全剧购买
	UpdateAt          *gtime.Time `json:"update_at"           description:"更新时间"`           // 更新时间
	CreateAt          *gtime.Time `json:"create_at"           description:"创建时间"`           // 创建时间
}
