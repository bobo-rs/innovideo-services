// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// VideoBuyBill is the golang structure for table video_buy_bill.
type VideoBuyBill struct {
	Id              uint        `json:"id"                description:"视频购买账单ID"`                 // 视频购买账单ID
	BuyId           uint        `json:"buy_id"            description:"购买ID"`                     // 购买ID
	Uid             uint        `json:"uid"               description:"用户ID"`                     // 用户ID
	BuyPanguCoin    uint        `json:"buy_pangu_coin"    description:"支付盘古币"`                    // 支付盘古币
	RefundPanguCoin uint        `json:"refund_pangu_coin" description:"退款盘古币"`                    // 退款盘古币
	BuyEpisodesNum  uint        `json:"buy_episodes_num"  description:"购买集数"`                     // 购买集数
	Content         string      `json:"content"           description:"购买内容，例如[视频名+集数，集数，集数。。。]"` // 购买内容，例如[视频名+集数，集数，集数。。。]
	Remark          string      `json:"remark"            description:"退款备注"`                     // 退款备注
	Status          uint        `json:"status"            description:"购买状态：0已购买，1已退款,2部分退款"`     // 购买状态：0已购买，1已退款,2部分退款
	Type            uint        `json:"type"              description:"购买类型：0单集购买，1全集购买，2余集买断"`   // 购买类型：0单集购买，1全集购买，2余集买断
	RefundAt        *gtime.Time `json:"refund_at"         description:"退款时间"`                     // 退款时间
	UpdateAt        *gtime.Time `json:"update_at"         description:"更新时间"`                     // 更新时间
	CreateAt        *gtime.Time `json:"create_at"         description:"创建时间"`                     // 创建时间
}
