// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// UserOauth is the golang structure for table user_oauth.
type UserOauth struct {
	Id       uint        `json:"id"        description:"用户授权ID"`        // 用户授权ID
	Uid      uint        `json:"uid"       description:"用户ID"`          // 用户ID
	Openid   string      `json:"openid"    description:"授权openid"`      // 授权openid
	Unionid  string      `json:"unionid"   description:"微信unionid"`     // 微信unionid
	Oauth    string      `json:"oauth"     description:"授权渠道，如：weixin"` // 授权渠道，如：weixin
	CreateAt *gtime.Time `json:"create_at" description:"创建时间"`          // 创建时间
}
