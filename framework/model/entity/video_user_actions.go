// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// VideoUserActions is the golang structure for table video_user_actions.
type VideoUserActions struct {
	Id       uint        `json:"id"        description:"视频收藏ID"`          // 视频收藏ID
	VideoId  uint        `json:"video_id"  description:"视频ID"`            // 视频ID
	UserId   uint        `json:"user_id"   description:"用户ID"`            // 用户ID
	Ip       string      `json:"ip"        description:"当前ip"`            // 当前ip
	Type     string      `json:"type"      description:"操作类型：C收藏，L点赞，T踩"` // 操作类型：C收藏，L点赞，T踩
	CreateAt *gtime.Time `json:"create_at" description:"创建时间"`            // 创建时间
}
