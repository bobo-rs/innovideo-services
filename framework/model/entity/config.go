// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// Config is the golang structure for table config.
type Config struct {
	Id           uint        `json:"id"            description:"配置ID"`   // 配置ID
	Name         string      `json:"name"          description:"配置名称"`   // 配置名称
	Description  string      `json:"description"   description:"配置详情简介"` // 配置详情简介
	Value        string      `json:"value"         description:"配置值"`    // 配置值
	DefaultValue string      `json:"default_value" description:"默认配置值"`  // 默认配置值
	GroupId      uint        `json:"group_id"      description:"配置组ID"`  // 配置组ID
	GroupName    string      `json:"group_name"    description:"配置组名"`   // 配置组名
	Remark       string      `json:"remark"        description:"备注"`     // 备注
	UpdateAt     *gtime.Time `json:"update_at"     description:"更新时间"`   // 更新时间
	CreateAt     *gtime.Time `json:"create_at"     description:"创建时间"`   // 创建时间
}
