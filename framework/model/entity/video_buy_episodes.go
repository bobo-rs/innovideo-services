// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// VideoBuyEpisodes is the golang structure for table video_buy_episodes.
type VideoBuyEpisodes struct {
	Id           uint        `json:"id"            description:"视频购买单集ID"` // 视频购买单集ID
	BillId       uint        `json:"bill_id"       description:"视频购买账单ID"` // 视频购买账单ID
	BuyId        uint        `json:"buy_id"        description:"视频购买ID"`   // 视频购买ID
	Uid          uint        `json:"uid"           description:"用户ID"`     // 用户ID
	VideoId      uint        `json:"video_id"      description:"视频ID"`     // 视频ID
	EpisodesId   uint        `json:"episodes_id"   description:"单集ID"`     // 单集ID
	EpisodesName string      `json:"episodes_name" description:"单集名称"`     // 单集名称
	EpisodesNum  uint        `json:"episodes_num"  description:"续集集数"`     // 续集集数
	CreateAt     *gtime.Time `json:"create_at"     description:"创建时间"`     // 创建时间
}
