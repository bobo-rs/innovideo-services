// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// PanguCoinOrder is the golang structure for table pangu_coin_order.
type PanguCoinOrder struct {
	Id              uint        `json:"id"                description:"盘古币订单ID"`                // 盘古币订单ID
	Uid             uint        `json:"uid"               description:"用户ID"`                   // 用户ID
	OrderNo         string      `json:"order_no"          description:"订单号"`                    // 订单号
	TradeNo         string      `json:"trade_no"          description:"商户交易号"`                  // 商户交易号
	PayNo           string      `json:"pay_no"            description:"第三方支付交易号"`               // 第三方支付交易号
	RefundNo        string      `json:"refund_no"         description:"退款交易号"`                  // 退款交易号
	PayCode         string      `json:"pay_code"          description:"支付别名"`                   // 支付别名
	PayName         string      `json:"pay_name"          description:"支付名"`                    // 支付名
	PayFee          float64     `json:"pay_fee"           description:"支付金额"`                   // 支付金额
	RefundFee       float64     `json:"refund_fee"        description:"退款金额"`                   // 退款金额
	PanguCoin       uint        `json:"pangu_coin"        description:"兑换盘古币数量"`                // 兑换盘古币数量
	RefundPanguCoin uint        `json:"refund_pangu_coin" description:"退回盘古币数量"`                // 退回盘古币数量
	Currency        string      `json:"currency"          description:"当前支付货币，默认人民币CNY"`        // 当前支付货币，默认人民币CNY
	PayAt           *gtime.Time `json:"pay_at"            description:"支付时间"`                   // 支付时间
	RefundAt        *gtime.Time `json:"refund_at"         description:"退款时间"`                   // 退款时间
	PayStatus       uint        `json:"pay_status"        description:"支付状态：0待支付，1已支付"`         // 支付状态：0待支付，1已支付
	Status          uint        `json:"status"            description:"状态：0待确认，1已完成，2已取消，3已退款"` // 状态：0待确认，1已完成，2已取消，3已退款
	Remark          string      `json:"remark"            description:"备注"`                     // 备注
	Content         string      `json:"content"           description:"描述内容"`                   // 描述内容
	UpdateAt        *gtime.Time `json:"update_at"         description:"更新时间"`                   // 更新时间
	CreateAt        *gtime.Time `json:"create_at"         description:"创建时间"`                   // 创建时间
}
