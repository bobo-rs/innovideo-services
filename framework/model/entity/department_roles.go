// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// DepartmentRoles is the golang structure for table department_roles.
type DepartmentRoles struct {
	Id       uint        `json:"id"        description:"部门角色关联ID"` // 部门角色关联ID
	RoleId   uint        `json:"role_id"   description:"角色ID"`     // 角色ID
	DepartId uint        `json:"depart_id" description:"部门ID"`     // 部门ID
	CreateAt *gtime.Time `json:"create_at" description:"创建时间"`     // 创建时间
}
