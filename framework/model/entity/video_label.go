// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// VideoLabel is the golang structure for table video_label.
type VideoLabel struct {
	Id       uint        `json:"id"        description:"视频标签ID"` // 视频标签ID
	VideoId  uint        `json:"video_id"  description:"视频ID"`   // 视频ID
	Value    string      `json:"value"     description:"标签值"`    // 标签值
	CreateAt *gtime.Time `json:"create_at" description:"创建时间"`   // 创建时间
}
