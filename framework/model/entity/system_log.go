// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// SystemLog is the golang structure for table system_log.
type SystemLog struct {
	Id           uint        `json:"id"            description:"日志id"`                     // 日志id
	ModuleName   string      `json:"module_name"   description:"操作模块名"`                    // 操作模块名
	Content      string      `json:"content"       description:"操作详情"`                     // 操作详情
	TagName      string      `json:"tag_name"      description:"TAG栏目名，例如：系统管理"`           // TAG栏目名，例如：系统管理
	Ip           string      `json:"ip"            description:"操作IP地址"`                   // 操作IP地址
	OperateType  int         `json:"operate_type"  description:"日志操作类型：0操作日志，1登录操作，2系统日志"` // 日志操作类型：0操作日志，1登录操作，2系统日志
	ApiUrl       string      `json:"api_url"       description:"api URL"`                  // api URL
	UserId       uint        `json:"user_id"       description:"用户ID"`                     // 用户ID
	ManageId     uint        `json:"manage_id"     description:"账户id"`                     // 账户id
	ManageName   string      `json:"manage_name"   description:"账户名"`                      // 账户名
	MerchantId   uint        `json:"merchant_id"   description:"商户ID"`                     // 商户ID
	ResponseText string      `json:"response_text" description:"资源响应"`                     // 资源响应
	ParamText    string      `json:"param_text"    description:"请求资源"`                     // 请求资源
	UpdateAt     *gtime.Time `json:"update_at"     description:"更新时间"`                     // 更新时间
	CreateAt     *gtime.Time `json:"create_at"     description:"创建时间"`                     // 创建时间
}
