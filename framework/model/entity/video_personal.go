// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// VideoPersonal is the golang structure for table video_personal.
type VideoPersonal struct {
	Id       uint        `json:"id"        description:"视频相关人员ID"`                          // 视频相关人员ID
	VideoId  uint        `json:"video_id"  description:"视频ID"`                              // 视频ID
	ActorsId uint        `json:"actors_id" description:"演员ID"`                              // 演员ID
	Name     string      `json:"name"      description:"参演名"`                               // 参演名
	Type     uint        `json:"type"      description:"角色类型：0导演，1副导演，2领衔主演，3主演，4客串嘉宾，5其他"` // 角色类型：0导演，1副导演，2领衔主演，3主演，4客串嘉宾，5其他
	Portray  string      `json:"portray"   description:"饰演角色"`                              // 饰演角色
	IsShow   uint        `json:"is_show"   description:"是否展示：0展示，1隐藏"`                      // 是否展示：0展示，1隐藏
	UpdateAt *gtime.Time `json:"update_at" description:"更新时间"`                              // 更新时间
	CreateAt *gtime.Time `json:"create_at" description:"创建时间"`                              // 创建时间
}
