// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// SmsTemplateOut is the golang structure for table sms_template_out.
type SmsTemplateOut struct {
	Id            uint        `json:"id"              description:"短信模板关联外部平台ID"`                             // 短信模板关联外部平台ID
	VarAlias      string      `json:"var_alias"       description:"系统模板变量"`                                   // 系统模板变量
	TemplateId    string      `json:"template_id"     description:"外部平台ID"`                                   // 外部平台ID
	FromType      string      `json:"from_type"       description:"平台类型：tencent腾讯，alipay阿里云[is_config,短信通道]"` // 平台类型：tencent腾讯，alipay阿里云[is_config,短信通道]
	OutTmplStatus string      `json:"out_tmpl_status" description:"外部模板状态"`                                   // 外部模板状态
	UpdateAt      *gtime.Time `json:"update_at"       description:"更新时间"`                                     // 更新时间
	CreateAt      *gtime.Time `json:"create_at"       description:"创建时间"`                                     // 创建时间
}
