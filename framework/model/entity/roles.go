// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// Roles is the golang structure for table roles.
type Roles struct {
	Id          uint        `json:"id"          description:"角色ID"`           // 角色ID
	Name        string      `json:"name"        description:"角色名"`            // 角色名
	Description string      `json:"description" description:"详情"`             // 详情
	RoleStatus  uint        `json:"role_status" description:"角色状态：0正常，1冻结"`   // 角色状态：0正常，1冻结
	Sort        uint        `json:"sort"        description:"排序：0-255，默认255"` // 排序：0-255，默认255
	UpdateAt    *gtime.Time `json:"update_at"   description:"更新时间"`           // 更新时间
	CreateAt    *gtime.Time `json:"create_at"   description:"创建时间"`           // 创建时间
}
