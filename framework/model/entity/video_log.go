// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// VideoLog is the golang structure for table video_log.
type VideoLog struct {
	Id         uint        `json:"id"          description:"视频日志ID"` // 视频日志ID
	VideoId    uint        `json:"video_id"    description:"视频ID"`   // 视频ID
	Content    string      `json:"content"     description:"操作内容"`   // 操作内容
	ManageId   uint        `json:"manage_id"   description:"管理员ID"`  // 管理员ID
	UserId     uint        `json:"user_id"     description:"用户ID"`   // 用户ID
	ManageName string      `json:"manage_name" description:"操作管理员名"` // 操作管理员名
	Url        string      `json:"url"         description:"操作URL"`  // 操作URL
	CreateAt   *gtime.Time `json:"create_at"   description:"创建时间"`   // 创建时间
}
