// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// PanguCoinBill is the golang structure for table pangu_coin_bill.
type PanguCoinBill struct {
	Id                 uint        `json:"id"                   description:"盘古币账单"`                                     // 盘古币账单
	Uid                uint        `json:"uid"                  description:"用户ID"`                                      // 用户ID
	OrderId            uint        `json:"order_id"             description:"盘古币充值订单ID"`                                 // 盘古币充值订单ID
	BillNo             string      `json:"bill_no"              description:"账单编号"`                                      // 账单编号
	TotalPanguCoin     uint        `json:"total_pangu_coin"     description:"初始盘古币-留存记录不变"`                              // 初始盘古币-留存记录不变
	AvailablePanguCoin uint        `json:"available_pangu_coin" description:"可用盘古币"`                                     // 可用盘古币
	ExpiredPanguCoin   uint        `json:"expired_pangu_coin"   description:"过期盘古币"`                                     // 过期盘古币
	ExpiredDate        *gtime.Time `json:"expired_date"         description:"过期时间"`                                      // 过期时间
	Status             uint        `json:"status"               description:"盘古币账单状态：0正常，1交易完成，2部分过期，3全额过期，4部分退款，5全额退款"` // 盘古币账单状态：0正常，1交易完成，2部分过期，3全额过期，4部分退款，5全额退款
	BillType           uint        `json:"bill_type"            description:"账单类型：0充值兑换，1活动赠送，2平台赠送，3新用户赠送, 4售后退回"`      // 账单类型：0充值兑换，1活动赠送，2平台赠送，3新用户赠送, 4售后退回
	Remark             string      `json:"remark"               description:"备注"`                                        // 备注
	UpdateAt           *gtime.Time `json:"update_at"            description:"更新时间"`                                      // 更新时间
	CreateAt           *gtime.Time `json:"create_at"            description:"创建时间"`                                      // 创建时间
}
