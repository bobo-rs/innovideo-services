// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// VideoPlaySequel is the golang structure for table video_play_sequel.
type VideoPlaySequel struct {
	Id            uint        `json:"id"              description:"播放明细ID"`                  // 播放明细ID
	PlayId        uint        `json:"play_id"         description:"播放ID"`                    // 播放ID
	VideoSequelId uint        `json:"video_sequel_id" description:"视频续集ID"`                  // 视频续集ID
	CurrPlayRate  uint        `json:"curr_play_rate"  description:"当前播放进度"`                  // 当前播放进度
	CurrPlayTime  uint        `json:"curr_play_time"  description:"当前播放时间"`                  // 当前播放时间
	PlaySequel    uint        `json:"play_sequel"     description:"播放续集"`                    // 播放续集
	Resolution    string      `json:"resolution"      description:"当前播放精度：480P，720P，1080P等"` // 当前播放精度：480P，720P，1080P等
	Ip            string      `json:"ip"              description:"当前ip"`                    // 当前ip
	UpdateAt      *gtime.Time `json:"update_at"       description:"更新时间"`                    // 更新时间
	CreateAt      *gtime.Time `json:"create_at"       description:"创建时间"`                    // 创建时间
}
