// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// Article is the golang structure for table article.
type Article struct {
	Id          uint        `json:"id"          description:"文章ID"`               // 文章ID
	Name        string      `json:"name"        description:"文章名"`                // 文章名
	AbbrevName  string      `json:"abbrev_name" description:"文章缩写名"`              // 文章缩写名
	Description string      `json:"description" description:"文章摘要"`               // 文章摘要
	Cid         uint        `json:"cid"         description:"文章分类ID"`             // 文章分类ID
	Keywords    string      `json:"keywords"    description:"关键词"`                // 关键词
	ImgUrl      string      `json:"img_url"     description:"图片地址"`               // 图片地址
	VideoUrl    string      `json:"video_url"   description:"视频地址"`               // 视频地址
	Content     string      `json:"content"     description:"文章内容"`               // 文章内容
	Status      uint        `json:"status"      description:"状态：0正常，1禁用"`         // 状态：0正常，1禁用
	IsShow      uint        `json:"is_show"     description:"是否显示：0正常，1隐藏"`       // 是否显示：0正常，1隐藏
	Sort        uint        `json:"sort"        description:"排序值：0-99999升序"`      // 排序值：0-99999升序
	AssocAlias  string      `json:"assoc_alias" description:"关联别名，例如：注册协议、网服协议等"` // 关联别名，例如：注册协议、网服协议等
	UpdateAt    *gtime.Time `json:"update_at"   description:"更新时间"`               // 更新时间
	CreateAt    *gtime.Time `json:"create_at"   description:"创建时间"`               // 创建时间
}
