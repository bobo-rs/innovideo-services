// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// EmailTemplate is the golang structure for table email_template.
type EmailTemplate struct {
	Id       uint        `json:"id"        description:"邮件模板ID"`                        // 邮件模板ID
	Name     string      `json:"name"      description:"模板名"`                           // 模板名
	VarName  string      `json:"var_name"  description:"模板变量名，例如：login_captcha[登录验证码]"` // 模板变量名，例如：login_captcha[登录验证码]
	Subject  string      `json:"subject"   description:"主题"`                            // 主题
	Content  string      `json:"content"   description:"邮件内容，变量{1}{2}等大括号包含数字"`         // 邮件内容，变量{1}{2}等大括号包含数字
	UpdateAt *gtime.Time `json:"update_at" description:"更新时间"`                          // 更新时间
	CreateAt *gtime.Time `json:"create_at" description:"创建时间"`                          // 创建时间
}
