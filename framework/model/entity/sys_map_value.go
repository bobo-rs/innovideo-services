// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// SysMapValue is the golang structure for table sys_map_value.
type SysMapValue struct {
	Id       uint        `json:"id"        description:"字典值ID"`             // 字典值ID
	Value    string      `json:"value"     description:"字典值"`               // 字典值
	Explain  string      `json:"explain"   description:"字典值解析"`             // 字典值解析
	Name     string      `json:"name"      description:"字典名-sys_map[name]"` // 字典名-sys_map[name]
	UpdateAt *gtime.Time `json:"update_at" description:"更新时间"`              // 更新时间
	CreateAt *gtime.Time `json:"create_at" description:"创建时间"`              // 创建时间
}
