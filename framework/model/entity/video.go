// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// =================================================================================

package entity

import (
	"github.com/gogf/gf/v2/os/gtime"
)

// Video is the golang structure for table video.
type Video struct {
	Id          uint        `json:"id"           description:"视频ID"`                               // 视频ID
	VideoNo     string      `json:"video_no"     description:"视频编号"`                               // 视频编号
	VideoName   string      `json:"video_name"   description:"视频名"`                                // 视频名
	Keywords    string      `json:"keywords"     description:"关键词"`                                // 关键词
	CateId1     uint        `json:"cate_id_1"    description:"一级类目"`                               // 一级类目
	CateId2     uint        `json:"cate_id_2"    description:"二级类目"`                               // 二级类目
	Region      string      `json:"region"       description:"地区"`                                 // 地区
	YearDate    string      `json:"year_date"    description:"上映年份"`                               // 上映年份
	ReleaseDate *gtime.Time `json:"release_date" description:"上映时间"`                               // 上映时间
	IsRecommend uint        `json:"is_recommend" description:"是否推荐：0否，1是"`                         // 是否推荐：0否，1是
	IsBest      uint        `json:"is_best"      description:"是否精品：0否，1是"`                         // 是否精品：0否，1是
	IsTop       uint        `json:"is_top"       description:"是否置顶：0否，1是"`                         // 是否置顶：0否，1是
	IsNew       uint        `json:"is_new"       description:"是否新剧：0否，1是"`                         // 是否新剧：0否，1是
	IsHot       uint        `json:"is_hot"       description:"是否热播：0否，1是"`                         // 是否热播：0否，1是
	FeeType     uint        `json:"fee_type"     description:"收费类型：0免费，1VIP付费"`                    // 收费类型：0免费，1VIP付费
	ImageUrl    string      `json:"image_url"    description:"海报"`                                 // 海报
	Browse      uint        `json:"browse"       description:"浏览量"`                                // 浏览量
	PlayNum     uint        `json:"play_num"     description:"播放量"`                                // 播放量
	Collect     uint        `json:"collect"      description:"收藏量"`                                // 收藏量
	ShareNum    uint        `json:"share_num"    description:"分享量"`                                // 分享量
	Score       float64     `json:"score"        description:"评分，最高分10分"`                          // 评分，最高分10分
	Status      uint        `json:"status"       description:"视频状态：0待审核，1审核通过，2驳回申请，3违规下架, 4视频删除"` // 视频状态：0待审核，1审核通过，2驳回申请，3违规下架, 4视频删除
	IsRelease   uint        `json:"is_release"   description:"是否发布：0未发布，1已发布"`                     // 是否发布：0未发布，1已发布
	UpdateAt    *gtime.Time `json:"update_at"    description:"更新时间"`                               // 更新时间
	CreateAt    *gtime.Time `json:"create_at"    description:"创建时间"`                               // 创建时间
}
