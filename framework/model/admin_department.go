package model

type (
	// DepartmentPeopleNumItem 部门人数熟悉
	DepartmentPeopleNumItem struct {
		DepartId uint `json:"depart_id" dc:""`
		Total    uint `json:"total" dc:"部门人数"`
	}

	// AdminIdAndDepartIdItem 管理员ID或部门ID属性
	AdminIdAndDepartIdItem struct {
		AdminId  uint `json:"admin_id" dc:"管理员ID"`
		DepartId uint `json:"depart_id" dc:"部门ID"`
	}
)
