package model

type (
	// ArticleCategoryListItem 获取文章类目列表属性
	ArticleCategoryListItem struct {
		ArticleCategoryBasicItem
		ArticleCategoryStatusItem
	}

	// ArticleCategoryDetailItem 获取文章类目详情属性
	ArticleCategoryDetailItem struct {
		ArticleCategoryBasicItem
		ArticleCategoryStatusItem
		ArticleCategoryAttrItem
	}

	// ArticleCategoryBasicItem 文章类目基础属性
	ArticleCategoryBasicItem struct {
		Id   uint   `json:"id" dc:"类目ID"`
		Name string `json:"name" dc:"类目名"`
		Pid  uint   `json:"pid" dc:"父级ID"`
	}

	// ArticleCategoryAttrItem 文章类目属性
	ArticleCategoryAttrItem struct {
		Description string `json:"description" dc:"类目详情"`
	}

	// ArticleCategoryStatusItem 文章类目状态属性
	ArticleCategoryStatusItem struct {
		Status uint `json:"status" dc:"类目状态"`
		IsShow uint `json:"is_show" dc:"是否显示"`
	}
)
