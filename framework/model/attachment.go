package model

type (
	// UploadAttachmentItem 上传附件详情
	UploadAttachmentItem struct {
		AttPath     string `json:"att_path" dc:"附件路径"`
		AttUrl      string `json:"att_url" dc:"附件地址"`
		AttType     string `json:"att_type" dc:"附件类型"`
		StorageType string `json:"storage_type" dc:"存储类型"`
		Hasher      string `json:"hasher" dc:"Hash值"`
		AttachId    string `json:"attach_id" dc:"附件ID"`
		Width       uint   `json:"width" dc:"宽度"`
		Height      uint   `json:"height" dc:"高度"`
		AttSize     int    `json:"att_size" dc:"内存大小"`
		Name        string `json:"name" dc:"附件名"`
		RealName    string `json:"real_name" dc:"附件原名"`
	}

	// AttachmentBasicItem 文件基础属性
	AttachmentBasicItem struct {
		Id          uint   `json:"id"           description:"附件ID"`                  // 附件ID
		CatId       uint   `json:"cat_id"       description:"附件分类"`                  // 附件分类
		Name        string `json:"name"         description:"附件名"`                   // 附件名
		RealName    string `json:"real_name"    description:"附件原名"`                  // 附件原名
		AttSize     int    `json:"att_size"     description:"附件尺寸大小"`                // 附件尺寸大小
		AttPath     string `json:"att_path"     description:"附件路径"`                  // 附件路径
		AttUrl      string `json:"att_url"      description:"附近地址"`                  // 附近地址
		AttType     string `json:"att_type"     description:"附件类型：image/png"`        // 附件类型：image/png
		Position    uint   `json:"position"     description:"排序位置：从小到大"`             // 排序位置：从小到大
		ChannelId   uint   `json:"channel_id"   description:"渠道ID"`                  // 渠道ID
		StorageType string `json:"storage_type" description:"存储类型：local本地，qiniu七牛等"` // 存储类型：local本地，qiniu七牛等
		Hasher      string `json:"hasher"       description:"Hash值"`                 // Hash值
		AttachId    string `json:"attach_id"    description:"附件ID"`                  // 附件ID
		Width       uint   `json:"width"        description:"宽度"`                    // 宽度
		Height      uint   `json:"height"       description:"高度"`                    // 高度
	}

	// AttachmentUrlItem 附件地址属性
	AttachmentUrlItem struct {
		AttPath string `json:"att_path" dc:"附件路径"`
		AttUrl  string `json:"att_url" dc:"附件URL"`
	}

	// AttachmentUrlAttrItem 附件URL属性信息
	AttachmentUrlAttrItem struct {
		AttachmentUrlItem
		Hasher         string `json:"hasher"  dc:"Hash值"`           // Hash值
		AttachId       string `json:"attach_id" dc:"附件ID"`          // 附件ID
		Width          uint   `json:"width" dc:"宽度"`                // 宽度
		Height         uint   `json:"height" dc:"高度"`               // 高度
		AttSize        uint   `json:"att_size" dc:"附件尺寸大小"`         // 附件尺寸大小
		AttType        string `json:"att_type" dc:"附件类型：image/png"` // 附件类型：image/png
		Duration       uint   `json:"duration" dc:"视频时长"`
		DurationString string `json:"duration_string" dc:"格式化视频时长"`
	}
)
