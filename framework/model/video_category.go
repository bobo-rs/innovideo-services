package model

type (
	// VideoCategoryLabelList 视频分类标签列表
	VideoCategoryLabelList []VideoCategoryItem

	// VideoCategoryTreeList 获取视频分类树形列表
	VideoCategoryTreeList []VideoCategoryTreeItem

	// VideoCategoryDetail 获取视频分类详情
	VideoCategoryDetail struct {
		VideoCategoryItem
		Pid uint `json:"pid" dc:"视频父级ID"`
	}
)

type (

	// VideoCategoryTreeItem 视频分类树形熟悉
	VideoCategoryTreeItem struct {
		VideoCategoryItem
		Pid uint `json:"pid" dc:"视频父级ID"`
	}

	// VideoCategoryItem 视频分类标签属性
	VideoCategoryItem struct {
		Id       uint   `json:"id" dc:"类目ID"`
		Name     string `json:"name" dc:"类目名"`
		TinyName string `json:"tiny_name" dc:"短标题"`
	}
)
