package model

type (
	// GenBuyEpisodesInput 生成购买剧集请求参数
	GenBuyEpisodesInput struct {
		Uid      uint
		VideoId  uint
		BuyId    uint
		BillId   uint
		Episodes []VideoEpisodesBuyItem
	}
)

type (
	// VideoBuyEpisodesWhereItem 视频购买剧集搜索条件属性
	VideoBuyEpisodesWhereItem struct {
		Uid        uint   `json:"uid" dc:"用户ID"`
		VideoId    uint   `json:"video_id" dc:"视频ID"`
		EpisodesId []uint `json:"episodes_id" dc:"续集ID"`
		BuyId      uint   `json:"buy_id" dc:"购买ID"`
		BillId     []uint `json:"bill_id" dc:"视频购买账单ID"`
	}

	// VideoBuyEpisodesAuthItem 用户购买续集结果授权属性
	VideoBuyEpisodesAuthItem struct {
		VideoId     uint `json:"video_id" dc:"视频ID"`
		EpisodesId  uint `json:"episodes_id" dc:"视频续集ID"`
		EpisodesNum uint `json:"episodes_num" dc:"集数"`
	}

	// VideoBuyEpisodesItem 视频购买剧集属性
	VideoBuyEpisodesItem struct {
		Id           uint   `json:"id" dc:"视频购买单集ID"`
		VideoId      uint   `json:"video_id" dc:"视频ID"`
		EpisodesId   uint   `json:"episodes_id" dc:"视频续集ID"`
		EpisodesName string `json:"episodes_name" dc:"剧集名"`
		EpisodesNum  uint   `json:"episodes_num" dc:"集数"`
	}
)
