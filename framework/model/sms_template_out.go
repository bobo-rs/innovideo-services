package model

import "gitee.com/bobo-rs/innovideo-services/enums"

type (
	// SmsTemplateOutListInput 获取外部平台SMS模板列表请求参数
	SmsTemplateOutListInput struct {
		Alias    string `json:"alias" dc:"系统模板变量"`
		FromType string `json:"from_type" dc:"平台类型：tencent腾讯，alipay阿里云"`
	}

	// SmsTemplateOutListOutput 获取外部SMS模板列表响应结果
	SmsTemplateOutListOutput []SmsTemplateOutItem
)

type (

	// SmsTemplateOutItem 外部SMS模板属性
	SmsTemplateOutItem struct {
		VarAlias string `json:"var_alias" dc:"模板变量"`
		SmsOutItem
	}

	// SmsOutItem 外部短信平台模板属性
	SmsOutItem struct {
		TemplateId    string `json:"template_id" dc:"平台短信模板ID"`
		FromType      string `json:"from_type" dc:"短信平台"`
		OutTmplStatus string `json:"out_tmpl_status" dc:"外部平台状态"`
	}

	// SmsSyncOutTmplStatusItem 同步外部平台短信状态属性
	SmsSyncOutTmplStatusItem struct {
		Id            uint   `json:"id" dc:"关联ID"`
		TemplateId    string `json:"template_id" dc:"模板ID"`
		OutTmplStatus string `json:"out_tmpl_status" dc:"外部模板状态"`
	}

	// SendOutFromSmsItem 通过手机号给平台提交短信发送信息
	SendOutFromSmsItem struct {
		Mobile []string `json:"mobile" dc:"发送手机号"`
		SendSmsTemplateItem
	}

	// SendSmsResultItem 发送短信记录属性
	SendSmsResultItem struct {
		Mobile string `json:"mobile" dc:"发送手机号"`
		SendSmsTemplateItem
		CurrTemplateItem
	}

	// SendSmsTemplateItem 发送短信模板属性
	SendSmsTemplateItem struct {
		VarAlias       enums.SmsAlias `json:"var_alias" dc:"模板变量"`
		TemplateParams []string       `json:"template_params" dc:"短信模板参数"`
		SmsType        enums.SmsType  `json:"sms_type" dc:"短信类型"`
		Code           string         `json:"code" dc:"短信验证码"`
	}
)
