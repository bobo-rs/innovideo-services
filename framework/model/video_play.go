package model

type (
	// VideoPlayHistoryListOutput 获取用户视频播放历史列表结果输出
	VideoPlayHistoryListOutput struct {
		CommonResponseItem
		Rows []VideoPlayHistoryListItem `json:"rows" dc:"数据列表"`
	}

	// VideoPlayHistorySaveInput 保存用户视频播放记录请求参数
	VideoPlayHistorySaveInput struct {
		VideoId      uint   `json:"video_id" dc:"视频ID"`
		EpisodesId   uint   `json:"episodes_id" dc:"视频续集ID"`
		CurrPlayTime uint   `json:"curr_play_time" dc:"当前播放时间"`
		Resolution   string `json:"resolution" dc:"清晰度"`
	}
)

type (
	// VideoPlayAuthItem 视频播放授权属性
	VideoPlayAuthItem struct {
		Id uint `json:"id" dc:"视频播放ID"`
		VideoPlayCurrAttrItem
		PlaySequelResult map[uint]bool `json:"play_sequel_result" dc:"播放续集结果"`
	}

	// VideoPlayHistoryListItem 获取用户视频播放历史列表属性
	VideoPlayHistoryListItem struct {
		VideoPlayHistoryItem
		VideoPlayListItem
	}

	// VideoPlayHistoryItem 用户视频播放历史属性
	VideoPlayHistoryItem struct {
		Id      uint `json:"play_id" dc:"视频播放ID"`
		VideoId uint `json:"video_id" dc:"视频ID"`
		VideoPlayCurrAttrItem
	}

	// VideoPlayBasicItem 视频播放基础信息
	VideoPlayBasicItem struct {
		Id         uint   `json:"id" dc:""`
		VideoId    uint   `json:"video_id" dc:"视频ID"`
		Resolution string `json:"resolution" dc:"清晰度"`
	}

	// VideoPlayCurrAttrItem 当前视频播放属性
	VideoPlayCurrAttrItem struct {
		CurrPlayRate   uint   `json:"curr_play_rate" dc:"当前播放进度"`
		CurrPlayTime   uint   `json:"curr_play_time" dc:"当前播放时间"`
		CurrPlaySequel uint   `json:"curr_play_sequel" dc:"当前播放续集"`
		Resolution     string `json:"resolution" dc:"播放精度"`
	}
)
