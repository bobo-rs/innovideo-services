package model

import "gitee.com/bobo-rs/innovideo-services/enums"

type (
	// CommonPaginationItem 公共分页属性
	CommonPaginationItem struct {
		Page int `json:"page" dc:"页码"`
		Size int `json:"size" dc:"查询数量"`
	}

	// CommonResponseItem 公共响应资源属性
	CommonResponseItem struct {
		Total int `json:"total" dc:"总数"`
	}

	// CommonListAndTotalInput 公共处理列表和总数请求参数
	CommonListAndTotalInput struct {
		CommonPaginationItem
		Where interface{} `json:"where"`
		Sort  string      `json:"sort"`
	}

	// CommonIncOrDecByIdItem 根据主键ID更新字段数量
	CommonIncOrDecByIdItem struct {
		Id []uint // ID字段
		CommonIncOrDecItem
	}

	// CommonIncOrDecItem 增量或减量属性
	CommonIncOrDecItem struct {
		Columns  string // 更新字段
		Amount   int    // 更新数量
		IncByDec bool   // 是否增量和减量：false增量，true减量
	}

	// CommonFormSideItem 平台端
	CommonFormSideItem struct {
		FormSide enums.FormSide `json:"form_side" dc:"平台端"`
	}

	// CommonSearchDateItem 公共搜索时间
	CommonSearchDateItem struct {
		SearchAt    string `json:"search_at" dc:"搜索时间"`
		IsDefaultAt bool   `json:"is_default_at" dc:"是否启用默认时间：false否, true启用"`
	}
)
