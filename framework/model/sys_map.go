package model

type (
	// SysMapGetListInput 获取系统字典列表请求参数
	SysMapGetListInput struct {
		CommonPaginationItem
		Name      string `json:"name" dc:"字典名"`
		IsDisable int    `json:"is_disable" dc:"是否禁用：0正常，1禁用"`
	}

	// SysMapGetListOutput 获取系统字典列表输出
	SysMapGetListOutput struct {
		CommonResponseItem
		Rows []SysMapListItem `json:"rows" dc:"数据列表"`
	}
)

type (
	// SysMapListItem 获取系统字典列表属性
	SysMapListItem struct {
		SysMapBasicItem
		SysMapAttrItem
		CreateAt string `json:"create_at" dc:"创建时间"`
	}

	// SysMapDetailItem 获取系统字典详情属性
	SysMapDetailItem struct {
		SysMapBasicItem
		SysMapAttrItem
	}

	// SysMapBasicItem 系统字典基础属性
	SysMapBasicItem struct {
		Id   uint   `json:"id" dc:"字典ID"`
		Name string `json:"name" dc:"字典名"`
	}

	// SysMapAttrItem 字典属性
	SysMapAttrItem struct {
		Explain   string `json:"explain" dc:"字典名解析"`
		IsDisable uint   `json:"is_disable" dc:"是否禁用：0正常，1禁用"`
	}
)
