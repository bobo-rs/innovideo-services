package model

type (
	// VideoUserActionsAttrItem 用户视频操作属性
	VideoUserActionsAttrItem struct {
		IsCollect bool `json:"is_collect" dc:"是否收藏：false否，true是"`
		IsLike    bool `json:"is_like" dc:"是否点赞：false否，true是"`
		IsTread   bool `json:"is_tread" dc:"是否踩：false否，true是"`
	}

	// VideoUserActionsBasicItem 用户视频操作基础属性
	VideoUserActionsBasicItem struct {
		VideoId uint   `json:"video_id" dc:"视频ID"`
		Type    string `json:"type" dc:"视频操作类型"`
	}
)
