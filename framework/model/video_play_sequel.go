package model

type (
	// VideoPlaySequelBasicItem 视频播放续集基础属性
	VideoPlaySequelBasicItem struct {
		Id            uint   `json:"id" dc:"播放续集ID"`
		PlayId        uint   `json:"play_id" dc:"播放ID"`
		VideoSequelId uint   `json:"video_sequel_id" dc:"视频续集ID"`
		Resolution    string `json:"resolution" dc:"视频清晰度"`
	}

	// VideoPlaySequelResultItem 视频播放续集结果属性
	VideoPlaySequelResultItem struct {
		VideoSequelId uint   `json:"video_sequel_id" dc:"视频续集ID"`
		CurrPlayRate  uint   `json:"curr_play_rate" dc:"当前播放进度"`
		CurrPlayTime  uint   `json:"curr_play_time" dc:"当前播放时间"`
		PlaySequel    uint   `json:"play_sequel" dc:"视频集数"`
		Resolution    string `json:"resolution" dc:"视频清晰度"`
	}
)
