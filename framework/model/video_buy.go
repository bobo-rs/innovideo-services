package model

import "gitee.com/bobo-rs/innovideo-services/enums"

type (
	// VideoBuyListInput 视频购买列表请求参数
	VideoBuyListInput struct {
		CommonPaginationItem
		CommonFormSideItem
		VideoBuyWhereItem
	}

	// VideoBuyListOutput 视频购买列表结果输出
	VideoBuyListOutput struct {
		CommonResponseItem
		Rows []VideoBuyListItem `json:"rows" dc:"数据列表"`
	}

	// VideoBuyDetailOutput 视频购买详情输出结果
	VideoBuyDetailOutput struct {
		VideoBuyDetailItem
		BillList []VideoBuyBillListItem `json:"bill_list" dc:"账单列表"`
	}
)

type (
	// VideoBuyAuthItem 视频购买授权属性
	VideoBuyAuthItem struct {
		Id                uint          `json:"id" dc:"视频购买ID"`
		CompletePanguCoin uint          `json:"complete_pangu_coin" dc:"全集购买盘古币"`
		AddPanguCoin      uint          `json:"add_pangu_coin" dc:"累计消费盘古币"`
		MorePanguCoin     uint          `json:"more_pangu_coin" dc:"剩余购买盘古币"`
		DiscountRate      float64       `json:"discount_rate" dc:"折扣比例=全集购买盘古币 /（单集盘古币数量 * 全集数）"`
		Status            uint          `json:"status" dc:"购买状态：0购买中，1已购买全集"`
		BuyEpisodesResult map[uint]bool `json:"buy_episodes_result" dc:"购买集数结果"`
	}

	// VideoBuyWhereItem 视频购买查询条件
	VideoBuyWhereItem struct {
		Uid       uint                 `json:"uid" dc:"用户ID"`
		VideoId   uint                 `json:"video_id" dc:"视频ID"`
		BuyNo     string               `json:"buy_no" dc:"购买编号"`
		VideoName string               `json:"video_name" dc:"视频名"`
		Status    enums.VideoBuyStatus `json:"status" dc:"购买状态：0购买中，1已购买全集"`
	}

	// VideoBuyListItem 视频购买列表属性
	VideoBuyListItem struct {
		VideoBuyItem
		VideoBuyCoinItem
		UpdateAt string `json:"update_at" dc:"更新时间"`
	}

	// VideoBuyDetailItem 视频购买详情属性
	VideoBuyDetailItem struct {
		VideoBuyItem
		VideoBuyCoinItem
	}

	// VideoBuyItem 视频购买信息
	VideoBuyItem struct {
		Id        uint   `json:"id" dc:"视频购买ID"`
		Uid       uint   `json:"uid" dc:"用户ID"`
		VideoId   uint   `json:"video_id" dc:"视频ID"`
		BuyNo     string `json:"buy_no" dc:"购买编号"`
		VideoName string `json:"video_name" dc:"视频名"`
		Status    uint   `json:"status" dc:"购买状态：0购买中，1已购买全集"`
	}

	// VideoBuyCoinItem 视频购买盘古币数据
	VideoBuyCoinItem struct {
		CompletePanguCoin uint `json:"complete_pangu_coin" dc:"全集购买盘古币"`
		AddPanguCoin      uint `json:"add_pangu_coin" dc:"累计消费盘古币"`
	}
)
