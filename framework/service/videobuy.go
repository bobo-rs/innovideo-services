// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"

	"gitee.com/bobo-rs/innovideo-services/framework/logic/base"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/framework/model/entity"
)

type (
	IVideoBuy interface {
		// GetBuyList 获取视频购买列表
		GetBuyList(ctx context.Context, in model.VideoBuyListInput) (out *model.VideoBuyListOutput, err error)
		// GetVideoBuyAuthDetail 获取用户购买视频授权信息
		GetVideoBuyAuthDetail(ctx context.Context, videoId uint) (detail *model.VideoBuyAuthItem, err error)
		// GetBuyDetail 获取视频购买详情
		GetBuyDetail(ctx context.Context, buyId uint) (out *model.VideoBuyDetailOutput, err error)
		// GetBillList 获取视频购买账单列表
		GetBillList(ctx context.Context, where model.VideoBuyBillListWhere) (rows []model.VideoBuyBillListItem, err error)
		// GetBillDetail 获取视频购买账单详情
		GetBillDetail(ctx context.Context, billId uint) (out *model.VideoBillDetailOutput, err error)
		// GenBuyBillAndGetId 生成视频购买账单并获取账单ID
		GenBuyBillAndGetId(ctx context.Context, item entity.VideoBuyBill) (uint, error)
		// RefundVideoBill 视频账单退款
		RefundVideoBill(ctx context.Context, in model.RefundVideoBillInput) error
		// BillRefundWithTX 视频账单退款-事物处理
		BillRefundWithTX(ctx context.Context, item model.PrepareRefundBillItem) error
		// CreateVideoPurchase 创建用户购买视频数据
		CreateVideoPurchase(ctx context.Context, in model.CreateVideoPurchaseInput) error
		// GenVideoSaleBillWithTX 生成视频购买销售账单-事务处理
		GenVideoSaleBillWithTX(ctx context.Context, in model.GenVideoSaleBillItem) error
		// GenVideoSaleBill 生成视频购买销售账单
		GenVideoSaleBill(ctx context.Context, in model.GenVideoSaleBillItem) error
		// GetEpisodesBuyList 获取剧集购买列表
		GetEpisodesBuyList(ctx context.Context, uid uint, in model.CreateVideoPurchaseInput) (episodes []model.VideoEpisodesBuyItem, err error)
		// GenAndGetBuyId 生成并获取购买ID
		GenAndGetBuyId(ctx context.Context, data entity.VideoBuy) (uint, error)
		// UpdateVideoBuyById 通过购买ID更新视频购买信息
		UpdateVideoBuyById(ctx context.Context, buyId uint, data interface{}) error
		// PrepareVideoSaleBill 准备销售账单的数据
		PrepareVideoSaleBill(ctx context.Context, in *model.CreateVideoPurchaseInput) (item *model.GenVideoSaleBillItem, err error)
		// FmtBuyEpisodeContent 格式化购买剧集内容
		FmtBuyEpisodeContent(episode []model.VideoBuyEpisodesItem) string
		// BuyEpisodesAuthResult 授权用户购买视频续集结果
		BuyEpisodesAuthResult(ctx context.Context, videoId uint) map[uint]bool
		// GetBuyEpisodesList 获取购买剧集列表
		GetBuyEpisodesList(ctx context.Context, where model.VideoBuyEpisodesWhereItem) (rows []model.VideoBuyEpisodesItem, err error)
		// GetBuyEpisodes 生成视频购买剧集数据
		GenBuyEpisodes(ctx context.Context, in model.GenBuyEpisodesInput) error
		// GetBuyEpisodesIdSet 获取已购买的剧集ID集合
		GetBuyEpisodesIdSet(ctx context.Context, videoId, uid uint) ([]uint, error)
		// IsPurchased 检验用户是否已购买过视频剧集
		IsPurchased(ctx context.Context, uid, videoId uint) bool
		// DeletePurchasedEpisodes 删除购买过的视频剧集（购买视频退款）
		DeletePurchasedEpisodes(ctx context.Context, billId uint, episodesId []uint) error
		// BuyModel 视频购买Model
		BuyModel() *base.TblBaseService
		// BillModel 视频购买账单明细Model
		BillModel() *base.TblBaseService
		// EpisodesModel 视频购买续集Model
		EpisodesModel() *base.TblBaseService
		// VideoBuyWhere 视频购买查询条件
		VideoBuyWhere(ctx context.Context, item model.VideoBuyWhereItem) map[string]interface{}
	}
)

var (
	localVideoBuy IVideoBuy
)

func VideoBuy() IVideoBuy {
	if localVideoBuy == nil {
		panic("implement not found for interface IVideoBuy, forgot register?")
	}
	return localVideoBuy
}

func RegisterVideoBuy(i IVideoBuy) {
	localVideoBuy = i
}
