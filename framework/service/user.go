// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"

	"gitee.com/bobo-rs/innovideo-services/enums"
	"gitee.com/bobo-rs/innovideo-services/framework/logic/base"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/framework/model/entity"
)

type (
	IUser interface {
		// SaveUser 保存用户详情-添加或编辑
		SaveUser(ctx context.Context, item model.UserSaveUserDetailItem) (uint, error)
		// UpdateUserNickname 更新用户昵称
		UpdateUserNickname(ctx context.Context, uid uint, nickname string) error
		// GetUserDetail 获取用户详情
		GetUserDetail(ctx context.Context) *model.UserDetailItem
		// AuthLoginAdminUser 登录用户认证管理员信息
		AuthLoginAdminUser(ctx context.Context) error
		// GetAdminUser 获取管理员登录详情
		GetAdminUser(ctx context.Context) *model.UserAdminDetailItem
		// ProcessAdminDetail 获取管理员详情
		ProcessAdminDetail(ctx context.Context, where interface{}, detail *model.UserAdminDetailItem) error
		// ProcessAdminDetailByUid 通过用户ID获取管理员信息
		ProcessAdminDetailByUid(ctx context.Context, uid uint) (detail *model.UserAdminDetailItem, err error)
		// ProcessAdminDetailById 通过管理员ID获取管理员信息
		ProcessAdminDetailById(ctx context.Context, adminId uint) (detail *model.UserAdminDetailItem, err error)
		// SendUpdatePwdCaptcha 用户修改密码-发送验证码-第一步
		SendUpdatePwdCaptcha(ctx context.Context) error
		// ValidateUpdatePwdCaptcha 修改密码-校验验证码-第二步
		ValidateUpdatePwdCaptcha(ctx context.Context, code string) error
		// UpdatePwd 修改密码-修改密码-第三步
		UpdatePwd(ctx context.Context, in model.UserUpdatePwdInput) error
		// SendForgotPwdCaptcha 找回密码-发送短信验证码-第一步
		SendForgotPwdCaptcha(ctx context.Context, in model.UserForgotPwdSendCaptchaInput) error
		// ValidateForgotPwdCaptcha 找回密码-检验验证码-第二步
		ValidateForgotPwdCaptcha(ctx context.Context, in model.UserForgotPwdValidateCaptchaInput) error
		// UpdateForgotPwd 找回密码-修改密码-第三步
		UpdateForgotPwd(ctx context.Context, in model.UserForgotPwdUpdatePwdInput) error
		// UpdateUserDetail 修改用户详情
		UpdateUserDetail(ctx context.Context, in model.UserUpdateDetailItem) error
		// UpdateUserDetailByUid 更加用户ID修改用户信息
		UpdateUserDetailByUid(ctx context.Context, uid uint, data interface{}) error
		// GetUserAuthDetail 获取授权登录用户详情
		GetUserAuthDetail(ctx context.Context) *model.UserBasicDetailItem
		// ProcessUserDetailByUid 通过用户ID获取用户详情
		ProcessUserDetailByUid(ctx context.Context, uid uint, pointer interface{}) error
		// SaveUserDevice 添加或编辑医生设备信息
		SaveUserDevice(ctx context.Context, device entity.UserDevice) error
		// ProcessLoginUserDevice 通过用户ID并处理用户设备信息
		ProcessLoginUserDevice(ctx context.Context, uid uint) (afterDevice *model.UserDeviceLoginAfterItem, err error)
		// DeviceUserAgent 通过UserAgent获取设备消息
		DeviceUserAgent(ctx context.Context) (item *model.UserDeviceUserAgentItem, err error)
		// DeviceHashId 用户ID生成并获取设备HashID
		DeviceHashId(uid uint, item *model.UserDeviceUserAgentItem) string
		// GenUserDeviceParam 生成用户设备参数
		GenUserDeviceParam(ctx context.Context, uid uint) (*entity.UserDevice, error)
		// ProcessDeviceDetailByHash 通过HashId获取登录设备详情
		ProcessDeviceDetailByHash(ctx context.Context, hashId string, pointer interface{}) error
		// ProcessDeviceIdByHash 通过HashID获取设备ID
		ProcessDeviceIdByHash(ctx context.Context, hashId string) (uint, error)
		// GetFundsDetailByUid 通过用户id获取用户资金信息
		GetFundsDetailByUid(ctx context.Context, uid uint) (detail *entity.UserFunds, err error)
		// UpdateFundsCoinByUid 通过用户ID更新用户盘古币
		UpdateFundsCoinByUid(ctx context.Context, uid, coin uint, up bool) error
		// UpdateFundsByUid 通过用户ID更新用户资金
		UpdateFundsByUid(ctx context.Context, uid uint, data interface{}) error
		// GetAndValidateFundsCoinsByUid 获取并验证用户资金盘古币可用金额
		GetAndValidateFundsCoinsByUid(ctx context.Context, uid, coins uint) (detail *entity.UserFunds, err error)
		// LoginAuthParseToken 通过Token授权认证登录信息是否有效
		LoginAuthParseToken(ctx context.Context, token string) error
		// Logout 退出登录
		Logout(ctx context.Context) error
		// ForceUserDeviceOffline 当前用户或管理员强制下线登录后的设备
		ForceUserDeviceOffline(ctx context.Context, deviceId uint) error
		// CurrUserLoginDeviceList 获取当前用户登陆设备列表
		CurrUserLoginDeviceList(ctx context.Context) (out *model.CurrUserLoginDeviceListOutput, err error)
		// SaveUserLogin 保存用户登录日志
		SaveUserLogin(ctx context.Context, login entity.UserLogin) error
		// ProcessUserLoginAfter 用户登录日志-后置处理
		ProcessUserLoginAfter(ctx context.Context, item model.UserLoginAfterItem) error
		// ParseToken 解析授权登录Token
		ParseToken(ctx context.Context, token string) (*model.UserAccountItem, error)
		// AuthLoginDevice 验证登录信息【后台或用户可强制下线】
		AuthLoginDevice(ctx context.Context, accountToken string) error
		// OfflineLoginDevice 下线用户登录设备
		OfflineLoginDevice(ctx context.Context, accountToken string, status enums.LoginOauthStatus) error
		// AfterLogout 用户退出登录-后置处理
		AfterLogout(ctx context.Context, after *model.LoginAfterLogoutItem) error
		// RemoveLoginCache 移除登录缓存信息（用户详情、管理员详情）-适用于（退出登录、强制退出、修改用户信息等）
		RemoveLoginCache(ctx context.Context, uid uint, accountToken string) error
		// ProcessLoginDetailByToken 通过Token获取当前登录详情
		ProcessLoginDetailByToken(ctx context.Context, token string, detail interface{}) error
		// GetLoginUserTokenByUid 通过用户ID获取用户登陆列表
		GetLoginUserTokenByUid(ctx context.Context, uid ...uint) ([]model.LoginUserIdAndTokenItem, error)
		// UserModel 实例用户基础Model
		UserModel() *base.TblBaseService
		// DetailModel 实例用户详情Model
		DetailModel() *base.TblBaseService
		// CredentialModel 实例用户证件Model
		CredentialModel() *base.TblBaseService
		// DeviceModel 实例用户设备Model
		DeviceModel() *base.TblBaseService
		// FundsModel 实例拥护资金Model
		FundsModel() *base.TblBaseService
		// LoginModel 实例用户登录Model
		LoginModel() *base.TblBaseService
		// AdminModel 实例用户管理员Model
		AdminModel() *base.TblBaseService
		// OauthAccountLogin 用户认证并验证账户密码登录
		OauthAccountLogin(ctx context.Context, in model.UserOauthAccountLoginItem) (*model.UserOauthLoginDetailItem, error)
		// Login 账户密码或手机号快速认证登录
		Login(ctx context.Context, in model.UserOauthLoginInput) (*model.UserOauthLoginOutput, error)
		// AfterUserOauth 后置操作-登录认证处理
		AfterUserOauth(ctx context.Context, afterDetail *model.UserOauthAfterValidateItem) (*model.UserOauthLoginItem, error)
		// UserOauthValidateAfter 用户登录认证后置规则验证
		UserOauthValidateAfter(ctx context.Context, after model.UserOauthAfterItem) (*model.UserOauthAfterValidateItem, error)
		// CreateOauthUser 用户登录创建用户消息
		CreateOauthUser(ctx context.Context, item *model.UserSaveUserDetailItem) error
		// OauthEmailCaptchaLogin 用户认证邮箱验证码登录
		OauthEmailCaptchaLogin(ctx context.Context, in model.UserOauthEmailCaptchaLogin) (detail *model.UserOauthLoginDetailItem, err error)
		// OauthSendEmailCaptcha 用户认证发送邮箱验证码
		OauthSendEmailCaptcha(ctx context.Context, in model.LoginSendEmailCaptchaInput) error
		// OauthSmsCaptchaLogin 用户授权短信验证码登录并校验验证码
		OauthSmsCaptchaLogin(ctx context.Context, in model.UserOauthSmsLoginItem) (*model.UserOauthLoginDetailItem, error)
		// UserLoginSendSmsCaptcha 用户认证登录发送短信验证码
		UserLoginSendSmsCaptcha(ctx context.Context, in model.UserLoginSendSmsCaptchaInput) error
		// DecryptMobile 解密手机号
		DecryptMobile(mobileCipher, secret string) (string, error)
		// QueryDetailByMobile 通过手机号获取用户详情
		QueryDetailByMobile(ctx context.Context, mobile string, pointer interface{}) error
		// QueryDetailByUid 通过用户ID获取用户详情
		QueryDetailByUid(ctx context.Context, uid uint, pointer interface{}) error
		// QueryDetailByAccount 通过用户账户获取用户详情
		QueryDetailByAccount(ctx context.Context, account string, pointer interface{}) error
		// QueryDetailByEmail 通过邮箱获取用户详情
		QueryDetailByEmail(ctx context.Context, email string, pointer interface{}) error
		// CheckUserExistsByMobile 通过手机号-检测用户是否存在
		CheckUserExistsByMobile(ctx context.Context, mobile string) bool
		// CheckUserExistsByAccount 通过账号-检查用户是否存在
		CheckUserExistsByAccount(ctx context.Context, account string) bool
		// CheckUserExistsByUid 通过用户ID检测用户是否存在
		CheckUserExistsByUid(ctx context.Context, uid uint) bool
		// ProcessAccountNameByUid 通过用户ID获取账户名
		ProcessAccountNameByUid(ctx context.Context, uid uint) (*model.UserAccountNameItem, error)
		// ProcessUserByUid 通过用户ID获取用户信息
		ProcessUserByUid(ctx context.Context, uid uint) (*entity.User, error)
	}
)

var (
	localUser IUser
)

func User() IUser {
	if localUser == nil {
		panic("implement not found for interface IUser, forgot register?")
	}
	return localUser
}

func RegisterUser(i IUser) {
	localUser = i
}
