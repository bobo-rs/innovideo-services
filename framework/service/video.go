// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"

	"gitee.com/bobo-rs/innovideo-services/enums"
	"gitee.com/bobo-rs/innovideo-services/framework/logic/base"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/framework/model/entity"
)

type (
	IVideo interface {
		// SaveVideo 保存视频信息
		SaveVideo(ctx context.Context, item model.VideoSaveItem) error
		// GetVideoList 获取视频列表
		GetVideoList(ctx context.Context, in model.VideoListInput) (out *model.VideoListOutput, err error)
		// GetVideoDetail 获取视频基础详情
		GetVideoDetail(ctx context.Context, in model.VideoDetailInput) (detail *model.VideoDetailItem, err error)
		// GetVideoPlayEpisodes 获取视频播放续集列表-并验证用户授权
		GetVideoPlayEpisodes(ctx context.Context, videoId uint) (out *model.VideoPlayEpisodesOutput, err error)
		// GetVideoPlayUrl 获取视频播放地址
		GetVideoPlayUrl(ctx context.Context, in model.VideoPlayUrlInput) (out *model.VideoPlayUrlOutput, err error)
		// GetVideoAuthDetail 获取用户前台授权视频详情
		GetVideoAuthDetail(ctx context.Context, videoId uint) (detail *model.VideoDetailAuthItem, err error)
		// GetVideoAuthDetailByNo 获取用户前台授权视频详情-视频编号
		GetVideoAuthDetailByNo(ctx context.Context, videoNo string) (detail *model.VideoDetailAuthItem, err error)
		// DeleteVideo 删除视频数据
		DeleteVideo(ctx context.Context, videoId uint) error
		// CheckVideoOperatorValid 检测视频操作业务是否有效，例如（是否发布）：当前已发布，需要更改为未发布
		CheckVideoOperatorValid(ctx context.Context, videoId uint, operator enums.VideoOperator) bool
		// PrepareUserVideoPlayUrl 整理用户视频播放地址
		PrepareUserVideoPlayUrl(ctx context.Context, in model.VideoPlayUrlInput, uid uint) (detail *model.VideoDetailAuthItem, err error)
		// SaveAttribute 添加|编辑视频属性
		SaveAttribute(ctx context.Context, item entity.VideoAttribute) error
		// SaveVideoEpisodes 保存视频续集数据
		SaveVideoEpisodes(ctx context.Context, item model.VideoEpisodesSaveItem) error
		// GetEpisodesList 获取视频续集列表数据
		GetEpisodesList(ctx context.Context, in model.VideoEpisodesListInput) (out *model.VideoEpisodesListOutput, err error)
		// GetEpisodesDetail 获取视频续集详情
		GetEpisodesDetail(ctx context.Context, episodesId uint) (detail *model.VideoEpisodesDetailItem, err error)
		// GetEpisodesIdSetByVideoId 通过视频ID获取视频续集ID集合
		GetEpisodesIdSetByVideoId(ctx context.Context, videoId uint) ([]uint, error)
		// MustGetEpisodesIdSetByVideoId 通过视频ID获取视频续集ID集合
		MustGetEpisodesIdSetByVideoId(ctx context.Context, videoId uint) []uint
		// GetEpisodesBuyList 获取待购买的剧集列表
		GetEpisodesBuyList(ctx context.Context, videoId uint, idSet []uint) (list []model.VideoEpisodesBuyItem, err error)
		// GetRemainderNotBuyList 获取剩余未购买的剧集列表
		GetRemainderNotBuyList(ctx context.Context, episodesIdSet []uint) (list []model.VideoEpisodesBuyItem, err error)
		// GetVideoEpisodesIds 获取视频剧集ID，适用于（获取免费剧集ID）
		GetVideoEpisodesIds(ctx context.Context, videoId uint, size int) (map[uint]bool, error)
		// SaveLabels 保存视频标签值
		SaveLabels(ctx context.Context, videoId uint, values ...string) error
		// GetVideoIdByValue 通过标签值获取视频ID
		GetVideoIdByValue(ctx context.Context, values ...string) ([]uint, error)
		// GetLabelValueList 通过视频ID获取视频标签值列表
		GetLabelValueList(ctx context.Context, videoId uint) ([]model.VideoLabelItem, error)
		// VideoModel 视频Model
		VideoModel() *base.TblBaseService
		// AttributeModel 视频属性Model
		AttributeModel() *base.TblBaseService
		// EpisodesModel 视频续集Model
		EpisodesModel() *base.TblBaseService
		// LabelModel 视频标签Model
		LabelModel() *base.TblBaseService
		// LogModel 视频操作日志Model
		LogModel() *base.TblBaseService
		// PersonalModel 视频相关人员Model
		PersonalModel() *base.TblBaseService
		// ResolutionModel 视频续集清晰度Model
		ResolutionModel() *base.TblBaseService
		// UserActionsModel 视频用户操作Model
		UserActionsModel() *base.TblBaseService
		// SaveVideoPersonal 保存视频相关人员资料
		SaveVideoPersonal(ctx context.Context, en entity.VideoPersonal) error
		// SaveVideoResolution 保存视频清晰度数据
		SaveVideoResolution(ctx context.Context, episodesId uint, params ...*model.VideoResolutionSaveItem) error
		// GetResolutionMapByEpisodesId 通过视频续集ID获取续集清晰度列表
		GetResolutionMapByEpisodesId(ctx context.Context, episodesId ...uint) (map[uint][]model.VideoResolutionListItem, error)
		// GetEpisodesResolutionDetail 获取视频续集详情及视频清晰度
		GetEpisodesResolutionDetail(ctx context.Context, episodesId uint, resolution string) (detail *model.EpisodesResolutionDetailItem, err error)
		// 用户视频关联操作（收藏、点赞、踩一下）
		VideoAssocAction(ctx context.Context, videoId uint, t enums.VideoUserActionType) error
		// GetUserVideoActions 获取用户视频操作属性（收藏、点赞、踩）
		GetUserVideoActions(ctx context.Context, videoId uint) (attr model.VideoUserActionsAttrItem)
		// GetUserActionVideoList 获取用户视频操作列表数据
		GetUserActionVideoList(ctx context.Context, t enums.VideoUserActionType, page, size int) (out *model.VideoUserListOutput, err error)
		// GetUserVideoIdByType 通过收点踩类型获取视频ID
		GetUserVideoIdByType(ctx context.Context, t enums.VideoUserActionType) []uint
		// VideoWhere 视频搜索条件
		VideoWhere(ctx context.Context, item model.VideoWhereItem) map[string]interface{}
		// VideoSort 视频列表排序
		VideoSort(by uint16) string
	}
)

var (
	localVideo IVideo
)

func Video() IVideo {
	if localVideo == nil {
		panic("implement not found for interface IVideo, forgot register?")
	}
	return localVideo
}

func RegisterVideo(i IVideo) {
	localVideo = i
}
