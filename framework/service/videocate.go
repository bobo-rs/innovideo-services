// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"

	"gitee.com/bobo-rs/innovideo-services/framework/logic/base"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/framework/model/entity"
)

type (
	IVideoCate interface {
		// SaveCategory 保存视频分类
		SaveCategory(ctx context.Context, data entity.VideoCategory) error
		// GetCategoryLabelList 获取视频分类标签列表（大类、子类）
		GetCategoryLabelList(ctx context.Context, pid uint) (list model.VideoCategoryLabelList, err error)
		// GetCategoryRecommendList 获取推荐栏目列表
		GetCategoryRecommendList(ctx context.Context) (list model.VideoCategoryLabelList, err error)
		// GetCategoryTreeList 获取视频分类树形结构
		GetCategoryTreeList(ctx context.Context) (treeList []map[string]interface{}, err error)
		// GetCategoryDetail 获取视频分类详情
		GetCategoryDetail(ctx context.Context, id uint) (detail *model.VideoCategoryDetail, err error)
		// QueryAndCacheCategoryList 查询并获取视频类目列表
		QueryAndCacheCategoryList(ctx context.Context, where interface{}, cacheKey string) (list model.VideoCategoryLabelList, err error)
		// CheckCategoryChildExistsById 检测子级类目是否存在
		CheckCategoryChildExistsById(ctx context.Context, pid uint) bool
		// CategoryModel 视频分类Model
		CategoryModel() *base.TblBaseService
	}
)

var (
	localVideoCate IVideoCate
)

func VideoCate() IVideoCate {
	if localVideoCate == nil {
		panic("implement not found for interface IVideoCate, forgot register?")
	}
	return localVideoCate
}

func RegisterVideoCate(i IVideoCate) {
	localVideoCate = i
}
