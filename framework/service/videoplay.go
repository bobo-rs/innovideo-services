// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"

	"gitee.com/bobo-rs/innovideo-services/framework/logic/base"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/framework/model/entity"
)

type (
	IVideoPlay interface {
		SavePlayHistory(ctx context.Context, in model.VideoPlayHistorySaveInput) error
		// GetPlayAuthDetail 获取用户播放视频信息
		GetPlayAuthDetail(ctx context.Context, videoId uint) (detail *model.VideoPlayAuthItem, err error)
		// GetPlayHistoryList 获取用户播放历史列表
		GetPlayHistoryList(ctx context.Context, in model.CommonPaginationItem) (out *model.VideoPlayHistoryListOutput, err error)
		// DeleteVideoPlay 用户删除播放记录
		DeleteVideoPlay(ctx context.Context, playId ...uint) error
		// PlayModel 视频播放Model
		PlayModel() *base.TblBaseService
		// SequelModel 视频播放续集Model
		SequelModel() *base.TblBaseService
		// SavePlaySequel 保存播放续集数据
		SavePlaySequel(ctx context.Context, en entity.VideoPlaySequel) error
		// GetPlayResultsMapByPlayId 通过播放ID获取用户播放视频结果集
		GetPlayResultsMapByPlayId(ctx context.Context, playId uint) (res map[uint]*model.VideoPlaySequelResultItem, err error)
		// MustPlayResultsMap 通过播放ID获取用户播放结果集
		MustPlayResultsMap(ctx context.Context, playId uint) map[uint]bool
	}
)

var (
	localVideoPlay IVideoPlay
)

func VideoPlay() IVideoPlay {
	if localVideoPlay == nil {
		panic("implement not found for interface IVideoPlay, forgot register?")
	}
	return localVideoPlay
}

func RegisterVideoPlay(i IVideoPlay) {
	localVideoPlay = i
}
