// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"

	"gitee.com/bobo-rs/innovideo-services/enums"
	"gitee.com/bobo-rs/innovideo-services/framework/logic/base"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/framework/model/entity"
)

type (
	IRbac interface {
		// AuthAdminMenu 获取管理员授权权限菜单列表
		AuthAdminMenu(ctx context.Context) (treeList []map[string]interface{}, err error)
		// AuthAdminMenuActionCode 获取管理员授权菜单权限码
		AuthAdminMenuActionCode(ctx context.Context) (out *model.PermCodeActionItem, err error)
		// AuthAdminGetDetail 获取鉴权管理员详情
		AuthAdminGetDetail(ctx context.Context) (out *model.UserAdminAuthDetailOutput)
		// AuthAdminPermId 获取管理员授权权限ID集合
		AuthAdminPermId(ctx context.Context) ([]uint, error)
		// SaveAdminDepartment 保存管理员关联部门数据
		SaveAdminDepartment(ctx context.Context, adminId uint, departId ...uint) error
		// GetDepartPeopleNumByDepartId 通过部门ID-获取部门人数MAP
		GetDepartPeopleNumByDepartId(ctx context.Context, departId ...uint) (map[uint]uint, error)
		// RemoveAdminDepartByAid 通过管理员ID解除关联部门-适用于，后台编辑管理员，移除部门之后，未添加新部门，后置清理掉部门
		RemoveAdminDepartByAid(ctx context.Context, adminId uint) error
		// GetDepartmentIdByAid 通过管理员ID获取部门ID
		GetDepartmentIdByAid(ctx context.Context, aid ...uint) ([]uint, error)
		// CheckAdminDepartByAid 通过管理员ID检测管理员是否关联部门
		CheckAdminDepartByAid(ctx context.Context, adminId uint) bool
		// CheckAdminDepartByDepartId 通过部门ID检测部门是否关联管理员
		CheckAdminDepartByDepartId(ctx context.Context, departId uint) bool
		// ProcessAdminIdAndDepartIdList 获取管理员ID关联部门ID列表
		ProcessAdminIdAndDepartIdList(ctx context.Context, where interface{}) (rows []model.AdminIdAndDepartIdItem, err error)
		// AddAdminInvite 添加管理员邀请记录
		AddAdminInvite(ctx context.Context, in model.AdminInviteAddInput) (out *model.AdminInviteAddOutput, err error)
		// GetInviteList 获取管理员邀请列表
		GetInviteList(ctx context.Context, in model.AdminInviteGetListInput) (out *model.AdminInviteGetListOutput, err error)
		// GetInviteDetail 获取管理员邀请记录详情
		GetInviteDetail(ctx context.Context, id uint) (detail *model.AdminInviteDetailItem, err error)
		// RefreshInviteExpired 刷新邀请效期时间
		RefreshInviteExpired(ctx context.Context, id uint) error
		// GetInviteExpired 获取邀请过期时间
		GetInviteExpired(ctx context.Context) string
		// CheckInviteExistsByToken 通过Token检测邀请信息是否存在
		CheckInviteExistsByToken(ctx context.Context, token string) bool
		// CheckInviteValidByToken 通过邀请Token检测邀请信息是否有效
		CheckInviteValidByToken(ctx context.Context, token string) error
		// GetInviteValidDetailById 通过邀请ID获取邀请有效详情数据
		GetInviteValidDetailById(ctx context.Context, id uint) (detail *model.AdminInviteValidItem, err error)
		// GetInviteUserByToken 通过邀请ID获取用户权限信息
		GetInviteUserByToken(ctx context.Context, token string) (detail *model.AdminInviteUserValidItem, err error)
		// SaveAdminUser 保存管理员信息
		SaveAdminUser(ctx context.Context, user model.UserAdminSaveItem) error
		// GetAdminUserList 获取管理员列表
		GetAdminUserList(ctx context.Context, in model.UserAdminGetListInput) (out *model.UserAdminGetListOutput, err error)
		// GetUserAdminDetail 获取管理员详情
		GetUserAdminDetail(ctx context.Context, id uint) (detail *model.UserAdminGetDetailOutput, err error)
		// RemoveUserAdmin 移除管理员信息
		RemoveUserAdmin(ctx context.Context, id uint) error
		// AuthInviteAdminUser 登录用户直接授权绑定用户
		AuthInviteAdminUser(ctx context.Context, token string) error
		// AddInviteAdminUser 添加管理员邀请注册用户
		AddInviteAdminUser(ctx context.Context, invite model.UserAdminInviteAddItem) (err error)
		// ProcessAdminDetail 获取管理员详情
		ProcessAdminDetail(ctx context.Context, where, pointer interface{}) error
		// ProcessAdminDetailByAid 通过管理员ID获取管理员信息
		ProcessAdminDetailByAid(ctx context.Context, aid uint, pointer interface{}) error
		// CheckAdminExistsId 检测管理员是否存在
		CheckAdminExistsId(ctx context.Context, aid uint) bool
		// GetUidByAid 通过管理员ID获取用户ID
		GetUidByAid(ctx context.Context, id ...uint) ([]uint, error)
		// CheckAdminExistsByUid 通过用户ID检测是否注册管理员
		CheckAdminExistsByUid(ctx context.Context, uid uint) bool
		// SaveDepartment 保存部门数据
		SaveDepartment(ctx context.Context, depart model.DepartmentSaveInput) error
		// GetDepartmentTreeList 获取部门列表-并构建成树形结构
		GetDepartmentTreeList(ctx context.Context) ([]map[string]interface{}, error)
		// GetDepartmentFindTreeList 获取部门树形列表并查找部门ID
		GetDepartmentFindTreeList(ctx context.Context, status enums.DepartStatus, departId ...uint) ([]map[string]interface{}, error)
		// GetDepartmentDetail 获取部门详情
		GetDepartmentDetail(ctx context.Context, departId uint) (detail *model.DepartmentDetailItem, err error)
		// RemoveDepartment 删除部门
		RemoveDepartment(ctx context.Context, departId uint) error
		// GetDepartNameByDepartId 通过部门ID获取部门名称
		GetDepartNameByDepartId(ctx context.Context, departId ...uint) (rows []model.DepartmentNameItem, err error)
		// ProcessDepartmentList 处理并获取部门列表
		ProcessDepartmentList(ctx context.Context, where interface{}) ([]model.DepartmentTreeItem, error)
		// CheckChildDepartByDepartId 通过部门ID检查子级部门是否存在
		CheckChildDepartByDepartId(ctx context.Context, departId uint) bool
		// CheckDepartmentById 通过部门ID检测部门是否存在
		CheckDepartmentById(ctx context.Context, id uint) bool
		// DepartmentTotalById 通过部门ID获取部门数量
		DepartmentTotalById(ctx context.Context, id ...uint) int
		// CheckDepartmentIsDisableById 通过部门ID检测部门是否被禁用：true正常，false禁用
		CheckDepartmentIsDisableById(ctx context.Context, id uint) bool
		// SaveDepartmentRoles 保存部门绑定角色数据
		SaveDepartmentRoles(ctx context.Context, departId uint, roleId ...uint) error
		// GetDepartRoleIdByDepartId 通过部门ID获取关联的角色ID
		GetDepartRoleIdByDepartId(ctx context.Context, departId ...uint) ([]uint, error)
		// GetPermissionsIdByDepartId 通过部门ID获取权限ID集合
		GetPermissionsIdByDepartId(ctx context.Context, departId ...uint) ([]uint, error)
		// RemoveDepartmentRolesByRoleId 通过角色ID删除部门绑定
		RemoveDepartmentRolesByRoleId(ctx context.Context, roleId uint) error
		// RemoveDepartmentRoleByDepartId 通过部门ID解除角色关联
		RemoveDepartmentRoleByDepartId(ctx context.Context, departId uint) error
		// PermissionsModel 权限模型
		PermissionsModel() *base.TblBaseService
		// AssocModel 权限关联角色模型
		AssocModel() *base.TblBaseService
		// RolesModel 角色模型
		RolesModel() *base.TblBaseService
		// DepartmentRoleModel 部门关联角色Model
		DepartmentRoleModel() *base.TblBaseService
		// DepartmentModel 部门Model
		DepartmentModel() *base.TblBaseService
		// AdminDepartmentModel 管理员部门关联Model
		AdminDepartmentModel() *base.TblBaseService
		// UserAdminModel 管理员用户Model
		UserAdminModel() *base.TblBaseService
		// InviteModel 管理员邀请Model
		InviteModel() *base.TblBaseService
		// SavePermissions 保存权限
		SavePermissions(ctx context.Context, pm entity.Permissions) error
		// GetPermTreeList 获取权限树形列表
		GetPermTreeList(ctx context.Context) ([]map[string]interface{}, error)
		// GetPermDetail 获取权限详情
		GetPermDetail(ctx context.Context, permId uint) (*entity.Permissions, error)
		// GetPermMenuTreeList 菜单列表-获取权限菜单树形列表
		GetPermMenuTreeList(ctx context.Context, id ...uint) ([]map[string]interface{}, error)
		// GetPermAssocTreeListByAssocId 通过关联ID-获取关联权限树形列表（角色关联、管理员关联）
		GetPermAssocTreeListByAssocId(ctx context.Context, assocType enums.PermAssocType, assocId uint) ([]map[string]interface{}, error)
		// GetPermAssocTreeList 获取权限关联树形列表-true关联子级菜单类型，false权限关联角色
		GetPermAssocTreeList(ctx context.Context, assocTreeType bool, findId ...uint) ([]map[string]interface{}, error)
		// GetMenuActionCode 通过权限ID获取权限码
		GetMenuActionCode(ctx context.Context, id ...uint) (out *model.PermCodeActionItem, err error)
		// PermCustomUpdateColumns 自定义字段更新权限字段
		PermCustomUpdateColumns(ctx context.Context, in model.PermCustomUpdateColumnInput) error
		// RemovePermissions 删除权限数据
		RemovePermissions(ctx context.Context, id uint) error
		// SavePermAssoc 保存关联权限数据
		SavePermAssoc(ctx context.Context, assocId uint, assocType enums.PermAssocType, permId ...uint) error
		// RemovePermAssocByPmId 权限删除或禁用移除关联
		RemovePermAssocByPmId(ctx context.Context, pmId uint) error
		// RemovePermAssocByAssocId 通过关联ID解除权限关联
		RemovePermAssocByAssocId(ctx context.Context, assocType enums.PermAssocType, assocId ...uint) error
		// GetPermissionsIdByRoleId 通过角色ID-获取权限ID集合
		GetPermissionsIdByRoleId(ctx context.Context, roleId ...uint) ([]uint, error)
		// GetPermissionsIdByAdminId 通过管理员ID-获取权限ID集合
		GetPermissionsIdByAdminId(ctx context.Context, adminId uint) ([]uint, error)
		// GetPermissionsIdByAssocId 通过关联ID和类型获取权限ID集合
		GetPermissionsIdByAssocId(ctx context.Context, assocType enums.PermAssocType, assocId ...uint) ([]uint, error)
		// CheckPermissionsExistsById 通过权限ID检测权限是否存在
		CheckPermissionsExistsById(ctx context.Context, id uint) bool
		// CheckPermissionsExistsByPid 通过父级ID检测子级权限是否存在
		CheckPermissionsExistsByPid(ctx context.Context, pid uint) bool
		// GetPermissionsTotalByPid 通过父级ID获取子权限数量
		GetPermissionsTotalByPid(ctx context.Context, pid uint) int
		// SaveRoles 保存角色信息
		SaveRoles(ctx context.Context, role model.RolesDetailItem) error
		// GetRolesList 获取角色列表
		GetRolesList(ctx context.Context, in model.RolesListInput) (out *model.RolesListOutput, err error)
		// GetAssocRolesList 获取关联角色列表
		GetAssocRolesList(ctx context.Context) (rows []model.RoleIdAndNameItem, err error)
		// GetRolesDetail 获取角色详情
		GetRolesDetail(ctx context.Context, roleId uint) (detail *model.RolesDetailItem, err error)
		// RemoveRoles 删除角色信息
		RemoveRoles(ctx context.Context, roleId uint) error
		// GetRolesNameListByRoleId 通过角色ID获取角色名列表
		GetRolesNameListByRoleId(ctx context.Context, roleId ...uint) (rows []model.RoleIdAndNameItem, err error)
		// CheckRolesExistsById 通过角色ID检测角色是否存在
		CheckRolesExistsById(ctx context.Context, id uint) bool
		// RolesTotalById 通过角色ID获取角色总数
		RolesTotalById(ctx context.Context, id ...uint) int
		// RolesWhere 角色条件
		RolesWhere(in model.RolesListInput) map[string]interface{}
		// UserAdminWhere 用户管理员搜索条件
		UserAdminWhere(in model.UserAdminWhereItem) map[string]interface{}
		// FormatUserAdmin 格式化用户管理员
		FormatUserAdmin(detail *model.UserAdminGetDetailItem)
	}
)

var (
	localRbac IRbac
)

func Rbac() IRbac {
	if localRbac == nil {
		panic("implement not found for interface IRbac, forgot register?")
	}
	return localRbac
}

func RegisterRbac(i IRbac) {
	localRbac = i
}
