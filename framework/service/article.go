// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"

	"gitee.com/bobo-rs/innovideo-services/framework/logic/base"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/framework/model/entity"
)

type (
	IArticle interface {
		// SaveArticle 保存文章数据
		SaveArticle(ctx context.Context, item entity.Article) error
		// GetArticleList 获取文章列表
		GetArticleList(ctx context.Context, in model.ArticleGetListInput) (out *model.ArticleGetListOutput, err error)
		// GetArticleDetail 获取文章详情
		GetArticleDetail(ctx context.Context, id uint) (detail *model.ArticleDetailItem, err error)
		// SaveCategory 保存文章分类数据
		SaveCategory(ctx context.Context, item entity.ArticleCategory) error
		// GetCategoryTreeList 获取文章分类树形列表
		GetCategoryTreeList(ctx context.Context, where interface{}) ([]map[string]interface{}, error)
		// GetCategoryAssocTreeList 获取文章分类关联列表
		GetCategoryAssocTreeList(ctx context.Context) ([]map[string]interface{}, error)
		// ArticleModel 文章Model
		ArticleModel() *base.TblBaseService
		// CategoryModel 文章分类Model
		CategoryModel() *base.TblBaseService
		// ProcessDetailById 通过文章ID获取文章详情
		ProcessDetailById(ctx context.Context, id uint, pointer interface{}) error
		// CheckExistsById 通过文章ID检测文章是否存在
		CheckExistsById(ctx context.Context, id uint) bool
		// ArticleWhere 处理并获取文章条件
		ArticleWhere(in model.ArticleWhereItem) map[string]interface{}
		// FormatArticle 格式化文章详情状态
		FormatArticle(detail *model.ArticleDetailFmtItem)
	}
)

var (
	localArticle IArticle
)

func Article() IArticle {
	if localArticle == nil {
		panic("implement not found for interface IArticle, forgot register?")
	}
	return localArticle
}

func RegisterArticle(i IArticle) {
	localArticle = i
}
