// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"

	"gitee.com/bobo-rs/innovideo-services/framework/logic/base"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"github.com/gogf/gf/v2/frame/g"
)

type (
	ISysMap interface {
		// GetList 获取字典列表数据
		GetList(ctx context.Context, in model.SysMapGetListInput) (out *model.SysMapGetListOutput, err error)
		// GetDetail 获取字典详情
		GetDetail(ctx context.Context, id uint) (detail *model.SysMapDetailItem, err error)
		// DeleteSysMap 删除系统字典
		DeleteSysMap(ctx context.Context, id uint) error
		// Map 通过字典名获取字典值
		Map(ctx context.Context, name string) (map[string]string, error)
		// MustMap 通过字典名获取字典
		MustMap(ctx context.Context, name string) map[string]string
		// Exists 检测字典是否存在
		Exists(ctx context.Context, name string) bool
		// ValExists 字典值检测是否存在
		ValExists(ctx context.Context, name string, value g.Var) (bool, error)
		// GetDetailById 通过字典ID获取字典详情
		GetDetailById(ctx context.Context, id uint, pointer interface{}) error
		// GetValueList 获取字典值列表
		GetValueList(ctx context.Context, name string) (rows []model.SysMapValueListItem, err error)
		// GetValueDetail 获取字典值详情
		GetValueDetail(ctx context.Context, id uint) (detail *model.SysMapValueDetailItem, err error)
		// GetMapAll 获取所有字典组值
		GetMapAll(ctx context.Context, isRefresh ...bool) (map[string]map[string]string, error)
		// GetValueDetailById 通过字典值ID获取字典值详情
		GetValueDetailById(ctx context.Context, id uint, pointer interface{}) error
		// MapModel 字典Model
		MapModel() *base.TblBaseService
		// ValueModel 字典值Model
		ValueModel() *base.TblBaseService
	}
)

var (
	localSysMap ISysMap
)

func SysMap() ISysMap {
	if localSysMap == nil {
		panic("implement not found for interface ISysMap, forgot register?")
	}
	return localSysMap
}

func RegisterSysMap(i ISysMap) {
	localSysMap = i
}
