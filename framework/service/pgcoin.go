// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"

	"gitee.com/bobo-rs/innovideo-services/enums"
	"gitee.com/bobo-rs/innovideo-services/framework/logic/base"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/framework/model/entity"
	"github.com/gogf/gf/v2/database/gdb"
)

type (
	IPgCoin interface {
		// GetBillList 获取账单列表
		GetBillList(ctx context.Context, in model.PGCoinBillListInput) (out *model.PGCoinBillListOutput, err error)
		// GetBillDetail 获取账单详情
		GetBillDetail(ctx context.Context, billId uint) (out *model.PGCoinBillDetailOutput, err error)
		// CorrectionUserAvailablePGCoin 根据盘古币账单为基础校正用户可用盘古币
		CorrectionUserAvailablePGCoin(ctx context.Context, uid uint) error
		// GetUserAvailablePGCoinBillList 获取用户可用盘古币账单列表
		GetUserAvailablePGCoinBillList(ctx context.Context, uid uint) (out *model.UserAvailablePGCoinListOutput, err error)
		// UpdatePGCoinBillComplete 盘古币消费完成-更新状态
		UpdatePGCoinBillComplete(ctx context.Context, idSet []uint) error
		// UpdateDeducePGCoinBill 更新减扣盘古币账单数量
		UpdateDeducePGCoinBill(ctx context.Context, id, moreCoin uint) error
		// UpdateBillExpiredAt 更新盘古币账单效期
		UpdateBillExpiredAt(ctx context.Context, in model.UpdateBillExpiredInput) error
		// QueryBillDetail 查询账单详情
		QueryBillDetail(ctx context.Context, id uint) (detail *model.PGCoinBillDetailItem, err error)
		// GenPGCoinBillItem 生成盘古币账单流水明细
		GenPGCoinBillItem(ctx context.Context, en ...entity.PanguCoinBillItem) error
		// GetBillItemList 获取账单明细列表
		GetBillItemList(ctx context.Context, where interface{}) (list []entity.PanguCoinBillItem, err error)
		// OrderModel 盘古币充值订单Model
		OrderModel() *base.TblBaseService
		// BillModel 盘古币账单Model
		BillModel() *base.TblBaseService
		// TransactionModel 盘古币流水Model
		TransactionModel() *base.TblBaseService
		// BillItemModel 盘古币账单明细Model
		BillItemModel() *base.TblBaseService
		// Config 读取盘古币配置
		Config(ctx context.Context) (config *model.PGCConfigItem, err error)
		// GetOrderList 获取盘古币充值订单列表
		GetOrderList(ctx context.Context, in model.PGCoinOrderListInput) (out *model.PGCoinOrderListOutput, err error)
		// GetUserOrderList 获取用户充值订单列表
		GetUserOrderList(ctx context.Context, in model.PGCoinUserOrderListInput) (out *model.PGCoinOrderListOutput, err error)
		// GetOrderDetail 获取盘古币充值订单详情
		GetOrderDetail(ctx context.Context, id uint, formSide enums.FormSide) (detail *model.PGCoinOrderDetailItem, err error)
		// CancelOrder 取消充值订单
		CancelOrder(ctx context.Context, id uint, formSide enums.FormSide) error
		// TodayUserRechargeCount  统计用户今日充值次数
		TodayUserRechargeCount(ctx context.Context, countType bool) int
		// CreateRechargePGCoinTryLock 创建充值盘古币订单数据-非阻塞锁
		CreateRechargePGCoinTryLock(ctx context.Context, in model.PGCoinRechargeInput) (out *model.PGCoinRechargeOutput, err error)
		// CreateRechargePGCoin 创建充值盘古币订单数据
		CreateRechargePGCoin(ctx context.Context, in model.PGCoinRechargeInput) (out *model.PGCoinRechargeOutput, err error)
		// GetPayPGCoinDetail 获取盘古币支付信息
		GetPayPGCoinDetail(ctx context.Context, in model.PGCoinPayDetailInput) (out *model.PaymentResultData, err error)
		// CallbackUpdateCoinsPayMethod 回调更新盘古币支付方式（PayPal支付必须）
		CallbackUpdateCoinsPayMethod(ctx context.Context, in model.CallbackUpdateCoinsPayMethodInput) error
		// ConfirmCoinsPayment 确认盘古币支付完成（场景：第三方支付完成-跳转到支付完成页）
		ConfirmCoinsPayment(ctx context.Context, in model.ConfirmCoinsPaymentInput) (detail *model.PGCoinsConfirmPayDetailItem, err error)
		// NotifyPGCoin 回调盘古币支付状态
		NotifyPGCoin(ctx context.Context, in model.PGCoinPayNotifyInput) error
		// GiveUserPGCoin 给用户账户赠送盘古币
		GiveUserPGCoin(ctx context.Context, uid, coins uint, t enums.PGCoinBillType) error
		// GenPGCoinBillTX 生成盘古币账单-事务处理
		GenPGCoinBillTX(ctx context.Context, in model.PGCoinRechargeBillInput) error
		// GenPGCoinConsumeBillTX 生成消费盘古币账单-事务处理
		GenPGCoinConsumeBillTX(ctx context.Context, in model.PGCoinConsumeInput) error
		// GenPGCoinBill 生成盘古币账单数据
		GenPGCoinBill(ctx context.Context, in model.PGCoinRechargeBillInput) error
		// GenPGCoinConsumeBill 生成盘古币消费账单
		GenPGCoinConsumeBill(ctx context.Context, in model.PGCoinConsumeInput) error
		// GetUserAvailableFundsAndBill 获取用户可用资金和账单
		GetUserAvailableFundsAndBill(ctx context.Context, uid uint) (funds *entity.UserFunds, bill *model.UserAvailablePGCoinListOutput, err error)
		// GetTransactionList 获取盘古币流水列表
		GetTransactionList(ctx context.Context, in model.PGCoinTransactionListInput) (out *model.PGCoinTransactionListOutput, err error)
		// GenPGCoinTransaction 生成盘古币流水记录
		GenPGCoinTransaction(ctx context.Context, uid, coin uint, tranType enums.PGCoinTransactionType) (tno string, err error)
		// PGCoinBillWhere 盘古币账单条件
		PGCoinBillWhere(in model.PGCoinBillWhere) map[string]interface{}
		// TransactionWhereModel 实例盘古币流水查询条件Model
		TransactionWhereModel(ctx context.Context, in model.PGCoinTransactionWhere) *gdb.Model
		// OrderWhereModel 盘古币订单查询Model
		OrderWhereModel(ctx context.Context, in model.PGCoinOrderWhere) *gdb.Model
	}
)

var (
	localPgCoin IPgCoin
)

func PgCoin() IPgCoin {
	if localPgCoin == nil {
		panic("implement not found for interface IPgCoin, forgot register?")
	}
	return localPgCoin
}

func RegisterPgCoin(i IPgCoin) {
	localPgCoin = i
}
