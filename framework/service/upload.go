// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"
	"os"

	"gitee.com/bobo-rs/creative-framework/pkg/config"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/framework/model/entity"
)

type (
	IUpload interface {
		// SaveMergeVideoFile 保存合并后的视频文件
		SaveMergeVideoFile(ctx context.Context, merge model.UploadBlockGenMerge, item model.UploadBlockFile) error
		// SaveAttachment 保存附件信息
		SaveAttachment(ctx context.Context, hashed string, f func(ctx context.Context) (*entity.Attachment, error)) error
		// GetUploadBlockSpace 获取上传空间文件块
		GetUploadBlockSpace(ctx context.Context) (out *model.UploadBlockSpaceOutput, err error)
		// UploadBlockToFileSystem 文件分块上传到文件系统
		UploadBlockToFileSystem(ctx context.Context, in model.UploadBlockInput) (out *model.UploadBlockOutput, err error)
		// MergeUploadedBlocks 合并已上传文件块
		MergeUploadedBlocks(ctx context.Context, hash string) (out *model.MergeUploadBlockOutput, err error)
		// CreateBlockSubTmpFile 创建并处理临时文件
		CreateBlockSubTmpFile(ctx context.Context, mainDir, blockHash string, f func(ctx context.Context, file *os.File) error) (string, error)
		// GetUploadBlockFileIndexMap 获取上传历史文件块索引字典
		GetUploadBlockFileIndexMap(ctx context.Context, hash string) (map[string]model.UploadBlockItem, error)
		// BlockMainTmpDir 获取或创建主文件临时目录
		BlockMainTmpDir(ctx context.Context, fileHash string) (string, error)
		// BlockFileDir 创建文件目录
		BlockFileDir(ctx context.Context, fileExt string) (string, error)
		// CreateFileDir 创建文件目录
		CreateFileDir(ctx context.Context, dirs ...string) (string, error)
		// InternalMergeBlocks 合并文件块（内部使用）
		InternalMergeBlocks(ctx context.Context, item model.UploadBlockMerge) (info *model.UploadBlockGenMerge, err error)
		// SetUploadBlockIsCompleted 设置上传文件块的完成信息
		SetUploadBlockIsCompleted(ctx context.Context, hash string, item model.UploadBlockFile) error
		// GetUploadBlockIsCompleted 获取上传文件块的完成信息
		GetUploadBlockIsCompleted(ctx context.Context, hash string) (*model.UploadBlockFile, error)
		// GetUploadBlockMergeCompleted 获取文件合并成功信息
		GetUploadBlockMergeCompleted(ctx context.Context, hash string) (*model.UploadBlockGenMerge, error)
		// SetUploadBlockMergeCompleted 设置上传文合并完成信息
		SetUploadBlockMergeCompleted(ctx context.Context, hash string, item *model.UploadBlockGenMerge) error
		// SetUploadBlockFileIndex 设置上传文件块索引
		SetUploadBlockFileIndex(ctx context.Context, item model.UploadBlockItem) ([]model.UploadBlockItem, error)
		// GetUploadBlockFileIndex 获取上传历史文件块索引
		GetUploadBlockFileIndex(ctx context.Context, hash string) ([]model.UploadBlockItem, error)
		// RemoveUploadBlockFileCache 移除上传文件索引及其他缓存
		RemoveUploadBlockFileCache(ctx context.Context, hash string) error
		// ProcessLocalUpload 处理本地上传文件
		ProcessLocalUpload(ctx context.Context, in model.UploadFileInput, item *model.UploadAttachmentItem) (err error)
		// FileUpload 文件上传
		FileUpload(ctx context.Context, in model.UploadFileInput) (out *model.UploadFileOutput, err error)
		// GetConfig 读取文件上传配置
		GetConfig(ctx context.Context) (rawConfig *config.UploadConfigItem, err error)
		// LocalUploadConfig 本地配置解析
		LocalUploadConfig(ctx context.Context) (*model.UploadConfigLocalItem, error)
	}
)

var (
	localUpload IUpload
)

func Upload() IUpload {
	if localUpload == nil {
		panic("implement not found for interface IUpload, forgot register?")
	}
	return localUpload
}

func RegisterUpload(i IUpload) {
	localUpload = i
}
