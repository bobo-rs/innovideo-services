// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"

	"gitee.com/bobo-rs/innovideo-services/enums"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
)

type (
	IPay interface {
		// Config 获取支付配置
		Config(ctx context.Context) (*model.PayConfig, error)
		// 支付回调更新数据
		Notify(ctx context.Context, in model.PayNotifyInput) (err error)
		// Payment 去支付
		Payment(ctx context.Context, in model.PaymentInput) (interface{}, error)
		// ConfirmPayment 确认支付（PayPal必须）
		ConfirmPayment(ctx context.Context, payNo string, payType enums.PayType, f func(ctx context.Context, tradeNo string) (interface{}, error)) (interface{}, error)
		// GenTradeNo 通过订单号生成商户交易号
		GenTradeNo(no string) string
		// SplitTradeNo 从商户交易单号中拆出订单号
		SplitTradeNo(tradeNo string) string
	}
)

var (
	localPay IPay
)

func Pay() IPay {
	if localPay == nil {
		panic("implement not found for interface IPay, forgot register?")
	}
	return localPay
}

func RegisterPay(i IPay) {
	localPay = i
}
