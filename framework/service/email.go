// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"

	"gitee.com/bobo-rs/innovideo-services/framework/logic/base"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/framework/model/entity"
)

type (
	IEmail interface {
		// TemplateModel 邮件模板Model
		TemplateModel() *base.TblBaseService
		// SendCaptcha 发送邮件验证码
		SendCaptcha(ctx context.Context, emailAddr, varName string) error
		// SendMessage 发送邮件消息
		SendMessage(ctx context.Context, varName string, addrs, tempParamsSet []string) error
		// SendMail 发送邮件
		SendMail(ctx context.Context, addrs []string, subject, content string) error
		// SaveTemplate 保存邮件模板数据
		SaveTemplate(ctx context.Context, en entity.EmailTemplate) error
		// GetTemplateList 获取邮件模板列表
		GetTemplateList(ctx context.Context, in model.EmailTemplateListInput) (out *model.EmailTemplateListOutput, err error)
		// GetTemplateByVarName 通过变量名获取邮件模板详情
		GetTemplateByVarName(ctx context.Context, varName string) (detail *model.EmailTemplateItem, err error)
		// QueryTemplateDetailByVarName 通过变量名查询邮件模板详情
		QueryTemplateDetailByVarName(ctx context.Context, varName string) (detail *model.EmailTemplateItem, err error)
		// RemoveCache 移除缓存
		RemoveCache(ctx context.Context, varName string)
	}
)

var (
	localEmail IEmail
)

func Email() IEmail {
	if localEmail == nil {
		panic("implement not found for interface IEmail, forgot register?")
	}
	return localEmail
}

func RegisterEmail(i IEmail) {
	localEmail = i
}
