// =================================================================================
// This is auto-generated by GoFrame CLI tool only once. Fill this file as you wish.
// =================================================================================

package dao

import (
	"gitee.com/bobo-rs/innovideo-services/framework/dao/internal"
)

// internalSysMapDao is internal type for wrapping internal DAO implements.
type internalSysMapDao = *internal.SysMapDao

// sysMapDao is the data access object for table sys_map.
// You can define custom methods on it to extend its functionality as you wish.
type sysMapDao struct {
	internalSysMapDao
}

var (
	// SysMap is globally public accessible object for table sys_map operations.
	SysMap = sysMapDao{
		internal.NewSysMapDao(),
	}
)

// Fill with you ideas below.
