// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// VideoPlaySequelDao is the data access object for table video_play_sequel.
type VideoPlaySequelDao struct {
	table   string                 // table is the underlying table name of the DAO.
	group   string                 // group is the database configuration group name of current DAO.
	columns VideoPlaySequelColumns // columns contains all the column names of Table for convenient usage.
}

// VideoPlaySequelColumns defines and stores column names for table video_play_sequel.
type VideoPlaySequelColumns struct {
	Id            string // 播放明细ID
	PlayId        string // 播放ID
	VideoSequelId string // 视频续集ID
	CurrPlayRate  string // 当前播放进度
	CurrPlayTime  string // 当前播放时间
	PlaySequel    string // 播放续集
	Resolution    string // 当前播放精度：480P，720P，1080P等
	Ip            string // 当前ip
	UpdateAt      string // 更新时间
	CreateAt      string // 创建时间
}

// videoPlaySequelColumns holds the columns for table video_play_sequel.
var videoPlaySequelColumns = VideoPlaySequelColumns{
	Id:            "id",
	PlayId:        "play_id",
	VideoSequelId: "video_sequel_id",
	CurrPlayRate:  "curr_play_rate",
	CurrPlayTime:  "curr_play_time",
	PlaySequel:    "play_sequel",
	Resolution:    "resolution",
	Ip:            "ip",
	UpdateAt:      "update_at",
	CreateAt:      "create_at",
}

// NewVideoPlaySequelDao creates and returns a new DAO object for table data access.
func NewVideoPlaySequelDao() *VideoPlaySequelDao {
	return &VideoPlaySequelDao{
		group:   "default",
		table:   "video_play_sequel",
		columns: videoPlaySequelColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *VideoPlaySequelDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *VideoPlaySequelDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *VideoPlaySequelDao) Columns() VideoPlaySequelColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *VideoPlaySequelDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *VideoPlaySequelDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *VideoPlaySequelDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
