// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// VideoCategoryDao is the data access object for table video_category.
type VideoCategoryDao struct {
	table   string               // table is the underlying table name of the DAO.
	group   string               // group is the database configuration group name of current DAO.
	columns VideoCategoryColumns // columns contains all the column names of Table for convenient usage.
}

// VideoCategoryColumns defines and stores column names for table video_category.
type VideoCategoryColumns struct {
	Id          string // 视频分类ID
	Name        string // 分类名
	TinyName    string // 短标题
	Pid         string // 父级ID
	Sort        string // 排序：升序排序0-99999
	IsShow      string // 是否显示
	IsRecommend string // 是否推荐首页：0否，1是
	UpdateAt    string // 更新时间
	CreateAt    string // 创建时间
}

// videoCategoryColumns holds the columns for table video_category.
var videoCategoryColumns = VideoCategoryColumns{
	Id:          "id",
	Name:        "name",
	TinyName:    "tiny_name",
	Pid:         "pid",
	Sort:        "sort",
	IsShow:      "is_show",
	IsRecommend: "is_recommend",
	UpdateAt:    "update_at",
	CreateAt:    "create_at",
}

// NewVideoCategoryDao creates and returns a new DAO object for table data access.
func NewVideoCategoryDao() *VideoCategoryDao {
	return &VideoCategoryDao{
		group:   "default",
		table:   "video_category",
		columns: videoCategoryColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *VideoCategoryDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *VideoCategoryDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *VideoCategoryDao) Columns() VideoCategoryColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *VideoCategoryDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *VideoCategoryDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *VideoCategoryDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
