// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// VideoBuyDao is the data access object for table video_buy.
type VideoBuyDao struct {
	table   string          // table is the underlying table name of the DAO.
	group   string          // group is the database configuration group name of current DAO.
	columns VideoBuyColumns // columns contains all the column names of Table for convenient usage.
}

// VideoBuyColumns defines and stores column names for table video_buy.
type VideoBuyColumns struct {
	Id                string // 视频购买ID
	Uid               string // 用户ID
	VideoId           string // 视频ID
	BuyNo             string // 视频购买编号
	VideoName         string // 视频名称
	CompletePanguCoin string // 全集盘古币数量
	AddPanguCoin      string // 已累计消费盘古币数量
	Status            string // 状态：0购买中，1已全剧购买
	UpdateAt          string // 更新时间
	CreateAt          string // 创建时间
}

// videoBuyColumns holds the columns for table video_buy.
var videoBuyColumns = VideoBuyColumns{
	Id:                "id",
	Uid:               "uid",
	VideoId:           "video_id",
	BuyNo:             "buy_no",
	VideoName:         "video_name",
	CompletePanguCoin: "complete_pangu_coin",
	AddPanguCoin:      "add_pangu_coin",
	Status:            "status",
	UpdateAt:          "update_at",
	CreateAt:          "create_at",
}

// NewVideoBuyDao creates and returns a new DAO object for table data access.
func NewVideoBuyDao() *VideoBuyDao {
	return &VideoBuyDao{
		group:   "default",
		table:   "video_buy",
		columns: videoBuyColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *VideoBuyDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *VideoBuyDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *VideoBuyDao) Columns() VideoBuyColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *VideoBuyDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *VideoBuyDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *VideoBuyDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
