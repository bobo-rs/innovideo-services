// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// VideoBuyBillDao is the data access object for table video_buy_bill.
type VideoBuyBillDao struct {
	table   string              // table is the underlying table name of the DAO.
	group   string              // group is the database configuration group name of current DAO.
	columns VideoBuyBillColumns // columns contains all the column names of Table for convenient usage.
}

// VideoBuyBillColumns defines and stores column names for table video_buy_bill.
type VideoBuyBillColumns struct {
	Id              string // 视频购买账单ID
	BuyId           string // 购买ID
	Uid             string // 用户ID
	BuyPanguCoin    string // 支付盘古币
	RefundPanguCoin string // 退款盘古币
	BuyEpisodesNum  string // 购买集数
	Content         string // 购买内容，例如[视频名+集数，集数，集数。。。]
	Remark          string // 退款备注
	Status          string // 购买状态：0已购买，1已退款,2部分退款
	Type            string // 购买类型：0单集购买，1全集购买，2余集买断
	RefundAt        string // 退款时间
	UpdateAt        string // 更新时间
	CreateAt        string // 创建时间
}

// videoBuyBillColumns holds the columns for table video_buy_bill.
var videoBuyBillColumns = VideoBuyBillColumns{
	Id:              "id",
	BuyId:           "buy_id",
	Uid:             "uid",
	BuyPanguCoin:    "buy_pangu_coin",
	RefundPanguCoin: "refund_pangu_coin",
	BuyEpisodesNum:  "buy_episodes_num",
	Content:         "content",
	Remark:          "remark",
	Status:          "status",
	Type:            "type",
	RefundAt:        "refund_at",
	UpdateAt:        "update_at",
	CreateAt:        "create_at",
}

// NewVideoBuyBillDao creates and returns a new DAO object for table data access.
func NewVideoBuyBillDao() *VideoBuyBillDao {
	return &VideoBuyBillDao{
		group:   "default",
		table:   "video_buy_bill",
		columns: videoBuyBillColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *VideoBuyBillDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *VideoBuyBillDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *VideoBuyBillDao) Columns() VideoBuyBillColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *VideoBuyBillDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *VideoBuyBillDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *VideoBuyBillDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
