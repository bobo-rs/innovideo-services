// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// UserLoginDao is the data access object for table user_login.
type UserLoginDao struct {
	table   string           // table is the underlying table name of the DAO.
	group   string           // group is the database configuration group name of current DAO.
	columns UserLoginColumns // columns contains all the column names of Table for convenient usage.
}

// UserLoginColumns defines and stores column names for table user_login.
type UserLoginColumns struct {
	Id              string // 用户登录记录ID
	UserId          string // 用户ID
	LoginIp         string // 登录IP
	LoginIpLocation string // 登录归属地
	Token           string // 登录token
	ExpireTime      string // 过期时间
	Status          string // 状态：0在线，1下线，2超时下线
	IsUse           string // 是否一直在使用；0是，1否
	DeviceType      string // 设备类型
	DeviceContent   string // 设备详情
	IsNewDevice     string // 是否新设备：0是，1新设备
	Mac             string // Mac地址
	UpdateAt        string // 更新时间
	CreateAt        string // 登录时间
}

// userLoginColumns holds the columns for table user_login.
var userLoginColumns = UserLoginColumns{
	Id:              "id",
	UserId:          "user_id",
	LoginIp:         "login_ip",
	LoginIpLocation: "login_ip_location",
	Token:           "token",
	ExpireTime:      "expire_time",
	Status:          "status",
	IsUse:           "is_use",
	DeviceType:      "device_type",
	DeviceContent:   "device_content",
	IsNewDevice:     "is_new_device",
	Mac:             "mac",
	UpdateAt:        "update_at",
	CreateAt:        "create_at",
}

// NewUserLoginDao creates and returns a new DAO object for table data access.
func NewUserLoginDao() *UserLoginDao {
	return &UserLoginDao{
		group:   "default",
		table:   "user_login",
		columns: userLoginColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *UserLoginDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *UserLoginDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *UserLoginDao) Columns() UserLoginColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *UserLoginDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *UserLoginDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *UserLoginDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
