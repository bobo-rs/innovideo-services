// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// PanguCoinTransactionDao is the data access object for table pangu_coin_transaction.
type PanguCoinTransactionDao struct {
	table   string                      // table is the underlying table name of the DAO.
	group   string                      // group is the database configuration group name of current DAO.
	columns PanguCoinTransactionColumns // columns contains all the column names of Table for convenient usage.
}

// PanguCoinTransactionColumns defines and stores column names for table pangu_coin_transaction.
type PanguCoinTransactionColumns struct {
	Id              string // 盘古币流水ID
	Uid             string // 用户ID
	TransactionNo   string // 流水号
	BeforePanguCoin string // 变动前盘古币
	PanguCoin       string // 变动盘古币
	AfterPanguCoin  string // 变动后盘古币（当前数量）
	Type            string // 流水类型：0收入，1支出
	Remark          string // 备注
	CreateAt        string // 流水记录时间
}

// panguCoinTransactionColumns holds the columns for table pangu_coin_transaction.
var panguCoinTransactionColumns = PanguCoinTransactionColumns{
	Id:              "id",
	Uid:             "uid",
	TransactionNo:   "transaction_no",
	BeforePanguCoin: "before_pangu_coin",
	PanguCoin:       "pangu_coin",
	AfterPanguCoin:  "after_pangu_coin",
	Type:            "type",
	Remark:          "remark",
	CreateAt:        "create_at",
}

// NewPanguCoinTransactionDao creates and returns a new DAO object for table data access.
func NewPanguCoinTransactionDao() *PanguCoinTransactionDao {
	return &PanguCoinTransactionDao{
		group:   "default",
		table:   "pangu_coin_transaction",
		columns: panguCoinTransactionColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *PanguCoinTransactionDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *PanguCoinTransactionDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *PanguCoinTransactionDao) Columns() PanguCoinTransactionColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *PanguCoinTransactionDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *PanguCoinTransactionDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *PanguCoinTransactionDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
