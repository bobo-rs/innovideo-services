// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// VideoLabelDao is the data access object for table video_label.
type VideoLabelDao struct {
	table   string            // table is the underlying table name of the DAO.
	group   string            // group is the database configuration group name of current DAO.
	columns VideoLabelColumns // columns contains all the column names of Table for convenient usage.
}

// VideoLabelColumns defines and stores column names for table video_label.
type VideoLabelColumns struct {
	Id       string // 视频标签ID
	VideoId  string // 视频ID
	Value    string // 标签值
	CreateAt string // 创建时间
}

// videoLabelColumns holds the columns for table video_label.
var videoLabelColumns = VideoLabelColumns{
	Id:       "id",
	VideoId:  "video_id",
	Value:    "value",
	CreateAt: "create_at",
}

// NewVideoLabelDao creates and returns a new DAO object for table data access.
func NewVideoLabelDao() *VideoLabelDao {
	return &VideoLabelDao{
		group:   "default",
		table:   "video_label",
		columns: videoLabelColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *VideoLabelDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *VideoLabelDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *VideoLabelDao) Columns() VideoLabelColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *VideoLabelDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *VideoLabelDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *VideoLabelDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
