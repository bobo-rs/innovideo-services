// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// SmsTemplateDao is the data access object for table sms_template.
type SmsTemplateDao struct {
	table   string             // table is the underlying table name of the DAO.
	group   string             // group is the database configuration group name of current DAO.
	columns SmsTemplateColumns // columns contains all the column names of Table for convenient usage.
}

// SmsTemplateColumns defines and stores column names for table sms_template.
type SmsTemplateColumns struct {
	Id       string // 模板ID
	Name     string // 模板名
	Content  string // 模板内容
	VarAlias string // 模板别名
	SmsType  string // 短信类型：0验证码，1消息通知，2营销短信
	Status   string // 模板状态：0关闭，1启用
	Remark   string // 备注
	UpdateAt string // 更新时间
	CreateAt string // 创建时间
}

// smsTemplateColumns holds the columns for table sms_template.
var smsTemplateColumns = SmsTemplateColumns{
	Id:       "id",
	Name:     "name",
	Content:  "content",
	VarAlias: "var_alias",
	SmsType:  "sms_type",
	Status:   "status",
	Remark:   "remark",
	UpdateAt: "update_at",
	CreateAt: "create_at",
}

// NewSmsTemplateDao creates and returns a new DAO object for table data access.
func NewSmsTemplateDao() *SmsTemplateDao {
	return &SmsTemplateDao{
		group:   "default",
		table:   "sms_template",
		columns: smsTemplateColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *SmsTemplateDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *SmsTemplateDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *SmsTemplateDao) Columns() SmsTemplateColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *SmsTemplateDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *SmsTemplateDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *SmsTemplateDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
