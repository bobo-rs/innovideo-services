// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// ActorsAchievementDao is the data access object for table actors_achievement.
type ActorsAchievementDao struct {
	table   string                   // table is the underlying table name of the DAO.
	group   string                   // group is the database configuration group name of current DAO.
	columns ActorsAchievementColumns // columns contains all the column names of Table for convenient usage.
}

// ActorsAchievementColumns defines and stores column names for table actors_achievement.
type ActorsAchievementColumns struct {
	Id          string // 演员个人成就ID
	ActorsId    string // 演员ID
	AchTitle    string // 成就Title
	AchieveDate string // 成就时间
	UpdateAt    string // 更新时间
	CreateAt    string // 创建时间
}

// actorsAchievementColumns holds the columns for table actors_achievement.
var actorsAchievementColumns = ActorsAchievementColumns{
	Id:          "id",
	ActorsId:    "actors_id",
	AchTitle:    "ach_title",
	AchieveDate: "achieve_date",
	UpdateAt:    "update_at",
	CreateAt:    "create_at",
}

// NewActorsAchievementDao creates and returns a new DAO object for table data access.
func NewActorsAchievementDao() *ActorsAchievementDao {
	return &ActorsAchievementDao{
		group:   "default",
		table:   "actors_achievement",
		columns: actorsAchievementColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *ActorsAchievementDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *ActorsAchievementDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *ActorsAchievementDao) Columns() ActorsAchievementColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *ActorsAchievementDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *ActorsAchievementDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *ActorsAchievementDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
