// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// PanguCoinOrderDao is the data access object for table pangu_coin_order.
type PanguCoinOrderDao struct {
	table   string                // table is the underlying table name of the DAO.
	group   string                // group is the database configuration group name of current DAO.
	columns PanguCoinOrderColumns // columns contains all the column names of Table for convenient usage.
}

// PanguCoinOrderColumns defines and stores column names for table pangu_coin_order.
type PanguCoinOrderColumns struct {
	Id              string // 盘古币订单ID
	Uid             string // 用户ID
	OrderNo         string // 订单号
	TradeNo         string // 商户交易号
	PayNo           string // 第三方支付交易号
	RefundNo        string // 退款交易号
	PayCode         string // 支付别名
	PayName         string // 支付名
	PayFee          string // 支付金额
	RefundFee       string // 退款金额
	PanguCoin       string // 兑换盘古币数量
	RefundPanguCoin string // 退回盘古币数量
	Currency        string // 当前支付货币，默认人民币CNY
	PayAt           string // 支付时间
	RefundAt        string // 退款时间
	PayStatus       string // 支付状态：0待支付，1已支付
	Status          string // 状态：0待确认，1已完成，2已取消，3已退款
	Remark          string // 备注
	Content         string // 描述内容
	UpdateAt        string // 更新时间
	CreateAt        string // 创建时间
}

// panguCoinOrderColumns holds the columns for table pangu_coin_order.
var panguCoinOrderColumns = PanguCoinOrderColumns{
	Id:              "id",
	Uid:             "uid",
	OrderNo:         "order_no",
	TradeNo:         "trade_no",
	PayNo:           "pay_no",
	RefundNo:        "refund_no",
	PayCode:         "pay_code",
	PayName:         "pay_name",
	PayFee:          "pay_fee",
	RefundFee:       "refund_fee",
	PanguCoin:       "pangu_coin",
	RefundPanguCoin: "refund_pangu_coin",
	Currency:        "currency",
	PayAt:           "pay_at",
	RefundAt:        "refund_at",
	PayStatus:       "pay_status",
	Status:          "status",
	Remark:          "remark",
	Content:         "content",
	UpdateAt:        "update_at",
	CreateAt:        "create_at",
}

// NewPanguCoinOrderDao creates and returns a new DAO object for table data access.
func NewPanguCoinOrderDao() *PanguCoinOrderDao {
	return &PanguCoinOrderDao{
		group:   "default",
		table:   "pangu_coin_order",
		columns: panguCoinOrderColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *PanguCoinOrderDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *PanguCoinOrderDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *PanguCoinOrderDao) Columns() PanguCoinOrderColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *PanguCoinOrderDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *PanguCoinOrderDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *PanguCoinOrderDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
