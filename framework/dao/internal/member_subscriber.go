// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// MemberSubscriberDao is the data access object for table member_subscriber.
type MemberSubscriberDao struct {
	table   string                  // table is the underlying table name of the DAO.
	group   string                  // group is the database configuration group name of current DAO.
	columns MemberSubscriberColumns // columns contains all the column names of Table for convenient usage.
}

// MemberSubscriberColumns defines and stores column names for table member_subscriber.
type MemberSubscriberColumns struct {
	Id           string // 会员订阅ID
	SubscriberNo string // 订阅编号
	Uid          string // 用户ID
	PayStatus    string // 支付状态：0待支付，1已支付
	Status       string // 订阅状态：0待确认，1服务中，2服务完成，3取消服务，4已退款
	PayCode      string // 支付方式别名
	PayName      string // 支付方式名
	DayFee       string // 折扣每日单价=支付总额/订阅天数
	SubTotalNum  string // 订阅天数
	PayFee       string // 支付金额
	RefundFee    string // 退款金额
	MbId         string // 会员身份ID
	MbName       string // 会员身份名
	SubStartDate string // 会员生效时间
	SubEndDate   string // 会员过期时间
	PayAt        string // 会员支付时间
	TradeNo      string // 商户交易支付号
	PayNo        string // 第三方支付交易号
	RefundNo     string // 第三方退款交易号
	RefundAt     string // 退款时间
	Remark       string // 备注
	UpdateAt     string // 更新时间
	CreateAt     string // 创建时间
}

// memberSubscriberColumns holds the columns for table member_subscriber.
var memberSubscriberColumns = MemberSubscriberColumns{
	Id:           "id",
	SubscriberNo: "subscriber_no",
	Uid:          "uid",
	PayStatus:    "pay_status",
	Status:       "status",
	PayCode:      "pay_code",
	PayName:      "pay_name",
	DayFee:       "day_fee",
	SubTotalNum:  "sub_total_num",
	PayFee:       "pay_fee",
	RefundFee:    "refund_fee",
	MbId:         "mb_id",
	MbName:       "mb_name",
	SubStartDate: "sub_start_date",
	SubEndDate:   "sub_end_date",
	PayAt:        "pay_at",
	TradeNo:      "trade_no",
	PayNo:        "pay_no",
	RefundNo:     "refund_no",
	RefundAt:     "refund_at",
	Remark:       "remark",
	UpdateAt:     "update_at",
	CreateAt:     "create_at",
}

// NewMemberSubscriberDao creates and returns a new DAO object for table data access.
func NewMemberSubscriberDao() *MemberSubscriberDao {
	return &MemberSubscriberDao{
		group:   "default",
		table:   "member_subscriber",
		columns: memberSubscriberColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *MemberSubscriberDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *MemberSubscriberDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *MemberSubscriberDao) Columns() MemberSubscriberColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *MemberSubscriberDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *MemberSubscriberDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *MemberSubscriberDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
