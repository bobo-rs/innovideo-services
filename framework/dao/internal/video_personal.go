// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// VideoPersonalDao is the data access object for table video_personal.
type VideoPersonalDao struct {
	table   string               // table is the underlying table name of the DAO.
	group   string               // group is the database configuration group name of current DAO.
	columns VideoPersonalColumns // columns contains all the column names of Table for convenient usage.
}

// VideoPersonalColumns defines and stores column names for table video_personal.
type VideoPersonalColumns struct {
	Id       string // 视频相关人员ID
	VideoId  string // 视频ID
	ActorsId string // 演员ID
	Name     string // 参演名
	Type     string // 角色类型：0导演，1副导演，2领衔主演，3主演，4客串嘉宾，5其他
	Portray  string // 饰演角色
	IsShow   string // 是否展示：0展示，1隐藏
	UpdateAt string // 更新时间
	CreateAt string // 创建时间
}

// videoPersonalColumns holds the columns for table video_personal.
var videoPersonalColumns = VideoPersonalColumns{
	Id:       "id",
	VideoId:  "video_id",
	ActorsId: "actors_id",
	Name:     "name",
	Type:     "type",
	Portray:  "portray",
	IsShow:   "is_show",
	UpdateAt: "update_at",
	CreateAt: "create_at",
}

// NewVideoPersonalDao creates and returns a new DAO object for table data access.
func NewVideoPersonalDao() *VideoPersonalDao {
	return &VideoPersonalDao{
		group:   "default",
		table:   "video_personal",
		columns: videoPersonalColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *VideoPersonalDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *VideoPersonalDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *VideoPersonalDao) Columns() VideoPersonalColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *VideoPersonalDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *VideoPersonalDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *VideoPersonalDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
