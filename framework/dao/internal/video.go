// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// VideoDao is the data access object for table video.
type VideoDao struct {
	table   string       // table is the underlying table name of the DAO.
	group   string       // group is the database configuration group name of current DAO.
	columns VideoColumns // columns contains all the column names of Table for convenient usage.
}

// VideoColumns defines and stores column names for table video.
type VideoColumns struct {
	Id          string // 视频ID
	VideoNo     string // 视频编号
	VideoName   string // 视频名
	Keywords    string // 关键词
	CateId1     string // 一级类目
	CateId2     string // 二级类目
	Region      string // 地区
	YearDate    string // 上映年份
	ReleaseDate string // 上映时间
	IsRecommend string // 是否推荐：0否，1是
	IsBest      string // 是否精品：0否，1是
	IsTop       string // 是否置顶：0否，1是
	IsNew       string // 是否新剧：0否，1是
	IsHot       string // 是否热播：0否，1是
	FeeType     string // 收费类型：0免费，1VIP付费
	ImageUrl    string // 海报
	Browse      string // 浏览量
	PlayNum     string // 播放量
	Collect     string // 收藏量
	ShareNum    string // 分享量
	Score       string // 评分，最高分10分
	Status      string // 视频状态：0待审核，1审核通过，2驳回申请，3违规下架, 4视频删除
	IsRelease   string // 是否发布：0未发布，1已发布
	UpdateAt    string // 更新时间
	CreateAt    string // 创建时间
}

// videoColumns holds the columns for table video.
var videoColumns = VideoColumns{
	Id:          "id",
	VideoNo:     "video_no",
	VideoName:   "video_name",
	Keywords:    "keywords",
	CateId1:     "cate_id1",
	CateId2:     "cate_id2",
	Region:      "region",
	YearDate:    "year_date",
	ReleaseDate: "release_date",
	IsRecommend: "is_recommend",
	IsBest:      "is_best",
	IsTop:       "is_top",
	IsNew:       "is_new",
	IsHot:       "is_hot",
	FeeType:     "fee_type",
	ImageUrl:    "image_url",
	Browse:      "browse",
	PlayNum:     "play_num",
	Collect:     "collect",
	ShareNum:    "share_num",
	Score:       "score",
	Status:      "status",
	IsRelease:   "is_release",
	UpdateAt:    "update_at",
	CreateAt:    "create_at",
}

// NewVideoDao creates and returns a new DAO object for table data access.
func NewVideoDao() *VideoDao {
	return &VideoDao{
		group:   "default",
		table:   "video",
		columns: videoColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *VideoDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *VideoDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *VideoDao) Columns() VideoColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *VideoDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *VideoDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *VideoDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
