// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// VideoResolutionDao is the data access object for table video_resolution.
type VideoResolutionDao struct {
	table   string                 // table is the underlying table name of the DAO.
	group   string                 // group is the database configuration group name of current DAO.
	columns VideoResolutionColumns // columns contains all the column names of Table for convenient usage.
}

// VideoResolutionColumns defines and stores column names for table video_resolution.
type VideoResolutionColumns struct {
	Id             string // 视频清晰度ID
	EpisodesId     string // 视频续集ID
	Resolution     string // 清晰度：480P，720P，1080P等
	AttachId       string // 附件ID
	VideoUrl       string // 视频地址
	Size           string // 视频大小（B）
	Duration       string // 时长（S）
	DurationString string // 格式化时长
	MimeType       string // 视频类型
	UpdateAt       string // 更新时间
	CreateAt       string // 创建时间
}

// videoResolutionColumns holds the columns for table video_resolution.
var videoResolutionColumns = VideoResolutionColumns{
	Id:             "id",
	EpisodesId:     "episodes_id",
	Resolution:     "resolution",
	AttachId:       "attach_id",
	VideoUrl:       "video_url",
	Size:           "size",
	Duration:       "duration",
	DurationString: "duration_string",
	MimeType:       "mime_type",
	UpdateAt:       "update_at",
	CreateAt:       "create_at",
}

// NewVideoResolutionDao creates and returns a new DAO object for table data access.
func NewVideoResolutionDao() *VideoResolutionDao {
	return &VideoResolutionDao{
		group:   "default",
		table:   "video_resolution",
		columns: videoResolutionColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *VideoResolutionDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *VideoResolutionDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *VideoResolutionDao) Columns() VideoResolutionColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *VideoResolutionDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *VideoResolutionDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *VideoResolutionDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
