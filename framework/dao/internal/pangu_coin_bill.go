// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// PanguCoinBillDao is the data access object for table pangu_coin_bill.
type PanguCoinBillDao struct {
	table   string               // table is the underlying table name of the DAO.
	group   string               // group is the database configuration group name of current DAO.
	columns PanguCoinBillColumns // columns contains all the column names of Table for convenient usage.
}

// PanguCoinBillColumns defines and stores column names for table pangu_coin_bill.
type PanguCoinBillColumns struct {
	Id                 string // 盘古币账单
	Uid                string // 用户ID
	OrderId            string // 盘古币充值订单ID
	BillNo             string // 账单编号
	TotalPanguCoin     string // 初始盘古币-留存记录不变
	AvailablePanguCoin string // 可用盘古币
	ExpiredPanguCoin   string // 过期盘古币
	ExpiredDate        string // 过期时间
	Status             string // 盘古币账单状态：0正常，1交易完成，2部分过期，3全额过期，4部分退款，5全额退款
	BillType           string // 账单类型：0充值兑换，1活动赠送，2平台赠送，3新用户赠送, 4售后退回
	Remark             string // 备注
	UpdateAt           string // 更新时间
	CreateAt           string // 创建时间
}

// panguCoinBillColumns holds the columns for table pangu_coin_bill.
var panguCoinBillColumns = PanguCoinBillColumns{
	Id:                 "id",
	Uid:                "uid",
	OrderId:            "order_id",
	BillNo:             "bill_no",
	TotalPanguCoin:     "total_pangu_coin",
	AvailablePanguCoin: "available_pangu_coin",
	ExpiredPanguCoin:   "expired_pangu_coin",
	ExpiredDate:        "expired_date",
	Status:             "status",
	BillType:           "bill_type",
	Remark:             "remark",
	UpdateAt:           "update_at",
	CreateAt:           "create_at",
}

// NewPanguCoinBillDao creates and returns a new DAO object for table data access.
func NewPanguCoinBillDao() *PanguCoinBillDao {
	return &PanguCoinBillDao{
		group:   "default",
		table:   "pangu_coin_bill",
		columns: panguCoinBillColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *PanguCoinBillDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *PanguCoinBillDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *PanguCoinBillDao) Columns() PanguCoinBillColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *PanguCoinBillDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *PanguCoinBillDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *PanguCoinBillDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
