// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// UserFundsDao is the data access object for table user_funds.
type UserFundsDao struct {
	table   string           // table is the underlying table name of the DAO.
	group   string           // group is the database configuration group name of current DAO.
	columns UserFundsColumns // columns contains all the column names of Table for convenient usage.
}

// UserFundsColumns defines and stores column names for table user_funds.
type UserFundsColumns struct {
	UserId                string // 用户ID
	RechargeTotalFee      string // 充值总金额
	AvailableFee          string // 可用金额
	FreezeFee             string // 冻结金额
	PanguCoinTotal        string // 兑换盘古币总数
	AvailablePanguCoin    string // 可用盘古币数
	FreezePanguCoin       string // 冻结盘古币数
	ConsumeTotalPanguCoin string // 消费总盘古币数
	ConsumeTotalFee       string // 消费总金额
	UpdateAt              string // 更新时间
}

// userFundsColumns holds the columns for table user_funds.
var userFundsColumns = UserFundsColumns{
	UserId:                "user_id",
	RechargeTotalFee:      "recharge_total_fee",
	AvailableFee:          "available_fee",
	FreezeFee:             "freeze_fee",
	PanguCoinTotal:        "pangu_coin_total",
	AvailablePanguCoin:    "available_pangu_coin",
	FreezePanguCoin:       "freeze_pangu_coin",
	ConsumeTotalPanguCoin: "consume_total_pangu_coin",
	ConsumeTotalFee:       "consume_total_fee",
	UpdateAt:              "update_at",
}

// NewUserFundsDao creates and returns a new DAO object for table data access.
func NewUserFundsDao() *UserFundsDao {
	return &UserFundsDao{
		group:   "default",
		table:   "user_funds",
		columns: userFundsColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *UserFundsDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *UserFundsDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *UserFundsDao) Columns() UserFundsColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *UserFundsDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *UserFundsDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *UserFundsDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
