// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// PanguCoinBillItemDao is the data access object for table pangu_coin_bill_item.
type PanguCoinBillItemDao struct {
	table   string                   // table is the underlying table name of the DAO.
	group   string                   // group is the database configuration group name of current DAO.
	columns PanguCoinBillItemColumns // columns contains all the column names of Table for convenient usage.
}

// PanguCoinBillItemColumns defines and stores column names for table pangu_coin_bill_item.
type PanguCoinBillItemColumns struct {
	Id            string // 账单明细ID
	BillId        string // 账单ID
	TransactionNo string // 流水单号
	Uid           string // 用户ID
	PanguCoin     string // 消费盘古币
	Type          string // 账单流水类型：0收入，1支出
	UpdateAt      string // 更新时间
	CreateAt      string // 创建时间
}

// panguCoinBillItemColumns holds the columns for table pangu_coin_bill_item.
var panguCoinBillItemColumns = PanguCoinBillItemColumns{
	Id:            "id",
	BillId:        "bill_id",
	TransactionNo: "transaction_no",
	Uid:           "uid",
	PanguCoin:     "pangu_coin",
	Type:          "type",
	UpdateAt:      "update_at",
	CreateAt:      "create_at",
}

// NewPanguCoinBillItemDao creates and returns a new DAO object for table data access.
func NewPanguCoinBillItemDao() *PanguCoinBillItemDao {
	return &PanguCoinBillItemDao{
		group:   "default",
		table:   "pangu_coin_bill_item",
		columns: panguCoinBillItemColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *PanguCoinBillItemDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *PanguCoinBillItemDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *PanguCoinBillItemDao) Columns() PanguCoinBillItemColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *PanguCoinBillItemDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *PanguCoinBillItemDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *PanguCoinBillItemDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
