// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// PermissonsAssocDao is the data access object for table permissons_assoc.
type PermissonsAssocDao struct {
	table   string                 // table is the underlying table name of the DAO.
	group   string                 // group is the database configuration group name of current DAO.
	columns PermissonsAssocColumns // columns contains all the column names of Table for convenient usage.
}

// PermissonsAssocColumns defines and stores column names for table permissons_assoc.
type PermissonsAssocColumns struct {
	Id            string // 角色权限关联ID
	AssocId       string // 角色ID
	PermissionsId string // 权限ID
	AssocType     string // 关联类型：0角色关联，1账户关联
	CreateAt      string // 创建时间
}

// permissonsAssocColumns holds the columns for table permissons_assoc.
var permissonsAssocColumns = PermissonsAssocColumns{
	Id:            "id",
	AssocId:       "assoc_id",
	PermissionsId: "permissions_id",
	AssocType:     "assoc_type",
	CreateAt:      "create_at",
}

// NewPermissonsAssocDao creates and returns a new DAO object for table data access.
func NewPermissonsAssocDao() *PermissonsAssocDao {
	return &PermissonsAssocDao{
		group:   "default",
		table:   "permissons_assoc",
		columns: permissonsAssocColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *PermissonsAssocDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *PermissonsAssocDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *PermissonsAssocDao) Columns() PermissonsAssocColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *PermissonsAssocDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *PermissonsAssocDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *PermissonsAssocDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
