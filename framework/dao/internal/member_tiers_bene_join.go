// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// MemberTiersBeneJoinDao is the data access object for table member_tiers_bene_join.
type MemberTiersBeneJoinDao struct {
	table   string                     // table is the underlying table name of the DAO.
	group   string                     // group is the database configuration group name of current DAO.
	columns MemberTiersBeneJoinColumns // columns contains all the column names of Table for convenient usage.
}

// MemberTiersBeneJoinColumns defines and stores column names for table member_tiers_bene_join.
type MemberTiersBeneJoinColumns struct {
	Id            string // 会员关联ID
	BenefitsAlias string // 会员可享服务别名
	MbTiersId     string // 会员身份ID
	CreateAt      string // 创建时间
}

// memberTiersBeneJoinColumns holds the columns for table member_tiers_bene_join.
var memberTiersBeneJoinColumns = MemberTiersBeneJoinColumns{
	Id:            "id",
	BenefitsAlias: "benefits_alias",
	MbTiersId:     "mb_tiers_id",
	CreateAt:      "create_at",
}

// NewMemberTiersBeneJoinDao creates and returns a new DAO object for table data access.
func NewMemberTiersBeneJoinDao() *MemberTiersBeneJoinDao {
	return &MemberTiersBeneJoinDao{
		group:   "default",
		table:   "member_tiers_bene_join",
		columns: memberTiersBeneJoinColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *MemberTiersBeneJoinDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *MemberTiersBeneJoinDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *MemberTiersBeneJoinDao) Columns() MemberTiersBeneJoinColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *MemberTiersBeneJoinDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *MemberTiersBeneJoinDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *MemberTiersBeneJoinDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
