// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// ActorsWorksDao is the data access object for table actors_works.
type ActorsWorksDao struct {
	table   string             // table is the underlying table name of the DAO.
	group   string             // group is the database configuration group name of current DAO.
	columns ActorsWorksColumns // columns contains all the column names of Table for convenient usage.
}

// ActorsWorksColumns defines and stores column names for table actors_works.
type ActorsWorksColumns struct {
	Id             string // 演员作品ID
	ActorsId       string // 演员ID
	WorksName      string // 作品名
	Type           string // 类型：0主演，1领衔主演，2客串嘉宾，3群众
	IsMasterpieces string // 是否代表作：0否，1是
	Portray        string // 饰演角色
	UpdateAt       string // 更新时间
	CreateAt       string // 创建时间
}

// actorsWorksColumns holds the columns for table actors_works.
var actorsWorksColumns = ActorsWorksColumns{
	Id:             "id",
	ActorsId:       "actors_id",
	WorksName:      "works_name",
	Type:           "type",
	IsMasterpieces: "is_masterpieces",
	Portray:        "portray",
	UpdateAt:       "update_at",
	CreateAt:       "create_at",
}

// NewActorsWorksDao creates and returns a new DAO object for table data access.
func NewActorsWorksDao() *ActorsWorksDao {
	return &ActorsWorksDao{
		group:   "default",
		table:   "actors_works",
		columns: actorsWorksColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *ActorsWorksDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *ActorsWorksDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *ActorsWorksDao) Columns() ActorsWorksColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *ActorsWorksDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *ActorsWorksDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *ActorsWorksDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
