// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// VideoUserActionsDao is the data access object for table video_user_actions.
type VideoUserActionsDao struct {
	table   string                  // table is the underlying table name of the DAO.
	group   string                  // group is the database configuration group name of current DAO.
	columns VideoUserActionsColumns // columns contains all the column names of Table for convenient usage.
}

// VideoUserActionsColumns defines and stores column names for table video_user_actions.
type VideoUserActionsColumns struct {
	Id       string // 视频收藏ID
	VideoId  string // 视频ID
	UserId   string // 用户ID
	Ip       string // 当前ip
	Type     string // 操作类型：C收藏，L点赞，T踩
	CreateAt string // 创建时间
}

// videoUserActionsColumns holds the columns for table video_user_actions.
var videoUserActionsColumns = VideoUserActionsColumns{
	Id:       "id",
	VideoId:  "video_id",
	UserId:   "user_id",
	Ip:       "ip",
	Type:     "type",
	CreateAt: "create_at",
}

// NewVideoUserActionsDao creates and returns a new DAO object for table data access.
func NewVideoUserActionsDao() *VideoUserActionsDao {
	return &VideoUserActionsDao{
		group:   "default",
		table:   "video_user_actions",
		columns: videoUserActionsColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *VideoUserActionsDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *VideoUserActionsDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *VideoUserActionsDao) Columns() VideoUserActionsColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *VideoUserActionsDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *VideoUserActionsDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *VideoUserActionsDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
