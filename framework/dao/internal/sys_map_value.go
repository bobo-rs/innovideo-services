// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// SysMapValueDao is the data access object for table sys_map_value.
type SysMapValueDao struct {
	table   string             // table is the underlying table name of the DAO.
	group   string             // group is the database configuration group name of current DAO.
	columns SysMapValueColumns // columns contains all the column names of Table for convenient usage.
}

// SysMapValueColumns defines and stores column names for table sys_map_value.
type SysMapValueColumns struct {
	Id       string // 字典值ID
	Value    string // 字典值
	Explain  string // 字典值解析
	Name     string // 字典名-sys_map[name]
	UpdateAt string // 更新时间
	CreateAt string // 创建时间
}

// sysMapValueColumns holds the columns for table sys_map_value.
var sysMapValueColumns = SysMapValueColumns{
	Id:       "id",
	Value:    "value",
	Explain:  "explain",
	Name:     "name",
	UpdateAt: "update_at",
	CreateAt: "create_at",
}

// NewSysMapValueDao creates and returns a new DAO object for table data access.
func NewSysMapValueDao() *SysMapValueDao {
	return &SysMapValueDao{
		group:   "default",
		table:   "sys_map_value",
		columns: sysMapValueColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *SysMapValueDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *SysMapValueDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *SysMapValueDao) Columns() SysMapValueColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *SysMapValueDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *SysMapValueDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *SysMapValueDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
