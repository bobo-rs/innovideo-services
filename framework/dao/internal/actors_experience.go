// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// ActorsExperienceDao is the data access object for table actors_experience.
type ActorsExperienceDao struct {
	table   string                  // table is the underlying table name of the DAO.
	group   string                  // group is the database configuration group name of current DAO.
	columns ActorsExperienceColumns // columns contains all the column names of Table for convenient usage.
}

// ActorsExperienceColumns defines and stores column names for table actors_experience.
type ActorsExperienceColumns struct {
	Id       string // 个人经历ID
	ActorsId string // 演员ID
	Content  string // 经历内容
	ExpDate  string // 相关经历时间
	UpdateAt string // 更新时间
	CreateAt string // 创建时间
}

// actorsExperienceColumns holds the columns for table actors_experience.
var actorsExperienceColumns = ActorsExperienceColumns{
	Id:       "id",
	ActorsId: "actors_id",
	Content:  "content",
	ExpDate:  "exp_date",
	UpdateAt: "update_at",
	CreateAt: "create_at",
}

// NewActorsExperienceDao creates and returns a new DAO object for table data access.
func NewActorsExperienceDao() *ActorsExperienceDao {
	return &ActorsExperienceDao{
		group:   "default",
		table:   "actors_experience",
		columns: actorsExperienceColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *ActorsExperienceDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *ActorsExperienceDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *ActorsExperienceDao) Columns() ActorsExperienceColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *ActorsExperienceDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *ActorsExperienceDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *ActorsExperienceDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
