// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// VideoLogDao is the data access object for table video_log.
type VideoLogDao struct {
	table   string          // table is the underlying table name of the DAO.
	group   string          // group is the database configuration group name of current DAO.
	columns VideoLogColumns // columns contains all the column names of Table for convenient usage.
}

// VideoLogColumns defines and stores column names for table video_log.
type VideoLogColumns struct {
	Id         string // 视频日志ID
	VideoId    string // 视频ID
	Content    string // 操作内容
	ManageId   string // 管理员ID
	UserId     string // 用户ID
	ManageName string // 操作管理员名
	Url        string // 操作URL
	CreateAt   string // 创建时间
}

// videoLogColumns holds the columns for table video_log.
var videoLogColumns = VideoLogColumns{
	Id:         "id",
	VideoId:    "video_id",
	Content:    "content",
	ManageId:   "manage_id",
	UserId:     "user_id",
	ManageName: "manage_name",
	Url:        "url",
	CreateAt:   "create_at",
}

// NewVideoLogDao creates and returns a new DAO object for table data access.
func NewVideoLogDao() *VideoLogDao {
	return &VideoLogDao{
		group:   "default",
		table:   "video_log",
		columns: videoLogColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *VideoLogDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *VideoLogDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *VideoLogDao) Columns() VideoLogColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *VideoLogDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *VideoLogDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *VideoLogDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
