// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// SysMapDao is the data access object for table sys_map.
type SysMapDao struct {
	table   string        // table is the underlying table name of the DAO.
	group   string        // group is the database configuration group name of current DAO.
	columns SysMapColumns // columns contains all the column names of Table for convenient usage.
}

// SysMapColumns defines and stores column names for table sys_map.
type SysMapColumns struct {
	Id        string // 系统字典ID
	Name      string // 字典名，例如：sys_user_sex
	Explain   string // 字典名解析值，例如：用户性别
	IsDisable string // 是否禁用：0正常，1禁用
	UpdateAt  string // 更新时间
	CreateAt  string // 创建时间
}

// sysMapColumns holds the columns for table sys_map.
var sysMapColumns = SysMapColumns{
	Id:        "id",
	Name:      "name",
	Explain:   "explain",
	IsDisable: "is_disable",
	UpdateAt:  "update_at",
	CreateAt:  "create_at",
}

// NewSysMapDao creates and returns a new DAO object for table data access.
func NewSysMapDao() *SysMapDao {
	return &SysMapDao{
		group:   "default",
		table:   "sys_map",
		columns: sysMapColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *SysMapDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *SysMapDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *SysMapDao) Columns() SysMapColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *SysMapDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *SysMapDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *SysMapDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
