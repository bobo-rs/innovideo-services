// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// ActorsDao is the data access object for table actors.
type ActorsDao struct {
	table   string        // table is the underlying table name of the DAO.
	group   string        // group is the database configuration group name of current DAO.
	columns ActorsColumns // columns contains all the column names of Table for convenient usage.
}

// ActorsColumns defines and stores column names for table actors.
type ActorsColumns struct {
	Id            string // 演员ID
	Name          string // 姓名
	StageName     string // 艺名
	FormerName    string // 曾用名
	Country       string // 国籍
	NativePlace   string // 祖籍
	PlaceBirth    string // 出生地
	BloodType     string // 血型
	Constellation string // 星座
	Zodiac        string // 生肖
	Birthday      string // 出生日期
	Sex           string // 性别：0男，1女
	Height        string // 身高（CM）
	Weight        string // 体重（KG）
	DebutAt       string // 出道时间
	Synopsis      string // 简介
	ImageUrl      string // 头像
	WorksNum      string // 作品数
	RwNum         string // 代表作品数
	Nation        string // 民族
	GradInst      string // 毕业院校
	GradDate      string // 毕业时间
	UpdateAt      string // 更新时间
	CreateAt      string // 创建时间
}

// actorsColumns holds the columns for table actors.
var actorsColumns = ActorsColumns{
	Id:            "id",
	Name:          "name",
	StageName:     "stage_name",
	FormerName:    "former_name",
	Country:       "country",
	NativePlace:   "native_place",
	PlaceBirth:    "place_birth",
	BloodType:     "blood_type",
	Constellation: "constellation",
	Zodiac:        "zodiac",
	Birthday:      "birthday",
	Sex:           "sex",
	Height:        "height",
	Weight:        "weight",
	DebutAt:       "debut_at",
	Synopsis:      "synopsis",
	ImageUrl:      "image_url",
	WorksNum:      "works_num",
	RwNum:         "rw_num",
	Nation:        "nation",
	GradInst:      "grad_inst",
	GradDate:      "grad_date",
	UpdateAt:      "update_at",
	CreateAt:      "create_at",
}

// NewActorsDao creates and returns a new DAO object for table data access.
func NewActorsDao() *ActorsDao {
	return &ActorsDao{
		group:   "default",
		table:   "actors",
		columns: actorsColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *ActorsDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *ActorsDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *ActorsDao) Columns() ActorsColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *ActorsDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *ActorsDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *ActorsDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
