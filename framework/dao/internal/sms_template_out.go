// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// SmsTemplateOutDao is the data access object for table sms_template_out.
type SmsTemplateOutDao struct {
	table   string                // table is the underlying table name of the DAO.
	group   string                // group is the database configuration group name of current DAO.
	columns SmsTemplateOutColumns // columns contains all the column names of Table for convenient usage.
}

// SmsTemplateOutColumns defines and stores column names for table sms_template_out.
type SmsTemplateOutColumns struct {
	Id            string // 短信模板关联外部平台ID
	VarAlias      string // 系统模板变量
	TemplateId    string // 外部平台ID
	FromType      string // 平台类型：tencent腾讯，alipay阿里云[is_config,短信通道]
	OutTmplStatus string // 外部模板状态
	UpdateAt      string // 更新时间
	CreateAt      string // 创建时间
}

// smsTemplateOutColumns holds the columns for table sms_template_out.
var smsTemplateOutColumns = SmsTemplateOutColumns{
	Id:            "id",
	VarAlias:      "var_alias",
	TemplateId:    "template_id",
	FromType:      "from_type",
	OutTmplStatus: "out_tmpl_status",
	UpdateAt:      "update_at",
	CreateAt:      "create_at",
}

// NewSmsTemplateOutDao creates and returns a new DAO object for table data access.
func NewSmsTemplateOutDao() *SmsTemplateOutDao {
	return &SmsTemplateOutDao{
		group:   "default",
		table:   "sms_template_out",
		columns: smsTemplateOutColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *SmsTemplateOutDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *SmsTemplateOutDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *SmsTemplateOutDao) Columns() SmsTemplateOutColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *SmsTemplateOutDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *SmsTemplateOutDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *SmsTemplateOutDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
