// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// VideoBuyEpisodesDao is the data access object for table video_buy_episodes.
type VideoBuyEpisodesDao struct {
	table   string                  // table is the underlying table name of the DAO.
	group   string                  // group is the database configuration group name of current DAO.
	columns VideoBuyEpisodesColumns // columns contains all the column names of Table for convenient usage.
}

// VideoBuyEpisodesColumns defines and stores column names for table video_buy_episodes.
type VideoBuyEpisodesColumns struct {
	Id           string // 视频购买单集ID
	BillId       string // 视频购买账单ID
	BuyId        string // 视频购买ID
	Uid          string // 用户ID
	VideoId      string // 视频ID
	EpisodesId   string // 单集ID
	EpisodesName string // 单集名称
	EpisodesNum  string // 续集集数
	CreateAt     string // 创建时间
}

// videoBuyEpisodesColumns holds the columns for table video_buy_episodes.
var videoBuyEpisodesColumns = VideoBuyEpisodesColumns{
	Id:           "id",
	BillId:       "bill_id",
	BuyId:        "buy_id",
	Uid:          "uid",
	VideoId:      "video_id",
	EpisodesId:   "episodes_id",
	EpisodesName: "episodes_name",
	EpisodesNum:  "episodes_num",
	CreateAt:     "create_at",
}

// NewVideoBuyEpisodesDao creates and returns a new DAO object for table data access.
func NewVideoBuyEpisodesDao() *VideoBuyEpisodesDao {
	return &VideoBuyEpisodesDao{
		group:   "default",
		table:   "video_buy_episodes",
		columns: videoBuyEpisodesColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *VideoBuyEpisodesDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *VideoBuyEpisodesDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *VideoBuyEpisodesDao) Columns() VideoBuyEpisodesColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *VideoBuyEpisodesDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *VideoBuyEpisodesDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *VideoBuyEpisodesDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
