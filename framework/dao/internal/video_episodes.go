// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// VideoEpisodesDao is the data access object for table video_episodes.
type VideoEpisodesDao struct {
	table   string               // table is the underlying table name of the DAO.
	group   string               // group is the database configuration group name of current DAO.
	columns VideoEpisodesColumns // columns contains all the column names of Table for convenient usage.
}

// VideoEpisodesColumns defines and stores column names for table video_episodes.
type VideoEpisodesColumns struct {
	Id            string // 视频集数ID
	VideoId       string // 视频ID
	AttachId      string // 附件ID
	ImageUrl      string // 当前集数海报
	EpisodesName  string // 续集标题
	Synopsis      string // 简介
	EpisodesNum   string // 集数
	ResolutionSet string // 清晰度集合：480P，720P，1080P等
	PlayNum       string // 播放次数
	UpdateAt      string // 更新时间
	CreateAt      string // 创建时间
}

// videoEpisodesColumns holds the columns for table video_episodes.
var videoEpisodesColumns = VideoEpisodesColumns{
	Id:            "id",
	VideoId:       "video_id",
	AttachId:      "attach_id",
	ImageUrl:      "image_url",
	EpisodesName:  "episodes_name",
	Synopsis:      "synopsis",
	EpisodesNum:   "episodes_num",
	ResolutionSet: "resolution_set",
	PlayNum:       "play_num",
	UpdateAt:      "update_at",
	CreateAt:      "create_at",
}

// NewVideoEpisodesDao creates and returns a new DAO object for table data access.
func NewVideoEpisodesDao() *VideoEpisodesDao {
	return &VideoEpisodesDao{
		group:   "default",
		table:   "video_episodes",
		columns: videoEpisodesColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *VideoEpisodesDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *VideoEpisodesDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *VideoEpisodesDao) Columns() VideoEpisodesColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *VideoEpisodesDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *VideoEpisodesDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *VideoEpisodesDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
