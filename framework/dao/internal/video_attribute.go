// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// VideoAttributeDao is the data access object for table video_attribute.
type VideoAttributeDao struct {
	table   string                // table is the underlying table name of the DAO.
	group   string                // group is the database configuration group name of current DAO.
	columns VideoAttributeColumns // columns contains all the column names of Table for convenient usage.
}

// VideoAttributeColumns defines and stores column names for table video_attribute.
type VideoAttributeColumns struct {
	VideoId           string // 视频ID
	Synopsis          string // 简介概要
	EpisodesNum       string // 总集数
	UpStatus          string // 视频更新状态：0更新中，1已完结
	Description       string // 视频宣传详情
	ShareTitle        string // 分享视频名
	ShareContent      string // 分享视频内容
	IsSeries          string // 是否连续剧：0单剧，1连续剧
	FreePlayNum       string // VIP视频免费播放集数
	TrialDuration     string // 试看时长
	Lang              string // 语言类型：中英双文
	CompletePanguCoin string // 全集购买盘古币
	SinglePanguCoin   string // 单集购买币
	AttachId          string // 附件ID
	UpdateAt          string // 更新时间
}

// videoAttributeColumns holds the columns for table video_attribute.
var videoAttributeColumns = VideoAttributeColumns{
	VideoId:           "video_id",
	Synopsis:          "synopsis",
	EpisodesNum:       "episodes_num",
	UpStatus:          "up_status",
	Description:       "description",
	ShareTitle:        "share_title",
	ShareContent:      "share_content",
	IsSeries:          "is_series",
	FreePlayNum:       "free_play_num",
	TrialDuration:     "trial_duration",
	Lang:              "lang",
	CompletePanguCoin: "complete_pangu_coin",
	SinglePanguCoin:   "single_pangu_coin",
	AttachId:          "attach_id",
	UpdateAt:          "update_at",
}

// NewVideoAttributeDao creates and returns a new DAO object for table data access.
func NewVideoAttributeDao() *VideoAttributeDao {
	return &VideoAttributeDao{
		group:   "default",
		table:   "video_attribute",
		columns: videoAttributeColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *VideoAttributeDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *VideoAttributeDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *VideoAttributeDao) Columns() VideoAttributeColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *VideoAttributeDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *VideoAttributeDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *VideoAttributeDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
