// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// MemberTiersDao is the data access object for table member_tiers.
type MemberTiersDao struct {
	table   string             // table is the underlying table name of the DAO.
	group   string             // group is the database configuration group name of current DAO.
	columns MemberTiersColumns // columns contains all the column names of Table for convenient usage.
}

// MemberTiersColumns defines and stores column names for table member_tiers.
type MemberTiersColumns struct {
	Id          string // 会员身份ID
	Name        string // 会员身份名
	Description string // 会员服务描述
	Tiers       string // 会员等级：0普通会员，1初级会员，2中级会员，3高级会员，4级顶级会员，5级超级会员，6至臻级会员
	MarketPrice string // 原价
	Price       string // 销售价
	ServiceDate string // 服务时间期限
	ServiceUnit string // 服务时间单位：d天，m月,y年
	Status      string // 会员状态：0启用，1禁用
	SaleNum     string // 销量
	ImageUrl    string // 会员推广图
	BgColor     string // 背景色
	Creator     string // 创建人名
	CreatorId   string // 创建人ID
	UpdateAt    string // 更新时间
	CreateAt    string // 创建时间
}

// memberTiersColumns holds the columns for table member_tiers.
var memberTiersColumns = MemberTiersColumns{
	Id:          "id",
	Name:        "name",
	Description: "description",
	Tiers:       "tiers",
	MarketPrice: "market_price",
	Price:       "price",
	ServiceDate: "service_date",
	ServiceUnit: "service_unit",
	Status:      "status",
	SaleNum:     "sale_num",
	ImageUrl:    "image_url",
	BgColor:     "bg_color",
	Creator:     "creator",
	CreatorId:   "creator_id",
	UpdateAt:    "update_at",
	CreateAt:    "create_at",
}

// NewMemberTiersDao creates and returns a new DAO object for table data access.
func NewMemberTiersDao() *MemberTiersDao {
	return &MemberTiersDao{
		group:   "default",
		table:   "member_tiers",
		columns: memberTiersColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *MemberTiersDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *MemberTiersDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *MemberTiersDao) Columns() MemberTiersColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *MemberTiersDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *MemberTiersDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *MemberTiersDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
