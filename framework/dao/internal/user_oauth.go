// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// UserOauthDao is the data access object for table user_oauth.
type UserOauthDao struct {
	table   string           // table is the underlying table name of the DAO.
	group   string           // group is the database configuration group name of current DAO.
	columns UserOauthColumns // columns contains all the column names of Table for convenient usage.
}

// UserOauthColumns defines and stores column names for table user_oauth.
type UserOauthColumns struct {
	Id       string // 用户授权ID
	Uid      string // 用户ID
	Openid   string // 授权openid
	Unionid  string // 微信unionid
	Oauth    string // 授权渠道，如：weixin
	CreateAt string // 创建时间
}

// userOauthColumns holds the columns for table user_oauth.
var userOauthColumns = UserOauthColumns{
	Id:       "id",
	Uid:      "uid",
	Openid:   "openid",
	Unionid:  "unionid",
	Oauth:    "oauth",
	CreateAt: "create_at",
}

// NewUserOauthDao creates and returns a new DAO object for table data access.
func NewUserOauthDao() *UserOauthDao {
	return &UserOauthDao{
		group:   "default",
		table:   "user_oauth",
		columns: userOauthColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *UserOauthDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *UserOauthDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *UserOauthDao) Columns() UserOauthColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *UserOauthDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *UserOauthDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *UserOauthDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
