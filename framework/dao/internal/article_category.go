// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// ArticleCategoryDao is the data access object for table article_category.
type ArticleCategoryDao struct {
	table   string                 // table is the underlying table name of the DAO.
	group   string                 // group is the database configuration group name of current DAO.
	columns ArticleCategoryColumns // columns contains all the column names of Table for convenient usage.
}

// ArticleCategoryColumns defines and stores column names for table article_category.
type ArticleCategoryColumns struct {
	Id          string // 文章类目ID
	Name        string // 文章类目名
	Description string // 详情
	Pid         string // 父级ID
	Status      string // 状态：0正常，1禁用
	IsShow      string // 是否显示：0显示，1隐藏
	UpdateAt    string // 更新时间
	CreateAt    string // 创建时间
}

// articleCategoryColumns holds the columns for table article_category.
var articleCategoryColumns = ArticleCategoryColumns{
	Id:          "id",
	Name:        "name",
	Description: "description",
	Pid:         "pid",
	Status:      "status",
	IsShow:      "is_show",
	UpdateAt:    "update_at",
	CreateAt:    "create_at",
}

// NewArticleCategoryDao creates and returns a new DAO object for table data access.
func NewArticleCategoryDao() *ArticleCategoryDao {
	return &ArticleCategoryDao{
		group:   "default",
		table:   "article_category",
		columns: articleCategoryColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *ArticleCategoryDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *ArticleCategoryDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *ArticleCategoryDao) Columns() ArticleCategoryColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *ArticleCategoryDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *ArticleCategoryDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *ArticleCategoryDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
