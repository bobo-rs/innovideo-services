// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// ArticleDao is the data access object for table article.
type ArticleDao struct {
	table   string         // table is the underlying table name of the DAO.
	group   string         // group is the database configuration group name of current DAO.
	columns ArticleColumns // columns contains all the column names of Table for convenient usage.
}

// ArticleColumns defines and stores column names for table article.
type ArticleColumns struct {
	Id          string // 文章ID
	Name        string // 文章名
	AbbrevName  string // 文章缩写名
	Description string // 文章摘要
	Cid         string // 文章分类ID
	Keywords    string // 关键词
	ImgUrl      string // 图片地址
	VideoUrl    string // 视频地址
	Content     string // 文章内容
	Status      string // 状态：0正常，1禁用
	IsShow      string // 是否显示：0正常，1隐藏
	Sort        string // 排序值：0-99999升序
	AssocAlias  string // 关联别名，例如：注册协议、网服协议等
	UpdateAt    string // 更新时间
	CreateAt    string // 创建时间
}

// articleColumns holds the columns for table article.
var articleColumns = ArticleColumns{
	Id:          "id",
	Name:        "name",
	AbbrevName:  "abbrev_name",
	Description: "description",
	Cid:         "cid",
	Keywords:    "keywords",
	ImgUrl:      "img_url",
	VideoUrl:    "video_url",
	Content:     "content",
	Status:      "status",
	IsShow:      "is_show",
	Sort:        "sort",
	AssocAlias:  "assoc_alias",
	UpdateAt:    "update_at",
	CreateAt:    "create_at",
}

// NewArticleDao creates and returns a new DAO object for table data access.
func NewArticleDao() *ArticleDao {
	return &ArticleDao{
		group:   "default",
		table:   "article",
		columns: articleColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *ArticleDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *ArticleDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *ArticleDao) Columns() ArticleColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *ArticleDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *ArticleDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *ArticleDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
