// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT. Created at 2024-08-19 14:37:15
// ==========================================================================

package internal

import (
	"context"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/frame/g"
)

// AdminInviteDao is the data access object for table admin_invite.
type AdminInviteDao struct {
	table   string             // table is the underlying table name of the DAO.
	group   string             // group is the database configuration group name of current DAO.
	columns AdminInviteColumns // columns contains all the column names of Table for convenient usage.
}

// AdminInviteColumns defines and stores column names for table admin_invite.
type AdminInviteColumns struct {
	Id            string // 邀请ID
	Token         string // 邀请Token
	AdminId       string // 邀请管理员ID
	ManageName    string // 邀请管理员名
	Expired       string // 过期时间
	Status        string // 状态：0邀请中，1已邀请，2已撤销
	Uid           string // 被邀请人用户ID
	RefreshNum    string // 刷新次数
	DepartId      string // 部门ID
	IsSuperManage string // 是否超管：0普管，1超管
	UpdateAt      string // 更新时间
	CreateAt      string // 创建时间
}

// adminInviteColumns holds the columns for table admin_invite.
var adminInviteColumns = AdminInviteColumns{
	Id:            "id",
	Token:         "token",
	AdminId:       "admin_id",
	ManageName:    "manage_name",
	Expired:       "expired",
	Status:        "status",
	Uid:           "uid",
	RefreshNum:    "refresh_num",
	DepartId:      "depart_id",
	IsSuperManage: "is_super_manage",
	UpdateAt:      "update_at",
	CreateAt:      "create_at",
}

// NewAdminInviteDao creates and returns a new DAO object for table data access.
func NewAdminInviteDao() *AdminInviteDao {
	return &AdminInviteDao{
		group:   "default",
		table:   "admin_invite",
		columns: adminInviteColumns,
	}
}

// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *AdminInviteDao) DB() gdb.DB {
	return g.DB(dao.group)
}

// Table returns the table name of current dao.
func (dao *AdminInviteDao) Table() string {
	return dao.table
}

// Columns returns all column names of current dao.
func (dao *AdminInviteDao) Columns() AdminInviteColumns {
	return dao.columns
}

// Group returns the configuration group name of database of current dao.
func (dao *AdminInviteDao) Group() string {
	return dao.group
}

// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *AdminInviteDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.table).Safe().Ctx(ctx)
}

// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *AdminInviteDao) Transaction(ctx context.Context, f func(ctx context.Context, tx gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}
