// =================================================================================
// This is auto-generated by GoFrame CLI tool only once. Fill this file as you wish.
// =================================================================================

package dao

import (
	"gitee.com/bobo-rs/innovideo-services/framework/dao/internal"
)

// internalMemberBenefitsDao is internal type for wrapping internal DAO implements.
type internalMemberBenefitsDao = *internal.MemberBenefitsDao

// memberBenefitsDao is the data access object for table member_benefits.
// You can define custom methods on it to extend its functionality as you wish.
type memberBenefitsDao struct {
	internalMemberBenefitsDao
}

var (
	// MemberBenefits is globally public accessible object for table member_benefits operations.
	MemberBenefits = memberBenefitsDao{
		internal.NewMemberBenefitsDao(),
	}
)

// Fill with you ideas below.
