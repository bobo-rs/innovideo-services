// =================================================================================
// This is auto-generated by GoFrame CLI tool only once. Fill this file as you wish.
// =================================================================================

package dao

import (
	"gitee.com/bobo-rs/innovideo-services/framework/dao/internal"
)

// internalPanguCoinBillItemDao is internal type for wrapping internal DAO implements.
type internalPanguCoinBillItemDao = *internal.PanguCoinBillItemDao

// panguCoinBillItemDao is the data access object for table pangu_coin_bill_item.
// You can define custom methods on it to extend its functionality as you wish.
type panguCoinBillItemDao struct {
	internalPanguCoinBillItemDao
}

var (
	// PanguCoinBillItem is globally public accessible object for table pangu_coin_bill_item operations.
	PanguCoinBillItem = panguCoinBillItemDao{
		internal.NewPanguCoinBillItemDao(),
	}
)

// Fill with you ideas below.
