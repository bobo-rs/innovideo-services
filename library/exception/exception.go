package exception

import (
	"fmt"
	"gitee.com/bobo-rs/creative-framework/pkg/bizcode"
	"gitee.com/bobo-rs/innovideo-services/enums"
	"github.com/gogf/gf/v2/errors/gcode"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/frame/g"
)

/*
HTTP状态码是用于表示Web服务器响应HTTP请求状态的数字代码。以下是您所要求的300-310、400-405和500-505之间常用状态码及其描述的详细列举：
300-310状态码
300 Multiple Choices（多种选择）：当服务器端有多个可供选择的资源，并且无法确定客户端所需资源的确切位置时，服务器可以返回此状态码。响应中会包含一个包含可选资源列表的实体和对应的URI地址。
301 Moved Permanently（永久重定向）：表示请求的网页已永久移动到新位置。客户端在接收到301响应后，应该使用新的URL发起后续的请求。
302 Found（临时重定向）：表示请求的资源暂时驻留在不同的URI下。除非特别指定了缓存头部指示，该状态码不可缓存。
303 See Other（查看其他位置）：请求者应当对不同的位置使用单独的GET请求来检索响应时，服务器返回此代码。
304 Not Modified（未修改）：自上次请求后，请求的网页未修改过。服务器返回此响应时，不会返回网页内容。
305 Use Proxy（使用代理）：请求者只能使用代理访问请求的网页。
307 Temporary Redirect（临时重定向）：与302类似，但要求客户端使用与原始请求相同的方法（如POST）对新的URL进行请求。
400-405状态码
400 Bad Request（错误请求）：由于语法错误或无法理解其内容，服务器无法或不会处理该请求。
401 Unauthorized（未授权）：请求需要进行身份验证。服务器通常会在响应头中包含一个WWW-Authenticate字段，以指明客户端应如何进行身份验证。
402 Payment Required（需要付款）：此状态码很少使用，且已被废弃。
403 Forbidden（禁止）：服务器理解请求客户端的请求，但是拒绝执行此请求。
404 Not Found（未找到）：服务器无法找到请求的资源。
405 Method Not Allowed（不允许的方法）：服务器知道请求实体的方法，但该方法不被该资源支持。
500-505状态码
500 Internal Server Error（内部服务器错误）：服务器遇到了一个未曾预料到的情况，导致其无法完成对请求的处理。
501 Not Implemented（尚未实施）：服务器不支持当前请求所需要的某个功能。
502 Bad Gateway（错误网关）：作为网关或代理工作的服务器尝试执行请求时，从上游服务器接收到无效的响应。
503 Service Unavailable（服务不可用）：由于临时的服务器维护或者过载，服务器当前无法处理请求。
504 Gateway Timeout（网关超时）：作为网关或代理工作的服务器尝试执行请求时，没有从上游服务器（URI标识出的服务器，例如HTTP、FTP、LDAP）或者辅助服务器（例如DNS）收到及时的响应。
505 HTTP Version Not Supported（不支持的HTTP版本）：服务器不支持请求中所用的HTTP协议版本。
以上列出的状态码及其描述涵盖了HTTP协议中大部分常用的状态码，但请注意，HTTP协议的状态码并不止这些，还有一些其他的状态码，如重定向状态码中的308 Permanent Redirect（永久重定向）等。
*/
var (
	CodeNil                     = bizcode.New(200, `OK`, ``)
	CodeMultipleChoices         = bizcode.New(300, `Multiple Choices`, `请求资源提供多种选择`)
	CodeMovedPermanently        = bizcode.New(301, `Moved Permanently`, `永久重定向`)
	CodeFound                   = bizcode.New(302, `Found`, `临时重定向`)
	CodeSeeOther                = bizcode.New(303, `See Other`, `查看其他位置`)
	CodeNotModified             = bizcode.New(304, `Not Modified`, `请求的网页未修改过`)
	CodeUseProxy                = bizcode.New(305, `Use Proxy`, `使用代理`)
	CodeTemporaryRedirect       = bizcode.New(307, `Temporary Redirect`, `临时重定向，客户端使用与原始请求相同的方法（如POST）对新的URL进行请求`)
	CodeBadRequest              = bizcode.New(400, `Bad Request`, `错误请求`)
	CodeUnauthorized            = bizcode.New(401, `Unauthorized`, `未授权，需要进行身份验证`)
	CodePaymentRequired         = bizcode.New(402, `Payment Required`, `需要付款`)
	CodeForbidden               = bizcode.New(403, `Forbidden`, `禁止访问,拒绝执行此请求`)
	CodeNotFund                 = bizcode.New(404, `Not Found`, `服务器无法找到请求的资源`)
	CodeMethodNotAllowed        = bizcode.New(405, `Method Not Allowed`, `不允许的方法，不被该资源支持`)
	CodeInternalServerError     = bizcode.New(500, `Internal Server Error`, `内部服务器错误`)
	CodeNotImplemented          = bizcode.New(501, `Not Implemented`, `服务器不支持当前请求所需要的某个功能`)
	CodeBadGateway              = bizcode.New(502, `Bad Gateway`, `错误网关,网关或代理工作的服务器尝试执行请求时,从上游服务器接收到无效的响应`)
	CodeServiceUnavailable      = bizcode.New(503, `Service Unavailable`, `服务不可用，临时的服务器维护或者过载，服务器当前无法处理请求`)
	CodeGatewayTimeout          = bizcode.New(504, `Gateway Timeout`, `网关超时`)
	CodeHTTPVersionNotSupported = bizcode.New(505, `HTTP Version Not Supported`, `不支持的HTTP版本`)
)

// NewCode 自定义错误状态码错误信息
func NewCode(code enums.ErrorCode, message string, data ...interface{}) error {
	var detail interface{}
	if len(data) > 0 {
		detail = data[0]
	} else {
		detail = g.Map{}
	}
	// 通用描述
	if message == "" {
		message = code.Fmt()
	}
	return gerror.NewCode(gcode.New(int(code), message, detail))
}

// NewCodef 自定义错误状态码-格式化
func NewCodef(code enums.ErrorCode, message string, format ...interface{}) error {
	return NewCode(code, fmt.Sprintf(message, format...))
}

// New 统一状态码错误处理
func New(message string, data ...interface{}) error {
	return NewCode(enums.ErrorUnify, message, data...)
}

// Newf 统一状态码错误处理-格式化
func Newf(message string, format ...interface{}) error {
	return New(fmt.Sprintf(message, format...))
}
