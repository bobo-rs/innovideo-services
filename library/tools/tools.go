package tools

import (
	"fmt"
	"gitee.com/bobo-rs/creative-framework/pkg/algorithm/uniquecode"
	"gitee.com/bobo-rs/creative-framework/pkg/utils"
	"gitee.com/bobo-rs/innovideo-services/consts"
	"github.com/gogf/gf/v2/util/grand"
	"regexp"
	"time"
)

// GenAccount 生成账户名
func GenAccount(ps ...string) string {
	prefix := consts.AccountPrefix
	if len(ps) > 0 {
		prefix = ps[0]
	}
	return fmt.Sprintf(`%s%s-%d`, prefix, grand.S(5), utils.RandInt(4))
}

// FmtSearchDate 格式化搜索时间
func FmtSearchDate(searchAt string) []string {
	if len(searchAt) == 0 {
		return nil
	}
	dateArr := regexp.MustCompile(`[(,、，|)]+`).Split(searchAt, -1)
	if len(dateArr) <= 1 {
		return nil
	}
	// 检测时间格式
	if !IsValidDateTime(dateArr[0]) || !IsValidDateTime(dateArr[1]) {
		return nil
	}
	return dateArr
}

// ExtractValidCodes 验证并过滤有效编号
func ExtractValidCodes(no string) []string {
	if len(no) == 0 {
		return nil
	}
	var (
		parts = regexp.MustCompile(`[(,，|、\t\r\n\s)]+`).Split(no, -1)
		codes []string
	)

	// 迭代过滤
	for _, part := range parts {
		// 验证是否有效编号
		if uniquecode.CheckUniqueCode(part, true) {
			codes = append(codes, part)
		}
	}
	return codes
}

// IsValidDateTime 检测是否时间
// Example:
// IsValidDateTime(`2006-01-01`) true
// IsValidDateTime(`2006-01-01 12:00:00`) true
// IsValidDateTime(`2006/01/01 12:00:00`) true
// IsValidDateTime(`2006.01.01 12:00:00`) true
func IsValidDateTime(value string) bool {
	if value == `` {
		return false
	}

	// 把其他时间格式替换成标准-
	value = regexp.MustCompile(`[(/.)]+`).ReplaceAllString(value, `-`)

	// 设置时间格式
	var layout = time.DateTime
	switch len(value) {
	case 8:
		layout = `20060102`
	case 10:
		// 2006-01-02
		layout = time.DateOnly
	case 17:
		layout = `20060102 15:04:05`
	case 19: // 考虑到时区的长度可能是19（如2006-01-02T15:04:05Z）
		layout = "2006-01-02T15:04:05Z"
	}

	// 解析是否时间
	if _, err := time.Parse(layout, value); err != nil {
		fmt.Println(err)
		return false
	}
	return true
}
