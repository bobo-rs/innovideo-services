package jwt

import (
	"context"
	"errors"
	"fmt"
	"gitee.com/bobo-rs/creative-framework/pkg/jwt"
	"gitee.com/bobo-rs/creative-framework/pkg/sredis"
	"gitee.com/bobo-rs/innovideo-services/library/tools"
	"time"
)

// createHook 创建Token数据
func createHook(ctx context.Context, item jwt.AuthTokenItem) error {
	var (
		cache    = sredis.NewCache()
		cacheKey = tools.TokenCacheKey(item.Token)
	)
	// 解析过期时间2023-01-01 00:00:00
	expire, err := time.Parse(time.DateTime, item.Expire)
	if err != nil {
		return fmt.Errorf(`解析Token效期时间失败%w`, err)
	}

	// 当前时间
	now := time.Now()

	// 缓存Token
	_, err = cache.GetOrSetFunc(ctx, cacheKey, func(ctx context.Context) (value interface{}, err error) {
		return item, nil
	}, expire.Sub(now))
	return err
}

// readHook 读取Token数据
func readHook(ctx context.Context, token string) (*jwt.AuthTokenItem, error) {
	item := &jwt.AuthTokenItem{}
	// 获取缓存
	r, err := sredis.NewCache().Get(ctx, tools.TokenCacheKey(token))
	if err != nil {
		return nil, err
	}
	// 转换为AuthTokenItem
	if err = r.Scan(&item); err != nil {
		return nil, fmt.Errorf(`读取Token转换AuthTokenItem失败%s`, err.Error())
	}

	// 是否已经注销
	if len(item.Token) == 0 {
		return nil, errors.New(`The token has not exists or it logout?`)
	}

	return item, nil
}

// removeHook 移除Token-退出登录或刷新token
func removeHook(ctx context.Context, token string) error {
	// 移除缓存
	_, err := sredis.NewCache().Remove(ctx, tools.TokenCacheKey(token))
	return err
}

// verifyHook 验证Token是否有效
func verifyHook(ctx context.Context, token string) error {
	item, err := readHook(ctx, token)
	if err != nil {
		return err
	}

	// 过期时间
	expire, err := time.Parse(time.DateTime, item.Expire)
	if err != nil {
		return err
	}

	// 当前时间
	now := time.Now().Unix()
	// 是否过期
	if now > expire.Unix() {
		_ = removeHook(ctx, token) // 移除
		return errors.New(`Token Expired`)
	}
	return nil
}
