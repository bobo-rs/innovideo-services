package user

import (
	"context"
	"gitee.com/bobo-rs/innovideo-services/enums"
	"gitee.com/bobo-rs/innovideo-services/library/exception"
)

// UpdatePwdValidate 修改密码-校验验证码是否验证通过
func (u *sUser) UpdatePwdValidate(ctx context.Context, mobile string) error {
	b, err := u.GetUpdatePwdCaptcha(ctx, mobile)
	if err != nil {
		return exception.NewCode(enums.ErrorUpdatePwdNotCaptcha, `修改密码-未校验验证码或已失效`)
	}
	// 是否有效
	if b == false {
		return exception.NewCode(enums.ErrorUpdatePwdNotCaptcha, `修改密码-验证码校验未通过`)
	}
	return nil
}
