package video

import (
	"gitee.com/bobo-rs/creative-framework/pkg/encrypt"
	"gitee.com/bobo-rs/innovideo-services/library/services/safety"
	"strconv"
)

type sVideo struct {
}

func New() *sVideo {
	return &sVideo{}
}

// GenSecret 通过视频编号加视频ID生成秘钥
func (v *sVideo) GenSecret(no string, videoId uint) (string, string) {
	shuffle := safety.New().Shuffle()
	return encrypt.NewM().MustEncryptString(no + strconv.Itoa(int(videoId)) + shuffle), shuffle
}
