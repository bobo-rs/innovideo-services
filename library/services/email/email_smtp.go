package email

import (
	"context"
	"gitee.com/bobo-rs/creative-framework/pkg/mail"
	"gitee.com/bobo-rs/innovideo-services/framework/service"
	"gitee.com/bobo-rs/innovideo-services/library/models"
)

// MailService 邮件发送服务
func (e *sEmail) MailService(ctx context.Context) (*mail.Services, error) {
	config, err := e.Config(ctx)
	if err != nil {
		return nil, err
	}
	return mail.New(config.Host, config.Username, config.Password, config.Port), nil
}

// Config EmailSMTP配置
func (e *sEmail) Config(ctx context.Context) (config *models.MailConfig, err error) {
	r, err := service.Config().GetGroupVar(ctx, `mail_smtp`)
	if err != nil {
		return nil, err
	}
	rawConfig := r.MapStrVar()
	config = &models.MailConfig{
		Id:                           rawConfig[`mail_smtp_id`].String(),
		Host:                         rawConfig[`mail_smtp_host`].String(),
		Port:                         rawConfig[`mail_smtp_port`].Int(),
		Username:                     rawConfig[`mail_smtp_username`].String(),
		Password:                     rawConfig[`mail_smtp_password`].String(),
		CaptchaExpire:                rawConfig[`mail_captcha_expire`].Int(),
		CaptchaSwitchIpHourThreshold: rawConfig[`mail_captcha_ip_hour_threshold`].Int(),
		CaptchaSwitchTodayThreshold:  rawConfig[`mail_captcha_today_threshold`].Int(),
		CaptchaSwitchTodayTotal:      rawConfig[`mail_captcha_today_total`].Int(),
	}
	return
}
