package models

type (
	// PayNotifyResponse 支付回调结果
	PayNotifyResponse struct {
		PayNo       string
		TradeNo     string
		PayCode     string
		Description string
		Scene       string // 支付场景：盘古币订单，商品订单，余额充值
	}

	// ConfirmPayResponse 确认支付结果
	ConfirmPayResponse struct {
		PayNo   string
		TradeNo string
		PayCode string
		Scene   string // 支付场景：盘古币订单，商品订单，余额充值
	}
)
