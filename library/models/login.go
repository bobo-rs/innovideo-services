package models

import "gitee.com/bobo-rs/innovideo-services/enums"

type (
	// LoginValidateBeforeItem 登录验证属性前置验证
	LoginValidateBeforeItem struct {
		Account  string          `json:"account" dc:"账户"`
		Pwd      string          `json:"pwd" dc:"密码-加密"`
		Mobile   string          `json:"mobile" dc:"手机号-加密"`
		Email    string          `json:"email" dc:"邮件-加密"`
		Sign     string          `json:"sign" dc:"签名"`
		IsRemove bool            `json:"is_remove" dc:"是否删除解密Sign缓存：false否，true删除"`
		Oauth    enums.UserOauth `json:"oauth" dc:"授权类型：账户密码登录，手机号登录"`
	}

	// LoginParseParamItem 登录解析参数属性
	LoginParseParamItem struct {
		*SignItem
		Pwd     string `json:"pwd" dc:"密码-解密后"`
		Mobile  string `json:"mobile" dc:"解密后"`
		Email   string `json:"email" dc:"解密邮箱"`
		Account string `json:"account" dc:"账户或手机号"`
	}

	// LoginValidateAfterItem 后置验证-登录验证属性
	LoginValidateAfterItem struct {
		Status enums.UserStatus `json:"status" dc:"用户状态"`
	}
)
