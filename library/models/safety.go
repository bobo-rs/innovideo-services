package models

type (
	// SafeSignItem 安全签名属性
	SafeSignItem struct {
		Secret string `json:"secret" dc:"密钥-适用于用户端入参敏感数据加密，例如手机号、身份证等"`
		Sign   string `json:"sign" dc:"签名-用于避免数据篡改参数值"`
	}

	// SignItem 安全密钥签名值
	SignItem struct {
		Nonce     string // 随机数
		Timestamp int64  // 时间秒戳
		Secret    string // 签名密钥
	}

	// AuthSignItem 授权签名
	AuthSignItem struct {
		Sign string `json:"sign" dc:"签名-用于避免数据篡改参数值"`
	}
)
