package models

type (
	// MailConfig 邮箱SMTP配置
	MailConfig struct {
		Host                         string
		Port                         int
		Username                     string
		Password                     string
		Id                           string
		CaptchaExpire                int // 验证码效期时间
		CaptchaSwitchIpHourThreshold int // Ip1小时内发送量
		CaptchaSwitchTodayThreshold  int // 同邮箱当日发送数量阈值
		CaptchaSwitchTodayTotal      int // 当日验证码发送总量
	}
)
