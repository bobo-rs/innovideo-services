package example

import (
	"fmt"
	"gitee.com/bobo-rs/innovideo-services/library/tools"
	"testing"
	"time"
)

func TestFmtSearchDate(t *testing.T) {
	fmt.Println(tools.FmtSearchDate(`2012-01-01,2022-01-01`))
	fmt.Println(tools.FmtSearchDate(`2012-01-01，2022-01-01`))
	fmt.Println(tools.FmtSearchDate(`2012-01-01|2022-01-01`))
	fmt.Println(tools.FmtSearchDate(`2012/01/01，2022/01/01`))
	fmt.Println(tools.FmtSearchDate(`2012-01-01 12:00:00，2022-01-01 12:00:00`))
	fmt.Println(tools.FmtSearchDate(`2012.01.01 12:00:00，2022/01/01 12:00:00`))
	fmt.Println(tools.FmtSearchDate(`20120101，20220101 12:00:00`))
	fmt.Println(time.Now().AddDate(0, -1, 0).Format(time.DateOnly))
}

func TestExtractValidCodes(t *testing.T) {
	fmt.Println(tools.ExtractValidCodes(`P12313213123233139999,3123123123123213,123123123123123`))
	fmt.Println(tools.ExtractValidCodes(`P12313213123213\t3123123123123213\r123123123123123`))
	fmt.Println(tools.ExtractValidCodes(`P12313213123213 3123123123123213|123123123123123、123123123123123`))
	fmt.Println(tools.ExtractValidCodes(`P20120331120000100006 P20120331120000100006|P20120331120000100006、PGC20120331120000100006`))
}
