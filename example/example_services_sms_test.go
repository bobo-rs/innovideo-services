package example

import (
	"fmt"
	"gitee.com/bobo-rs/creative-framework/pkg/sms/models"
	"gitee.com/bobo-rs/innovideo-services/library/services/sms"
	_ "github.com/gogf/gf/contrib/drivers/mysql/v2"
	"testing"
)

func TestSMS_SendCode(t *testing.T) {
	service, err := sms.New().Service()
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(service.Send([]string{`13456789012`}, models.SendSmsInput{
		TemplateId:     `123123`,
		TemplateParams: []string{`345566`},
	}))
	sms.New().AfterCaptchaSendAsync(Ctx, `13456789012`)
}
