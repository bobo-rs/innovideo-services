package example

import (
	"encoding/json"
	"fmt"
	"gitee.com/bobo-rs/innovideo-services/enums"
	_ "gitee.com/bobo-rs/innovideo-services/framework/logic"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/framework/service"
	"gitee.com/bobo-rs/innovideo-services/library/services/safety"
	"testing"
)

func TestAccountLogin(t *testing.T) {

	safe, err := service.Common().GetSign(Ctx, enums.SafeTagsLogin)
	if err != nil {
		fmt.Println(err)
		return
	}
	param := model.UserOauthAccountLoginItem{
		Account: `is_6Ky9k-8466`,
		Pwd:     safety.New().AESHandler(safe.Secret).MustEncryptString([]byte(`a123456.`)),
	}
	fmt.Println(service.User().Login(Ctx, model.UserOauthLoginInput{
		UserOauthAccountLoginItem: param,
		Oauth:                     enums.UserOauthAccount,
		UserOauthLoginSignItem: model.UserOauthLoginSignItem{
			Sign: safe.Sign,
		},
	}))
}

func TestMobileLogin_Captcha(t *testing.T) {
	t.Run(`验证码登录`, func(t *testing.T) {
		safe, err := service.Common().GetSign(Ctx, enums.SafeTagsLogin)
		if err != nil {
			fmt.Println(err)
			return
		}
		mobile := safety.New().AESHandler(safe.Secret).MustEncryptString([]byte(`13456789012`))
		param := model.UserOauthSmsLoginItem{
			UserOauthSendSmsCaptchaItem: model.UserOauthSendSmsCaptchaItem{
				Mobile: mobile,
			},
			OauthCaptchaCode: model.OauthCaptchaCode{
				Code: `775594`,
			},
		}
		fmt.Println(service.User().Login(Ctx, model.UserOauthLoginInput{
			UserOauthSendSmsCaptchaItem: param.UserOauthSendSmsCaptchaItem,
			OauthCaptchaCode:            param.OauthCaptchaCode,
			Oauth:                       enums.UserOauthSmsCaptcha,
			UserOauthLoginSignItem: model.UserOauthLoginSignItem{
				Sign: safe.Sign,
			},
		}))
	})
}

func TestSendMobile_Captcha(t *testing.T) {

	safe, err := service.Common().GetSign(Ctx, enums.SafeTagsSmsCaptcha)
	if err != nil {
		fmt.Println(err)
		return
	}
	mobile := safety.New().AESHandler(safe.Secret).MustEncryptString([]byte(`13456789012`))
	fmt.Println(service.User().UserLoginSendSmsCaptcha(Ctx, model.UserLoginSendSmsCaptchaInput{
		UserOauthSendSmsCaptchaItem: model.UserOauthSendSmsCaptchaItem{
			Mobile: mobile,
		},
		UserOauthLoginSignItem: model.UserOauthLoginSignItem{
			Sign: safe.Sign,
		},
	}))
	fmt.Println(mobile)
}

func TestGetSign_Login(t *testing.T) {
	safe, err := service.Common().GetSign(Ctx, enums.SafeTagsLogin)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(safe)
	// 加密
	fmt.Println(safety.New().AESHandler(safe.Secret).MustEncryptString([]byte(`a1234567`)))
	fmt.Println(safety.New().AESHandler(`66064e6a5151e88f8c2763beb7c508bc`).DecryptString(`00000000000000000000000000000000ac1081009ae6f189d4be054a90443c56`))
}

func TestCurrUserDeviceList(t *testing.T) {
	r, err := service.User().CurrUserLoginDeviceList(Ctx)
	if err != nil {
		fmt.Println(err)
		return
	}
	marshal, err := json.Marshal(r.Rows)
	if err != nil {
		return
	}
	fmt.Println(string(marshal))
}

func TestEmailLogin_SendCaptcha(t *testing.T) {
	item, _ := service.Common().GetSign(Ctx, enums.SafeTagsEmailCaptcha)
	fmt.Println(item)
	// 加密示例
	email, _ := service.Common().EncryptExampleValue(Ctx, `xxx@xxx.com`, item.Secret)
	fmt.Println(service.User().OauthSendEmailCaptcha(Ctx, model.LoginSendEmailCaptchaInput{
		OauthEmail: model.OauthEmail{
			Email: email,
		},
		UserOauthLoginSignItem: model.UserOauthLoginSignItem{
			Sign: item.Sign,
		},
	}))
}

func TestEmailLogin_CaptchaLogin(t *testing.T) {
	item, _ := service.Common().GetSign(Ctx, enums.SafeTagsLogin)
	fmt.Println(item)
	email, _ := service.Common().EncryptExampleValue(Ctx, `xxx@xxx.com`, item.Secret)
	fmt.Println(service.User().Login(Ctx, model.UserOauthLoginInput{
		Oauth: enums.UserOauthEmailCaptcha,
		OauthCaptchaCode: model.OauthCaptchaCode{
			Code: `345164`,
		},
		OauthEmail: model.OauthEmail{
			Email: email,
		},
		UserOauthLoginSignItem: model.UserOauthLoginSignItem{
			Sign: item.Sign,
		},
	}))
}
