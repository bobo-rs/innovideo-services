package example

import (
	"fmt"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/framework/model/entity"
	"gitee.com/bobo-rs/innovideo-services/framework/service"
	"testing"
)

func TestSaveEmailTemplate(t *testing.T) {
	t.Run(`保存邮件模板`, func(t *testing.T) {
		fmt.Println(service.Email().SaveTemplate(Ctx, entity.EmailTemplate{
			Name:    `登录验证码`,
			VarName: `login-captcha`,
			Subject: `创想空间登录验证码`,
			Content: "尊敬的用户您好：\r\n" +
				"   您正在登录系统，验证码：{1}，请不要外泄，谢谢",
		}))
	})
}

func TestGetEmailTemplateList(t *testing.T) {
	t.Run(`获取邮件列表`, func(t *testing.T) {
		fmt.Println(service.Email().GetTemplateList(Ctx, model.EmailTemplateListInput{
			CommonPaginationItem: model.CommonPaginationItem{
				Page: 1,
				Size: 20,
			},
		}))
	})
}

func TestGetTemplateByVarName(t *testing.T) {
	t.Run(`通过变量名获取邮件详情`, func(t *testing.T) {
		fmt.Println(service.Email().GetTemplateByVarName(Ctx, `login-captcha`))
	})
}

func TestSendEmailCaptcha(t *testing.T) {
	t.Run(`发送邮件验证码`, func(t *testing.T) {
		fmt.Println(service.Email().SendCaptcha(Ctx, `zhouyp@91160.com`, `login-captcha`))
	})
}
