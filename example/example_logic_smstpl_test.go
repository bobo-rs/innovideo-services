package example

import (
	"fmt"
	"gitee.com/bobo-rs/innovideo-services/enums"
	_ "gitee.com/bobo-rs/innovideo-services/framework/logic"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/framework/model/entity"
	"gitee.com/bobo-rs/innovideo-services/framework/service"
	"testing"
)

func TestSaveTemplate(t *testing.T) {
	tmpl := entity.SmsTemplate{
		Name:     `注册验证码`,
		Content:  `您的验证码：{1}，请勿泄露给他人，回复R退订`,
		VarAlias: string(enums.SmsAliasRegisterCaptcha),
		SmsType:  uint(enums.SmsTypeCaptcha),
		Status:   0,
		Remark:   `短信验证码`,
	}
	detail, err := service.SmsTpl().GetTemplateDetail(Ctx, 2)
	if err != nil {
		fmt.Println(err)
		//return
	}
	if detail != nil {
		// 编辑资料
		tmpl.Id = detail.Id
		fmt.Println(service.SmsTpl().SaveTemplate(Ctx, tmpl))
		return
	}
	fmt.Println(service.SmsTpl().SaveTemplate(Ctx, tmpl))
}

func TestGetTemplateList(t *testing.T) {
	fmt.Println(service.SmsTpl().GetTemplateList(Ctx, model.SmsTemplateListInput{
		CommonPaginationItem: model.CommonPaginationItem{
			Page: 1,
			Size: 10,
		},
		SmsTemplateWhereItem: model.SmsTemplateWhereItem{
			SmsType: 0,
			Status:  -1,
			Name:    `注册验证码`,
			Alias:   string(enums.SmsAliasRegisterCaptcha),
		},
	}))
}

func TestDeleteTemplate(t *testing.T) {
	fmt.Println(service.SmsTpl().DeleteTemplate(Ctx, 1))
}

func TestCurrSyncOutFromTemplateStatus(t *testing.T) {
	fmt.Println(service.SmsTpl().CurrSyncOutFromTemplateStatus(Ctx))
}

func TestCurrApplyOutFromTemplate(t *testing.T) {
	fmt.Println(service.SmsTpl().CurrApplyOutFromTemplate(Ctx, 2))
}

func TestGetTemplateOutList(t *testing.T) {
	fmt.Println(service.SmsTpl().GetTemplateOutList(Ctx, model.SmsTemplateOutListInput{
		Alias:    string(enums.SmsAliasRegisterCaptcha),
		FromType: ``,
	}))
}

func TestSyncOutFromTemplateStatus(t *testing.T) {
	fmt.Println(service.SmsTpl().SyncOutFromTemplateStatus(Ctx, `sms_tencent`))
}

func TestSendCaptcha(t *testing.T) {
	fmt.Println(`发送短信验证码`)
	fmt.Println(service.SmsTpl().SendCaptcha(Ctx, `13456789012`, enums.SmsAliasRegisterCaptcha))
}

func TestValidateCode(t *testing.T) {
	fmt.Println(`验证短信验证码`)
	fmt.Println(service.SmsTpl().ValidateCode(Ctx, `13456789012`, `456789`))
}

func TestSendSmsMessage(t *testing.T) {
	fmt.Println(`发送消息通知`)
	fmt.Println(service.SmsTpl().SendSmsMessage(Ctx, model.SendSmsTemplateItem{
		TemplateParams: []string{`123`, `234`, `345`},
		VarAlias:       enums.SmsAliasRegisterCaptcha,
	}, `13456789012`))
}

func TestSendSmsMarket(t *testing.T) {
	fmt.Println(`发送营销短信`)
	fmt.Println(service.SmsTpl().SendSmsMarket(Ctx, model.SendSmsTemplateItem{
		TemplateParams: []string{`123`, `234`, `345`},
		VarAlias:       enums.SmsAliasRegisterCaptcha,
	}, `13456789012`))
}
