package example

import (
	"gitee.com/bobo-rs/innovideo-services/framework/service"
	"testing"
)

func TestConfig_RemoveCache(t *testing.T) {
	service.Config().RemoveConfigCache(Ctx, `sms_driver`)
}
