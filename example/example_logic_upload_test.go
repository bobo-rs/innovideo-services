package example

import (
	"bytes"
	"fmt"
	"gitee.com/bobo-rs/creative-framework/pkg/encrypt"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/framework/service"
	"gitee.com/bobo-rs/innovideo-services/library/services/block"
	"io"
	"os"
	"path/filepath"
	"testing"
)

func TestReadBlocks(t *testing.T) {
	fileBlock := block.New()
	// 读取块
	filename := `原始生活21天-情侣篇.mp4`
	filePath := `I:\视频处理\录屏目录\` + filename

	blocks, err := fileBlock.ReadBlocks(filePath, 30)
	//blocks, err := tools.FileBlockChunk(`I:\视频处理\录屏目录\单挑荒野-巴塔哥尼亚\单挑荒野-巴塔哥尼亚_001 (1).mp4`, 1)
	fmt.Println(err)
	total := len(blocks) - 1
	for key, b := range blocks {
		input := model.UploadBlockInput{
			Block: b,
			UploadBlockFile: model.UploadBlockFile{
				Filename: filename,
				MimeType: `video/mp4`,
			},
			Index:    key + 1,
			FileHash: encrypt.NewM().MustEncrypt(filename),
			IsLast:   total == key,
		}
		out, err := service.Upload().UploadBlockToFileSystem(Ctx, input)
		if err != nil {
			fmt.Println(err)
			break
		}
		fmt.Println(`完成`, out)
	}
}

func TestMergeUploadBlock(t *testing.T) {
	t.Run(``, func(t *testing.T) {
		//filename := `原始生活21天-情侣篇.mp4`
		fmt.Println(encrypt.NewM().MustEncryptString(`原始生活21天-海上漂流 (1) (22).mp4`))
		// 合并文件
		//fmt.Println(service.Upload().MergeUploadedBlocks(Ctx, encrypt.NewM().MustEncrypt(filename)))
	})
}

func TestCutFileBlock(t *testing.T) {
	// 读取块
	filename := `单挑荒野-巴塔哥尼亚_001 (1).mp4`
	filePath := `I:\视频处理\录屏目录\单挑荒野-巴塔哥尼亚\` + filename

	blocks, err := block.New().ToBase64(filePath, 10)
	fmt.Println(encrypt.NewM().MustEncryptString(filename))

	fmt.Println(err)
	for key, b := range blocks {
		file, err := os.Create(filepath.Join(`/uploads/tmp/`, fmt.Sprintf(`%d.txt`, key)))
		if err != nil {
			fmt.Println(err)
			break
		}
		defer func() {
			_ = file.Close()
		}()
		if _, err = io.Copy(file, bytes.NewBufferString(b)); err != nil {
			fmt.Println(`写入文件失败`, err)
			break
		}
		_ = file.Close()
	}
}

func TestGetUploadBlockSpace(t *testing.T) {
	t.Run(`获取上传空间文件块`, func(t *testing.T) {
		fmt.Println(service.Upload().GetUploadBlockSpace(Ctx))
	})
}
