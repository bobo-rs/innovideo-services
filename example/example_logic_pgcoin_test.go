package example

import (
	"fmt"
	"gitee.com/bobo-rs/creative-framework/pkg/utils"
	"gitee.com/bobo-rs/innovideo-services/enums"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/framework/service"
	"gitee.com/bobo-rs/innovideo-services/library/services/pay"
	"github.com/gogf/gf/v2/encoding/gjson"
	"sync"
	"testing"
)

var (
	Pagination = model.CommonPaginationItem{
		Page: 1,
		Size: 10,
	}
)

func TestPGCConfig(t *testing.T) {
	config, _ := service.PgCoin().Config(Ctx)
	fmt.Println(gjson.MustEncodeString(config))
}

func TestGetTransactionList(t *testing.T) {
	fmt.Println(service.PgCoin().GetTransactionList(Ctx, model.PGCoinTransactionListInput{
		CommonPaginationItem:   Pagination,
		PGCoinTransactionWhere: model.PGCoinTransactionWhere{
			//TransactionNo: `P201901311200001000010,P201901311200001000020`,
		},
	}))
}

func TestGetPGCoinOrderList(t *testing.T) {
	fmt.Println(service.PgCoin().GetOrderList(Ctx, model.PGCoinOrderListInput{
		CommonPaginationItem: Pagination,
		PGCoinOrderWhere: model.PGCoinOrderWhere{
			CommonSearchDateItem: model.CommonSearchDateItem{
				//SearchAt:    `2024-08-01,2024-08-31`,
				IsDefaultAt: true,
			},
			Status:    enums.PGCoinOrderStatus(-1),
			PayStatus: enums.PGCoinOrderPay(-1),
			PayNo:     `P20190131120000100001,P20190131120000100002`,
			OrderNo:   `P20190131120000100001,P20190131120000100002`,
			TradeNo:   `P20190131120000100001,P20190131120000100002`,
		},
	}))
}

func TestGiveUserPGCoin(t *testing.T) {
	// 平台赠送
	fmt.Println(service.PgCoin().GiveUserPGCoin(Ctx, 0, 10000, enums.PGCoinBillTypeFormGive))
}

func TestGenPGCoinConsumeBillTX(t *testing.T) {
	// 消费盘古币
	fmt.Println(service.PgCoin().GenPGCoinConsumeBillTX(Ctx, model.PGCoinConsumeInput{
		ConsumePGCoin: 180000,
		Uid:           1,
	}))
}

func TestGetBillList(t *testing.T) {
	fmt.Println(service.PgCoin().GetBillList(Ctx, model.PGCoinBillListInput{
		CommonPaginationItem: Pagination,
		PGCoinBillWhere: model.PGCoinBillWhere{
			Status: enums.PGCoinBillStatus(-1),
			Type:   enums.PGCoinBillType(-1),
		},
	}))
}

func TestGetBillDetail(t *testing.T) {
	fmt.Println(service.PgCoin().GetBillDetail(Ctx, 1))
}

func TestCorrectionUserAvailablePGCoin(t *testing.T) {
	fmt.Println(service.PgCoin().CorrectionUserAvailablePGCoin(Ctx, 1))
}

func TestGetOrderDetail(t *testing.T) {
	t.Run(`获取订单详情`, func(t *testing.T) {
		fmt.Println(service.PgCoin().GetOrderDetail(Ctx, 1, enums.FormSideAdm))
	})
}

func TestCreateRechargePGCoinTryLock(t *testing.T) {
	t.Run(`生成盘古币账单数据`, func(t *testing.T) {
		wg := sync.WaitGroup{}
		for i := 0; i < 10; i++ {
			wg.Add(1)
			go func(i int) {
				fmt.Println(service.PgCoin().CreateRechargePGCoinTryLock(Ctx, model.PGCoinRechargeInput{
					RechargePGC: uint(utils.RandInt(6)),
				}))
				fmt.Println(fmt.Sprintf(`第%d页`, i))
				wg.Done()
			}(i)
		}
		wg.Wait()
	})
}

func TestPay_PayPalPayment(t *testing.T) {
	other := map[string]interface{}{
		`currency`:  `USD`,
		`content`:   `盘古币充值1000`,
		`returnUrl`: `http://www.160dyf.com?pay_code=paypal`,
		`cancelUrl`: `http://www.160dyf.com/admin`,
	}
	t.Run(`PayPal支付`, func(t *testing.T) {
		fmt.Println(
			pay.New().Register(Ctx, `paypal`).Payment(
				Ctx,
				`BC20240823161944329734`,
				10.00,
				other,
			))
	})
}

func TestPay_ConfirmPayment(t *testing.T) {
	t.Run(`PayPal支付回调确认支付`, func(t *testing.T) {
		fmt.Println(
			pay.New().Register(Ctx, `paypal`).ConfirmPayment(Ctx, `54729121J3958625D`),
		)
	})
}

func TestPay_Payment(t *testing.T) {
	t.Run(`盘古币支付`, func(t *testing.T) {
		fmt.Println(service.Pay().Payment(Ctx, model.PaymentInput{
			PaymentItem: model.PaymentItem{
				PayType: `paypal`,
				No:      `BC20240815153402501116`,
				Event:   `coins`,
			},
		}))
	})
}

func TestPgCoin_ConfirmPayment1(t *testing.T) {
	t.Run(`盘古币确认支付`, func(t *testing.T) {
		fmt.Println(service.PgCoin().ConfirmCoinsPayment(Ctx, model.ConfirmCoinsPaymentInput{
			PayNo:   `5VM116415J595221D`,
			OrderNo: `BC20240815153402501116`,
			PayType: `paypal`,
		}))
	})
}
