package example

import (
	"fmt"
	"gitee.com/bobo-rs/innovideo-services/enums"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/framework/model/entity"
	"gitee.com/bobo-rs/innovideo-services/framework/service"
	"gitee.com/bobo-rs/innovideo-services/library/services/safety"
	"testing"
)

func TestGetUserActionVideoList(t *testing.T) {
	fmt.Println(service.Video().GetUserActionVideoList(
		Ctx, enums.VideoUserActionTypeCollect, 1, 20,
	))
}

func TestGetPlayHistoryList(t *testing.T) {
	fmt.Println(service.VideoPlay().GetPlayHistoryList(Ctx, model.CommonPaginationItem{
		Page: 1,
		Size: 20,
	}))
}

func TestGetCategoryTreeList(t *testing.T) {
	fmt.Println(service.VideoCate().GetCategoryTreeList(Ctx))
}

func TestGetCategoryLabelList(t *testing.T) {
	fmt.Println(service.VideoCate().GetCategoryLabelList(Ctx, 0))
}

func TestSaveVideoEpisodes(t *testing.T) {
	fmt.Println(service.Video().SaveVideoEpisodes(Ctx, model.VideoEpisodesSaveItem{
		VideoEpisodes: entity.VideoEpisodes{
			VideoId:      1,
			EpisodesName: `第一集`,
			EpisodesNum:  1,
			Synopsis:     `第一集`,
			AttachId:     `919b793ad5e173284b8ff48c713e0b31`,
		},
		ResolutionList: []*model.VideoResolutionSaveItem{
			{VideoResolutionBasicItem: model.VideoResolutionBasicItem{
				Resolution: `1080P`,
				AttachId:   `919b793ad5e173284b8ff48c713e0b31`,
			}},
		},
	}))
}

func TestGetEpisodesList(t *testing.T) {
	fmt.Println(service.Video().GetEpisodesList(Ctx, model.VideoEpisodesListInput{
		VideoId:  1,
		FormSide: enums.FormSideAdm,
	}))
}

func TestGetBuyEpisodesList(t *testing.T) {
	fmt.Println(service.VideoBuy().GetBuyEpisodesList(Ctx, model.VideoBuyEpisodesWhereItem{
		//BuyId:  1,
		BillId: []uint{1, 2, 3},
	}))
}

func TestGetVideoAuthDetailByNo(t *testing.T) {
	fmt.Println(service.Video().GetVideoAuthDetailByNo(Ctx, `V20240730173057800825`))
}

func TestGetVideoPlayUrl(t *testing.T) {
	fmt.Println(service.Video().GetVideoPlayUrl(Ctx, model.VideoPlayUrlInput{
		VideoNo:    `V20240730173057800825`,
		EpisodesId: 6,
	}))
}

func TestDecryptVideoUrl(t *testing.T) {
	buff, err := safety.New().
		AESHandler(`8437b899347c3e2a98c4c752f692ed8f`).
		DecryptString(`49a4f8022a3a467a7d3df3be60feb83509cbebf25951713fa50c4281d8ed8112fa0c9d2dc0fe2a9c0d3626a7d6785ef9c0450004cd381488563e21eb8c8d1945f3fba16d5ee83470d7c0eb3de851e99f`)
	fmt.Println(string(buff), err)
}

func TestGetVideoPlayEpisodes(t *testing.T) {
	t.Run(`运行获取视频剧集列表`, func(t *testing.T) {
		fmt.Println(service.Video().GetVideoPlayEpisodes(Ctx, 1))
	})
}

func TestGetVideoDetail(t *testing.T) {
	fmt.Println(service.Video().GetVideoDetail(Ctx, model.VideoDetailInput{
		VideoId: 1,
	}))
}

func TestGetCategoryRecommendList(t *testing.T) {
	t.Run(`获取顶级栏目`, func(t *testing.T) {
		fmt.Println(service.VideoCate().GetCategoryRecommendList(Ctx))
		fmt.Println(service.VideoCate().GetCategoryLabelList(Ctx, 0))
	})
}
