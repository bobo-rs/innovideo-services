package example

import (
	"fmt"
	"gitee.com/bobo-rs/innovideo-services/enums"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/framework/model/entity"
	"gitee.com/bobo-rs/innovideo-services/framework/service"
	"testing"
)

func TestSavePermissions(t *testing.T) {
	fmt.Println(service.Rbac().SavePermissions(Ctx, entity.Permissions{
		Name: `系统管理`,
		Icon: `fa-home`,
	}))
	fmt.Println(service.Rbac().SavePermissions(Ctx, entity.Permissions{
		Name:     `系统设置`,
		Path:     `/admin/system/setting`,
		Pid:      1,
		MenuType: 1,
	}))
	fmt.Println(service.Rbac().SavePermissions(Ctx, entity.Permissions{
		Name:     `基础设置`,
		Path:     `/admin/system/basic/setting`,
		Pid:      1,
		MenuType: 1,
	}))
	fmt.Println(service.Rbac().SavePermissions(Ctx, entity.Permissions{
		Name:     `保存`,
		ApiPath:  `/admapi/config/save`,
		Pid:      2,
		MenuType: 2,
		PermCode: `save`,
	}))
	fmt.Println(service.Rbac().SavePermissions(Ctx, entity.Permissions{
		Name:     `保存`,
		ApiPath:  `/admapi/config/save`,
		Pid:      3,
		MenuType: 2,
		PermCode: `save`,
	}))
}

func TestRbac_GetPermTreeList(t *testing.T) {
	fmt.Println(service.Rbac().GetPermTreeList(Ctx))
}

func TestRbac_GetPermDetail(t *testing.T) {
	fmt.Println(service.Rbac().GetPermDetail(Ctx, 1))
}

func TestRbac_PermCustomUpdateColumns(t *testing.T) {
	fmt.Println(service.Rbac().PermCustomUpdateColumns(Ctx, model.PermCustomUpdateColumnInput{
		PermId:  4,
		Columns: enums.PermUpdateColumnsShow,
		Value:   1, // 隐藏
	}))
	fmt.Println(service.Rbac().PermCustomUpdateColumns(Ctx, model.PermCustomUpdateColumnInput{
		PermId:  4,
		Columns: enums.PermUpdateColumnsShow,
		Value:   0, // 显示
	}))
	fmt.Println(service.Rbac().PermCustomUpdateColumns(Ctx, model.PermCustomUpdateColumnInput{
		PermId:  4,
		Columns: enums.PermUpdateColumnsSort,
		Value:   225,
	}))
	fmt.Println(service.Rbac().PermissionsModel().UpdatePri(Ctx, 4, enums.PermUpdateColumnsSort.Fmt(), 255))
}
