package example

import (
	"fmt"
	"gitee.com/bobo-rs/innovideo-services/enums"
	"gitee.com/bobo-rs/innovideo-services/framework/model"
	"gitee.com/bobo-rs/innovideo-services/framework/service"
	"testing"
)

func TestCreateVideoPurchase(t *testing.T) {
	t.Run(`购买视频`, func(t *testing.T) {
		fmt.Println(service.VideoBuy().CreateVideoPurchase(Ctx, model.CreateVideoPurchaseInput{
			VideoId:       1,
			BuyType:       enums.VideoBuyTypeME,
			EpisodesIdSet: []uint{6},
		}))
	})
}

func TestRefundVideoBill(t *testing.T) {
	t.Run(`视频账单退款`, func(t *testing.T) {
		fmt.Println(service.VideoBuy().RefundVideoBill(Ctx, model.RefundVideoBillInput{
			BillId:        1,
			RefundCoins:   30,
			EpisodesIdSet: []uint{6},
		}))
	})
}
