package enums

type (
	VideoCategoryShow      int
	VideoCategoryRecommend int
)

const (
	VideoCategoryShowShow VideoCategoryShow = iota
	VideoCategoryShowHide
)

const (
	VideoCategoryRecommendNot VideoCategoryRecommend = iota
	VideoCategoryRecommendYes
)

func (s VideoCategoryShow) Is() bool {
	switch s {
	case VideoCategoryShowShow, VideoCategoryShowHide:
		return true
	default:
		return false
	}
}

func (r VideoCategoryRecommend) Is() bool {
	switch r {
	case VideoCategoryRecommendNot, VideoCategoryRecommendYes:
		return true
	default:
		return false
	}
}

func (s VideoCategoryShow) Fmt() string {
	switch s {
	case VideoCategoryShowShow:
		return `显示`
	case VideoCategoryShowHide:
		return `隐藏`
	default:
		return ``
	}
}

func (r VideoCategoryRecommend) Fmt() string {
	switch r {
	case VideoCategoryRecommendNot:
		return `否`
	case VideoCategoryRecommendYes:
		return `是`
	default:
		return ``
	}
}
