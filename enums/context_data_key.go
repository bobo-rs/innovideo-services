package enums

type ContextDataKey string

const (
	ContextDataBasicUser ContextDataKey = `contextUserKey` // 用户信息上下文KEY
	ContextDataAdminUser ContextDataKey = `adminUser`
	ContextDataUser      ContextDataKey = `user`
	ContextDataWitheUrl  ContextDataKey = `whiteUrl` // 白名单URL地址
)

func (k ContextDataKey) Fmt() string {
	switch k {
	case ContextDataBasicUser:
		return `用户基础信息上下文KEY`
	case ContextDataUser:
		return `用户详情`
	case ContextDataAdminUser:
		return `管理员详情`
	case ContextDataWitheUrl:
		return `授权可越权白名单`
	default:
		return ``
	}
}
