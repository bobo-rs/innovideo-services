package enums

type (
	PayEvent string
	PayType  string
)

const (
	PayEventCoins PayEvent = `coins`
	PayEventOrder PayEvent = `order`
)

const (
	PayTypePayPal PayType = `paypal`
)

func (e PayEvent) Is() bool {
	switch e {
	case PayEventCoins, PayEventOrder:
		return true
	default:
		return false
	}
}

func (t PayType) Is() bool {
	switch t {
	case PayTypePayPal:
		return true
	default:
		return false
	}
}

func (e PayEvent) Fmt() string {
	switch e {
	case PayEventCoins:
		return `充值盘古币`
	case PayEventOrder:
		return `订单支付`
	default:
		return ``
	}
}

func (t PayType) Fmt() string {
	switch t {
	case PayTypePayPal:
		return `PayPal支付`
	default:
		return ``
	}
}
