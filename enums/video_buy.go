package enums

type (
	VideoBuyStatus     int
	VideoBuyType       int
	VideoBuyBillStatus int
)

const (
	VideoBuyStatusNot VideoBuyStatus = iota
	VideoBuyStatusYes
)

const (
	VideoBuyTypeSE VideoBuyType = iota // 单集购买
	VideoBuyTypeFE                     // 全集购买
	VideoBuyTypeME                     // 余集买断
)

const (
	VideoBuyBillStatusNormal VideoBuyBillStatus = iota
	VideoBuyBillStatusRefund
	VideoBuyBillStatusPartRefund
)

func (s VideoBuyStatus) Is() bool {
	switch s {
	case VideoBuyStatusNot, VideoBuyStatusYes:
		return true
	default:
		return false
	}
}

func (t VideoBuyType) Is() bool {
	switch t {
	case VideoBuyTypeSE, VideoBuyTypeFE, VideoBuyTypeME:
		return true
	default:
		return false
	}
}

func (s VideoBuyBillStatus) Is() bool {
	switch s {
	case VideoBuyBillStatusNormal, VideoBuyBillStatusRefund, VideoBuyBillStatusPartRefund:
		return true
	default:
		return false
	}
}

func (s VideoBuyBillStatus) Fmt() string {
	switch s {
	case VideoBuyBillStatusNormal:
		return `正常`
	case VideoBuyBillStatusPartRefund:
		return `部分退款`
	case VideoBuyBillStatusRefund:
		return `全额退款`
	default:
		return ``
	}
}

func (t VideoBuyType) Fmt() string {
	switch t {
	case VideoBuyTypeSE:
		return `单集购买`
	case VideoBuyTypeFE:
		return `全集购买`
	case VideoBuyTypeME:
		return `余集买断`
	default:
		return ``
	}
}

func (s VideoBuyStatus) Fmt() string {
	switch s {
	case VideoBuyStatusNot:
		return `购买中`
	case VideoBuyStatusYes:
		return `已全集购买`
	default:
		return ``
	}
}
