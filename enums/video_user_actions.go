package enums

type (
	VideoUserActionType string
)

const (
	VideoUserActionTypeCollect VideoUserActionType = `C`
	VideoUserActionTypeLike    VideoUserActionType = `L`
	VideoUserActionTypeTread   VideoUserActionType = `T`
)

func (t VideoUserActionType) Is() bool {
	switch t {
	case VideoUserActionTypeCollect, VideoUserActionTypeLike, VideoUserActionTypeTread:
		return true
	default:
		return false
	}
}
