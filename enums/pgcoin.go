package enums

type (
	PGCoinOrderPay        int
	PGCoinOrderStatus     int
	PGCoinBillStatus      int
	PGCoinBillType        int
	PGCoinTransactionType int
	PGCoinUserTAB         string
)

const (
	PGCoinOrderPayPending PGCoinOrderPay = iota
	PGCoinOrderPayPaid
)

const (
	PGCoinOrderStatusPending PGCoinOrderStatus = iota
	PGCoinOrderStatusSuccess
	PGCoinOrderStatusCancel
	PGCoinOrderStatusRefund
)

const (
	PGCoinBillStatusNormal PGCoinBillStatus = iota
	PGCoinBillStatusComplete
	PGCoinBillStatusPartExpired
	PGCoinBillStatusExpired
	PGCoinBillStatusPartRefund
	PGCoinBillStatusRefund
)

const (
	PGCoinBillTypeRecharge PGCoinBillType = iota
	PGCoinBillTypeActGive
	PGCoinBillTypeFormGive
	PGCoinBillTypeNewUserGive
	PGCoinBillTypeSaleRefund
)

const (
	PGCoinTransactionTypeIncome PGCoinTransactionType = iota
	PGCoinTransactionTypeConsume
)

const (
	PGCoinUserTABAll     PGCoinUserTAB = `all`     // 全部
	PGCoinUserTABPending PGCoinUserTAB = `pending` // 待支付
	PGCoinUserTABOK      PGCoinUserTAB = `ok`      // 已完成
)

func (p PGCoinOrderPay) Is() bool {
	switch p {
	case PGCoinOrderPayPending, PGCoinOrderPayPaid:
		return true
	default:
		return false
	}
}

func (s PGCoinOrderStatus) Is() bool {
	switch s {
	case PGCoinOrderStatusPending,
		PGCoinOrderStatusSuccess,
		PGCoinOrderStatusCancel,
		PGCoinOrderStatusRefund:
		return true
	default:
		return false
	}
}

func (t PGCoinBillType) Is() bool {
	switch t {
	case PGCoinBillTypeRecharge, PGCoinBillTypeActGive, PGCoinBillTypeFormGive, PGCoinBillTypeNewUserGive, PGCoinBillTypeSaleRefund:
		return true
	default:
		return false
	}
}

func (s PGCoinBillStatus) Is() bool {
	switch s {
	case PGCoinBillStatusNormal,
		PGCoinBillStatusComplete,
		PGCoinBillStatusPartExpired,
		PGCoinBillStatusExpired,
		PGCoinBillStatusPartRefund,
		PGCoinBillStatusRefund:
		return true
	default:
		return false
	}
}

func (t PGCoinTransactionType) Is() bool {
	switch t {
	case PGCoinTransactionTypeIncome, PGCoinTransactionTypeConsume:
		return true
	default:
		return false
	}
}

func (t PGCoinTransactionType) Fmt() string {
	switch t {
	case PGCoinTransactionTypeIncome:
		return `收入`
	case PGCoinTransactionTypeConsume:
		return `支出`
	default:
		return ``
	}
}

func (s PGCoinBillStatus) Fmt() string {
	switch s {
	case PGCoinBillStatusNormal:
		return `正常`
	case PGCoinBillStatusComplete:
		return `交易完成`
	case PGCoinBillStatusPartRefund:
		return `部分退款`
	case PGCoinBillStatusRefund:
		return `全额退款`
	case PGCoinBillStatusPartExpired:
		return `部分过期`
	case PGCoinBillStatusExpired:
		return `全额过期`
	default:
		return ``
	}
}

func (p PGCoinOrderPay) Fmt() string {
	switch p {
	case PGCoinOrderPayPending:
		return `待支付`
	case PGCoinOrderPayPaid:
		return `已支付`
	default:
		return ``
	}
}

func (s PGCoinOrderStatus) Fmt() string {
	switch s {
	case PGCoinOrderStatusPending:
		return `待确认`
	case PGCoinOrderStatusSuccess:
		return `已完成`
	case PGCoinOrderStatusRefund:
		return `已退款`
	case PGCoinOrderStatusCancel:
		return `已取消`
	default:
		return ``
	}
}
