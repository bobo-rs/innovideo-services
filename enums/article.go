package enums

type (
	ArticleStatus     int
	ArticleShow       int
	ArtCategoryStatus int
	ArtCategoryShow   int
)

const (
	ArticleStatusOk ArticleStatus = iota
	ArticleStatusDisable
)

const (
	ArticleShowOk ArticleShow = iota
	ArticleShowHide
)

const (
	ArtCategoryStatusOk ArtCategoryStatus = iota
	ArtCategoryStatusDisable
)

const (
	ArtCategoryShowOk ArtCategoryShow = iota
	ArtCategoryShowHide
)

func (s ArticleStatus) Fmt() string {
	switch s {
	case ArticleStatusOk:
		return `正常`
	case ArticleStatusDisable:
		return `禁用`
	default:
		return ``
	}
}

func (s ArticleShow) Fmt() string {
	switch s {
	case ArticleShowOk:
		return `显示`
	case ArticleShowHide:
		return `隐藏`
	default:
		return ``
	}
}

func (s ArtCategoryShow) Fmt() string {
	switch s {
	case ArtCategoryShowHide:
		return `隐藏`
	case ArtCategoryShowOk:
		return `显示`
	default:
		return ``
	}
}

func (a ArtCategoryStatus) Fmt() string {
	switch a {
	case ArtCategoryStatusOk:
		return `正常`
	case ArtCategoryStatusDisable:
		return `禁用`
	default:
		return ``
	}
}
