package enums

type (
	MaterialTemplateStatusEnums         uint
	MaterialTemplateWordsWordsTypeEnums uint
)

const (
	MaterialTemplateStatusAudit MaterialTemplateStatusEnums = iota
	MaterialTemplateStatusOk
	MaterialTemplateStatusOff
)

const (
	MaterialTemplateWordsWordsTypeDefault MaterialTemplateWordsWordsTypeEnums = iota
	MaterialTemplateWordsWordsTypeNoSep
	MaterialTemplateWordsWordsTypeSep
)
