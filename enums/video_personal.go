package enums

type (
	VideoPersonalType  int
	VidePersonalIsShow int
)

const (
	VideoPersonalTypeDirector VideoPersonalType = iota
	VideoPersonalTypeSubDirector
	VideoPersonalMainStarring
	VideoPersonalStarring
	VideoPersonalGuest
	VideoPersonalOther
)

const (
	VideoPersonalShow VidePersonalIsShow = iota
	VideoPersonalHide
)

func (t VideoPersonalType) Is() bool {
	switch t {
	case VideoPersonalTypeDirector,
		VideoPersonalTypeSubDirector,
		VideoPersonalMainStarring,
		VideoPersonalStarring,
		VideoPersonalGuest,
		VideoPersonalOther:
		return true
	default:
		return false

	}
}

func (s VidePersonalIsShow) Is() bool {
	switch s {
	case VideoPersonalShow, VideoPersonalHide:
		return true
	default:
		return false
	}
}
