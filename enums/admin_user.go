package enums

type (
	AdminManageStatus  int
	AdminIsSuperManage int
)

const (
	AdminManageStatusOk AdminManageStatus = iota
	AdminManageStatusLock
	AdminManageStatusDepart
	AdminManageStatusOV
	AdminManageStatusLogoff
)

const (
	AdminIsSuperMangeOrdinary AdminIsSuperManage = iota
	AdminIsSuperManageSuper
)

func (s AdminManageStatus) Fmt() string {
	switch s {
	case AdminManageStatusOk:
		return `正常`
	case AdminManageStatusLock:
		return `锁定`
	case AdminManageStatusDepart:
		return `离职`
	case AdminManageStatusOV:
		return `违规`
	case AdminManageStatusLogoff:
		return `注销`
	default:
		return ``
	}
}

func (m AdminIsSuperManage) Fmt() string {
	switch m {
	case AdminIsSuperMangeOrdinary:
		return `普管`
	case AdminIsSuperManageSuper:
		return `超管`
	default:
		return ``
	}
}
