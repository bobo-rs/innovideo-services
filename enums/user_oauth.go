package enums

type (
	UserOauth          string
	UserLoginStatus    uint
	UserDeviceDisabled uint
	LoginOauthStatus   uint
	UserAdminStatus    uint
)

const (
	UserOauthAccount        UserOauth = `account`
	UserOauthSmsCaptcha     UserOauth = `sms-captcha`
	UserOauthWechatScanCode UserOauth = `wechat-scan-code`
	UserOauthAlipayScanCode UserOauth = `alipay-scan-code`
	UserOauthEmailCaptcha   UserOauth = `email-captcha`
)

const (
	UserLoginStatusOk UserLoginStatus = iota
	UserLoginStatusSP
	UserLoginStatusSM
	UserLoginStatusND
)

const (
	UserDeviceDisabledOk UserDeviceDisabled = iota
	UserDeviceDisabledOff
)

const (
	LoginOauthStatusOk LoginOauthStatus = iota
	LoginOauthStatusOL
	LoginOauthStatusTimeout
)

const (
	UserAdminStatusOk UserAdminStatus = iota
	UserAdminStatusLock
	UserAdminStatusLO
	UserAdminStatusVIO
	UserAdminStatusLG
)

func (o UserOauth) Fmt() string {
	switch o {
	case UserOauthAccount:
		return `账号密码`
	case UserOauthSmsCaptcha:
		return `短信验证码`
	case UserOauthWechatScanCode:
		return `支付宝扫码`
	case UserOauthAlipayScanCode:
		return `微信扫码`
	case UserOauthEmailCaptcha:
		return `邮箱验证码`
	default:
		return ``
	}
}

func (s UserLoginStatus) Fmt() string {
	switch s {
	case UserLoginStatusOk:
		return `ok` // 账户正常
	case UserLoginStatusSP:
		return `set-pwd` // 设置密码
	case UserLoginStatusSM:
		return `set-mobile` // 设置手机号
	case UserLoginStatusND:
		return `new-device` // 新设备
	default:
		return ``

	}
}

func (d UserDeviceDisabled) Fmt() string {
	switch d {
	case UserDeviceDisabledOk:
		return `正常`
	case UserDeviceDisabledOff:
		return `禁用`
	default:
		return ``
	}
}

func (s LoginOauthStatus) Fmt() string {
	switch s {
	case LoginOauthStatusOk:
		return `正常`
	case LoginOauthStatusOL:
		return `下线`
	case LoginOauthStatusTimeout:
		return `超时下线`
	default:
		return ``
	}
}

func (s UserAdminStatus) Fmt() string {
	switch s {
	case UserAdminStatusOk:
		return `正常`
	case UserAdminStatusLock:
		return `锁定`
	case UserAdminStatusVIO:
		return `违规`
	case UserAdminStatusLG:
		return `注销`
	case UserAdminStatusLO:
		return `离职`
	default:
		return ``
	}
}
