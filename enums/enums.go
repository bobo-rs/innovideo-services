package enums

type (
	// SexEnums 枚举性别类型
	SexEnums uint
	VipType  uint
	FormSide uint
)

const (
	SexMan SexEnums = iota
	SexWoman
	SexSecrecy
)

const (
	VipTypeFree VipType = iota
	VipTypeGuest
)

const (
	FormSideAdm FormSide = iota // 运营后台
	FormSideMer                 // 商户后台
	FormSideBus                 // 业务用户端
	FormSideCli                 // 终端
)

// Format 性别格式化
func (e SexEnums) Fmt() string {
	switch e {
	case SexMan:
		return `男`
	case SexWoman:
		return `女`
	default:
		return `保密`
	}
}

func (s FormSide) Is() bool {
	switch s {
	case FormSideAdm, FormSideMer, FormSideBus, FormSideCli:
		return true
	default:
		return false
	}
}

func (s FormSide) Fmt() string {
	switch s {
	case FormSideAdm:
		return `运营后台`
	case FormSideMer:
		return `商户后台`
	case FormSideBus:
		return `用户业务端`
	case FormSideCli:
		return `命令终端`
	default:
		return ``
	}
}
