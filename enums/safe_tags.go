package enums

type SafeTags string

const (
	SafeTagsLogin        SafeTags = `login`
	SafeTagsUpload       SafeTags = `upload`
	SafeTagsSmsCaptcha   SafeTags = `sms-captcha`
	SafeTagsForgotPwd    SafeTags = `forgot-pwd`
	SafeTagsUptPwd       SafeTags = `upt-pwd`
	SafeTagsEmailCaptcha SafeTags = `email-captcha`
)

func (t SafeTags) Fmt() string {
	switch t {
	case SafeTagsLogin:
		return `注册登录`
	case SafeTagsUpload:
		return `文件上传`
	case SafeTagsSmsCaptcha:
		return `短信验证码`
	case SafeTagsForgotPwd:
		return `找回密码`
	case SafeTagsUptPwd:
		return `修改密码`
	default:
		return ``
	}
}
