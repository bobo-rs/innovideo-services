package enums

type (
	VideoFeeType   int
	VideoBeat      int
	VideoHot       int
	VideoRecommend int
	VideoTop       int
	VideoNew       int
	VideoStatus    int
	VideoRelease   int
	VideoSeries    int
	VideoOperator  uint
)

const (
	VideoFeeTypeFee VideoFeeType = iota
	VideoFeeTypeVIP
)

const (
	VideoBeatNot VideoBeat = iota
	VideoBeatYes
)

const (
	VideoHotNot VideoHot = iota
	VideoHotYes
)

const (
	VideoRecommendNot VideoRecommend = iota
	VideoRecommendYes
)

const (
	VideoNewNot VideoNew = iota
	VideoNewYes
)

const (
	VideoStatusAudit VideoStatus = iota
	VideoStatusSuccess
	VideoStatusReject
	VideoStatusOff
	VideoStatusDel
)

const (
	VideoReleaseNot VideoRelease = iota
	VideoReleaseYes
)

const (
	VideoTopNot VideoTop = iota
	VideoTopYes
)

const (
	VideoSeriesSingle VideoSeries = iota
	VideoSeriesMany
)

const (
	VideoOperatorTop VideoOperator = iota
	VideoOperatorRecommend
	VideoOperatorHot
	VideoOperatorBeat
	VideoOperatorNew
	VideoOperatorRelease
)

func (n VideoNew) Is() bool {
	switch n {
	case VideoNewNot, VideoNewYes:
		return true
	default:
		return false
	}
}

func (r VideoRecommend) Is() bool {
	switch r {
	case VideoRecommendNot, VideoRecommendYes:
		return true
	default:
		return false
	}
}

func (r VideoRelease) Is() bool {
	switch r {
	case VideoReleaseNot, VideoReleaseYes:
		return true
	default:
		return false
	}
}

func (s VideoStatus) Is() bool {
	switch s {
	case VideoStatusAudit, VideoStatusReject, VideoStatusSuccess, VideoStatusOff, VideoStatusDel:
		return true
	default:
		return false
	}
}

func (b VideoBeat) Is() bool {
	switch b {
	case VideoBeatYes, VideoBeatNot:
		return true
	default:
		return false
	}
}

func (h VideoHot) Is() bool {
	switch h {
	case VideoHotNot, VideoHotYes:
		return true
	default:
		return false
	}
}

func (t VideoFeeType) Is() bool {
	switch t {
	case VideoFeeTypeFee, VideoFeeTypeVIP:
		return true
	default:
		return false
	}
}

func (t VideoTop) Is() bool {
	switch t {
	case VideoTopNot, VideoTopYes:
		return true
	default:
		return false
	}
}

func (s VideoSeries) Is() bool {
	switch s {
	case VideoSeriesMany, VideoSeriesSingle:
		return true
	default:
		return false
	}
}
