package consts

import "time"

const (
	AccountPrefix = `cs_`          // 账户前缀
	SortAsc       = `asc`          // 排序升序
	SortDesc      = `desc`         // 排序降序
	TimeDay       = time.Hour * 24 // 一天
)
