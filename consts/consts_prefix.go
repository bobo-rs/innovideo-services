package consts

const (
	VideoNoPrefix              = `V`  // 视频编号
	VideoBuyNoPrefix           = `B`  // 视频购买订单编号
	VideoPlayNoPrefix          = `P`  // 视频播放编号前缀
	PanGuCoinBillPrefix        = `BG` // 盘古币账单编号前缀
	PanGuCoinTransactionPrefix = `T`  // 盘古币流水编号前缀
	PanGuCoinOrderNoPrefix     = `BC` // 盘古币充值兑换订单编号前缀
)
