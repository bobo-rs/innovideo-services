package consts

import "time"

const (
	// CachePrefix 缓存前缀
	CachePrefix = `innovideo.`
)

// JWT TOKEN
const (
	JWTTokenHash             = `jwt.token.hash:`               // JSON Web Tokens 缓存KEY
	LoginAuthExpire          = time.Minute * 30                // 登录授权缓存登录信息效期时间
	LoginAuthToken           = `login.auth.user.token:`        // 登录授权缓存Token：login.auth.user.token:{uid}-{token}
	LoginAdminAuthToken      = `login.admin.auth.user.token:`  // 管理员登陆TOKEN：login.admin.auth.user.token:{token}
	LoginAdminAuthPermMenu   = `login.admin.auth.perm.menu:`   // 登录管理员权限菜单：login.admin.auth.perm.menu:{token}
	LoginAdminAuthPermAction = `login.admin.auth.perm.action:` // 登录管理员操作权限：login.admin.auth.perm.action:{token}
	LoginAdminAuthPermId     = `login.admin.auth.perm.id:`     // 登录管理员权限ID集合：login.admin.auth.perm.id:{token}
)

// 系统业务配置
const (
	SysConfigKey = `sys.config.group.name:` // 系统业务配置缓存 sys.config.group.name:{group}-{name}或配置组sys.config.group.name:{group}
)

// SMS短信服务
const (
	SmsTemplateVarAlias = `sms.template.var.alias:` // 短信模板变量内容缓存
)

// 安全规则
const (
	// SafeSecretPrefix 密钥
	SafeSecretPrefix           = `safe.secret.`           // 安全密钥前缀
	SafeSecretTagIp            = `tag.ip:`                // 安全密钥缓存KEY idea.space.safe.secret.tag.mac:{tag}-{mac}
	SafeSecret1MinuteLockTagIp = `1minute.lock.tag.ip:`   // 同一Mac1分钟内锁定 idea.space.safe.secret.1minute.lock.tag.mac:{tag}-{mac}
	SafeSecret1HourLockTagIp   = `1hour.lock.num.tag.ip:` // 同一Mac1小时内刷新数量 idea.space.safe.secret.1hour.lock.tag.mac:{tag}-{mac}
	SafeSecret1MinuteTag       = `1minute.lock.num.tag:`  // 同个tag一分钟刷新数量，锁定半小时 idea.space.safe.secret.1minute.refresh.num.lock.tag：{tag}
	SafeSecret24HourTag        = `24hour.lock.num.tag:`   // 同个tag24小时刷新数量，锁定截止当天0点 idea.space.safe.secret.24hour.lock.tag：{tag}

	// SafeSmsCaptchaPrefix 短信验证码规则
	SafeSmsCaptchaPrefix            = `safe.sms.captcha.`         // 缓存前缀【CachePrefix + SafeSmsCaptchaPrefix】
	SafeSmsCaptchaSendMobile        = `send.mobile:`              // 短信验证码发送
	SafeSmsCaptcha1MinuteLockMobile = `mobile.1minute.send.lock:` // idea.space.safe.sms.captcha.mobile.1minute.send.lock:{mobile} // 同一手机号锁定一分钟
	SafeSmaCaptcha1MinuteLockIp     = `ip.1minute.send.lock:`     // 同一Mac锁定一分钟 idea.space.safe.sms.captcha.mac.1minute.send.lock:{mac}
	SafeSmsCaptchaHalfHourNumIp     = `ip.half.hour.send.num:`    // 同一Mac半小时发送数量
	SafeSmsCaptcha1HourNumMobile    = `mobile.1hour.send.num:`    // 同一手机号一小时发送数量 idea.space.safe.sms.captcha.mobile.1hour.send.num:{mobile}
	SafeSmsCaptcha1MinuterNum       = `1minute.send.num`          // 一分钟发送数量
	SafeSmsCaptcha1HourNum          = `1hour.send.num`            // 1小时发送数量
	SafeSmsCaptcha24HourNum         = `24hour.send.num`           // 24小时发送数量

	// SafeLoginPrefix 账户登录安全规则
	SafeLoginPrefix    = `safe.user.login.` // 账户安全缓存前缀
	SafeLoginIp        = `ip:`              // 用户登录MAC地址 mac:{mac}
	SafeLoginAccountIp = `ip.account:`      // 用户账户登录key mac.account:{account}-{mac}

	// 用户找回密码-安全规则
	SafeForgotPwdIp      = `safe.user.forgot.pwd.ip:`             // 用户找回密码：safe.user.forgot.pwd.mac:{ip}
	SafeForgotPwdCaptcha = `safe.user.forgot.pwd.captcha.mobile:` // 找回密码-验证短信验证码：safe.user.forgot.pwd.captcha.mobile:{mobile}

	// 用户修改密码-安全规则
	SafeUpdatePwdCaptcha = `safe.user.update.pwd.captcha.mobile:` // 修改密码-验证短信验证码：safe.user.update.pwd.captcha.mobile:{mobile}
)

// 系统字典
const (
	SysMapGroup = `sys.map.group` // 系统字典组
)

const (
	AdminInviteUserLock = `admin.invite.user.lock.token:` // 管理员邀请用户锁定
)

const (
	VideoAuthDetailNo     = `video.auth.detail.no:`     // 视频授权播放详情
	VideoCategoryLabelTop = `video.category.label.top`  // 视频标签顶级栏目
	VideoCategoryLabelCid = `video.category.label.cid:` // 指定视频栏目列表
)

const (
	PGCUserTodayRechargeLock = `pgc.user.today.recharge.lock.uid:` // 今日充值锁定一天
	PGCUserRechargeFreqLock  = `pgc.user.recharge.freq.lock:uid:`  // 用户充值频率控制
)

const (
	EmailTemplateDetailVarName = `email.template.detail.var_name:` // 邮件模板变量详情
)

const (
	EmailCaptchaPrefix               = `email.captcha.`
	EmailCaptchaSendIp1HourLock      = `send.1hour.lock.ip:`          // 同一个IP一小时发送频率锁定
	EmailCaptchaSendEmail1minuteLock = `send.email.1minute.lock:`     // 锁定一分钟内邮箱发送频率
	EmailCaptchaSendTodayCountLock   = `send.email.today.count.lock:` // 同一个邮箱当日发送总量限制
	EmailCaptchaSendCode             = `send.code.email:`             // 邮箱验证码
	EmailCaptchaSendTodayTotalLock   = `today.send.total.lock`        // 邮箱验证码当然发送总量
)

const (
	UploadBlockMainIndexFile  = `upload.block.main.index.file.hash:` // 上传文件块主索引文件HASH表
	UploadBlockIsCompleted    = `upload.block.is.completed.hash:`    // 上传文件块是否已完成
	UploadBlockMergeCompleted = `upload.block.merge.completed.hash:` // 上传文件块合并完成
	UploadBlockSpace          = `upload.block.space.hash:`           // 上传文件块空间
)
