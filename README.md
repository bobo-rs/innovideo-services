# 创想视频服务逻辑库

#### 介绍
创想视频服务逻辑库，安全库、service、Login、dao等库

#### 软件架构
项目采用goframe为核心框架，拆分了internal库里面的service、logic、model、dao等，提炼出来统一管理和维护，方便多应用下接口端使用，降低耦合和冗余率
，用于管理数据库操作、逻辑管理、服务管理、工具操作类管理等


#### 安装教程

1.  git clone git@gitee.com:bobo-rs/creative-services.git ```服务端库```
2.  依赖于goframe框架和[创想视频工具库](https://gitee.com/bobo-rs/creative-framework)
3.  创想视频工具库：go get -u gitee.com/bobo-rs/creative-framework

#### 使用说明

1.  文件目录解析
```text
├─consts--常量库
├─enums--Enums库
├─example--示例库
│  └─manifest--业务配置（参考接口端的业务配置）
│      └─config
├─framework--核心服务层
│  ├─boot--枚举库（命令管理，注：gf官方项目保留库，无需理会，不允许删除）
│  ├─dao--DAO层（数据库管理层）
│  │  └─internal--（数据库内部库）
│  ├─logic--逻辑库
│  │  ├─admin--管理员
│  │  ├─article--文章管理
│  │  ├─attachment--附件管理
│  │  ├─base--基类管理（实现基础：增删改查操作）
│  │  ├─bizctx--业务上下文（例如登录用户信息、设置自定义上下文等）
│  │  ├─common--公共管理
│  │  ├─config--系统配置管理
│  │  ├─middleware--中间件管理
│  │  ├─rbac--RBAC管理
│  │  ├─smstpl--短信管理
│  │  ├─syslog--日志管理
│  │  ├─sysmap--字·管理
│  │  ├─upload--文件上传
│  │  └─user--用户管理
│  ├─model--定义数据结构库
│  │  ├─do--数据领域对象
│  │  └─entity--数据模型
│  ├─service--服务层（用于操作logic层和接口层处理，命令管理-gf gen service）
│  └─validate--自定义验证器
├─hack--命令行管理
├─library--核心逻辑库（用于降低framework库logic层分离服务库）
│  ├─exception--错误返回库
│  ├─models--服务数据结构层
│  ├─services--核心服务层
│  │  ├─design--设计服务
│  │  ├─jwt--JWT服务
│  │  ├─login--登录服务
│  │  ├─material--素材服务
│  │  ├─safety--安全服务
│  │  ├─sms--短信服务
│  │  └─user--用户服务
│  └─tools--工具库
└─maps--字典库
```    
2.  数据库字段或新增表，手动执行```gf gen dao```更新库，刷新项目数据字段和表
3.  Service[服务层]：通过```gf gen service```维护，可应用于IDE，自动维护，参考[官方配置](https://goframe.org/pages/viewpage.action?pageId=49770772)
4.  Logic[逻辑层]：根据每个模块新增一个逻辑子库，例如：
```text
1.  新增A子库，在logic目录下，创建a目录，在其目录下新建文件a.go、g_new.go等文件
2.  需要到logic目录下logic.go，导入新建的A子库[_ "gitee.com/bobo-rs/innovideo-services/framework/logic/a"]
3.  导入注册之后，在接口端会在入口main文件统一导入
```
5.  Model[数据结构层]：定义logic层输入输出和自定义数据结构
6.  Validate[自定义验证层]：注册自定义验证器层，便于在接口输入时验证拦截
7.  Consts：定义常量库，在服务端定义的常量，在接口端可以共享
8.  Enums：枚举库，通用库
9.  Library[核心内容库]
```text
1.  Exception：错误公共返回库，重写gerror接口
2.  Models：数据结构层
3.  Services[服务层]：拆分Logic复杂的逻辑，提炼高可复用的业务模块，分离到核心内容库
4.  Tools：工具库，各种便捷方法
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 免责协议

用户在使用、修改、分发项目时，应自行承担由此产生的风险和责任。开发者不对用户的使用行为负责，也不对用户因使用项目而产生的任何后果负责，
开发者对项目可能产生的任何直接、间接、特殊、偶然或后果性损害不承担任何责任，包括但不限于数据丢失、业务中断、利润损失等。
