package maps

import "gitee.com/bobo-rs/innovideo-services/enums"

type (
	VarAliasMap      map[enums.SmsAlias]string
	SmsTmplStatusMap map[enums.SmsStatus]string
	SmsTypeMap       map[enums.SmsType]string
)

var (
	// SmsTemplateStatus 短信模板状态
	SmsTemplateStatus = SmsTmplStatusMap{
		enums.SmsStatusOff: enums.SmsStatusOff.Fmt(),
		enums.SmsStatusOk:  enums.SmsStatusOk.Fmt(),
	}

	// SmsTemplateVarAlias 短信模板变量MAP
	SmsTemplateVarAlias = VarAliasMap{
		enums.SmsAliasRegisterCaptcha: enums.SmsAliasRegisterCaptcha.Fmt(),
	}

	// SmsTemplateSmsType 短信模板类型MAP
	SmsTemplateSmsType = SmsTypeMap{
		enums.SmsTypeCaptcha: enums.SmsTypeCaptcha.Fmt(),
		enums.SmsTypeMessage: enums.SmsTypeMessage.Fmt(),
		enums.SmsTypeMarket:  enums.SmsTypeMarket.Fmt(),
	}
)
